package com.alinesno.cloud.portal.desktop.web.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 内容模块
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-20 12:44:59
 */
@Entity
@Table(name="module")
public class ModuleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模块名称
     */
	@Column(name="module_name")
	private String moduleName;
    /**
     * 模块链接
     */
	@Column(name="module_path")
	private String modulePath;
    /**
     * 模块图标
     */
	@Column(name="module_logo")
	private String moduleLogo;
    /**
     * 模块描述
     */
	@Column(name="module_desc")
	private String moduleDesc;
    /**
     * 模块排序
     */
	@Column(name="module_sort")
	private String moduleSort;
    /**
     * 所属菜单
     */
	@Column(name="menus_id")
	private String menusId;
    /**
     * 模块父类
     */
	@Column(name="module_parent_id")
	private String moduleParentId = "0";

	 /**
     * 链接打开状态
     */
	@Column(name="open_target")
	private String openTarget ;
	
	/**
	 * 链接规划状态
	 */
	@Column(name="module_design")
	private String moduleDesign = "0" ;

	@Transient
	private List<ModuleEntity> subModule ; 
	
	public List<ModuleEntity> getSubModule() {
		return subModule;
	}

	public void setSubModule(List<ModuleEntity> subModule) {
		this.subModule = subModule;
	}

	public String getModuleDesign() {
		return moduleDesign;
	}

	public void setModuleDesign(String moduleDesign) {
		this.moduleDesign = moduleDesign;
	}

	public String getOpenTarget() {
		return openTarget;
	}

	public void setOpenTarget(String openTarget) {
		this.openTarget = openTarget;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModulePath() {
		return modulePath;
	}

	public void setModulePath(String modulePath) {
		this.modulePath = modulePath;
	}

	public String getModuleLogo() {
		return moduleLogo;
	}

	public void setModuleLogo(String moduleLogo) {
		this.moduleLogo = moduleLogo;
	}

	public String getModuleDesc() {
		return moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public String getModuleSort() {
		return moduleSort;
	}

	public void setModuleSort(String moduleSort) {
		this.moduleSort = moduleSort;
	}

	public String getMenusId() {
		return menusId;
	}

	public void setMenusId(String menusId) {
		this.menusId = menusId;
	}

	public String getModuleParentId() {
		return moduleParentId;
	}

	public void setModuleParentId(String moduleParentId) {
		this.moduleParentId = moduleParentId;
	}


	@Override
	public String toString() {
		return "ModuleEntity{" +
			"moduleName=" + moduleName +
			", modulePath=" + modulePath +
			", moduleLogo=" + moduleLogo +
			", moduleDesc=" + moduleDesc +
			", moduleSort=" + moduleSort +
			", menusId=" + menusId +
			", moduleParentId=" + moduleParentId +
			"}";
	}
}
