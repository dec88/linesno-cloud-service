package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Entity
@Table(name="upload_images")
public class UploadImagesEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 文件地址
     */
	@Column(name="images_path")
	private String imagesPath;
    /**
     * 海报描述
     */
	@Column(name="images_desc")
	private String imagesDesc;
    /**
     * 图片类型(1海报/2图片/9其它)
     */
	@Column(name="images_type")
	private Integer imagesType;
    /**
     * 图片名称
     */
	@Column(name="images_name")
	private String imagesName;


	public String getImagesPath() {
		return imagesPath;
	}

	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}

	public String getImagesDesc() {
		return imagesDesc;
	}

	public void setImagesDesc(String imagesDesc) {
		this.imagesDesc = imagesDesc;
	}

	public Integer getImagesType() {
		return imagesType;
	}

	public void setImagesType(Integer imagesType) {
		this.imagesType = imagesType;
	}

	public String getImagesName() {
		return imagesName;
	}

	public void setImagesName(String imagesName) {
		this.imagesName = imagesName;
	}


	@Override
	public String toString() {
		return "UploadImagesEntity{" +
			"imagesPath=" + imagesPath +
			", imagesDesc=" + imagesDesc +
			", imagesType=" + imagesType +
			", imagesName=" + imagesName +
			"}";
	}
}
