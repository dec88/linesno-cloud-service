package com.alinesno.cloud.portal.desktop.web.repository;

import java.util.List;

import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathTypeEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
public interface LinkPathTypeRepository extends IBaseJpaRepository<LinkPathTypeEntity, String> {

	List<LinkPathTypeEntity> findAllByLinkGroup(String group);

	List<LinkPathTypeEntity> findAllByLinkGroupAndLinkTypeCode(String group, String type);

}
