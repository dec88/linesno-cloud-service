package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:03:36
 */
@Entity
@Table(name="user_function")
public class UserFunctionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 功能名称
     */
	@Column(name="function_name")
	private String functionName;

	/**
	 * 功能图标
	 */
	@Column(name="function_logo")
	private String functionLogo;

	/**
	 * 链接id
	 */
	@Column(name="lid ")
	private String lid;
	
    /**
     * 功能路径
     */
	@Column(name="function_path")
	private String functionPath;
    /**
     * 功能描述
     */
	@Column(name="function_desc")
	private String functionDesc;
    /**
     * 所属用户
     */
	private String uid;
    /**
     * 排序
     */
	@Column(name="function_sort")
	private Integer functionSort;
    /**
     * 点击次数
     */
	@Column(name="click_count")
	private Integer clickCount = 0 ;
    /**
     * 是否为常用功能
     */
	@Column(name="often_function")
	private String oftenFunction;
	
	 /**
     * 链接打开状态
     */
	@Column(name="open_target")
	private String openTarget ;

	public String getOpenTarget() {
		return openTarget;
	}

	public void setOpenTarget(String openTarget) {
		this.openTarget = openTarget;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionPath() {
		return functionPath;
	}

	public void setFunctionPath(String functionPath) {
		this.functionPath = functionPath;
	}

	public String getFunctionDesc() {
		return functionDesc;
	}

	public void setFunctionDesc(String functionDesc) {
		this.functionDesc = functionDesc;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Integer getFunctionSort() {
		return functionSort;
	}

	public void setFunctionSort(Integer functionSort) {
		this.functionSort = functionSort;
	}

	public Integer getClickCount() {
		return clickCount;
	}

	public void setClickCount(Integer clickCount) {
		this.clickCount = clickCount;
	}

	public String getOftenFunction() {
		return oftenFunction;
	}

	public void setOftenFunction(String oftenFunction) {
		this.oftenFunction = oftenFunction;
	}

	public String getLid() {
		return lid;
	}

	public void setLid(String lid) {
		this.lid = lid;
	}

	public String getFunctionLogo() {
		return functionLogo;
	}

	public void setFunctionLogo(String functionLogo) {
		this.functionLogo = functionLogo;
	}

	@Override
	public String toString() {
		return "UserFunctionEntity{" +
			"functionName=" + functionName +
			", functionPath=" + functionPath +
			", functionDesc=" + functionDesc +
			", uid=" + uid +
			", functionSort=" + functionSort +
			", clickCount=" + clickCount +
			", oftenFunction=" + oftenFunction +
			"}";
	}
}
