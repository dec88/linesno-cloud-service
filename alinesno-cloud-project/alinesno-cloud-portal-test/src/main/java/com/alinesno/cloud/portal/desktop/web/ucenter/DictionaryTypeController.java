package com.alinesno.cloud.portal.desktop.web.ucenter;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.base.boot.service.IManagerCodeTypeService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * 字典控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@RequestMapping("portal/ucenter/dictionaryType")
@Scope(SpringInstanceScope.PROTOTYPE)
public class DictionaryTypeController extends FeignMethodController<ManagerCodeTypeEntity , IManagerCodeTypeService>  {

	private static final Logger log = LoggerFactory.getLogger(DictionaryTypeController.class) ; 

	@Reference
	private IManagerCodeTypeService managerCodeTypeService ; 
	
//	/**
//	 * 代码管理查询功能  
//	 * @return
//	 */
//	@GetMapping("/list")
//    public void list(){
//    }

	@DataFilter
	@TranslateCode(value="[{hasStatus:has_status}]", plugin="dictionaryTypeTranslatePlugin")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerCodeTypeService , page) ;
    }
	
	
//	/**
//	 * 保存新对象 
//	 * @param model
//	 * @param managerCodeDto
//	 * @return
//	 */
//	@FormToken(remove=true)
//	@ResponseBody
//	@PostMapping("/saveType")
//	public ResponseBean saveType(Model model , HttpServletRequest request, ManagerCodeTypeEntity managerCodeTypeDto) {
//		
//		managerCodeTypeDto = managerCodeTypeService.save(managerCodeTypeDto) ; 
//		
//		return ResponseGenerator.ok(null) ; 	
//	}
	
//	/**
//	 * 代码管理查询功能  
//	 * @return
//	 */
//	@FormToken(save=true)
//	@GetMapping("/addType")
//    public void addType(Model model , HttpServletRequest request){
//    }
	
//	/**
//	 * 代码管理查询功能  
//	 * @return
//	 */
//	@FormToken(save=true)
//	@GetMapping("/modifyType")
//    public void modifyType(Model model , String id){
//		Assert.hasLength(id , "主键不能为空.");
//		
//		ManagerCodeTypeEntity code = managerCodeTypeService.getOne(id) ; 
//	
//		model.addAttribute("bean", code) ; 
//    }
	
//	/**
//	 * 保存
//	 * @param model
//	 * @param request
//	 * @param managerApplicationDto
//	 * @return
//	 */
//	@ResponseBody
//	@PostMapping("/updateType")
//	public ResponseBean updateType(Model model , HttpServletRequest request, ManagerCodeTypeEntity managerCodeTypeDto) {
//		
//		ManagerCodeTypeEntity oldBean = managerCodeTypeService.getOne(managerCodeTypeDto.getId()) ; 
//		
//		BeanUtils.copyProperties(managerCodeTypeDto, oldBean);
//		
//		managerCodeTypeDto = managerCodeTypeService.save(oldBean) ; 
//		return ResponseGenerator.ok(null) ; 	
//	}
	
//	/**
//	 * 删除
//	 */
//	@ResponseBody
//	@PostMapping("/deleteType")
//    public ResponseBean deleteType(@RequestParam(value = "rowsId[]") String[] rowsId){
//		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
//		if(rowsId != null && rowsId.length > 0) {
//			managerCodeTypeService.deleteByIds(rowsId); 
//		}
//		return ResponseGenerator.ok(null) ; 
//    }

	@Override
	public IManagerCodeTypeService getFeign() {
		return managerCodeTypeService;
	}
 
}
















