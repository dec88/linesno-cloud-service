package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-20 22:20:43
 */
@Entity
@Table(name="link_path")
public class LinkPathEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 链接地址
     */
	@Column(name="link_path")
	private String linkPath;
    /**
     * 链接图标
     */
	@Column(name="link_logo")
	private String linkLogo;
    /**
     * 链接描述
     */
	@Column(name="link_desc")
	private String linkDesc;
    /**
     * 链接打开状态
     */
	@Column(name="link_target")
	private String linkTarget;
    /**
     * 链接描述
     */
	@Column(name="link_design")
	private String linkDesign;
    /**
     * 链接id
     */
	@Column(name="link_type")
	private String linkType;
    /**
     * 链接排序
     */
	@Column(name="link_sort")
	private Integer linkSort;
    /**
     * 链接名称
     */
	@Column(name="link_name")
	private String linkName;


	public String getLinkPath() {
		return linkPath;
	}

	public void setLinkPath(String linkPath) {
		this.linkPath = linkPath;
	}

	public String getLinkLogo() {
		return linkLogo;
	}

	public void setLinkLogo(String linkLogo) {
		this.linkLogo = linkLogo;
	}

	public String getLinkDesc() {
		return linkDesc;
	}

	public void setLinkDesc(String linkDesc) {
		this.linkDesc = linkDesc;
	}

	public String getLinkTarget() {
		return linkTarget;
	}

	public void setLinkTarget(String linkTarget) {
		this.linkTarget = linkTarget;
	}

	public String getLinkDesign() {
		return linkDesign;
	}

	public void setLinkDesign(String linkDesign) {
		this.linkDesign = linkDesign;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public Integer getLinkSort() {
		return linkSort;
	}

	public void setLinkSort(Integer linkSort) {
		this.linkSort = linkSort;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}


	@Override
	public String toString() {
		return "LinkPathEntity{" +
			"linkPath=" + linkPath +
			", linkLogo=" + linkLogo +
			", linkDesc=" + linkDesc +
			", linkTarget=" + linkTarget +
			", linkDesign=" + linkDesign +
			", linkType=" + linkType +
			", linkSort=" + linkSort +
			", linkName=" + linkName +
			"}";
	}
}
