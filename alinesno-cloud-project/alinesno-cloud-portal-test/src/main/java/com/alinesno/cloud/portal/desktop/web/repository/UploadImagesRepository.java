package com.alinesno.cloud.portal.desktop.web.repository;

import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.UploadImagesEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
public interface UploadImagesRepository extends IBaseJpaRepository<UploadImagesEntity, String> {

}
