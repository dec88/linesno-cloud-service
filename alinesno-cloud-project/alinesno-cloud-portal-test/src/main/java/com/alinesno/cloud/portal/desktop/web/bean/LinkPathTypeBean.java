package com.alinesno.cloud.portal.desktop.web.bean;

import java.util.List;

import com.alinesno.cloud.portal.desktop.web.entity.LinkPathEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-20 21:26:06
 */
public class LinkPathTypeBean {

    /**
     * 类型名称
     */
	private String linkTypeName;
	
    /**
     * 类型代码
     */
	private String linkTypeCode;
	
    /**
     * 链接分组
     */
	private String linkGroup;

	/**
	 * 链接地址
	 */
	private List<LinkPathEntity> links ; 

	public List<LinkPathEntity> getLinks() {
		return links;
	}

	public void setLinks(List<LinkPathEntity> links) {
		this.links = links;
	}

	public String getLinkTypeName() {
		return linkTypeName;
	}

	public void setLinkTypeName(String linkTypeName) {
		this.linkTypeName = linkTypeName;
	}

	public String getLinkTypeCode() {
		return linkTypeCode;
	}

	public void setLinkTypeCode(String linkTypeCode) {
		this.linkTypeCode = linkTypeCode;
	}

	public String getLinkGroup() {
		return linkGroup;
	}

	public void setLinkGroup(String linkGroup) {
		this.linkGroup = linkGroup;
	}


	@Override
	public String toString() {
		return "LinkPathTypeEntity{" +
			"linkTypeName=" + linkTypeName +
			", linkTypeCode=" + linkTypeCode +
			", linkGroup=" + linkGroup +
			"}";
	}
}
