/**
 * 显示图标 
 * @returns
 */
function showIcons() {
	var index = layer
			.open({
				type : 2,
				title : false, // '菜单管理',
				closeBtn : 0,
				shadeClose : true,
				shade : 0.8,
				area : [ '50%', '95%' ],
				content : ctx + 'pages/portal/ucenter/project/icons?id=1',
				btn : [ '确认', '取消' ],
				yes : function(index, layero) {
					console.log("index = " + index + " layero = " + layero);
					var selectResourceParentNode = $(layero).find("iframe")[0].contentWindow.document.getElementById("selectResourceParentNode")

					var resourceName = $(selectResourceParentNode).attr("data-name");
					var id = $(selectResourceParentNode).attr("data-id");

					console.log("selectResourceId = " + id+ " , selectResourceName = " + resourceName);

					$("span#applicationIconsLabel").html('<i class="' + resourceName+ '" aria-hidden="true"></i>');
					$("input#applicationIcons").val(id);

					layer.close(index);
				}
			});
}

/**
 * 显示应用
 * @returns
 */
function showApplication(){
	var index = layer.open({
		type : 2,
		title : false , //'菜单管理',
		closeBtn:0 ,
		shadeClose : true,
		shade : 0.8,
		area : [ '50%', '95%' ],
		content : ctx+'portal/ucenter/project/select' , 
		btn: ['确认', '取消'] , 
		yes: function(index, layero){
			console.log("index = " + index + " layero = " + layero) ; 
			var selectResourceParentNode = $(layero).find("iframe")[0].contentWindow.document.getElementById("selectResourceParentNode")
			
			var resourceName = $(selectResourceParentNode).attr("data-applicationName") ; 	
			var id = $(selectResourceParentNode).attr("data-id") ; 	
			
			console.log("applicationId = " + id + " , applicationIdLabel = " + resourceName) ;
			
			$("input[name=applicationIdLabel]").val(resourceName) ; 
			$("input[name=applicationId]").val(id) ; 
			
			if($.fn.zTree != null){
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				treeObj.reAsyncChildNodes(null, "refresh");
			}
			
			layer.close(index) ; 
	 	}
	});		
}

