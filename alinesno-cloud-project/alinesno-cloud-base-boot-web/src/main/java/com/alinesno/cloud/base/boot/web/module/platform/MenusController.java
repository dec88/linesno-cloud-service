package com.alinesno.cloud.base.boot.web.module.platform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.enums.MenuEnums;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.base.boot.web.bean.ManagerResourceBean;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.auth.OperationPermissions;

import cn.hutool.core.bean.BeanUtil;

/**
 * 菜单管理 
 * @author LuoAnDong
 * @since 2018年12月7日 下午10:58:20
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/menus")
public class MenusController extends FeignMethodController<ManagerResourceEntity, IManagerResourceService> {

	private static final Logger log = LoggerFactory.getLogger(MenusController.class) ; 

	@Reference
	private IManagerResourceService managerResourceService ; 
	
	@TranslateCode("[{hasStatus:has_status,menuType:menus_type}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page , boolean showBtn){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		RestWrapper wrapper = page.getRestWrapper() ; 
	
		if(!showBtn) {
			wrapper.ne("menuType", MenuEnums.MENU_BUTTON.value); 
		}
		
		return this.toPage(model, managerResourceService , page , wrapper) ;
    }
	

	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@Override
	public void modify(HttpServletRequest request ,Model model , String id){
		ManagerResourceEntity oldBean = managerResourceService.getOne(id) ; 
		model.addAttribute("bean", oldBean) ; 
    }

	/**
	 * 显示所有菜单数据
	 * @param model
	 * @param id
	 * @return
	 */
	@ResponseBody
	@PostMapping("/menusData")
    public List<ManagerResourceEntity> menusData(Model model , String id){
		
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
		
		return managerResourceService.findAll()  ;
    }

	/**
	 * 显示所有父级菜单
	 * @param model
	 * @param id
	 */
	@GetMapping("/parents")
    public void menus(Model model , String id){
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
    }

	/**
	 * 更新父类
	 * @param model
	 * @param request
	 * @param id
	 */
	@ResponseBody
	@GetMapping("/updateResourceParent")
    public ResponseBean updateResourceParent(Model model , HttpServletRequest request , String id , String resourceParent){
		ManagerResourceEntity e = managerResourceService.getOne(id) ; 
		
		e.setResourceParent(resourceParent); 
		managerResourceService.save(e) ; 
		
		return ResponseGenerator.ok() ; 
    }

	/**
	 * 调整配置和按钮
	 * @param model
	 * @param id
	 */
	@GetMapping("/buttons")
    public void buttons(Model model , String menuId){
		log.debug("菜单id:{}" , menuId);
		model.addAttribute("menuId", menuId) ; 
    }

	/**
	 * 保存按钮
	 */
	@OperationPermissions("sys:user:save")
	@ResponseBody
	@PostMapping("buttonSave")
	public ResponseBean buttonSave(@Validated ManagerResourceBean bean) {

		ManagerResourceEntity e = new ManagerResourceEntity() ;
		BeanUtil.copyProperties(bean , e , copyOptions);
		
		fillOperator(e);  // 填充实体信息
		e.setMenuType(MenuEnums.MENU_BUTTON.value);
		log.debug("保存按钮id:{}" , JSONObject.toJSON(e));
	
		// 设置所属应用
		String menudId = bean.getResourceParent() ; 
		ManagerResourceEntity parentMenu = managerResourceService.getOne(menudId) ; //.findById(menudId).get() ; 
		e.setApplicationId(parentMenu.getApplicationId());
		
		managerResourceService.save(e) ;
		return ResponseGenerator.genSuccessMessage("按钮["+bean.getResourceName()+"]保存成功") ;
	}

	@Override
	public IManagerResourceService getFeign() {
		return managerResourceService;
	}

}
