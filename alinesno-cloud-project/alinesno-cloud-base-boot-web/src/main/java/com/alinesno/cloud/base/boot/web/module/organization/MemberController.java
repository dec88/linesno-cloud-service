package com.alinesno.cloud.base.boot.web.module.organization;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerUserEntity;
import com.alinesno.cloud.base.boot.service.IManagerUserService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

/**
 * 部门管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/member")
public class MemberController extends FeignMethodController<ManagerUserEntity , IManagerUserService> {
	
	private static final Logger log = LoggerFactory.getLogger(MemberController.class);

	@Reference
	private IManagerUserService managerUserService ; 

	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerUserService , page) ;
    }

	@Override
	public com.alinesno.cloud.base.boot.service.IManagerUserService getFeign() {
		return managerUserService;
	}

}
