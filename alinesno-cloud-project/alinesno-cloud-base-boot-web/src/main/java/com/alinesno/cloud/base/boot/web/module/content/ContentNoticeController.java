package com.alinesno.cloud.base.boot.web.module.content;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ContentNoticeEntity;
import com.alinesno.cloud.base.boot.service.IContentNoticeService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;

/**
 * 消息通知
 * 
 * @author LuoAnDong
 * @since 2019年4月4日 下午2:09:22
 */
@Controller
@RequestMapping("boot/platform/notice")
@Scope(SpringInstanceScope.PROTOTYPE)
public class ContentNoticeController  extends FeignMethodController<ContentNoticeEntity , IContentNoticeService> {

	private static final Logger log = LoggerFactory.getLogger(ContentNoticeController.class);

	@Reference
	private IContentNoticeService IContentNoticeService ; 
	
	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@GetMapping("/list")
	public void list() {
	}

	/**
	 * 保存新对象
	 * 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove = true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model, HttpServletRequest request, ContentNoticeEntity managerCodeDto) {
		managerCodeDto = IContentNoticeService.save(managerCodeDto);
		return ResponseGenerator.ok(null);
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@FormToken(save = true)
	@GetMapping("/add")
	public void add(Model model, HttpServletRequest request) {
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
//	@FormToken(save = true)
//	@GetMapping("/modify")
//	@Override
//	public void modify(Model model, HttpServletRequest request ,  String id) {
//		Assert.hasLength(id, "主键不能为空.");
//		ContentNoticeEntity code = IContentNoticeService.getOne(id);
//		model.addAttribute("bean", code);
//	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
	public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId) {
		log.debug("rowsId = {}", ToStringBuilder.reflectionToString(rowsId));
		if (rowsId != null && rowsId.length > 0) {
			IContentNoticeService.deleteByIds(rowsId);
		}
		return ResponseGenerator.ok(null);
	}

	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page = {}", ToStringBuilder.reflectionToString(page));
		return this.toPage(model, IContentNoticeService, page);
	}

//	private DatatablesPageBean toPage(Model model, IContentNoticeService IContentNoticeService, DatatablesPageBean page) {
//
//		RestWrapper restWrapper = new RestWrapper();
//		restWrapper.builderCondition(page.getCondition());
//		restWrapper.setPageNumber(page.getStart());
//		restWrapper.setPageSize(page.getLength());
//
//		Page<ContentNoticeEntity> pageableResult = IContentNoticeService.findAllByWrapperAndPageable(restWrapper);
//
//		DatatablesPageBean p = new DatatablesPageBean();
//		p.setData(pageableResult.getContent());
//		p.setDraw(page.getDraw());
//		p.setRecordsFiltered((int) pageableResult.getTotalElements());
//		p.setRecordsTotal((int) pageableResult.getTotalElements());
//
//		return p;
//	}
	
	/**
	 * 保存
	 * @param model
	 * @param request
	 * @param managerApplicationDto
	 * @return
	 */
//	@FormToken(remove=true)
//	@ResponseBody
//	@PostMapping("/update")
	@Override
	public ResponseBean update(Model model , HttpServletRequest request, ContentNoticeEntity ContentNoticeEntity) {
		
		ContentNoticeEntity oldBean = IContentNoticeService.getOne(ContentNoticeEntity.getId()) ; 
		BeanUtil.copyProperties(ContentNoticeEntity, oldBean , CopyOptions.create().setIgnoreNullValue(true));
		
		ContentNoticeEntity = IContentNoticeService.save(oldBean) ; 
		return ResponseGenerator.ok(null) ; 	
	}

	@Override
	public IContentNoticeService getFeign() {
		return IContentNoticeService;
	}

}
