package com.alinesno.cloud.base.boot.web.module.platform;

import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.base.boot.entity.ManagerSearchEntity;
import com.alinesno.cloud.base.boot.service.IManagerSearchService;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

/**
 * 应用管理 
 * @author LuoAnDong
 * @since 2018年12月7日 下午10:58:20
 */
@Controller
@RequestMapping("boot/platform/search")
public class SearchController  extends FeignMethodController<ManagerSearchEntity, IManagerSearchService>  {

	private static final Logger log = LoggerFactory.getLogger(SearchController.class) ;

	@Reference
	private IManagerSearchService managerSearchService ; 

	/**
	 * 进入搜索配置页面
	 * @param menuId
	 */
	@GetMapping("search")
	public void search(String menuId) {
		log.debug("menuId:{}" , menuId);
	}
	
	@Override
	public IManagerSearchService getFeign() {
		return managerSearchService ;
	} 

}
