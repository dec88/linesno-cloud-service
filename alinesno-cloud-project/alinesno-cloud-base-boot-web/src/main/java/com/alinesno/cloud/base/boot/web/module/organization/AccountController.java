package com.alinesno.cloud.base.boot.web.module.organization;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.base.boot.service.IManagerRoleService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.login.aop.AccountRecord;

/**
 * 后台账户管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/account")
public class AccountController extends FeignMethodController<ManagerAccountEntity , IManagerAccountService> {

	private static final Logger log = LoggerFactory.getLogger(AccountController.class) ; 

	@Reference
	private IManagerAccountService iManagerAccountService ; 
	
	@Reference
	private IManagerRoleService iManagerRoleService ; 
	
	@TranslateCode(value="[{hasStatus:has_status,rolePower:role_power}]" , plugin="departmentTranslatePlugin" , operator=true)
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, iManagerAccountService , page) ;
    }

	/**
	 * 认证
	 */
	@GetMapping("/auth")
    public ResponseBean auth(String accountId){
		log.debug("id = {}" , accountId);
		ManagerAccountEntity bean = iManagerAccountService.findEntityById(accountId) ; 
		return ResponseGenerator.ok(bean) ; 
    }

	/**
	 * 认证
	 */
	@ResponseBody
	@PostMapping("/role")
    public List<ManagerRoleEntity> role(String accountId){
		log.debug("id = {}" , accountId);
		List<ManagerRoleEntity> bean = iManagerRoleService.findByAccountId(accountId) ; 
		return bean ; 
    }
	
	/**
	 * 授权 
	 */
	@ResponseBody
	@PostMapping("/authAccount")
    public ResponseBean authAccount(String accountId , String rolesId){
		log.debug("id = {}" , accountId);
	
		ManagerAccountEntity bean = iManagerAccountService.findEntityById(accountId) ; 
		boolean isAuth = iManagerRoleService.authAccount(bean, rolesId) ; 
		
		return ResponseGenerator.ok(isAuth) ; 
    }

	/**
	 * 详情
	 */
//	@ResponseBody
//	@GetMapping("/detail")
	@Override
    public ResponseBean detail(String id){
		log.debug("id = {}" , id);
		ManagerAccountEntity bean = iManagerAccountService.findEntityById(id) ; 
		return ResponseGenerator.ok(bean) ; 
    }

	/**
	 * 保存
	 * @param model
	 * @param request
	 * @param managerApplicationDto
	 * @return
	 */
	@ResponseBody
	@PostMapping("/update")
	public ResponseBean update(Model model , HttpServletRequest request, ManagerAccountEntity dto) {
		
		ManagerAccountEntity oldBean = iManagerAccountService.getOne(dto.getId()) ; 
		BeanUtils.copyProperties(dto, oldBean);
		
		dto = iManagerAccountService.save(oldBean) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			iManagerAccountService.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }

	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
    }
	
	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@AccountRecord("保存新账户.")
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model , HttpServletRequest request, ManagerAccountEntity dto) {
		log.debug("account dto:{}" , ToStringBuilder.reflectionToString(dto));
		dto = iManagerAccountService.save(dto) ; 
		return ResponseGenerator.ok(null) ; 	
	}

	@Override
	public IManagerAccountService getFeign() {
		return iManagerAccountService;
	}

}
