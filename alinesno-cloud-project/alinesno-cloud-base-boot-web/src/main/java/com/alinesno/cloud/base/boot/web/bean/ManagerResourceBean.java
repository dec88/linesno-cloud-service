package com.alinesno.cloud.base.boot.web.bean;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.alinesno.cloud.common.facade.dto.BaseDto;

/**
 * 数据权限
 * 
 * @author LuoAnDong
 * @since 2019年10月2日 下午5:57:17
 */
@SuppressWarnings("serial")
public class ManagerResourceBean extends BaseDto {
	
	/**
	 * 资源名称
	 */
	@NotBlank(message = "资源名称不能为空")
	@Length(min=2 , max=12 , message = "资源名称长度为{min}至{max}个字符")
	private String resourceName;
	
	/**
	 * 资源链接
	 */
	private String resourceLink;
	
	/**
	 * 资源图标
	 */
	private String resourceIcon;

	/**
	 * 权限标识 
	 */
	@NotBlank(message = "权限标识不能为空")
	private String permission ; 
	
	/**
	 * 资源父类
	 */
	@NotBlank(message = "菜单所属父类不能为空")
	private String resourceParent ; 
	
	/**
	 * 资源排序
	 */
	@NotNull(message = "顺序不能为空")
	@Min(value=1 , message="顺序值最小值为{value}")
	@Max(value=10 , message="顺序值最大值为{value}")
	private Integer resourceOrder;

	/**
	 * 权限脚本
	 */
	private String permissionScript ; 

	public String getPermissionScript() {
		return permissionScript;
	}

	public void setPermissionScript(String permissionScript) {
		this.permissionScript = permissionScript;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceLink() {
		return resourceLink;
	}

	public void setResourceLink(String resourceLink) {
		this.resourceLink = resourceLink;
	}

	public String getResourceIcon() {
		return resourceIcon;
	}

	public void setResourceIcon(String resourceIcon) {
		this.resourceIcon = resourceIcon;
	}

	public String getResourceParent() {
		return resourceParent;
	}

	public void setResourceParent(String resourceParent) {
		this.resourceParent = resourceParent;
	}

	public Integer getResourceOrder() {
		return resourceOrder;
	}

	public void setResourceOrder(Integer resourceOrder) {
		this.resourceOrder = resourceOrder;
	}

}
