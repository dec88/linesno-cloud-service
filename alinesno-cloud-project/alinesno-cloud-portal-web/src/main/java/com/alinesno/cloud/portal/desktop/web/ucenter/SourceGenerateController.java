package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerSourceGenerateEntity;
import com.alinesno.cloud.base.boot.service.IManagerSourceGenerateService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * 代码生成器
 * 
 * @author LuoAnDong
 * @since 2019年4月8日 下午10:29:02
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter/source")
public class SourceGenerateController
		extends FeignMethodController<ManagerSourceGenerateEntity, IManagerSourceGenerateService> {

	private static final Logger log = LoggerFactory.getLogger(SourceGenerateController.class);

	@Reference
	private IManagerSourceGenerateService managerSourceGenerateService ; 

	@TranslateCode(value = "[{hasStatus:has_status}]")
	@DataFilter
	@ResponseBody
	@RequestMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page = {}", ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerSourceGenerateService, page);
	}

	/**
	 * 下载文件 (分布式部署待优化)
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/code")
	public ResponseEntity<byte[]> download(HttpServletRequest request, String id) throws IOException {
		byte[] body = managerSourceGenerateService.generatorCode(id) ; 
		
		ManagerSourceGenerateEntity e = this.getFeign().getOne(id) ; 
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attchement;filename=" + e.getFeignServerPath() + ".zip" ) ;
		HttpStatus statusCode = HttpStatus.OK;
		ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body, headers, statusCode);
		return entity;
	}

	/**
	 * 上传代码到git上面
	 * @param request
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/git")
	public ResponseBean git(HttpServletRequest request, String id) throws IOException {
		String path = managerSourceGenerateService.git(id) ; 
		return ResponseGenerator.ok(path);
	}

	/**
	 * 配置中心
	 * @param request
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	@ResponseBody
	@RequestMapping("/jenkins")
	public ResponseBean jenkins(HttpServletRequest request, String id) throws IOException, URISyntaxException {
		String path = managerSourceGenerateService.jenkins(id) ; 
		return ResponseGenerator.ok(path);
	}
	
	/**
	 * 持续集成
	 * @param request
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	@ResponseBody
	@RequestMapping("/ci")
	public ResponseBean ci(HttpServletRequest request, String id) throws IOException, URISyntaxException {
		String path = managerSourceGenerateService.git(id) ; 
		path = managerSourceGenerateService.jenkins(id) ; 
		return ResponseGenerator.ok(path);
	}

	@Override
	public IManagerSourceGenerateService getFeign() {
		return managerSourceGenerateService;
	}

	/**
	 * 进入设置界面 
	 */
	@RequestMapping("/settings")
	public void settings() {
		log.debug("开发平台配置.");
	}

}
