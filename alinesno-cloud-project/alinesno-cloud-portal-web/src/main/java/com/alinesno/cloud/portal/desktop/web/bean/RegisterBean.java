package com.alinesno.cloud.portal.desktop.web.bean;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

/**
 * 注册实体对象 
 * @author LuoAnDong
 * @since 2019年7月26日 下午4:46:45
 */
public class RegisterBean {

	@Email(message = "注册用户名只能为邮箱.")
	@NotBlank(message = "邮箱不能为空.")
	@Length(max = 32 , message = "邮箱最大长度不能超过{max}位")
	private String loginName ; // 邮箱 

//	@Phone(message = "注册用户名只能为手机号.")
//	@NotBlank(message = "注册名不能为空.")
//	private String loginNameByPhone ; 
	
	private boolean isPhone = true ;  // 是否为手机注册(默认为true)
	
	@NotBlank(message = "密码不能为空.")
	@Length(min=3 ,max = 32 , message = "密码最大长度不能超过{max}位,最小不能小于{min}位")
	private String password ; // 密码
	
	@NotBlank(message = "验证码不能为空.")
	@Length(min=3 , max=16 ,message = "验证码最小长度为{min}位,最大长度为{max}")
	private String phoneCode ; // 手机验证码
	
	private boolean original ; // 是否同意协议
	
//	public String getLoginNameByPhone() {
//		return loginNameByPhone;
//	}
//	public void setLoginNameByPhone(String loginNameByPhone) {
//		this.loginNameByPhone = loginNameByPhone;
//	}
	
	public boolean isPhone() {
		return isPhone;
	}
	public void setPhone(boolean isPhone) {
		this.isPhone = isPhone;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	public boolean isOriginal() {
		return original;
	}
	public void setOriginal(boolean original) {
		this.original = original;
	}
	
}
