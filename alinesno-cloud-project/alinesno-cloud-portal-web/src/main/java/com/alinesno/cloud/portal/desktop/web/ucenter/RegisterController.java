package com.alinesno.cloud.portal.desktop.web.ucenter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.common.core.annotations.Phone;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.login.aop.AccountRecord;
import com.alinesno.cloud.common.web.login.aop.AccountRecord.RecordType;
import com.alinesno.cloud.common.web.login.constants.LoginConstants;
import com.alinesno.cloud.portal.desktop.web.bean.RegisterBean;

/**
 * 显示设置
 * @author LuoAnDong
 * @since 2019年4月19日 上午6:18:05
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("public/portal/ucenter")
public class RegisterController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(RegisterController.class) ; 

	@Reference
	private IManagerAccountService managerAccountService ; 

	/**
	 * 用户注册
	 * @param model
	 * @param request
	 */
	@AccountRecord(type=RecordType.LOGIN_REGISTER)
	@GetMapping(value = "register")
	public String register(Model model , HttpServletRequest request) {
		log.debug("进入注册页面.");
		
		session.setAttribute(LoginConstants.VERIFY_CODE_ACTUAL_EMAIL,"alinesno") ; 
		return "portal/ucenter/register" ; 
	}
	
	/**
	 * 用户登陆 
	 * @param model
	 * @param request
	 */
	@AccountRecord(type=RecordType.LOGIN_GET)
	@GetMapping(value = "login")
	public String login(Model model , String phone , HttpServletRequest request) {
		log.debug("进入登陆页面.");
		model.addAttribute("phone", phone) ; 
		return "portal/ucenter/login" ; 
	}

	/**
	 * 发送手机验证码
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@PostMapping(value = "phoneValidateCode")
	public ResponseBean phoneValidateCode(
			@NotBlank(message = "手机号不能为空") 
			@Phone(message = "手机号格式不正确")
			String phone , 
			HttpSession session) {
		log.debug("发送手机【{}】验证码." , phone);
		
		session.setAttribute(LoginConstants.VERIFY_CODE_ACTUAL_EMAIL , "alinesno") ; 
		return ResponseGenerator.genSuccessMessage("生成验证码成功.");
	}

	/**
	 * 用户注册
	 * @param model
	 * @param request
	 * @return
	 */
	@AccountRecord(type=RecordType.LOGIN_REGISTER)
	@ResponseBody
	@PostMapping(value = "register")
	public ResponseBean registerSave(Model model , HttpSession session ,@Validated RegisterBean registerBean) {
		
		Object validateCode = session.getAttribute(LoginConstants.VERIFY_CODE_ACTUAL_EMAIL) ; 
		log.debug("注册实体:{}. , validateCode:{}" , ToStringBuilder.reflectionToString(registerBean) , validateCode);
		
//		if(registerBean.isPhone()) {
//			registerBean.setLoginName(registerBean.getLoginNameByPhone());
//		}
		
		Assert.notNull(validateCode,"验证码异常.");
		Assert.isTrue(validateCode.equals(registerBean.getPhoneCode()),"注册验证码错误,请重新发送验证码.");

		boolean isRegister = managerAccountService.registAccount(registerBean.getLoginName(), registerBean.getPassword() , registerBean.getPhoneCode()) ; 
		return isRegister?ok():fail() ; 
	}


}
