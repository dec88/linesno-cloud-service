package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.entity.ContentPostsEntity;
import com.alinesno.cloud.base.boot.service.IContentPostTypeService;
import com.alinesno.cloud.base.boot.service.IContentPostsService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;

/**
 * 内容管理
 * @author LuoAnDong
 * @since 2019年4月4日 下午1:23:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter/content")
public class ContentPostController  extends FeignMethodController<ContentPostsEntity , IContentPostsService> {

	private static final Logger log = LoggerFactory.getLogger(ContentPostController.class) ; 

	@Reference
	private IContentPostsService iContentPostsService ; 
	
	@Reference
	private IContentPostTypeService iContentPostTypeService ; 
	
//	/**
//	 * 保存新对象 
//	 * @param model
//	 * @param ContentPostsEntity
//	 * @return
//	 */
//	@FormToken(remove=true)
//	@ResponseBody
//	@PostMapping("/save")
//	public ResponseBean save(Model model , HttpServletRequest request, ContentPostsEntity ContentPostsEntity) {
//		ContentPostsEntity.setPostAuthor(CurrentAccountSession.get(request).getId());  
//		ContentPostsEntity = iContentPostsService.save(ContentPostsEntity) ; 
//		return ResponseGenerator.ok(null) ; 	
//	}
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
		
//		List<ContentPostsEntity> codeTypes = iContentPostsService.findAll() ; 
//		model.addAttribute("codeTypes", codeTypes) ; 
		
		setPostTypes(model); 
    }
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@Override
	public void modify(HttpServletRequest request ,Model model , String id){
		Assert.hasLength(id , "主键不能为空.");
		ContentPostsEntity code = iContentPostsService.getOne(id) ; 
	
//		List<ContentPostsEntity> codeTypes = iContentPostsService.findAll() ; 
//		model.addAttribute("codeTypes", codeTypes) ; 
	
		setPostTypes(model); 
		model.addAttribute("bean", code) ; 
    }
	
	private void setPostTypes(Model model) {
		RestWrapper wrapper = RestWrapper.create() ; 
		wrapper.eq("operatorId", CurrentAccountSession.get(request).getId()) ; 
		List<ContentPostTypeEntity> codeTypes = iContentPostTypeService.findAll(wrapper) ; 
		
		model.addAttribute("codeTypes", codeTypes) ; 
	}

	
//	/**
//	 * 删除
//	 */
//	@ResponseBody
//	@PostMapping("/delete")
//    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
//		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
//		if(rowsId != null && rowsId.length > 0) {
//			iContentPostsService.deleteByIds(rowsId); 
//		}
//		return ResponseGenerator.ok(null) ; 
//    }

	@DataFilter
	@TranslateCode(value="[{hasStatus:has_status}]" , plugin = "contentTranslatePlugin")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, iContentPostsService , page) ;
    }

	@Override
	public IContentPostsService getFeign() {
		return this.iContentPostsService ;
	}
	
}
