package com.alinesno.cloud.portal.desktop.web.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;
import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;
import com.alinesno.cloud.portal.desktop.web.entity.UserFunctionEntity;
import com.alinesno.cloud.portal.desktop.web.entity.UserModuleEntity;
import com.alinesno.cloud.portal.desktop.web.service.IModuleService;
import com.alinesno.cloud.portal.desktop.web.service.IUserFunctionService;
import com.alinesno.cloud.portal.desktop.web.service.IUserModuleService;

/**
 * <p>内容模块 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/desktop/web/module")
public class ModuleController extends  LocalMethodBaseController<ModuleEntity , IModuleService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ModuleController.class);

	@Autowired
	private IUserModuleService userModuleService ; 
	
	@Autowired
	private IUserFunctionService userFunctionService ; 
	
	@Override
	public void modify(HttpServletRequest request, Model model, String id) {
		super.modify(request, model, id);
		
		List<ModuleEntity> parentModule = feign.findAllByModuleParentId(null) ; 
		model.addAttribute("parentModule" , parentModule) ; 
	}

	@Override
	public void add(Model model, HttpServletRequest request) {
		super.add(model, request);
		
		List<ModuleEntity> parentModule = feign.findAllByModuleParentId(null) ; 
		model.addAttribute("parentModule" , parentModule) ; 
	}

	@TranslateCode(value="[{hasStatus:has_status},{openTarget:open_target_type}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }


	@ResponseBody
	@GetMapping("/changeModuleDesign")
	public ResponseBean changeModuleDesign(Model model, String id) {
		log.debug("id:{}" , id);
		Optional<ModuleEntity> parentModule = feign.findById(id) ; 
		if(parentModule.isPresent()) {
			ModuleEntity m = parentModule.get() ; 
			int designInt = Integer.parseInt(m.getModuleDesign())+1 ; 
			
			log.debug("designInt:{} , newDesignInt:{}" , designInt , designInt%2);
			
			m.setModuleDesign(String.valueOf(designInt%2)) ; 
			feign.save(m) ;
		}
		return ResponseGenerator.genSuccessResult() ; 
	}
	

	@ResponseBody
	@PostMapping("/authAccount")
	public ResponseBean authAccount(Model model, String rolesId , HttpServletRequest request) {
		log.debug("rolesId:{}" , rolesId);
		ManagerAccountEntity account = CurrentAccountSession.get(request) ; 

		boolean b = userModuleService.updateModule(account.getId() , rolesId) ; 
		log.debug("update functions:{}" , b);
		
		return ResponseGenerator.genSuccessResult() ; 
	}

	/**
	 * 模块功能功能点击
	 * @param model
	 * @param id
	 * @param type
	 * @return
	 */
	@ResponseBody
	@PostMapping("/click")
	public ResponseBean click(Model model, String id, int type) {
		log.debug("id:{} , type:{}" , id , type);
		if(type == 1) { // 模块
			Optional<UserModuleEntity> mp = userModuleService.findById(id) ; 
			if(mp.isPresent()) {
				UserModuleEntity m = mp.get() ; 
				m.setClickCount((m.getClickCount()==null?0:m.getClickCount())+1);
				userModuleService.save(m) ; 
			}
		} else if(type == 0) {  // 功能
			Optional<UserFunctionEntity> mp = userFunctionService.findById(id) ; 
			if(mp.isPresent()) {
				UserFunctionEntity m = mp.get() ; 
				m.setClickCount((m.getClickCount()==null?0:m.getClickCount())+1);
				userFunctionService.save(m) ; 
			}
		}
		return ResponseGenerator.genSuccessResult() ; 
	}
	
	/**
	 * 修改状态
	 * @return
	 */
	@ResponseBody
	@GetMapping("/designStatus")
    public ResponseBean designStatus(String id){
		boolean b = feign.modifyDesignStatus(id) ; 
		return ResponseGenerator.ok(b) ; 
    }
	
}


























