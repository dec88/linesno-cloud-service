package com.alinesno.cloud.portal.desktop.web.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.portal.desktop.web.bean.ModuleBean;
import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;

/**
 * <p> 内容模块 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@NoRepositoryBean
public interface IModuleService extends IBaseService<ModuleEntity, String> {

	/**
	 * 通过菜单查询模块
	 * @param menusId
	 * @return
	 */
	List<ModuleBean> findAllByMenusId(String menusId);

	/**
	 * 父类parent
	 * @param i
	 * @return
	 */
	List<ModuleEntity> findAllByModuleParentId(String parentId);

	/**
	 * 变更状态 
	 * @param id
	 * @return
	 */
	boolean modifyDesignStatus(String id);

	/**
	 * 查询所有模块 
	 * @return
	 */
	List<ModuleEntity> findAllModule();

}
