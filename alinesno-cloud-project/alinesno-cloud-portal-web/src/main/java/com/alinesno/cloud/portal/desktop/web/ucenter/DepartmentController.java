package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.base.boot.service.IManagerDepartmentService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * 部门管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter/department")
public class DepartmentController extends FeignMethodController<ManagerDepartmentEntity , IManagerDepartmentService> {
	
	private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

	@Reference
	private IManagerDepartmentService managerDepartmentFeigin ; 

	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerDepartmentFeigin , page) ;
    }

	/**
	 * 显示所有菜单数据
	 * @param model
	 * @param id
	 * @return
	 */
	@DataFilter
	@ResponseBody
	@GetMapping("/departmentData")
    public List<ManagerDepartmentEntity> menusData(Model model , String applicationId , DatatablesPageBean page){
		
		log.debug("application id = {}" , applicationId);
		model.addAttribute("applicationId", applicationId) ; 
		
		RestWrapper restWrapper = new RestWrapper() ; 
		if(StringUtils.isNotBlank(applicationId)) {
			restWrapper.eq("applicationId", applicationId) ;
		}
		restWrapper.builderCondition(page.getCondition()) ; 

		List<ManagerDepartmentEntity> list = managerDepartmentFeigin.findAllWithApplication(restWrapper)  ; 
		
		return list ; 
    }

	/**
	 * 显示所有父级菜单
	 * @param model
	 * @param id
	 */
	@GetMapping("/parents")
    public void menus(Model model , String id){
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
    }

	@Override
	public IManagerDepartmentService getFeign() {
		return managerDepartmentFeigin;
	}
	
}
