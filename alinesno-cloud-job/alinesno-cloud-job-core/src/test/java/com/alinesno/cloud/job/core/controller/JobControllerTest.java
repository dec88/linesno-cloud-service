package com.alinesno.cloud.job.core.controller;

import static org.junit.Assert.fail;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.job.core.JobApplication;
import com.alinesno.cloud.job.core.dynamic.service.JobService;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

@RunWith(SpringRunner.class)   
@SpringBootTest(classes={JobApplication.class})// 指定启动类
public class JobControllerTest {
	
	private static final Logger log = LoggerFactory.getLogger(JobControllerTest.class) ; 

	@Autowired
	JobService jobService;
	
	@Autowired
    ZookeeperRegistryCenter zookeeperRegistryCenter;

	@Autowired
	DataSource dataSource ; 
	
	@Test
	public void testList() {
		log.info("=====> data source:{}" , dataSource);
		
	}

	@Test
	public void testAddJob() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveJob() {
		fail("Not yet implemented");
	}

}
