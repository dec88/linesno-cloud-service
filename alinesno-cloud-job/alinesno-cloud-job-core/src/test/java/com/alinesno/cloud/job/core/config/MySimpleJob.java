package com.alinesno.cloud.job.core.config;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.job.core.annotation.ElasticJobConf;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

/**
 * 简单工作任务 
 * @author LuoAnDong
 * @since 2019年10月5日 上午9:53:26
 */
@ElasticJobConf(name = "MySimpleJob-2", cron = "0/10 * * * * ?", 
				shardingItemParameters = "0=0,1=1", 
				description = "简单任务" , 
				eventTraceRdbDataSource = "dataSource")
public class MySimpleJob implements SimpleJob {

	private static final Logger log = LoggerFactory.getLogger(MySimpleJob.class) ; 
	
	public void execute(ShardingContext context) {
		
		String shardParamter = context.getShardingParameter();
		String time = new SimpleDateFormat("HH:mm:ss").format(new Date());
		
		log.info("分片参数:{} , 开始执行简单任务: {}" , shardParamter, time);
	}

}