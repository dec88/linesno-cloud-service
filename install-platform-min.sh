#!/bin/bash
#--------------------------------------------------
# This script is for: 安装基础环境
#--------------------------------------------------

set -o nounset
set -o errexit
#set -o xtrace

PLATFORM_MIN_URL=https://gitee.com/landonniao/linesno-cloud-service/blob/master/container/compose/90-dev-compose-platform-min.yaml
curl -C- -O --retry 3 "$PLATFORM_MIN_URL"

yum install docker
yum install docker-compose
docker-compose -f 90-dev-compose-platform-min.yaml up -d
