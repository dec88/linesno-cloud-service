package com.alinesno.cloud.operation.cmdb.controller.manager;

import java.sql.Timestamp;
import java.util.Optional;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.operation.cmdb.advice.ConvertCode;
import com.alinesno.cloud.operation.cmdb.controller.BaseController;
import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;
import com.alinesno.cloud.operation.cmdb.repository.BudinessServerRepository;
import com.alinesno.cloud.operation.cmdb.web.bean.JqDatatablesPageBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResultGenerator;

/**
 * 后台主面板
 * @author LuoAnDong
 * @since 2018年8月14日 下午12:58:05
 */
@Controller
@RequestMapping("/manager/")
public class ManagerServerController extends BaseController{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private BudinessServerRepository budinessServerRepository; // 订单详情服务
	
	@GetMapping("/server_add")
	public String add(Model model) {
		return WX_MANAGER+"server_add";
	}

	@GetMapping("/server_list")
	public String list(Model model) {
		
		createMenus(model); 
		return WX_MANAGER+"server_list";
	}
	
	@GetMapping("/server_modify/{id}")
	public String serverModify(Model model , @PathVariable("id") String id) {
		logger.debug("id = {}" , id);
		
		ServerEntity bean = budinessServerRepository.findById(id).get() ; 
		model.addAttribute("bean" , bean) ; 
		
		return WX_MANAGER+"server_modify";
	}
	 
	/**
	 * 数据列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ConvertCode
	@ResponseBody
	@GetMapping("/server_list_data")
	public Object serverListData(Model model ,JqDatatablesPageBean page) {
//		return this.toPage(model , budinessServerRepository, page , Sort.by(Direction.DESC, "serverOrder")) ; 
		
		JqDatatablesPageBean p = this.toPage(model, 
				page.buildFilter(ServerEntity.class , request)  , 
				budinessServerRepository, 
				page); 
		
		return p ;  
	}
	
	@ResponseBody
	@PostMapping("/server_add_save")
	public ResponseBean<String> addSave(Model model , ServerEntity bean) {
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(bean));
		
		bean.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		bean.setAddTime(new Timestamp(System.currentTimeMillis()));
		
		bean.setMasterCode(currentManager().getMasterCode());
		
		budinessServerRepository.save(bean) ; 
		return ResultGenerator.genSuccessMessage("保存成功");
	}
	
	@ResponseBody
	@PostMapping("/server_save")
	public ResponseBean<String> save(Model model , ServerEntity e) {
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
		
		ServerEntity bean = budinessServerRepository.findById(e.getId()).get() ; 
		
		bean.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		bean.setAddTime(new Timestamp(System.currentTimeMillis()));
		
		bean.setServerName(e.getServerName());
		bean.setServerDesc(e.getServerDesc());
		bean.setServerOrder(e.getServerOrder());
		bean.setFieldProp(e.getFieldProp());
		
		budinessServerRepository.save(bean) ; 
		return ResultGenerator.genSuccessMessage("保存成功");
	}
	
	@ResponseBody
	@GetMapping("/server_changeStatus")
	public ResponseBean<String> changeStatus(String id) {
		logger.debug("change status uid = {}" , id);
		Optional<ServerEntity> userOption = budinessServerRepository.findById(id) ; 
		
		if(userOption.isPresent()) {
			ServerEntity user = userOption.get() ; 
			user.setHasStatus((1+user.getHasStatus())%2);
			budinessServerRepository.save(user) ; 
			return ResultGenerator.genFailMessage("修改成功.") ; 
		}
		
		return ResultGenerator.genFailMessage("修改失败.") ; 
	}
	

}
