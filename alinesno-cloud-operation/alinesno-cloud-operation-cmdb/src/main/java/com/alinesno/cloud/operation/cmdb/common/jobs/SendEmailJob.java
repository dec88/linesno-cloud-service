package com.alinesno.cloud.operation.cmdb.common.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.alinesno.cloud.operation.cmdb.third.email.RunEmailService;


/**
 * 定时任务,判断订单是否超时,只能在本地运行，服务器上被判断成垃圾邮件。
 * 
 * @author LuoAnDong
 * @since 2018年8月11日 下午12:37:58
 */
// @Component
public class SendEmailJob {
	
	private static final Logger logger = LoggerFactory.getLogger(SendEmailJob.class) ;
	
	@Autowired
	private RunEmailService runEmailService; 
	 
	// 每分钟做一次订单表扫描，并查询出未接的订单，并判断时间是否超过时间 
	@Scheduled(cron = "0 0/1 * * * ?")
	public void timerToNow() {
		logger.debug("发送邮件定时任务.");
		runEmailService.sendRunmanEmail(); 
	}

}