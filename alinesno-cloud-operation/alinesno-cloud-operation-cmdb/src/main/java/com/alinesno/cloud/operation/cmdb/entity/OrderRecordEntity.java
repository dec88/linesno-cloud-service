package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 订单记录,记录订单的变化过程 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:04:10
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_order_record")
public class OrderRecordEntity extends BaseEntity {

	private String orderId ; //订单id
	private String actionCode ; //变化代码
	private String actionName ; //变化名称
	private String actionDetail ; //详情
	private String actionMan ; //操作变化的人
	
	public OrderRecordEntity() {
		super();
	}
	public OrderRecordEntity(String orderId, String actionCode, String actionName, String actionMan) {
		super();
		this.orderId = orderId;
		this.actionCode = actionCode;
		this.actionName = actionName;
		this.actionMan = actionMan;
	}
	public String getActionDetail() {
		return actionDetail;
	}
	public void setActionDetail(String actionDetail) {
		this.actionDetail = actionDetail;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getActionMan() {
		return actionMan;
	}
	public void setActionMan(String actionMan) {
		this.actionMan = actionMan;
	}
	
}
