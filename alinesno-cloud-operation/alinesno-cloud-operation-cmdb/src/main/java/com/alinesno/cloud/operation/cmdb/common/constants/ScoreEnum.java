package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 评分
 * @author LuoAnDong
 * @since 2018年10月19日 下午10:16:48
 */
public enum ScoreEnum {
	
	FINISH_ORDER(1,"完成订单") , 
	SCORE_BEST(1,"优质订单") ,
	SCORE_BAD(-1,"差评订单") ; 
	
//	ACTION_FINISH_ORDER("finish_order" , "完成订单") ; 
	
	public int score ;
	public String text;

	ScoreEnum(int score , String text) {
		this.score = score;
		this.text = text;
	}

}
