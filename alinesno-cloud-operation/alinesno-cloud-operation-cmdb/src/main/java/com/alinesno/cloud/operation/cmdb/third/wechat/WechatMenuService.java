package com.alinesno.cloud.operation.cmdb.third.wechat;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.error.WxErrorException;

/**
 * 微信菜单生成 
 * @author LuoAnDong
 * @since 2018年10月8日 下午11:17:37
 */
@Component
public class WechatMenuService {
	
	private static final Logger logger = LoggerFactory.getLogger(WechatMenuService.class);

	@Autowired
	private WechatConfig wechatConfig ; 
	
	/**
	 * 生成菜单 
	 * @return
	 */
	public boolean createMenu(List<WxMenuButton> buttons) {
		WxMenu wxMenu = new WxMenu();
		try {
			wxMenu.setButtons(buttons);
			String responseStr = wechatConfig.getInstance().getMenuService().menuCreate(wxMenu);
			if(responseStr != null) {
				logger.debug("菜单生成失败:{}" , responseStr);
			}
			return true ; 
		} catch (WxErrorException e) {
			logger.error("菜单生成错误:{}" , e);
		} 
		return false ; 
	}
	
}
