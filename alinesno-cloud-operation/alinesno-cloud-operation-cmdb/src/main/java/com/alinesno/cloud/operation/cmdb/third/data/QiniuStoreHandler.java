package com.alinesno.cloud.operation.cmdb.third.data;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.operation.cmdb.common.id.SnowflakeIdWorker;
import com.alinesno.cloud.operation.cmdb.common.util.DateUtil;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;

/**
 * 七牛数据存储方案
 *
 * @author LuoAnDong
 * @since 2018年2月23日 下午2:37:04
 */
@SuppressWarnings("deprecation")
@Component
public class QiniuStoreHandler {

	//日志记录
	private final  Logger logger = LoggerFactory.getLogger(QiniuStoreHandler.class);

	// 设置需要操作的账号的AK和SK
	@Value("${application.compoment.data.qiniu.access-key}")
	private  String accessKey ;

	@Value("${application.compoment.data.qiniu.secret-key}")
	private  String secretKey  ;

	@Value("${application.compoment.data.qiniu.space-bucket}")
	private  String bucket ;

	@Value("${application.compoment.data.qiniu.space-zone}")
	private  String zone ;

	@Value("${application.compoment.data.qiniu.host-domain}")
	private  String domain ;

	@Value("${application.compoment.data.qiniu.host-domain-deadline}")
	private  String deadline ;

	private  Gson gson = new Gson() ;

	/**
	 * 普通上传
	 * @param localFilePath
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public  String upload(String localFilePath) throws IOException {

		Configuration cfg = new Configuration(getZone(zone));
		UploadManager uploadManager = new UploadManager(cfg);

		Auth auth = Auth.create(accessKey, secretKey) ;
		String key = DateUtil.formatDate(new Date(),"yyyyMMddHHmmss") + SnowflakeIdWorker.getId() ;
		String upToken = auth.uploadToken(bucket);

		try {
			Response response = uploadManager.put(localFilePath, key, upToken);
			DefaultPutRet putRet = gson.fromJson(response.bodyString(), DefaultPutRet.class);

			logger.debug("put key = {} , hash = {}" , putRet.key , putRet.hash);

		} catch (QiniuException ex) {
			Response r = ex.response;
			try {
				logger.debug(r.bodyString());
			} catch (QiniuException ex2) {
				logger.error("上传文件错误:",ex2);
			}

			return null ;
		}

		return  (domain.endsWith("/")?domain:domain+"/")+key ;
	}

	/**
	 * 获取到区域
	 * @param zone2
	 * @return
	 */
	private  Zone getZone(String zone) {
		Zone z = Zone.zoneNa0() ;  //北美
		if(StringUtils.isNoneBlank(zone)) {
			if(zone.equals("0")) {
				z = Zone.zone0() ;
			}else  if(zone.equals("1")) {
				z = Zone.zone1() ;
			}else  if(zone.equals("2")) {
				z = Zone.zone2() ;
			}
		}
		return z ;
	}

	/**
	 * 获取私有下载链接
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public  String privateUrl(String fileName) throws IOException {

		String encodedFileName = URLEncoder.encode(fileName, "utf-8");
		String publicUrl = String.format("%s/%s", domain , encodedFileName);

		long expireInSeconds = Integer.parseInt(deadline) ;//以秒为单位
		Auth auth = Auth.create(accessKey, secretKey) ;
		String finalUrl = auth.privateDownloadUrl(publicUrl, expireInSeconds);

		return finalUrl ;
	}

	/**
	 * 根据id删除文件
	 * @param fileId
	 * @return
	 */
	public boolean deleteData(String fileId) {
		//构造一个带指定Zone对象的配置类
		Configuration cfg = new Configuration(this.getZone(zone));
		Auth auth = Auth.create(accessKey, secretKey);
		BucketManager bucketManager = new BucketManager(auth, cfg);
		try {
		    bucketManager.delete(bucket, fileId);
		} catch (QiniuException ex) {
		    logger.error("code = {} , response = {}" , ex.code() , ex.response.toString());
		    return false;
		}
		return true;
	}

}
