package com.alinesno.cloud.operation.cmdb.common.util;

import java.util.Random;

/**
 * 订单工具类
 * @author LuoAnDong
 * @since 2018年10月22日 下午6:41:25
 */
public class OrderBuildUtils {

	private static final int ORDER_LENGTH = 3;

	/**
	 * 生成订单<br/>
	 * <b>思路</b><br/>
	 * 1. 订单生成规则: 当天日期+3订单数量(不足补零)+两位随机数
	 * 2. 避免订单暴露业务信息
	 * @return
	 */
	public static String createOrder(int count) {
		String timeF = DateUtil.formatDate(DateUtil.DEFAULT_PATTERN);
		String orderNum = String.format("%0" + ORDER_LENGTH + "d", count);
		
		String endNum = String.format("%02d",new Random().nextInt(99));
		
		return timeF + orderNum + endNum;
	}
	
}
