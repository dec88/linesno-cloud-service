package com.alinesno.cloud.operation.cmdb.common.constants;
/**
 * 常量切换
 * @author LuoAnDong
 * @since 2018年4月10日 上午8:01:11
 */
public interface RunConstants {
	
	String PREFIX_NAME = "ad_weixin_" ;
	
	String PHONE = "phone" ;  //手机号
	String PHONE_CODE = "code"; //前端手机号

	String SCHOOL = "masterCode" ; //机房id

	String CURRENT_USER_OPENID = PREFIX_NAME+"open_id";

	String CURRENT_USER = "current_user" ; //当前用户
	
	String CURRENT_MANAGER = "current_manager" ; //当前管理员

	String OPENID = "openId"; //登陆用户

	String FROM_WHERE = "fromWhere" ;

	String SALAID = "salaId";

	String FULL_SERVER_REQUEST_URL = "full_server_request_url"; //进入server服务路径地址 

	String SEND_PHONE_CODE = "send_phone_code";  //手机验证码

	/**
	 * 页面cookies值 
	 * @author LuoAnDong
	 * @since 2018年4月10日 上午8:01:51
	 */
	interface COOKIES {
		String PAGE_DATA_COOKIES_NAME = PREFIX_NAME+"run_info" ; 
	}
	
	/**
	 * 全局变量
	 * @author LuoAnDong
	 * @since 2018年4月11日 上午7:56:13
	 */
	interface GOOBAL {
		String CURRENT_USER = PREFIX_NAME+"current_user" ; //全局用户
	}
	
}