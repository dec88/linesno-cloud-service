/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.3.13-MariaDB : Database - alinesno_cloud_operation_cmdb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */; 

/*Table structure for table `cmdb_account` */

DROP TABLE IF EXISTS `cmdb_account`;

CREATE TABLE `cmdb_account` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `last_login_time` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `role_power` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s00mhdt1w0b4bc900jk5is3wp` (`login_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_account` */

insert  into `cmdb_account`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`account_status`,`last_login_ip`,`last_login_time`,`login_name`,`name`,`password`,`role_id`,`role_power`,`salt`,`user_id`,`master_code`) values ('551480428817547264','2019-03-02 19:06:20','0',NULL,NULL,NULL,0,0,'001','0','2019-08-26 17:33:48','1','127.0.0.1','2019-08-26 17:33:48','admin','超级管理员','admin',NULL,'9','1234',NULL,NULL),('551488609866219520','2019-03-02 19:38:51','0',NULL,NULL,NULL,0,0,'001','0','2019-05-17 13:10:17','1','0:0:0:0:0:0:0:1','2019-05-17 13:10:17','001','广州海珠区服务器管理员','1c7c86dd4a3524215a674fb5d3c92797',NULL,'0','129678',NULL,NULL),('552606042869989376','2019-03-05 21:39:08','0',NULL,NULL,NULL,0,0,'002','0','2019-03-06 07:57:25','1','0:0:0:0:0:0:0:1','2019-03-06 07:57:25','switch','南宁珠海机房管理员','fd27d63f639b550b1af94fc440362470',NULL,'0','717213',NULL,'002'),('555515470858420224','2019-03-13 22:20:09','0',NULL,NULL,NULL,0,0,'002','0','2019-05-17 13:10:38','1','0:0:0:0:0:0:0:1','2019-05-17 13:10:38','002','广州机房管理员','eaf4548d784fcbb941a1506744c6e4fc',NULL,'0','790870',NULL,'001'),('578932695254433792','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-08-29 11:11:41','1','127.0.0.1','2019-08-29 11:11:41','manager','广州机房管理员','2ae403ad10a1ef836b136b2a9e0222ca',NULL,'0','227054',NULL,'001');

/*Table structure for table `cmdb_account_resource` */

DROP TABLE IF EXISTS `cmdb_account_resource`;

CREATE TABLE `cmdb_account_resource` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `data_scope` varchar(255) DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_account_resource` */

insert  into `cmdb_account_resource`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`account_id`,`data_scope`,`resource_id`,`master_code`) values ('551488609899773952','2019-03-02 19:38:51','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:38:51','551488609866219520',NULL,'551482687840321536',NULL),('551488609903968256','2019-03-02 19:38:51','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:38:51','551488609866219520',NULL,'551482687907430400',NULL),('551488609903968257','2019-03-02 19:38:51','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:38:51','551488609866219520',NULL,'551482687915819008',NULL),('551488609908162560','2019-03-02 19:38:51','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:38:51','551488609866219520',NULL,'551482687924207616',NULL),('551488609908162561','2019-03-02 19:38:51','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:38:51','551488609866219520',NULL,'551482687928401920',NULL),('552748363053793281','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687928401920',NULL),('552748363053793280','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687924207616',NULL),('552748363049598977','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687915819008',NULL),('552748363049598976','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687915819006',NULL),('552748363045404673','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687915819007',NULL),('552748363045404672','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687907430400',NULL),('552748363041210368','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687840321536',NULL),('578932695564812288','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687920013312',NULL),('578932695560617985','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687928401920',NULL),('578932695560617984','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687915819009',NULL),('578932695556423680','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687915819006',NULL),('578932695552229377','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687915819007',NULL),('578932695552229376','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687924207616',NULL),('578932695548035072','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687915819008',NULL),('552748363057987584','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687920013312',NULL),('552748363057987585','2019-03-06 07:04:39','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 07:04:39','552606042869989376',NULL,'551482687936790528',NULL),('578932695543840768','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'55148268791581114',NULL),('578932695539646464','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'55148268791581113',NULL),('578932695535452160','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687907430400',NULL),('578932695531257856','2019-05-17 13:11:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 13:11:50','578932695254433792',NULL,'551482687840321536',NULL),('576430771024166912','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687840321536',NULL),('576430771087081472','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687907430400',NULL),('576430771087081473','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'5514826879158111',NULL),('576430771091275776','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687915819008',NULL),('576430771095470080','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687924207616',NULL),('576430771116441600','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687915819007',NULL),('576430771120635904','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687915819006',NULL),('576430771124830208','2019-05-10 15:30:05','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-10 15:30:05','555515470858420224',NULL,'551482687915819009',NULL);

/*Table structure for table `cmdb_address` */

DROP TABLE IF EXISTS `cmdb_address`;

CREATE TABLE `cmdb_address` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `address_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `floor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_address` */

/*Table structure for table `cmdb_apply` */

DROP TABLE IF EXISTS `cmdb_apply`;

CREATE TABLE `cmdb_apply` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `apply_content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `apply_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `apply_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_apply` */

/*Table structure for table `cmdb_asserts` */

DROP TABLE IF EXISTS `cmdb_asserts`;

CREATE TABLE `cmdb_asserts` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `assert_detail` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `assert_man` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `assert_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `assert_num` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `assert_tags` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `assert_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `business_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `manager_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_asserts` */

/*Table structure for table `cmdb_database_apply_info` */

DROP TABLE IF EXISTS `cmdb_database_apply_info`;

CREATE TABLE `cmdb_database_apply_info` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `apply_status` int(11) DEFAULT NULL,
  `data_space_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data_space_size` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dba_login_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `host_ip` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `host_pwd` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `index_space_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `index_space_size` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `manager_man` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `space_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_database_apply_info` */

/*Table structure for table `cmdb_database_info` */

DROP TABLE IF EXISTS `cmdb_database_info`;

CREATE TABLE `cmdb_database_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `data_space_name` varchar(255) DEFAULT NULL,
  `dba_login_name` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `host_ip` varchar(255) DEFAULT NULL,
  `host_pwd` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `index_space_name` varchar(255) DEFAULT NULL,
  `manager_man` varchar(255) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `space_desc` varchar(255) DEFAULT NULL,
  `apply_status` int(11) DEFAULT NULL,
  `data_space_size` varchar(255) DEFAULT NULL,
  `index_space_size` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_database_info` */

insert  into `cmdb_database_info`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`master_code`,`tenant_id`,`update_time`,`data_space_name`,`dba_login_name`,`end_time`,`host_ip`,`host_pwd`,`hostname`,`index_space_name`,`manager_man`,`send_time`,`space_desc`,`apply_status`,`data_space_size`,`index_space_size`) values ('555509380791926784','2019-03-13 21:55:57','0',NULL,NULL,NULL,0,0,'001','0','2019-03-13 22:30:59','31231','12312','2019-03-14 00:00:00',NULL,NULL,NULL,'23123','123123','2019-03-13 00:00:00','312312',0,NULL,NULL),('555511622483836928','2019-03-13 22:04:52','0',NULL,NULL,NULL,0,0,'001','0','2019-03-13 22:31:00','wdjk_space','wdjk','2019-06-13 00:00:00',NULL,NULL,NULL,'wdjk_space_index','张三','2019-03-11 00:00:00','网点监控',0,NULL,NULL),('575959648176177152','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575959648209731584','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575959648213925888','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575959648222314496','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575959648222314497','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575959648226508800','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575959648230703104','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575959648230703105','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575959648234897408','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575959648239091712','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space',NULL,'2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575959648239091713','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL),('575959648243286016','2019-05-09 08:18:01','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:18:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL),('575960003932848128','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960003937042432','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960003937042433','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960003941236736','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960003945431040','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960003949625344','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960003949625345','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960003953819648','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960003958013952','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960003958013953','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960003962208256','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL),('575960003966402560','2019-05-09 08:19:26','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:19:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL),('575960892928163840','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893016244224','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893028827136','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893045604352','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893058187264','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893070770176','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893087547392','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893100130304','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893112713216','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893121101824','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893133684736','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893150461952','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893163044864','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893175627776','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893188210688','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893204987904','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893217570816','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893234348032','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893246930944','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893259513856','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893272096768','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893288873984','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893301456896','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893314039808','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893326622720','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893339205632','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893351788544','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893368565760','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893381148672','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893393731584','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893406314496','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893414703104','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893431480320','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893444063232','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893456646144','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893469229056','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893481811968','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893490200576','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893502783488','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893515366400','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893527949312','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893536337920','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893548920832','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893557309440','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893574086656','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893582475264','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893595058176','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893603446784','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893616029696','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893624418304','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893637001216','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893649584128','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893662167040','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893670555648','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893683138560','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893695721472','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893708304384','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893720887296','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893729275904','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893741858816','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893750247424','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893762830336','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893771218944','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893787996160','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893796384768','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893808967680','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893817356288','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893825744896','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893838327808','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('575960893850910720','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-21 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-21 00:00:00','示例使用数据库',0,'25G','25G'),('575960893859299328','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-12 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-12 00:00:00','示例使用数据库',0,'16G','16G'),('575960893871882240','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-13 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-13 00:00:00','示例使用数据库',0,'17G','17G'),('575960893880270848','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-14 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-14 00:00:00','示例使用数据库',0,'18G','18G'),('575960893892853760','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-15 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-15 00:00:00','示例使用数据库',0,'19G','19G'),('575960893901242368','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-16 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-16 00:00:00','示例使用数据库',0,'20G','20G'),('575960893913825280','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-17 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-17 00:00:00','示例使用数据库',0,'21G','21G'),('575960893930602496','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-18 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-18 00:00:00','示例使用数据库',0,'22G','22G'),('575960893938991104','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-19 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-19 00:00:00','示例使用数据库',0,'23G','23G'),('575960893951574016','2019-05-09 08:22:58','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 08:22:58','database_space','demo_database_name','2019-06-20 00:00:00','172.20.200.11',NULL,NULL,'database_index_space','李四','2018-12-20 00:00:00','示例使用数据库',0,'24G','24G'),('576157059632857088','2019-05-09 21:22:27','0',NULL,NULL,NULL,0,0,'001','0','2019-05-19 21:08:17',NULL,'23123','2019-05-17 00:00:00',NULL,NULL,NULL,NULL,'123','2019-05-09 00:00:00','123123',0,'16G','16G');

/*Table structure for table `cmdb_deploy_info` */

DROP TABLE IF EXISTS `cmdb_deploy_info`;

CREATE TABLE `cmdb_deploy_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `business_type` varchar(255) DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `get_address` varchar(255) DEFAULT NULL,
  `goods_name` varchar(255) DEFAULT NULL,
  `goods_order_id` varchar(255) DEFAULT NULL,
  `goods_type` varchar(255) DEFAULT NULL,
  `goods_weight` varchar(255) DEFAULT NULL,
  `machine_id` varchar(255) DEFAULT NULL,
  `order_cpu` varchar(255) DEFAULT NULL,
  `order_memory` varchar(255) DEFAULT NULL,
  `order_network` varchar(255) DEFAULT NULL,
  `order_pwd` varchar(255) DEFAULT NULL,
  `order_storage` varchar(255) DEFAULT NULL,
  `order_system` varchar(255) DEFAULT NULL,
  `order_username` varchar(255) DEFAULT NULL,
  `price_cert` varchar(255) DEFAULT NULL,
  `receive_address` varchar(255) DEFAULT NULL,
  `receive_phone` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `total_pay` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `machine_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_deploy_info` */

insert  into `cmdb_deploy_info`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`master_code`,`tenant_id`,`update_time`,`business_type`,`discount`,`end_time`,`floor`,`get_address`,`goods_name`,`goods_order_id`,`goods_type`,`goods_weight`,`machine_id`,`order_cpu`,`order_memory`,`order_network`,`order_pwd`,`order_storage`,`order_system`,`order_username`,`price_cert`,`receive_address`,`receive_phone`,`remarks`,`send_time`,`total_pay`,`user_id`,`machine_ip`) values ('552744896658669568','2019-03-06 06:50:53','0',NULL,NULL,NULL,0,0,'002','0','2019-03-06 06:50:53',NULL,0,'2019-03-22 00:00:00',NULL,NULL,'百色Dubbo服务器',NULL,NULL,NULL,'552614238661640192','4','12','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-13 00:00:00',NULL,NULL,'172.20.200.124'),('576154927911403520','2019-05-09 21:13:59','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:13:59',NULL,0,'2019-03-14 00:00:00',NULL,NULL,'测试虚拟机',NULL,NULL,NULL,'575961356704940032','8','675','987987',NULL,'66','098098',NULL,NULL,'研发部',NULL,NULL,'2019-03-11 00:00:00',NULL,NULL,'172.20.200.121'),('576154927953346560','2019-05-09 21:13:59','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:13:59',NULL,0,'2019-03-14 00:00:00',NULL,NULL,'测试虚拟机',NULL,NULL,NULL,'575961356721717248','8','675','987987',NULL,'66','098098',NULL,NULL,'研发部',NULL,NULL,'2019-03-11 00:00:00',NULL,NULL,'172.20.200.112'),('576154927974318080','2019-05-09 21:13:59','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:13:59',NULL,0,'2019-03-14 00:00:00',NULL,NULL,'测试虚拟机',NULL,NULL,NULL,'575961356738494464','8','675','987987',NULL,'66','098098',NULL,NULL,'研发部',NULL,NULL,'2019-03-11 00:00:00',NULL,NULL,'172.20.200.113'),('576155049600745472','2019-05-09 21:14:28','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:14:28',NULL,0,'2019-03-27 00:00:00',NULL,NULL,'南宁业务服务器',NULL,NULL,NULL,'555872892596781056','2','32G','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-28 00:00:00',NULL,NULL,'172.20.200.11'),('576155049617522688','2019-05-09 21:14:28','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:14:28',NULL,0,'2019-03-27 00:00:00',NULL,NULL,'南宁业务服务器',NULL,NULL,NULL,'555872938784456704','2','32G','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-28 00:00:00',NULL,NULL,'172.20.200.123'),('576155049634299904','2019-05-09 21:14:28','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:14:28',NULL,0,'2019-03-27 00:00:00',NULL,NULL,'南宁业务服务器',NULL,NULL,NULL,'575809727116607488','2','32G','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-28 00:00:00',NULL,NULL,'172.20.200.112'),('576155049651077120','2019-05-09 21:14:28','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:14:28',NULL,0,'2019-03-27 00:00:00',NULL,NULL,'南宁业务服务器',NULL,NULL,NULL,'575809727213076480','2','32G','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-28 00:00:00',NULL,NULL,'172.20.200.113'),('576155049663660032','2019-05-09 21:14:28','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:14:28',NULL,0,'2019-03-27 00:00:00',NULL,NULL,'南宁业务服务器',NULL,NULL,NULL,'575809727242436608','2','32G','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-28 00:00:00',NULL,NULL,'172.20.200.114'),('576155049676242944','2019-05-09 21:14:28','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:14:28',NULL,0,'2019-03-27 00:00:00',NULL,NULL,'南宁业务服务器',NULL,NULL,NULL,'575809727267602432','2','32G','内外网',NULL,'200','Centos7_x64',NULL,NULL,'研发部',NULL,NULL,'2019-03-28 00:00:00',NULL,NULL,'172.20.200.115'),('576156687329656832','2019-05-09 21:20:59','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:20:59',NULL,0,'2019-03-14 00:00:00',NULL,NULL,'12312',NULL,NULL,NULL,'555872938784456704','31231','1231','23123',NULL,'23123','123',NULL,NULL,'12312',NULL,NULL,'2019-03-13 00:00:00',NULL,NULL,'172.20.200.123'),('576156687367405568','2019-05-09 21:20:59','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:20:59',NULL,0,'2019-03-14 00:00:00',NULL,NULL,'12312',NULL,NULL,NULL,'575809727116607488','31231','1231','23123',NULL,'23123','123',NULL,NULL,'12312',NULL,NULL,'2019-03-13 00:00:00',NULL,NULL,'172.20.200.112'),('576156687384182784','2019-05-09 21:20:59','0',NULL,NULL,NULL,0,0,'001','0','2019-05-09 21:20:59',NULL,0,'2019-03-14 00:00:00',NULL,NULL,'12312',NULL,NULL,NULL,'575809727213076480','31231','1231','23123',NULL,'23123','123',NULL,NULL,'12312',NULL,NULL,'2019-03-13 00:00:00',NULL,NULL,'172.20.200.113');

/*Table structure for table `cmdb_email` */

DROP TABLE IF EXISTS `cmdb_email`;

CREATE TABLE `cmdb_email` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `email_owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `fail_desc` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `fail_time` int(11) NOT NULL,
  `last_send_fila_time` datetime DEFAULT NULL,
  `rend_status` int(11) NOT NULL,
  `send_time` int(11) NOT NULL,
  `sex` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `success_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_email` */

/*Table structure for table `cmdb_goods_order` */

DROP TABLE IF EXISTS `cmdb_goods_order`;

CREATE TABLE `cmdb_goods_order` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `goods_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `goods_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `goods_num` int(11) NOT NULL,
  `goods_order_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `goods_pic` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `goods_prices` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `goods_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `select_goods_num` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_goods_order` */

/*Table structure for table `cmdb_headth` */

DROP TABLE IF EXISTS `cmdb_headth`;

CREATE TABLE `cmdb_headth` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `check_loop` int(11) NOT NULL,
  `check_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `headth_job` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `headth_link` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `headth_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_headth_time` datetime DEFAULT NULL,
  `tcp_host` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tcp_port` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_headth` */

insert  into `cmdb_headth`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`master_code`,`tenant_id`,`update_time`,`check_loop`,`check_type`,`headth_job`,`headth_link`,`headth_status`,`last_headth_time`,`tcp_host`,`tcp_port`) values ('578933053255057408','2019-05-17 13:13:15','0',NULL,NULL,NULL,0,0,'001','0','2019-05-17 13:13:15',120,'http','应用服务器','http://cloud.linesno.com',NULL,'2019-05-17 13:13:15','192.168.1.125','8081'),('578933054014226432','2019-05-17 13:13:16','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:00',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:16','192.168.1.125','8081'),('578933054811144192','2019-05-17 13:13:16','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:00',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:16','192.168.1.125','8082'),('578933055595479040','2019-05-17 13:13:16','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:01',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:16','192.168.1.125','8083'),('578933056509837312','2019-05-17 13:13:16','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:01',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:16','192.168.1.125','8084'),('578933057432584192','2019-05-17 13:13:16','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:02',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:16','192.168.1.125','8085'),('578933058208530432','2019-05-17 13:13:17','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:02',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:17','192.168.1.125','8086'),('578933058971893760','2019-05-17 13:13:17','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:03',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:17','192.168.1.125','8087'),('578933059752034304','2019-05-17 13:13:17','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:03',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:17','192.168.1.125','8088'),('578933060532174848','2019-05-17 13:13:17','0',NULL,NULL,NULL,0,1,'001','0','2019-05-17 13:24:03',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 13:13:17','192.168.1.125','8089'),('579042693145952256','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 20:28:56',120,'http','应用服务器','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8081'),('579042693494079488','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:03',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8081'),('579042693515051008','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:04',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8082'),('579042693540216832','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:04',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8083'),('579042693569576960','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:05',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8084'),('579042693594742784','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:05',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8085'),('579042693632491520','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:05',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8086'),('579042693649268736','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:06',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8087'),('579042693678628864','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:06',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8088'),('579042693695406080','2019-05-17 20:28:56','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:29:07',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:28:56','192.168.1.125','8089'),('579042781079535616','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-17 20:29:17',120,'http','应用服务器','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8081'),('579042781117284352','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:08',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8081'),('579042781159227392','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:09',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8082'),('579042781213753344','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:09',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8083'),('579042781259890688','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:10',120,'tcp','分布式配置中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8084'),('579042781314416640','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:10',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8085'),('579042781356359680','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:11',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8086'),('579042781377331200','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:11',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8087'),('579042781415079936','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:11',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8088'),('579042781431857152','2019-05-17 20:29:17','0',NULL,NULL,NULL,0,1,NULL,'0','2019-05-17 20:30:12',120,'tcp','分布式缓存中心','http://cloud.linesno.com',NULL,'2019-05-17 20:29:17','192.168.1.125','8089');

/*Table structure for table `cmdb_headth_logger` */

DROP TABLE IF EXISTS `cmdb_headth_logger`;

CREATE TABLE `cmdb_headth_logger` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `headth_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_headth_time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `startheadth_time` datetime DEFAULT NULL,
  `error_message` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_headth_logger` */

/*Table structure for table `cmdb_integral_record` */

DROP TABLE IF EXISTS `cmdb_integral_record`;

CREATE TABLE `cmdb_integral_record` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `action_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `action_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `action_remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `score_total` int(11) NOT NULL,
  `score_value` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_integral_record` */

/*Table structure for table `cmdb_links` */

DROP TABLE IF EXISTS `cmdb_links`;

CREATE TABLE `cmdb_links` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `link_desc` varchar(255) DEFAULT NULL,
  `link_icons` varchar(255) DEFAULT NULL,
  `link_path` varchar(255) DEFAULT NULL,
  `link_tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_links` */

insert  into `cmdb_links`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`master_code`,`tenant_id`,`update_time`,`link_desc`,`link_icons`,`link_path`,`link_tags`) values ('552757982056677376','2019-03-06 07:42:53','0',NULL,NULL,NULL,0,0,'002','0','2019-03-06 07:42:53','23123',NULL,'123123','1'),('552758098897403904','2019-03-06 07:43:21','0',NULL,NULL,NULL,0,0,'002','0','2019-03-06 07:43:21','123123',NULL,'123123','7'),('552758526393450496','2019-03-06 07:45:02','0',NULL,NULL,NULL,0,0,'002','0','2019-03-06 07:45:02','123123',NULL,'123123','8'),('552758651173994496','2019-03-06 07:45:32','0',NULL,NULL,NULL,0,0,'002','0','2019-03-06 07:45:32','123123',NULL,'123123','1');

/*Table structure for table `cmdb_logger` */

DROP TABLE IF EXISTS `cmdb_logger`;

CREATE TABLE `cmdb_logger` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `record_channel` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `record_msg` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `record_params` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `record_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `record_user` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `record_user_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_logger` */

/*Table structure for table `cmdb_machine` */

DROP TABLE IF EXISTS `cmdb_machine`;

CREATE TABLE `cmdb_machine` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `machine_desc` varchar(255) DEFAULT NULL,
  `machine_ip` varchar(255) DEFAULT NULL,
  `machine_name` varchar(255) DEFAULT NULL,
  `machine_num` int(11) NOT NULL,
  `machine_open_port` varchar(255) DEFAULT NULL,
  `machine_pic` varchar(255) DEFAULT NULL,
  `machine_prices` varchar(255) DEFAULT NULL,
  `machine_pwd` varchar(255) DEFAULT NULL,
  `machine_rootname` varchar(255) DEFAULT NULL,
  `machine_status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_machine` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `master_machineip` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `machine_cpu` varchar(255) DEFAULT NULL,
  `machine_memery` varchar(255) DEFAULT NULL,
  `machine_remark` varchar(255) DEFAULT NULL,
  `machine_storage` varchar(255) DEFAULT NULL,
  `machine_system` varchar(255) DEFAULT NULL,
  `machine_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_machine` */

insert  into `cmdb_machine`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`discount`,`machine_desc`,`machine_ip`,`machine_name`,`machine_num`,`machine_open_port`,`machine_pic`,`machine_prices`,`machine_pwd`,`machine_rootname`,`machine_status`,`user_id`,`master_machine`,`master_code`,`master_machineip`,`user_name`,`machine_cpu`,`machine_memery`,`machine_remark`,`machine_storage`,`machine_system`,`machine_type`) values ('552581798836568064','2019-03-05 20:02:47','0',NULL,NULL,NULL,0,0,'001','0','2019-03-05 20:08:06',0,'消息中间件用品及相关管理平台','172.20.168.111','消息中间件',0,NULL,'http://data.linesno.com/20190305200247552581797808963584',NULL,'admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('552579484394455040','2019-03-05 19:53:36','0',NULL,NULL,NULL,0,0,'001','0','2019-03-05 19:56:20',0,'用于安装zk及相关服务，如Dubbo控制台等','172.20.200.11','Zookeeper服务器',1,NULL,'http://data.linesno.com/20190305195335552579483392016384','11','admin',NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('552580446928502784','2019-03-05 19:57:25','0',NULL,NULL,NULL,0,0,'001','0','2019-03-05 19:57:25',0,'用于持续集成平台及管理','172.20.200.123','持续集成服务器',0,NULL,'http://data.linesno.com/20190305195724552580445926064128',NULL,'admin',NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('552610167326244864','2019-03-05 21:55:31','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 22:18:12',0,'部署消息中间件及相关管理后台服务','172.20.200.11','消息中间件',0,NULL,'http://data.linesno.com/20190305215530552610166059565056',NULL,'admin',NULL,'0',NULL,'192.168.1.123','002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('552614238661640192','2019-03-05 22:11:42','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 06:17:05',0,'用于安装zk及相关服务，如Dubbo控制台等','172.20.200.124','持续集成服务器',0,NULL,'http://data.linesno.com/20190305221141552614237164273664',NULL,'admin',NULL,'0',NULL,'172.20.168.90','002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('552614472557002752','2019-03-05 22:12:37','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-06 06:54:14',0,'部署消息中间件及相关管理后台服务','172.20.200.113','Zookeeper服务器',0,NULL,'http://data.linesno.com/20190305221237552614471529398272',NULL,'admin',NULL,'0',NULL,'172.20.174.12','002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('555518645019607040','2019-03-13 22:32:46','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-13 22:32:46',0,'123123','172.20.200.11','123123',0,NULL,NULL,NULL,'1234qwer',NULL,'0',NULL,'172.20.200.9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('555871537576542208','2019-03-14 21:55:02','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-14 21:55:02',0,'部署消息中间件及相关管理后台服务','172.20.200.11','123123',0,NULL,NULL,NULL,'1234qwer',NULL,'0',NULL,'172.20.200.9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('555872892596781056','2019-03-14 22:00:25','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-14 22:00:25',0,'部署消息中间件及相关管理后台服务','172.20.200.11','123123',0,NULL,NULL,NULL,'1234qwer',NULL,'0',NULL,'帮我取','001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('555872938784456704','2019-03-14 22:00:36','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-14 22:00:36',0,'用于安装zk及相关服务，如Dubbo控制台等','172.20.200.123','Zookeeper服务器',0,NULL,NULL,NULL,'1234qwer',NULL,'0',NULL,'帮我做','001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727116607488','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727213076480','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727242436608','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727267602432','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727292768256','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727309545472','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727330516992','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727347294208','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727372460032','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575809727393431552','2019-05-08 22:22:17','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:22:17',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('575810913274167296','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:22',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,'0',NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913366441984','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:23',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,'0',NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913391607808','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:23',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,'0',NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913412579328','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:25',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,'0',NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913437745152','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:25',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,'0',NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913454522368','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:27:00',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913479688192','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:27:00',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913496465408','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:27:00',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913513242624','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:27:00',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575810913538408448','2019-05-08 22:27:00','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-08 22:27:00',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979688230912','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979734368256','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979751145472','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979767922688','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979784699904','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979809865728','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979826642944','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979843420160','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979864391680','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575951979881168896','2019-05-09 07:47:33','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:33',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489',NULL,'172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063834357760','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063851134976','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063867912192','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063884689408','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063901466624','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063918243840','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063943409664','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063960186880','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063976964096','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575952063993741312','2019-05-09 07:47:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 07:47:53',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356247760896','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356264538112','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356281315328','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356298092544','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356314869760','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356331646976','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356344229888','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356356812800','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356369395712','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356386172928','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356402950144','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356419727360','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356436504576','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356457476096','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356474253312','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356491030528','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356507807744','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356524584960','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356537167872','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356553945088','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356566528000','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356579110912','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356608471040','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356621053952','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356637831168','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356650414080','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356667191296','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356679774208','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356692357120','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356704940032','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356721717248','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础服务器','172.20.200.112','基础服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356738494464','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'监控运维平台','172.20.200.113','监控运维平台',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356751077376','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'注册中心','172.20.200.114','注册中心',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356763660288','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础业务服务器','172.20.200.115','基础业务服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356780437504','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础前端服务器','172.20.200.116','基础前端服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356793020416','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'基础功能服务器','172.20.200.117','基础功能服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356809797632','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.118','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356822380544','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'分布式缓存服务器','172.20.200.119','分布式缓存服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356839157760','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.120','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL),('575961356851740672','2019-05-09 08:24:48','0',NULL,NULL,NULL,0,0,NULL,'0','2019-05-09 08:24:48',0,'日志服务器','172.20.200.121','日志服务器',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'552591122380095489','001','172.20.200.11','罗小东',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cmdb_master_machine` */

DROP TABLE IF EXISTS `cmdb_master_machine`;

CREATE TABLE `cmdb_master_machine` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `master_address` varchar(255) DEFAULT NULL,
  `master_name` varchar(255) DEFAULT NULL,
  `master_order` int(11) NOT NULL,
  `master_owner` varchar(255) DEFAULT NULL,
  `master_owner_code` varchar(255) DEFAULT NULL,
  `master_properties` varchar(255) DEFAULT NULL,
  `master_properties_code` varchar(255) DEFAULT NULL,
  `master_province` varchar(255) DEFAULT NULL,
  `master_province_code` varchar(255) DEFAULT NULL,
  `master_type` varchar(255) DEFAULT NULL,
  `master_type_name` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_master_machine` */

insert  into `cmdb_master_machine`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`master_address`,`master_name`,`master_order`,`master_owner`,`master_owner_code`,`master_properties`,`master_properties_code`,`master_province`,`master_province_code`,`master_type`,`master_type_name`,`master_code`) values ('552591122338152448','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'广州海珠机房',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'001'),('552605227866390528','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'南宁珠海机房',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'002');

/*Table structure for table `cmdb_network_acl` */

DROP TABLE IF EXISTS `cmdb_network_acl`;

CREATE TABLE `cmdb_network_acl` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `network_device` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `operator_man` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_app_business_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_application_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_area` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_device_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_end_time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_host_mask` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_hostip` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_hostname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `source_vlan_number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_application_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_area` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_host_mask` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_hostip` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_hostname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_network_open_port` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_network_protocol` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `target_vlan_number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_network_acl` */

/*Table structure for table `cmdb_order_history` */

DROP TABLE IF EXISTS `cmdb_order_history`;

CREATE TABLE `cmdb_order_history` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `error_msg` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `info_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order_evaluation` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `receive_manager_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sala_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `send_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_order_history` */

/*Table structure for table `cmdb_order_info` */

DROP TABLE IF EXISTS `cmdb_order_info`;

CREATE TABLE `cmdb_order_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `business_type` varchar(255) DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `get_address` varchar(255) DEFAULT NULL,
  `goods_name` varchar(255) DEFAULT NULL,
  `goods_order_id` varchar(255) DEFAULT NULL,
  `goods_type` varchar(255) DEFAULT NULL,
  `goods_weight` varchar(255) DEFAULT NULL,
  `price_cert` varchar(255) DEFAULT NULL,
  `receive_address` varchar(255) DEFAULT NULL,
  `receive_phone` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `total_pay` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `order_cpu` varchar(255) DEFAULT NULL,
  `order_memory` varchar(255) DEFAULT NULL,
  `order_network` varchar(255) DEFAULT NULL,
  `order_pwd` varchar(255) DEFAULT NULL,
  `order_storage` varchar(255) DEFAULT NULL,
  `order_system` varchar(255) DEFAULT NULL,
  `order_username` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_order_info` */

insert  into `cmdb_order_info`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`business_type`,`discount`,`floor`,`get_address`,`goods_name`,`goods_order_id`,`goods_type`,`goods_weight`,`price_cert`,`receive_address`,`receive_phone`,`remarks`,`send_time`,`total_pay`,`user_id`,`master_code`,`order_cpu`,`order_memory`,`order_network`,`order_pwd`,`order_storage`,`order_system`,`order_username`,`end_time`) values ('555512514108981248','2019-03-13 22:08:24','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-13 22:08:24',NULL,0,NULL,NULL,'123123',NULL,NULL,NULL,NULL,'123123',NULL,NULL,'2019-03-12 00:00:00',NULL,NULL,'001','123123','23123','1231',NULL,'1231','23123',NULL,'2019-03-21 00:00:00');

/*Table structure for table `cmdb_order_record` */

DROP TABLE IF EXISTS `cmdb_order_record`;

CREATE TABLE `cmdb_order_record` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `action_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `action_detail` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `action_man` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `action_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_order_record` */

/*Table structure for table `cmdb_orders` */

DROP TABLE IF EXISTS `cmdb_orders`;

CREATE TABLE `cmdb_orders` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `error_msg` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `info_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order_evaluation` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `receive_manager_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sala_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `send_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_orders` */

/*Table structure for table `cmdb_params` */

DROP TABLE IF EXISTS `cmdb_params`;

CREATE TABLE `cmdb_params` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `param_desc` varchar(255) DEFAULT NULL,
  `param_name` varchar(255) DEFAULT NULL,
  `param_type` varchar(255) DEFAULT NULL,
  `param_type_name` varchar(255) DEFAULT NULL,
  `param_value` varchar(255) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `use_param` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_params` */

insert  into `cmdb_params`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`end_time`,`param_desc`,`param_name`,`param_type`,`param_type_name`,`param_value`,`start_time`,`use_param`,`master_code`) values ('552591122417844224','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'运营时间段','running_time',NULL,NULL,'06:12-23:59',NULL,0,'001'),('552591122422038528','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'订单超时时间','order_outtime',NULL,NULL,'900',NULL,0,'001'),('552591122426232832','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'人员订单超时时间','order_sala_outtime',NULL,NULL,'60',NULL,0,'001'),('552591122430427136','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'订单最小费用','order_min_pay',NULL,NULL,'2',NULL,0,'001'),('552591122430427137','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'超市时间段','running_shop_time',NULL,NULL,'06:12-23:59',NULL,0,'001'),('552591122434621440','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'超市下单最小费用','running_shop_min_pay',NULL,NULL,'10',NULL,0,'001'),('552591122438815744','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'用户申请类型(simple/complex)','user_apply_type',NULL,NULL,'simple',NULL,0,'001'),('552591122438815745','2019-03-05 20:39:50','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 20:39:50',NULL,'用户申请说明','user_apply_desc',NULL,NULL,'为保障云主机服务和客户体验,请镖师遵守如下规则:\n1. 镖师接单必须积极接单,订单任务及时处理并签收;\n2. 如因其它原因不能及时跑单,及时与客户沟通或给管理员反馈;\n3. 订单完成,镖师需在公众号及时【签收】',NULL,0,'001'),('552605227962859520','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'运营时间段','running_time',NULL,NULL,'06:12-23:59',NULL,0,'002'),('552605227967053824','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'订单超时时间','order_outtime',NULL,NULL,'900',NULL,0,'002'),('552605227971248128','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'人员订单超时时间','order_sala_outtime',NULL,NULL,'60',NULL,0,'002'),('552605227971248129','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'订单最小费用','order_min_pay',NULL,NULL,'2',NULL,0,'002'),('552605227975442432','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'超市时间段','running_shop_time',NULL,NULL,'06:12-23:59',NULL,0,'002'),('552605227979636736','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'超市下单最小费用','running_shop_min_pay',NULL,NULL,'10',NULL,0,'002'),('552605227983831040','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'用户申请类型(simple/complex)','user_apply_type',NULL,NULL,'simple',NULL,0,'002'),('552605227988025344','2019-03-05 21:35:53','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-05 21:35:53',NULL,'用户申请说明','user_apply_desc',NULL,NULL,'为保障云主机服务和客户体验,请镖师遵守如下规则:\n1. 镖师接单必须积极接单,订单任务及时处理并签收;\n2. 如因其它原因不能及时跑单,及时与客户沟通或给管理员反馈;\n3. 订单完成,镖师需在公众号及时【签收】',NULL,0,'002');

/*Table structure for table `cmdb_resources` */

DROP TABLE IF EXISTS `cmdb_resources`;

CREATE TABLE `cmdb_resources` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `icons` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resource_sort` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_resources` */

insert  into `cmdb_resources`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`icons`,`link`,`name`,`resource_sort`,`master_code`) values ('551482687840321536','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-tachometer-alt','/manager/main','仪盘表',20,NULL),('551482687907430400','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-chart-area','/manager/order_list','申请管理',10,NULL),('551482687915819008','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-tags','/manager/machine_list','虚机管理',8,NULL),('551482687920013312','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-user','/manager/member_list','会员管理',1,NULL),('551482687924207616','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-edit','/manager/server_list','主机管理',7,NULL),('551482687928401920','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-bell-slash','/manager/params_list','参数管理',3,NULL),('551482687936790528','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-table','/manager/account_list','账户管理',1,NULL),('551482687915819007','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-cubes','/manager/deploy_list','部署管理',7,NULL),('551482687915819006','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-link','/manager/headth_list','任务管理',6,NULL),('551482687915819009','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-database','/manager/database_list','数据管理',6,NULL),('55148268791581113','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-leaf','/manager/asserts_list','资产管理',9,NULL),('55148268791581114','2019-03-02 19:15:19','0',NULL,NULL,NULL,0,0,NULL,'0','2019-03-02 19:15:19','fa-camera','/manager/network_list','网络管理',9,NULL);

/*Table structure for table `cmdb_school` */

DROP TABLE IF EXISTS `cmdb_school`;

CREATE TABLE `cmdb_school` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `school_address` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_order` int(11) NOT NULL,
  `school_owner` varchar(255) DEFAULT NULL,
  `school_owner_code` varchar(255) DEFAULT NULL,
  `school_properties` varchar(255) DEFAULT NULL,
  `school_properties_code` varchar(255) DEFAULT NULL,
  `school_province` varchar(255) DEFAULT NULL,
  `school_province_code` varchar(255) DEFAULT NULL,
  `school_type` varchar(255) DEFAULT NULL,
  `school_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_school` */

insert  into `cmdb_school`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`school_address`,`school_name`,`school_order`,`school_owner`,`school_owner_code`,`school_properties`,`school_properties_code`,`school_province`,`school_province_code`,`school_type`,`school_type_name`) values ('551484383555485696',NULL,'0',NULL,NULL,NULL,0,0,'001','0','2019-03-02 19:22:18',NULL,'广州海珠客村机房',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cmdb_score` */

DROP TABLE IF EXISTS `cmdb_score`;

CREATE TABLE `cmdb_score` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `score` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `score_content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_score` */

/*Table structure for table `cmdb_server` */

DROP TABLE IF EXISTS `cmdb_server`;

CREATE TABLE `cmdb_server` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `server_code` varchar(255) DEFAULT NULL,
  `server_desc` varchar(255) DEFAULT NULL,
  `server_name` varchar(255) DEFAULT NULL,
  `server_order` int(11) NOT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_server` */

insert  into `cmdb_server`(`id`,`add_time`,`application_id`,`delete_manager`,`delete_time`,`field_prop`,`has_delete`,`has_status`,`school_code`,`tenant_id`,`update_time`,`server_code`,`server_desc`,`server_name`,`server_order`,`master_code`) values ('551484383589040128','2019-03-02 19:36:11','0',NULL,NULL,'生活',0,0,'001','0','2019-03-02 19:49:51','1','搭建持续集成平台','172.20.200.9',4,NULL),('551484383593234432','2019-03-02 19:35:33','0',NULL,NULL,'物理机',0,0,'001','0','2019-03-02 19:35:33','2','测试服务器','172.20.200.99',3,NULL),('551484383597428736','2019-03-02 19:35:22','0',NULL,NULL,'物理机',0,0,'001','0','2019-03-02 19:35:22','3','数据库服务器','172.20.200.120',2,NULL),('551484383601623040','2019-03-02 19:36:02','0',NULL,NULL,'物理机',0,0,'001','0','2019-03-02 19:36:02','4','测试服务器','172.20.200.87',1,NULL),('552587028735197184','2019-03-05 20:23:34','0',NULL,NULL,'newBuy.png',0,0,NULL,'0','2019-03-05 20:23:34','1','生活用品-水果-宵夜-早晚餐-药品等','帮我买',4,NULL),('552587028735197185','2019-03-05 20:23:34','0',NULL,NULL,'newGet.png',0,0,NULL,'0','2019-03-05 20:23:34','2','快递-文件-礼物-货物等','帮我取',3,NULL),('552587028739391488','2019-03-05 20:23:34','0',NULL,NULL,'newSend.png',0,0,NULL,'0','2019-03-05 20:23:34','3','打印-缴费-装系统-送东西-接待等','帮我做',2,NULL),('552587028743585792','2019-03-05 20:23:34','0',NULL,NULL,'newLine.png',0,0,NULL,'0','2019-03-05 20:23:34','4','海报设计-网站开发-活动策划-简历指导','帮设计',1,NULL),('552591122380095488','2019-05-10 14:22:48','0',NULL,NULL,' 张三',0,0,NULL,'0','2019-05-10 14:22:48','1','开发服务器主机','192.168.1.110',4,'001'),('552591122380095489','2019-05-10 14:23:12','0',NULL,NULL,'李四',0,0,NULL,'0','2019-05-10 14:23:12','2','快递-文件-礼物-货物等','192.168.1.119',3,'001'),('552591122384289792','2019-05-10 14:22:57','0',NULL,NULL,'newSend.png',0,0,NULL,'0','2019-05-10 14:22:57','3','打印-缴费-装系统','192.168.1.112',2,'001'),('552591122388484096','2019-05-10 14:23:05','0',NULL,NULL,'newLine.png',0,0,NULL,'0','2019-05-10 14:23:05','4','海报设计-网站开发','192.168.1.114',1,'001'),('552605227920916480','2019-03-05 21:37:50','0',NULL,NULL,'使用中',0,0,NULL,'0','2019-03-06 06:58:46','1','持续集成服务器','172.20.168.90',4,'002'),('552605227925110784','2019-03-05 21:35:53','0',NULL,NULL,'使用中',0,0,NULL,'0','2019-03-05 22:26:44','2','业务服务服务器','172.20.174.12',3,'002'),('552605227929305088','2019-03-05 21:35:53','0',NULL,NULL,'使用中',0,0,NULL,'0','2019-03-05 22:26:41','3','前端应用服务器','192.168.1.123',2,'002'),('552605227929305089','2019-03-05 21:35:53','0',NULL,NULL,'使用中',0,0,NULL,'0','2019-03-05 21:35:53','4','业务测试服务器','192.168.2.124',1,'002');

/*Table structure for table `cmdb_sms_fail` */

DROP TABLE IF EXISTS `cmdb_sms_fail`;

CREATE TABLE `cmdb_sms_fail` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `biz_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `busness_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `out_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sign_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `template_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `validate_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_sms_fail` */

/*Table structure for table `cmdb_sms_send` */

DROP TABLE IF EXISTS `cmdb_sms_send`;

CREATE TABLE `cmdb_sms_send` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `biz_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `busness_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `out_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sign_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `template_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `validate_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_sms_send` */

/*Table structure for table `cmdb_sms_template` */

DROP TABLE IF EXISTS `cmdb_sms_template`;

CREATE TABLE `cmdb_sms_template` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `template_content` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `template_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `template_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_sms_template` */

/*Table structure for table `cmdb_tag` */

DROP TABLE IF EXISTS `cmdb_tag`;

CREATE TABLE `cmdb_tag` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tag_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_tag` */

/*Table structure for table `cmdb_user` */

DROP TABLE IF EXISTS `cmdb_user`;

CREATE TABLE `cmdb_user` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `apply_pass_account` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `apply_time` datetime DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `head_img_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `id_card` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `integral` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `open_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `privileges` tinyblob DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `qq` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `qr_scene` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `qr_scene_str` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `real_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `real_name_tmpl` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `run_weight` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `school_message` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `sex_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `subscribe` bit(1) DEFAULT NULL,
  `subscribe_scene` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `subscribe_time` bigint(20) DEFAULT NULL,
  `tag_ids` tinyblob DEFAULT NULL,
  `union_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_user` */

/*Table structure for table `cmdb_wechat_message` */

DROP TABLE IF EXISTS `cmdb_wechat_message`;

CREATE TABLE `cmdb_wechat_message` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `card_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `consume_source` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `detail` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `device_status` int(11) DEFAULT NULL,
  `device_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `error_count` int(11) DEFAULT NULL,
  `event` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `event_key` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `expired_time` bigint(20) DEFAULT NULL,
  `fail_reason` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `fail_time` bigint(20) DEFAULT NULL,
  `fee` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `filter_count` int(11) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `friend_user_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `from_kf_account` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `from_user` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `hard_ware` tinyblob DEFAULT NULL,
  `is_chat_room` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_give_by_friend` int(11) DEFAULT NULL,
  `is_restore_member_card` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_return_back` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `kf_account` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `location_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `location_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `locationx` double DEFAULT NULL,
  `locationy` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `media_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL,
  `modify_balance` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `modify_bonus` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `msg` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `msg_id` bigint(20) DEFAULT NULL,
  `msg_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `old_user_card_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `op_type` int(11) DEFAULT NULL,
  `open_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `original_fee` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outer_id` int(11) DEFAULT NULL,
  `outer_str` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `pic_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `poi_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `precision_` double DEFAULT NULL,
  `recognition` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remark_amount` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `result` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `scale` double DEFAULT NULL,
  `sent_count` int(11) DEFAULT NULL,
  `staff_open_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `store_uniq_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `thumb_media_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ticket` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `to_kf_account` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `to_user` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `trans_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_card_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `verify_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_wechat_message` */

/*Table structure for table `cmdb_wechat_template` */

DROP TABLE IF EXISTS `cmdb_wechat_template`;

CREATE TABLE `cmdb_wechat_template` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `error_message` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `template_content` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `template_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `template_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `cmdb_wechat_template` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
