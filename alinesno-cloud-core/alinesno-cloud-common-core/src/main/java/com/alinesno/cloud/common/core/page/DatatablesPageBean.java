package com.alinesno.cloud.common.core.page;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;

import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 分页数据
 * 
 * @author LuoAnDong
 * @since 2018年8月16日 上午8:59:44
 */
public class DatatablesPageBean {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	// -------------------------- 兼容新版本bootstrap table_start --------------
	private int pageSize ; // 每页显示
	private int pageNum ; // 开始条数
	private int total ; // 总条数
	private int code = HttpStatus.OK.value() ; // 状态码
	private Object rows ; // 返回的数据
	
	@SuppressWarnings("unused")
	private boolean isBootstrapTable ; // 判断是否为bootstrap
	// -------------------------- 兼容新版本bootstrap table_end --------------
	
	private int start; // 开始条数
	private int length; // 每页显示
	private int recordsFiltered;
	private Object data;
	private int draw;
	private int recordsTotal;
	private Map<String, Object> condition = new ConcurrentHashMap<String, Object>();
	private RestWrapper restWrapper = new RestWrapper() ; 
	 
	public RestWrapper getRestWrapper() {
		return restWrapper;
	}

	public void setRestWrapper(RestWrapper restWrapper) {
		this.restWrapper = restWrapper;
	}

	public boolean isBootstrapTable() {
		if(this.pageSize != 0) {
			return true ; 
		}else {
			return false ; 
		}
	}

	public int getStart() {
		return start ;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public Map<String, Object> getCondition() {
		return condition;
	}

	public void setCondition(Map<String, Object> condition) {
		this.condition = condition;
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getRows() {
		return rows;
	}

	public void setRows(Object rows) {
		this.rows = rows;
	}

	/**
	 * 从request中获得参数Map，并返回可读的Map
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes"})
	public <T> Specification buildFilter(T t, HttpServletRequest request) {
		return restWrapper.toSpecification() ; 
	}

	/**
	 * 返回创建条件
	 * @return
	 */
	public RestWrapper buildWrapper() {
		getRestWrapper().builderCondition(this.getCondition()) ; 
		return this.restWrapper ;
	}
	
}
