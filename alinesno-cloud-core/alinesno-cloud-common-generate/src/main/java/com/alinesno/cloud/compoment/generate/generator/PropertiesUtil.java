package com.alinesno.cloud.compoment.generate.generator;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc properties 资源文件解析工具
 * @author LuoAnDong 
 */
public class PropertiesUtil {

	private static final Logger log = LoggerFactory.getLogger(PropertiesUtil.class) ; 
	
	private static String resource = "/application.properties";
	private static Properties props;

	public static Properties getProps() {
		if (props == null) {
			try {
				props = new Properties();
				InputStream fis = PropertiesUtil.class.getResourceAsStream(resource);
				props.load(fis);
			} catch (Exception e) {
				log.warn("无法加载配置[application.properties]");
			}
		}
		return props;
	}

	public static String getString(String key) {
		return getProps().getProperty(key);
	}
}