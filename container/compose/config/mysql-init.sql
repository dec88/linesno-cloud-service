/*
 Navicat Premium Data Transfer

 Source Server         : 115.28.233.17_root
 Source Server Type    : MySQL
 Source Server Version : 100313
 Source Host           : 115.28.233.17
 Source Database       : alinesno_cloud_portal

 Target Server Type    : MySQL
 Target Server Version : 100313
 File Encoding         : utf-8

 Date: 11/02/2019 09:10:10 AM
*/

use alinesno_database ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `link_path`
-- ----------------------------
DROP TABLE IF EXISTS `link_path`;
CREATE TABLE `link_path` (
  `link_path` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `link_logo` varchar(255) DEFAULT NULL COMMENT '链接图标',
  `link_desc` varchar(255) DEFAULT NULL COMMENT '链接描述',
  `link_target` varchar(255) DEFAULT NULL COMMENT '链接打开状态',
  `link_design` varchar(255) DEFAULT NULL COMMENT '链接描述',
  `link_type` varchar(255) DEFAULT NULL COMMENT '链接id',
  `link_sort` int(11) DEFAULT NULL COMMENT '链接排序',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `link_path`
-- ----------------------------
BEGIN;
INSERT INTO `link_path` VALUES ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-html/', 'fab fa-html5', 'HTML，即超文本标记语言（Hyper Text Markup Language）', '_self', '0', 'html_css', '1', '580133910793420800', '2019-05-20 12:45:02', '0', null, null, null, '0', '0', '0', '2019-06-02 03:03:41', '学习 HTML', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fab fa-css3', '层叠样式表（Cascading StyleSheet）', '_self', '0', 'html_css', '2', '580134160992043008', '2019-05-20 12:46:02', '0', null, null, null, '0', '0', '0', '2019-06-02 03:04:16', '学习 CSS', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-javascript/_book/', 'fab fa-js', 'HTML5 是下一代 HTML 标准', '_self', '0', 'html_css', '3', '580134271180603392', '2019-05-20 12:46:28', '0', null, null, null, '0', '0', '0', '2019-06-02 03:04:36', '学习 JavaScript', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-bootstrap/_book/', 'fab fa-bootstrap', 'Bootstrap4 目前是 Bootstrap 最新版本', '_self', '0', 'html_css', '4', '580134381474021376', '2019-05-20 12:46:54', '0', null, null, null, '0', '0', '0', '2019-06-02 03:04:53', '学习 Bootstrap4', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-vuejs/_book/', 'fab fa-vuejs', 'vue.js是一套构建用户界面的渐进式框架', '_self', '0', 'html_css', '5', '580134524315238400', '2019-05-20 12:47:28', '0', null, null, null, '0', '0', '0', '2019-06-04 07:45:45', '学习 VueJS', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-git/_book/', 'fab fa-gitkraken', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'dev', '5', '580134653369778176', '2019-05-20 12:47:59', '0', null, null, null, '0', '0', '0', '2019-06-02 00:23:08', '学习 Git', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-java/_book/', 'fab fa-java', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'dev', '5', '580134707690209280', '2019-05-20 12:48:12', '0', null, null, null, '0', '0', '0', '2019-06-02 00:23:09', '学习 Java', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-thyeleamf/_book/', 'fas fa-file-alt', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'dev', '5', '580134778599112704', '2019-05-20 12:48:29', '0', null, null, null, '0', '0', '0', '2019-06-02 01:27:39', '学习 Thymeleaf', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-springmvc/_book/', 'fab fa-html5', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'dev', '5', '580134851991044096', '2019-05-20 12:48:47', '0', null, null, null, '0', '0', '0', '2019-06-02 03:09:00', '学习 SpringMvc', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-maven/_book/', 'fab fa-magento', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'dev', '5', '580134905959153664', '2019-05-20 12:48:59', '0', null, null, null, '0', '0', '0', '2019-06-02 00:23:12', '学习 Maven', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-sql/_book/', 'fas fa-database', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '1', 'database', '5', '580135038767595520', '2019-05-20 12:49:31', '0', null, null, null, '0', '0', '0', '2019-06-02 02:22:12', '学习 SQL', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-mysql/_book/', 'fab fa-html5', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'database', '5', '580135096909037568', '2019-05-20 12:49:45', '0', null, null, null, '0', '0', '0', '2019-06-02 03:05:44', '学习 MySQL', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-redis/_book/', 'fab fa-replyd', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'database', '5', '580135158133293056', '2019-05-20 12:50:00', '0', null, null, null, '0', '0', '0', '2019-06-02 01:38:32', '学习 Redis', null, null, null, null), ('http://cloud.linesno.com', 'fab fa-html5', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '1', 'database', '5', '580135221488254976', '2019-05-20 12:50:15', '0', null, null, null, '0', '0', '0', '2019-05-20 12:50:15', '学习 SQLiter', null, null, null, null), ('http://cloud.linesno.com', 'fab fa-html5', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '1', 'operation', '5', '580135413063090176', '2019-05-20 12:51:00', '0', null, null, null, '0', '0', '0', '2019-05-20 12:51:00', '学习 Ansible', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-jenkins/_book/', 'fab fa-jenkins', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'operation', '5', '580135457581432832', '2019-05-20 12:51:11', '0', null, null, null, '0', '0', '0', '2019-06-02 03:05:57', '学习 Jenkins', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-nginx/_book/', 'fab fa-searchengin', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'operation', '5', '580135500984090624', '2019-05-20 12:51:21', '0', null, null, null, '0', '0', '0', '2019-06-02 03:06:11', '学习 Nginx', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-markdown/_book/', 'fab fa-markdown', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'operation', '5', '580135563999313920', '2019-05-20 12:51:36', '0', null, null, null, '0', '0', '0', '2019-06-02 03:48:27', '学习 Markdown', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-centos/_book/', 'fab fa-redhat', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'server', '5', '580135648724254720', '2019-05-20 12:51:57', '0', null, null, null, '0', '0', '0', '2019-06-02 03:06:34', '学习 CentOS', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-shell/_book/', 'fab fa-html5', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'server', '5', '580135684350672896', '2019-05-20 12:52:05', '0', null, null, null, '0', '0', '0', '2019-06-02 03:06:47', '学习 Shell', null, null, null, null), ('http://cloud.linesno.com', 'fab fa-docker', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '1', 'server', '5', '580135734267084800', '2019-05-20 12:52:17', '0', null, null, null, '0', '0', '0', '2019-05-20 12:52:17', '学习 Docker', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-test-jmeter/_book/', 'fas fa-parking', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'test', '5', '580135939548905472', '2019-05-20 12:53:06', '0', null, null, null, '0', '0', '0', '2019-06-02 03:26:09', '学习 JMeter', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-test-junit/_book/', 'fas fa-tape', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '_self', '0', 'test', '5', '580135978757259264', '2019-05-20 12:53:15', '0', null, null, null, '0', '0', '0', '2019-06-07 09:36:40', '学习 JUnit', null, null, null, null), ('http://cloud.linesno.com', 'far fa-address-card', '关于我们', '_self', '0', 'agree', '1', '580280504067031040', '2019-05-20 22:27:33', '0', null, null, null, '0', '0', '0', '2019-05-20 22:28:43', '关于我们', null, null, null, null), ('http://cloud.linesno.com', 'far fa-address-card', '用户协议', '_self', '0', 'agree', '1', '580280561210228736', '2019-05-20 22:27:46', '0', null, null, null, '0', '0', '0', '2019-05-20 22:28:50', '用户协议', null, null, null, null), ('http://cloud.linesno.com', 'far fa-address-card', '法律声明和隐私政策', '_self', '0', 'agree', '1', '580280600221450240', '2019-05-20 22:27:56', '0', null, null, null, '0', '0', '0', '2019-05-20 22:28:58', '法律声明和隐私政策', null, null, null, null), ('http://cloud.linesno.com', 'far fa-address-card', '公告', '_self', '0', 'agree', '1', '580280671629475840', '2019-05-20 22:28:13', '0', null, null, null, '0', '0', '0', '2019-05-20 22:29:05', '公告', null, null, null, null), ('http://eureka.linesno.com', 'fas fa-parking', '公司开发注册中心地址', '_blank', '0', 'dev_register', '1', '582126513416044544', '2019-05-26 00:42:56', '0', null, null, null, '0', '0', '0', '2019-05-26 00:43:18', '公司开发注册中心', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '公网开发注册中心', '_blank', '1', 'dev_register', '1', '582126907944861696', '2019-05-26 00:44:30', '0', null, null, null, '0', '0', '0', '2019-05-26 00:44:30', '公网开发注册中心', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '公网开发注册中心', '_blank', '1', 'test_register', '1', '582126986772611072', '2019-05-26 00:44:49', '0', null, null, null, '0', '0', '0', '2019-05-26 00:44:49', '公司测试注册中心', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '公网开发注册中心', '_blank', '1', 'test_register', '1', '582127020712919040', '2019-05-26 00:44:57', '0', null, null, null, '0', '0', '0', '2019-05-26 00:44:57', '现场测试注册中心', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '现场生产注册中心', '_blank', '1', 'product_register', '1', '582127135062228992', '2019-05-26 00:45:24', '0', null, null, null, '0', '0', '0', '2019-05-26 00:45:24', '现场生产注册中心', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '迈安报表工具', '_blank', '0', 'dev_third', '1', '582130395454111744', '2019-05-26 00:58:21', '0', null, null, null, '0', '0', '0', '2019-05-26 01:02:59', '迈安报表工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '扫描控件', '_blank', '0', 'dev_third', '1', '582130485845557248', '2019-05-26 00:58:43', '0', null, null, null, '0', '0', '0', '2019-05-26 01:02:52', '扫描控件工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '工作流后台管理系统', '_blank', '0', 'dev_third', '1', '582130581601517568', '2019-05-26 00:59:06', '0', null, null, null, '0', '0', '0', '2019-05-26 01:02:45', '工作流后台管理系统工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '工作流后台配置系统工具', '_blank', '0', 'dev_third', '1', '582130901371060224', '2019-05-26 01:00:22', '0', null, null, null, '0', '0', '0', '2019-05-26 01:02:13', '工作流后台配置系统工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '迈安报表工具', '_blank', '0', 'test_third', '1', '582132141823885312', '2019-05-26 01:05:18', '0', null, null, null, '0', '0', '0', '2019-05-26 01:05:18', '迈安报表工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '扫描控件工具', '_blank', '0', 'test_third', '1', '582132241530880000', '2019-05-26 01:05:41', '0', null, null, null, '0', '0', '0', '2019-05-26 01:05:41', '扫描控件工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '工作流后台配置系统工具', '_blank', '0', 'test_third', '1', '582132295251525632', '2019-05-26 01:05:54', '0', null, null, null, '0', '0', '0', '2019-05-26 01:05:54', '工作流后台配置系统工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '工作流后台管理系统', '_blank', '0', 'test_third', '1', '582132338356387840', '2019-05-26 01:06:04', '0', null, null, null, '0', '0', '0', '2019-05-26 01:06:04', '工作流后台管理系统', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '迈安报表工具', '_blank', '1', 'product_third', '1', '582134723229253632', '2019-05-26 01:15:33', '0', null, null, null, '0', '0', '0', '2019-05-26 01:16:30', '迈安报表工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '扫描控件工具', '_blank', '1', 'product_third', '1', '582134793332850688', '2019-05-26 01:15:50', '0', null, null, null, '0', '0', '0', '2019-05-26 01:15:50', '扫描控件工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '工作流后台配置系统工具', '_blank', '1', 'product_third', '1', '582134851021307904', '2019-05-26 01:16:04', '0', null, null, null, '0', '0', '0', '2019-05-26 01:16:04', '工作流后台配置系统工具', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-docker', '工作流后台管理系统', '_blank', '1', 'product_third', '1', '582134895199911936', '2019-05-26 01:16:14', '0', null, null, null, '0', '0', '0', '2019-05-26 01:16:14', '工作流后台管理系统', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '持续集成平台', '_blank', '1', 'dev_jenkins', '1', '582164561541464064', '2019-05-26 03:14:07', '0', null, null, null, '0', '0', '0', '2019-05-26 03:16:35', '持续集成平台', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '持续集成平台', '_blank', '1', 'dev_jenkins', '1', '582164632047714304', '2019-05-26 03:14:24', '0', null, null, null, '0', '0', '0', '2019-05-26 03:14:24', '持续集成平台', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '南宁持续集成平台', '_blank', '1', 'dev_jenkins', '1', '582164709675892736', '2019-05-26 03:14:42', '0', null, null, null, '0', '0', '0', '2019-05-26 03:14:42', '南宁持续集成平台', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '南宁持续集成平台', '_blank', '1', 'test_jenkins', '1', '582164806908248064', '2019-05-26 03:15:06', '0', null, null, null, '0', '0', '0', '2019-05-26 03:15:06', '南宁持续集成平台', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '持续集成平台', '_blank', '1', 'product_jenkins', '1', '582164857906790400', '2019-05-26 03:15:18', '0', null, null, null, '0', '0', '0', '2019-05-26 03:15:18', '持续集成平台', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '持续集成平台', '_blank', '1', 'uat_jenkins', '1', '582164891377336320', '2019-05-26 03:15:26', '0', null, null, null, '0', '0', '0', '2019-05-26 03:15:26', '持续集成平台', null, null, null, null), ('http://jenkins.linesno.com', 'fab fa-docker', '公网持续集成平台', '_blank', '1', 'dev_jenkins', '1', '582165007387590656', '2019-05-26 03:15:53', '0', null, null, null, '0', '0', '0', '2019-05-26 03:15:53', '公网持续集成平台', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/echo-student-manager/_book/01_document/04_入门示例_学习计划.html', 'fab fa-github', '通过一个入门的学生管理系统，学习和完善学习过程，熟悉和了解开发环境和开发流程，分布式服务，容器的使用和自动化学习测试。', '_self', '1', 'training', '1', '582308190046126080', '2019-05-26 12:44:51', '0', null, null, null, '0', '0', '0', '2019-05-26 12:47:49', '服务化学习案例', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/echo-student-manager/_book/01_document/21_自动化测试_学习计划.html', 'fab fa-docker', '业务风控最佳业务实践。一站式打通数据引入、特征加工、策略模型、实时决策各环节，实现全场景风险管控。', '_self', '1', 'training', '1', '582308416580485120', '2019-05-26 12:45:45', '0', null, null, null, '0', '0', '0', '2019-05-26 13:07:30', '测试入门学习计划', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/linesno-cloud-guide/_book/', 'fas fa-compass', '针对电商、社交、金融等APP的营销活动场景，精准识别“羊毛党”，保障营销资金安全、提升活动效果。', '_self', '1', 'training', '1', '582308593043243008', '2019-05-26 12:46:27', '0', null, null, null, '0', '0', '0', '2019-05-26 12:46:27', '接入企业级统一研发平台指引', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-springboot/_book/', 'fas fa-chalkboard-teacher', '技术学习资料及学习过程收集，新技术研发资料及相关学习资料，减少学习过程中门槛，快速正确的获取知识。', '_self', '1', 'training', '1', '582308759494197248', '2019-05-26 12:47:07', '0', null, null, null, '0', '0', '0', '2019-06-02 03:30:25', '技术学习资料', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/echo-student-manager/_book/01_document/21_%E8%87%AA%E5%8A%A8%E5%8C%96%E6%B5%8B%E8%AF%95_%E5%AD%A6%E4%B9%A0%E8%AE%A1%E5%88%92.html', 'fas fa-user-graduate', '通过一个入门的学生管理系统，学习和完善学习过程，熟悉和了解开发环境和开发流程，分布式服务，容器的使用和自动化学习测试。', '_self', '1', 'training_case', '1', '582665682907299840', '2019-05-27 12:25:24', '0', null, null, null, '0', '0', '0', '2019-05-27 12:25:24', '学生管理系统', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/echo-student-manager/_book/01_document/21_%E8%87%AA%E5%8A%A8%E5%8C%96%E6%B5%8B%E8%AF%95_%E5%AD%A6%E4%B9%A0%E8%AE%A1%E5%88%92.html', 'fas fa-user-tie', '企业工作流和审批管理案例，通过这个案例熟悉企业多人管理，权限管理和工作流的运用等，多部门和多角色的使用。', '_self', '1', 'training_case', '1', '582665817498320896', '2019-05-27 12:25:56', '0', null, null, null, '0', '0', '0', '2019-05-27 12:25:56', '客户关系管理系统', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-project-graduate/_book/', 'fas fa-user-astronaut', '通过对高校论文选题的高并发场景，多人抢课功能，通过此案例学习高并发场景和对性能要求的场景，类似于电商的秒杀项目或者抢课现状。', '_self', '1', 'training_case', '1', '582665973249605632', '2019-05-27 12:26:33', '0', null, null, null, '0', '0', '0', '2019-05-28 02:14:54', '高校毕业生论文选题系统', null, null, null, null), ('http://boot.linesno.com', 'fas fa-chalkboard-teacher', '基础权限和配置，致力于打造成企业级微服务解决方案，主要实现应用接入配置，用户管理、部门管理、角色管理、数据字典、国际化等核心功能', '_blank', '1', 'portal_product', '1', '583566658245754880', '2019-05-30 00:05:33', '0', null, null, null, '0', '0', '0', '2019-06-01 23:55:02', '平台配置管理中心', null, null, null, null), ('http://cmdb.linesno.com', 'fab fa-redhat', '配置管理数据库，它存储与管理企业 IT 架构中设备的各种配置信息，它支撑服务流程的运转、发挥着配置信息的价值', '_blank', '1', 'portal_product', '1', '583566794799710208', '2019-05-30 00:06:06', '0', null, null, null, '0', '0', '0', '2019-06-01 10:32:10', '资产管理中心', null, null, null, null), ('http://cloud.linesno.com/public/gitbook/document?target=http://gitbook.linesno.com/linesno-cloud-env/_book/', 'fab fa-symfony', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'portal_product', '1', '583566858909646848', '2019-05-30 00:06:21', '0', null, null, null, '0', '0', '0', '2019-06-01 10:25:51', '软件管理中心', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-grade/_book/', 'fas fa-graduation-cap', '根据每个学习人员不同的特点，针对性地做出小组培训计划，安排在平台中有晋升可能的人员参加培养和成长， 充分发掘其潜力，帮助他们适应更高层次的需求', '_self', '1', 'portal_product', '1', '583566957865861120', '2019-05-30 00:06:44', '0', null, null, null, '0', '0', '0', '2019-06-02 13:22:11', '平台人才梯度培养方案', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fas fa-closed-captioning', '在于帮助我们编写更好、更有效的自动化验收测试', '_self', '0', 'test', '1', '585496080238510080', '2019-06-04 07:52:23', '0', null, null, null, '0', '0', '0', '2019-06-04 07:52:46', '学习Serenity BDD', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-test-postman/_book/', 'fas fa-tape', 'Postman+Jenkins+newman实现接口自动化测试', '_self', '0', 'test', '4', '586609921512964096', '2019-06-07 09:38:23', '0', null, null, null, '0', '0', '0', '2019-06-21 10:29:41', 'Postman接口自动化测试', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-elk/_book/', 'fas fa-clipboard-list', '技术学习资料及学习过程收集，新技术研发资料及相关学习资料，减少学习过程中门槛，快速正确的获取知识。', '_self', '0', 'elk', '1', '594075137242824704', '2019-06-28 00:02:30', '0', null, null, null, '0', '0', '0', '2019-06-28 00:08:16', '分布式日志接入的配置', null, null, null, null), ('http://eureka.linesno.com', 'fab fa-drupal', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_blank', '1', 'elk', '2', '594075665741905920', '2019-06-28 00:04:36', '0', null, null, null, '0', '0', '0', '2019-06-28 00:04:36', '分布式日志监控中心', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/document-front-design/', 'fas fa-drafting-compass', '服务器管理和数据库管理规范，提供单体应用系统、分布式微服务前后端的代码生成器，实现代码和业务的生成基础生成，并按规范提供模板。', '_self', '1', 'front_pages', '1', '602141423889809408', '2019-07-20 06:15:02', '0', null, null, null, '0', '0', '0', '2019-07-20 06:36:05', '前端视觉识别', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/document-front/', 'fab fa-css3-alt', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'front_pages', '2', '602143350094561280', '2019-07-20 06:22:41', '0', null, null, null, '0', '0', '0', '2019-07-20 06:22:41', '前端样式管理规范和标签说明', null, null, null, null), ('/public/gitbook/document?target=http://pages.linesno.com/', 'fas fa-laptop-code', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'front_pages', '3', '602143648208912384', '2019-07-20 06:23:53', '0', null, null, null, '0', '0', '0', '2019-07-20 06:23:53', '前端标签使用示例', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-quality/', 'fas fa-suitcase', '业务风控最佳业务实践。一站式打通数据引入、特征加工、策略模型、实时决策各环节，实现全场景风险管控。', '_self', '1', 'auto_test', '1', '602261972917944320', '2019-07-20 14:14:03', '0', null, null, null, '0', '0', '0', '2019-07-20 14:14:03', '软件测试和质量管理', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fas fa-tools', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'auto_test', '1', '602262185250390016', '2019-07-20 14:14:54', '0', null, null, null, '0', '0', '0', '2019-07-20 14:14:54', '测试工具介绍及使用教程', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fas fa-tape', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_blank', '1', 'auto_test', '1', '602262386342100992', '2019-07-20 14:15:42', '0', null, null, null, '0', '0', '0', '2019-07-20 14:15:42', '接口测试', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fas fa-robot', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'auto_test', '1', '602262537471262720', '2019-07-20 14:16:18', '0', null, null, null, '0', '0', '0', '2019-07-20 14:16:18', '持续自动化测试', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fas fa-rocket', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'auto_test', '4', '602262783744016384', '2019-07-20 14:17:17', '0', null, null, null, '0', '0', '0', '2019-07-20 14:17:17', '性能测试 PTS', null, null, null, null), ('/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', 'fab fa-redhat', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '_self', '1', 'auto_test', '1', '602262910823038976', '2019-07-20 14:17:47', '0', null, null, null, '0', '0', '0', '2019-07-20 14:17:47', '渗透测试', null, null, null, null), ('http://eureka.linesno.com', 'fas fa-hdd', '应当制定强制性国家标准。强制性国家标准由国务院有关行政主管部门依据职责提出、组织起草、征求意见和技术审查，由国务院标准化行政主管部门负责立项、编号和对外通报。强制性国家标准由国务院批准发布或授权发布。', '_self', '1', 'minio', '1', '616364495496806400', '2019-08-28 12:12:27', '0', null, null, null, '0', '0', '0', '2019-08-28 12:19:28', '云存储接入规范', null, null, null, null), ('http://minio.linesno.com', 'fas fa-cloud-upload-alt', 'Minio 是一个基于Go语言的对象存储服务。它实现了大部分亚马逊S3云存储服务接口，可以看做是是S3的开源版本，非常适合于存储大容量非结构化的数据，例如图片、视频、日志文件、备份数据和容器/虚拟机镜像等', '_blank', '1', 'minio', '1', '616364830877548544', '2019-08-28 12:13:47', '0', null, null, null, '0', '0', '0', '2019-08-28 12:22:50', '基于MinIO云存储管理', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `link_path_type`
-- ----------------------------
DROP TABLE IF EXISTS `link_path_type`;
CREATE TABLE `link_path_type` (
  `link_type_name` varchar(255) DEFAULT NULL COMMENT '链接名称',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `link_type_code` varchar(255) DEFAULT NULL,
  `link_group` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `link_path_type`
-- ----------------------------
BEGIN;
INSERT INTO `link_path_type` VALUES ('前端基础', '580141802384785408', '2019-05-20 13:16:24', '0', null, null, null, '0', '0', '0', '2019-05-20 13:27:30', 'html_css', 'learn', null, null, null, null), ('开发', '580141824589430784', '2019-05-20 13:16:29', '0', null, null, null, '0', '0', '0', '2019-05-20 13:27:45', 'dev', 'learn', null, null, null, null), ('数据库', '580143689272131584', '2019-05-20 13:23:54', '0', null, null, null, '0', '0', '0', '2019-05-20 13:27:53', 'database', 'learn', null, null, null, null), ('服务器', '580143729168351232', '2019-05-20 13:24:03', '0', null, null, null, '0', '0', '0', '2019-05-20 13:27:59', 'server', 'learn', null, null, null, null), ('运维', '580143846621446144', '2019-05-20 13:24:31', '0', null, null, null, '0', '0', '0', '2019-05-20 13:28:04', 'operation', 'learn', null, null, null, null), ('软件测试', '580143875146907648', '2019-05-20 13:24:38', '0', null, null, null, '0', '0', '0', '2019-05-20 13:28:10', 'test', 'learn', null, null, null, null), ('底部链接', '580279835226537984', '2019-05-20 22:24:53', '0', null, null, null, '0', '0', '0', '2019-05-20 22:24:53', 'agree', 'footer', null, null, null, null), ('底部链接', '580280042047668224', '2019-05-20 22:25:43', '0', null, null, null, '0', '0', '0', '2019-05-20 22:25:43', 'support', 'footer', null, null, null, null), ('开发注册中心', '582126011496267776', '2019-05-26 00:40:56', '0', null, null, null, '0', '0', '0', '2019-05-26 00:40:56', 'dev_register', 'register', null, null, null, null), ('测试注册中心', '582126052017438720', '2019-05-26 00:41:06', '0', null, null, null, '0', '0', '0', '2019-05-26 00:41:06', 'test_register', 'register', null, null, null, null), ('生产注册中心', '582126096904880128', '2019-05-26 00:41:16', '0', null, null, null, '0', '0', '0', '2019-05-26 00:41:16', 'product_register', 'register', null, null, null, null), ('测试环境第三方技术', '582130007833313280', '2019-05-26 00:56:49', '0', null, null, null, '0', '0', '0', '2019-05-26 01:01:09', 'test_third', 'develop', null, null, null, null), ('开发环境第三方技术', '582131267370221568', '2019-05-26 01:01:49', '0', null, null, null, '0', '0', '0', '2019-05-26 01:01:49', 'dev_third', 'develop', null, null, null, null), ('生产环境第三方技术', '582134340058611712', '2019-05-26 01:14:02', '0', null, null, null, '0', '0', '0', '2019-05-26 01:14:02', 'product_third', 'develop', null, null, null, null), ('开发持续集成平台', '582163755337515008', '2019-05-26 03:10:55', '0', null, null, null, '0', '0', '0', '2019-05-26 03:10:55', 'dev_jenkins', 'cicd', null, null, null, null), ('测试持续集成平台', '582163769442959360', '2019-05-26 03:10:58', '0', null, null, null, '0', '0', '0', '2019-05-26 03:17:10', 'test_jenkins', 'cicd', null, null, null, null), ('生产持续集成平台', '582163814506561536', '2019-05-26 03:11:09', '0', null, null, null, '0', '0', '0', '2019-05-26 03:11:09', 'product_jenkins', 'cicd', null, null, null, null), ('现场测试持续集成平台', '582163872018857984', '2019-05-26 03:11:23', '0', null, null, null, '0', '0', '0', '2019-05-26 03:12:55', 'uat_jenkins', 'cicd', null, null, null, null), ('服务化人才培养梯度', '582307853906214912', '2019-05-26 12:43:31', '0', null, null, null, '0', '0', '0', '2019-05-26 12:59:37', 'training', 'culture_gradient', null, null, null, null), ('学习项目案例', '582663999871516672', '2019-05-27 12:18:42', '0', null, null, null, '0', '0', '0', '2019-05-27 12:18:42', 'training_case', 'culture_gradient', null, null, null, null), ('平台产品服务', '583566235938062336', '2019-05-30 00:03:52', '0', null, null, null, '0', '0', '0', '2019-05-30 00:17:24', 'portal_product', 'product', null, null, null, null), ('ELK分布式日志监控', '594074311434698752', '2019-06-27 23:59:13', '0', null, null, null, '0', '0', '0', '2019-06-28 00:01:32', 'elk', 'operation', null, null, null, null), ('前端产品视觉设计和规划', '602138940845064192', '2019-07-20 06:05:10', '0', null, null, null, '0', '0', '0', '2019-07-20 14:34:24', 'front_pages', 'front', null, null, null, null), ('软件项目质量管理', '602261483618828288', '2019-07-20 14:12:07', '0', null, null, null, '0', '0', '0', '2019-09-12 10:37:34', 'auto_test', 'test', null, null, null, null), ('分布式云存储中心', '616361944160403456', '2019-08-28 12:02:18', '0', null, null, null, '0', '0', '0', '2019-08-28 12:23:33', 'minio', 'storage', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `menus_name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `menus_icons` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `menus_link` varchar(255) DEFAULT NULL COMMENT '菜单链接',
  `open_target` varchar(255) DEFAULT NULL COMMENT '打开目标',
  `has_nav` varchar(255) DEFAULT NULL COMMENT '是否主导航',
  `menus_sort` int(11) DEFAULT NULL COMMENT '排序顺序',
  `type_id` varchar(255) DEFAULT NULL COMMENT '所属类型',
  `menus_design_status` varchar(255) DEFAULT NULL COMMENT '菜单类型规划状态',
  `parent_menus_id` varchar(255) DEFAULT NULL COMMENT '父类菜单Id',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `menus`
-- ----------------------------
BEGIN;
INSERT INTO `menus` VALUES ('架构', 'fas fa-pencil-ruler', '/public/architecture', '_self', '1', '1', '', '', '579599272278753280', '579599272278753280', '2019-05-18 20:20:35', '0', null, null, null, '0', '0', '0', '2019-10-15 00:11:38', null, null, null, null), ('教程', 'fas fa-audio-description', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-design/20_%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B/', '_self', '1', '3', '', '', '579599272278753280', '579599471512387584', '2019-05-18 20:21:22', '0', null, null, null, '0', '0', '0', '2019-11-01 13:36:43', null, null, null, null), ('博客', 'fab fa-wordpress', 'http://switch.linesno.com', '_blank', '1', '4', '', '', '579599272278753280', '579599622532497408', '2019-05-18 20:21:58', '0', null, null, null, '0', '0', '0', '2019-06-21 12:13:03', null, null, null, null), ('数据仓库', 'fab fa-asymmetrik', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-data/', '_self', '1', '2', '', '', '579599272278753280', '579600035587555328', '2019-05-18 20:23:37', '0', null, null, null, '0', '0', '0', '2019-09-07 14:08:54', null, null, null, null), ('源码', 'fab fa-gitkraken', 'https://gitee.com/landonniao', '_blank', '1', '7', '', '', '579599272278753280', '579600162633023488', '2019-05-18 20:24:07', '0', null, null, null, '0', '0', '0', '2019-06-01 10:30:13', null, null, null, null), ('关于', 'fab fa-airbnb', '/pages/portal/views/about/home', '_self', '1', '9', '', '', '579599272278753280', '586548506542473216', '2019-06-07 05:34:21', '0', null, null, null, '0', '0', '0', '2019-09-08 00:32:37', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `menus_type`
-- ----------------------------
DROP TABLE IF EXISTS `menus_type`;
CREATE TABLE `menus_type` (
  `type_menus_name` varchar(255) DEFAULT NULL COMMENT '类型名称',
  `type_menus_type` varchar(255) DEFAULT NULL COMMENT '菜单类型(1主导航/2子菜单/3链接/4模块)',
  `type_menus_sort` int(11) DEFAULT NULL COMMENT '菜单排序',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `module`
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `module_name` varchar(255) DEFAULT NULL COMMENT '模块名称',
  `module_path` varchar(255) DEFAULT NULL COMMENT '模块链接',
  `module_logo` varchar(255) DEFAULT NULL COMMENT '模块图标',
  `module_desc` varchar(255) DEFAULT NULL COMMENT '模块描述',
  `module_sort` varchar(255) DEFAULT NULL COMMENT '模块描述',
  `menus_id` varchar(255) DEFAULT NULL COMMENT '所属菜单',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `module_parent_id` varchar(255) DEFAULT NULL COMMENT '模块父类',
  `open_target` varchar(255) DEFAULT NULL,
  `open_design` varchar(255) DEFAULT NULL,
  `module_design` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='内容模块';

-- ----------------------------
--  Records of `module`
-- ----------------------------
BEGIN;
INSERT INTO `module` VALUES ('资产管理', '/pages/portal/views/cmdb/home', 'fas fa-cat', '服务器管理和监控预警管理', '', '579599272278753280', '580016924490989568', '2019-05-20 05:00:10', '0', null, null, null, '0', '0', '0', '2019-10-17 13:22:44', '580285944490360832', '_self', null, '1', null, null, null, null), ('容器镜像', 'https://cn.aliyun.com/product/acr', 'fas fa-crow', '暂时使用阿里云(企业使用Nexus3)', '1', '579599272278753280', '580285606282657792', '2019-05-20 22:47:49', '0', null, null, null, '0', '0', '0', '2019-10-17 13:19:36', '580285817872711680', '_blank', null, '1', null, null, null, null), ('镜像管理', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580285817872711680', '2019-05-20 22:48:40', '0', null, null, null, '0', '0', '0', '2019-10-17 13:18:42', '0', '_blank', null, '0', null, null, null, null), ('产品运维', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580285944490360832', '2019-05-20 22:49:10', '0', null, null, null, '0', '0', '0', '2019-05-20 22:49:10', '0', '_blank', null, '0', null, null, null, null), ('分布存储', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580285980821422080', '2019-05-20 22:49:19', '0', null, null, null, '0', '0', '0', '2019-05-20 22:49:19', '0', '_blank', null, '0', null, null, null, null), ('基础服务', '#', 'fas fa-leaf', '服务器网络管理', '3', '579599272278753280', '580286010957496320', '2019-05-20 22:49:26', '0', null, null, null, '0', '0', '0', '2019-06-02 01:25:13', '0', '', null, '0', null, null, null, null), ('开发支持', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580286046042849280', '2019-05-20 22:49:34', '0', null, null, null, '0', '0', '0', '2019-05-26 02:27:10', '0', '_blank', null, '1', null, null, null, null), ('前端组件', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580286082688483328', '2019-05-20 22:49:43', '0', null, null, null, '0', '0', '0', '2019-09-08 08:39:02', '0', '_blank', null, '1', null, null, null, null), ('梯度培养', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580286120273641472', '2019-05-20 22:49:52', '0', null, null, null, '0', '0', '0', '2019-05-26 02:21:11', '0', '_blank', null, '1', null, null, null, null), ('平台管理', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580286150803980288', '2019-05-20 22:19:19', '0', null, null, null, '0', '0', '0', '2019-05-20 22:49:59', '0', '_blank', null, '0', null, null, null, null), ('数据分析', '#', 'fas fa-leaf', '服务器网络管理', '1', '579599272278753280', '580286182202540032', '2019-05-20 22:50:07', '0', null, null, null, '0', '0', '0', '2019-05-20 22:50:07', '0', '_blank', null, '0', null, null, null, null), ('负载均衡', '/pages/portal/views/load/home', 'fas fa-bug', '负载均衡', '1', '579599272278753280', '580286460595273728', '2019-05-20 22:51:13', '0', null, null, null, '0', '0', '0', '2019-05-26 03:28:42', '580285817872711680', '_self', null, '1', null, null, null, null), ('运维工单', '/pages/portal/views/operation/home', 'fas fa-hippo', '运维工单处理平台', '1', '579599272278753280', '580286872752750592', '2019-05-20 22:52:51', '0', null, null, null, '0', '0', '0', '2019-10-10 06:14:33', '580285944490360832', '_self', null, '0', null, null, null, null), ('主机监控', '/pages/portal/views/hosts/home', 'fas fa-otter', '主机监控', '1', '579599272278753280', '580286997835284480', '2019-05-20 22:53:21', '0', null, null, null, '0', '0', '0', '2019-10-19 23:58:12', '580285944490360832', '_self', null, '0', null, null, null, null), ('APM监控', 'http://zipkin.linesno.com', 'fas fa-fire-extinguisher', 'APM监控', '1', '579599272278753280', '580287120610951168', '2019-05-20 22:53:50', '0', null, null, null, '0', '0', '0', '2019-10-19 23:46:54', '580285944490360832', '_blank', null, '1', null, null, null, null), ('应用监控', 'http://bootadmin.linesno.com/', 'fas fa-paw', '基于SpringBoot的应用监控', '1', '579599272278753280', '580287275024252928', '2019-05-20 22:54:27', '0', null, null, null, '0', '0', '0', '2019-10-20 05:44:31', '580285944490360832', '_blank', null, '1', null, null, null, null), ('分布式日志', '/pages/portal/views/elk/home', 'fas fa-shield-alt', '分布式日志', '1', '579599272278753280', '580287409938235392', '2019-05-20 22:54:59', '0', null, null, null, '0', '0', '0', '2019-10-19 23:57:54', '580285944490360832', '_self', null, '0', null, null, null, null), ('注册中心', 'http://dubbo.linesno.com', 'fas fa-paper-plane', '注册中心', '1', '579599272278753280', '580287532432883712', '2019-05-20 22:55:28', '0', null, null, null, '0', '0', '0', '2019-08-29 03:11:07', '580285944490360832', '_blank', null, '1', null, null, null, null), ('基于MinIO云存储管理', 'http://minio.linesno.com/minio/login', 'fas fa-audio-description', '可以看做是是S3的开源版本', '1', '579599272278753280', '580287701119401984', '2019-05-20 22:56:09', '0', null, null, null, '0', '0', '0', '2019-10-15 23:05:56', '580285980821422080', '_blank', null, '1', null, null, null, null), ('引导组件', '/pages/portal/views/boot/home', 'fas fa-ambulance', '引导组件', '1', '579599272278753280', '580287903662342144', '2019-05-20 22:56:57', '0', null, null, null, '0', '0', '0', '2019-05-26 03:26:30', '580286010957496320', '_self', null, '1', null, null, null, null), ('工作流组件', '/pages/portal/views/flowable/home', 'fas fa-skiing-nordic', '工作流组件', '1', '579599272278753280', '580288082725568512', '2019-05-20 22:57:40', '0', null, null, null, '0', '0', '0', '2019-05-26 03:26:33', '580286010957496320', '_self', null, '1', null, null, null, null), ('日志组件', '/pages/portal/views/logger/home', 'fas fa-archive', '日志组件', '1', '579599272278753280', '580288205417349120', '2019-05-20 22:58:09', '0', null, null, null, '0', '0', '0', '2019-05-26 03:26:35', '580286010957496320', '_self', null, '1', null, null, null, null), ('通知组件', '/public/gitbook/document?target=http://gitbook.linesno.com/document-base-notice/', 'fas fa-cart-plus', '通知组件', '1', '579599272278753280', '580288435495895040', '2019-05-20 22:59:04', '0', null, null, null, '0', '0', '0', '2019-06-19 14:36:30', '580286010957496320', '_self', null, '1', null, null, null, null), ('存储组件', '/pages/portal/views/files/home', 'fas fa-car-side', '存储组件', '1', '579599272278753280', '580288545592180736', '2019-05-20 22:59:30', '0', null, null, null, '0', '0', '0', '2019-05-26 03:26:38', '580286010957496320', '_self', null, '1', null, null, null, null), ('网关组件', '/pages/portal/views/esb/home', 'fas fa-skating', '接口组件', '1', '579599272278753280', '580288659564003328', '2019-05-20 22:59:57', '0', null, null, null, '0', '0', '0', '2019-10-15 23:07:19', '580286010957496320', '_self', null, '1', null, null, null, null), ('单点登陆', '/public/gitbook/document?target=http://gitbook.linesno.com/document-base-sso/', 'fas fa-horse', '单点登陆', '1', '579599272278753280', '580288861725261824', '2019-05-20 23:00:45', '0', null, null, null, '0', '0', '0', '2019-06-21 01:51:07', '580286010957496320', '_self', null, '1', null, null, null, null), ('开发规范', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-service-technique/03_项目规范/01_服务工程规范.html', 'fas fa-skiing-nordic', '开发规范', '1', '579599272278753280', '580289073281761280', '2019-05-20 23:01:36', '0', null, null, null, '0', '0', '0', '2019-05-26 03:26:48', '580286046042849280', '_self', null, '1', null, null, null, null), ('开发接入', '/public/gitbook/document?target=http://gitbook.linesno.com/linesno-cloud-guide/_book/', 'fas fa-skiing-nordic', '开发接入', '1', '579599272278753280', '580289359970828288', '2019-05-20 23:02:44', '0', null, null, null, '0', '0', '0', '2019-05-26 03:26:53', '580286046042849280', '_self', null, '1', null, null, null, null), ('开发技术', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-service-technique/', 'fas fa-oil-can', '开发技术', '1', '579599272278753280', '580289506129739776', '2019-05-20 23:03:19', '0', null, null, null, '0', '0', '0', '2019-09-07 14:22:39', '580286046042849280', '_self', null, '1', null, null, null, null), ('配置平台', 'http://boot.linesno.com/alinesno-cloud-base-boot-web/login', 'fas fa-desktop', '配置平台', '1', '579599272278753280', '580289658638827520', '2019-05-20 23:03:55', '0', null, null, null, '0', '0', '0', '2019-05-26 03:29:39', '580286046042849280', '_blank', null, '1', null, null, null, null), ('前端设计', '/pages/portal/views/front/home', 'fab fa-html5', '前端设计', '1', '579599272278753280', '580289884523069440', '2019-05-20 23:04:49', '0', null, null, null, '0', '0', '0', '2019-07-20 14:00:27', '580286082688483328', '_self', null, '1', null, null, null, null), ('公网CDN', '/pages/portal/views/front/cdn', 'fab fa-css3', '公网CDN', '1', '579599272278753280', '580290006938025984', '2019-05-20 23:05:18', '0', null, null, null, '0', '0', '0', '2019-06-08 06:30:13', '580286082688483328', '_self', null, '0', null, null, null, null), ('基础入门（零基础）', '/pages/portal/views/training/learn', 'fab fa-earlybirds', '基础入门（零基础）', '1', '579599272278753280', '580290397872324608', '2019-05-20 23:06:52', '0', null, null, null, '0', '0', '0', '2019-05-26 03:28:08', '580286120273641472', '_self', null, '1', null, null, null, null), ('平台人才梯度（技能）', '/pages/portal/views/training/home', 'fas fa-user-tag', '平台人才梯度（技能）', '1', '579599272278753280', '580290532807278592', '2019-05-20 23:07:24', '0', null, null, null, '0', '0', '0', '2019-05-26 03:28:09', '580286120273641472', '_self', null, '1', null, null, null, null), ('学习案例（实战）', '/pages/portal/views/training/case', 'fas fa-laptop-code', '学习案例（实战）', '1', '579599272278753280', '580290662620987392', '2019-05-20 23:07:55', '0', null, null, null, '0', '0', '0', '2019-05-26 03:28:10', '580286120273641472', '_self', null, '1', null, null, null, null), ('平台设计', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-design/', 'fas fa-desktop', '平台设计', '1', '579599272278753280', '580290902895886336', '2019-05-20 23:08:52', '0', null, null, null, '0', '0', '0', '2019-09-19 03:05:31', '580286150803980288', '_self', null, '1', null, null, null, null), ('项目管理', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-project/', 'fas fa-compass', '项目管理', '1', '579599272278753280', '580290998412771328', '2019-05-20 23:09:15', '0', null, null, null, '0', '0', '0', '2019-10-01 01:28:25', '580286150803980288', '_blank', null, '1', null, null, null, null), ('代码管理', '/pages/portal/views/source/home', 'fas fa-code', '代码管理', '1', '579599272278753280', '580291099369668608', '2019-05-20 23:09:39', '0', null, null, null, '0', '0', '0', '2019-05-26 03:06:55', '580286150803980288', '_self', null, '1', null, null, null, null), ('软件管理', '/public/gitbook/document?target=http://gitbook.linesno.com/document-platform-env/', 'fas fa-cat', '软件管理', '1', '579599272278753280', '580291208090222592', '2019-05-20 23:10:05', '0', null, null, null, '0', '0', '0', '2019-10-04 10:03:22', '580286150803980288', '_self', null, '1', null, null, null, null), ('私服管理', 'http://nexus.linesno.com/nexus/', 'fas fa-oil-can', '私服管理(待升级Nexus3)', '1', '579599272278753280', '580291330870083584', '2019-05-20 23:10:34', '0', null, null, null, '0', '0', '0', '2019-10-16 14:04:42', '580286046042849280', '_blank', null, '1', null, null, null, null), ('持续集成', 'http://jenkins.linesno.com/', 'fab fa-jenkins', '持续集成', '1', '579599272278753280', '580291452488122368', '2019-05-20 23:11:03', '0', null, null, null, '0', '0', '0', '2019-06-01 23:57:00', '580286150803980288', '_blank', null, '1', null, null, null, null), ('软件测试', '/pages/portal/views/test/home', 'fas fa-robot', '软件测试', '1', '579599272278753280', '580291548361523200', '2019-05-20 23:11:26', '0', null, null, null, '0', '0', '0', '2019-10-19 07:53:45', '586822553813647360', '_self', null, '1', null, null, null, null), ('数据挖掘', '/pages/portal/views/test/home', 'fas fa-robot', '数据挖掘', '1', '579599272278753280', '580291698546966528', '2019-05-20 23:12:02', '0', null, null, null, '0', '0', '0', '2019-05-20 23:12:02', '580286182202540032', '_self', null, '0', null, null, null, null), ('文字识别', '/pages/portal/views/test/home', 'far fa-file-word', '文字识别', '1', '579599272278753280', '580291894186082304', '2019-05-20 23:12:48', '0', null, null, null, '0', '0', '0', '2019-07-20 13:59:30', '580286217069789184', '_self', null, '0', null, null, null, null), ('人脸识别', 'fas fa-user-nurse', 'far fa-file-word', '人脸识别', '1', '579599272278753280', '580291955737493504', '2019-05-20 23:13:03', '0', null, null, null, '0', '0', '0', '2019-05-20 23:13:03', '580286217069789184', null, null, '0', null, null, null, null), ('图像识别', 'http://cloud.linesno.com', 'far fa-images', '图像识别', '1', '579599272278753280', '580292070967607296', '2019-05-20 23:13:31', '0', null, null, null, '0', '0', '0', '2019-05-20 23:13:31', '580286217069789184', '_blank', null, '0', null, null, null, null), ('文本审核', 'http://cloud.linesno.com', 'fas fa-user-circle', '文本审核', '1', '579599272278753280', '580292148721614848', '2019-05-20 23:13:49', '0', null, null, null, '0', '0', '0', '2019-05-20 23:13:49', '580286217069789184', '_blank', null, '0', null, null, null, null), ('语音识别', 'http://cloud.linesno.com', 'far fa-images', '语音识别', '1', '579599272278753280', '580292215163584512', '2019-05-20 23:14:05', '0', null, null, null, '0', '0', '0', '2019-05-20 23:14:05', '580286217069789184', '_blank', null, '0', null, null, null, null), ('知识理解', 'http://cloud.linesno.com', 'fas fa-user-circle', '知识理解', '1', '579599272278753280', '580292318754504704', '2019-05-20 23:14:30', '0', null, null, null, '0', '0', '0', '2019-05-20 23:14:30', '580286217069789184', '_blank', null, '0', null, null, null, null), ('打印组件', '/pages/portal/views/print/home', 'fas fa-car-side', '打印组件', '1', '579599272278753280', '585856583238418432', '2019-06-05 07:44:54', '0', null, null, null, '0', '0', '0', '2019-07-20 14:01:47', '580286010957496320', '_self', null, '1', null, null, null, null), ('自动化部署', '/pages/portal/views/cmdb/home', 'fab fa-dev', '分布式自动化部署项目结构和管理', '1', '579599272278753280', '586611260590653440', '2019-06-07 09:43:43', '0', null, null, null, '0', '0', '0', '2019-10-11 04:56:47', '580285944490360832', '_self', null, '1', null, null, null, null), ('质量管理', '/pages/portal/views/cmdb/home', 'fas fa-leaf', '企业效能平台搭建和建设', '1', '579599272278753280', '586822553813647360', '2019-06-07 23:43:19', '0', null, null, null, '0', '0', '0', '2019-10-17 13:44:10', '0', '_self', null, '0', null, null, null, null), ('接口测试', 'http://yapi.demo.qunar.com/', 'fab fa-weixin', '在线接口测试工具', '1', '579599272278753280', '586825530808991744', '2019-06-07 23:55:09', '0', null, null, null, '0', '0', '0', '2019-10-17 14:03:42', '586822553813647360', '_blank', null, '1', null, null, null, null), ('综合测试', '/pages/portal/views/cmdb/home', 'fab fa-opera', '压力和安全测试', '2', '579599272278753280', '586829649988812800', '2019-06-08 00:11:31', '0', null, null, null, '0', '0', '0', '2019-10-17 14:07:33', '586822553813647360', '_self', null, '1', null, null, null, null), ('接口文档', 'https://www.showdoc.cc/', 'fas fa-fingerprint', '接口文档管理工具', '2', '579599272278753280', '586830260171964416', '2019-06-08 00:13:56', '0', null, null, null, '0', '0', '0', '2019-10-30 22:25:37', '580286046042849280', '_blank', null, '1', null, null, null, null), ('自动化测试', '/pages/portal/views/cmdb/home', 'fas fa-passport', 'UI自动化测试工具', '1', '579599272278753280', '586835879314587648', '2019-06-08 00:36:16', '0', null, null, null, '0', '0', '0', '2019-10-17 14:08:40', '586822553813647360', '_self', null, '1', null, null, null, null), ('分布式配置中心', 'http://config.linesno.com', 'fab fa-confluence', '分布式配置中心', '1', '579599272278753280', '604635450656686080', '2019-07-27 03:25:25', '0', null, null, null, '0', '0', '0', '2019-07-27 05:09:58', '580286046042849280', '_blank', null, '1', null, null, null, null), ('开发数据库', 'http://db4free.net/', 'fas fa-database', '账号密码：alinesno_demo', '3', '579599272278753280', '629743354149601280', '2019-10-04 10:15:15', '0', null, null, null, '0', '0', '0', '2019-10-11 05:02:25', '580286046042849280', '_blank', null, '1', null, null, null, null), ('K8s容器', 'https://119.23.247.48:37332', 'fab fa-docker', 'kubernetes开发版本', '2', '', '634501430731866112', '2019-10-17 13:22:09', '0', null, null, null, '0', '0', '0', '2019-10-17 13:33:30', '580285817872711680', '_blank', null, '1', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `upload_images`
-- ----------------------------
DROP TABLE IF EXISTS `upload_images`;
CREATE TABLE `upload_images` (
  `images_path` varchar(255) DEFAULT NULL COMMENT '文件地址',
  `images_desc` varchar(255) DEFAULT NULL COMMENT '海报描述',
  `images_type` varchar(255) DEFAULT NULL COMMENT '图片类型(1海报/2图片/9其它)',
  `images_name` varchar(255) DEFAULT NULL COMMENT '图片名称',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `images_descript` text DEFAULT NULL COMMENT '链接内容描述',
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `user_function`
-- ----------------------------
DROP TABLE IF EXISTS `user_function`;
CREATE TABLE `user_function` (
  `function_name` varchar(255) DEFAULT NULL COMMENT '功能名称',
  `function_path` varchar(255) DEFAULT NULL COMMENT '功能路径',
  `function_desc` varchar(255) DEFAULT NULL COMMENT '功能描述',
  `uid` varchar(255) DEFAULT NULL COMMENT '所属用户',
  `function_sort` int(11) DEFAULT NULL COMMENT '排序',
  `click_count` int(11) DEFAULT NULL COMMENT '点击次数',
  `often_function` varchar(255) DEFAULT NULL COMMENT '是否为常用功能',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `function_logo` varchar(255) DEFAULT NULL,
  `lid` varchar(255) DEFAULT NULL,
  `open_target` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `user_function`
-- ----------------------------
BEGIN;
INSERT INTO `user_function` VALUES ('学习 SQL', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-sql/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '579313994527932416', '0', '0', null, '591942300230221824', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fas fa-database', '580135038767595520', '_self', null, null, null, null), ('学习 MySQL', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-mysql/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '579313994527932416', '0', '0', null, '591942300297330688', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fab fa-html5', '580135096909037568', '_self', null, null, null, null), ('学习 Redis', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-redis/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '579313994527932416', '0', '0', null, '591942300314107904', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fab fa-replyd', '580135158133293056', '_self', null, null, null, null), ('学习 Ansible', 'http://cloud.linesno.com', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '579313994527932416', '0', '0', null, '591942300318302208', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fab fa-html5', '580135413063090176', '_self', null, null, null, null), ('公司开发注册中心', 'http://eureka.linesno.com', '公司开发注册中心地址', '579313994527932416', '0', '1', null, '591942300322496512', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 08:38:49', 'fas fa-parking', '582126513416044544', '_blank', null, null, null, null), ('公网开发注册中心', 'http://eureka.linesno.com', '公网开发注册中心', '579313994527932416', '0', '0', null, '591942300335079424', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fab fa-docker', '582126907944861696', '_blank', null, null, null, null), ('工作流后台管理系统', 'http://eureka.linesno.com', '工作流后台管理系统', '579313994527932416', '0', '0', null, '591942300343468032', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fab fa-docker', '582134895199911936', '_blank', null, null, null, null), ('接入企业级统一研发平台指引', '/public/gitbook/document?target=http://gitbook.linesno.com/linesno-cloud-guide/_book/', '针对电商、社交、金融等APP的营销活动场景，精准识别“羊毛党”，保障营销资金安全、提升活动效果。', '579313994527932416', '0', '0', null, '591942300351856640', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fas fa-compass', '582308593043243008', '_self', null, null, null, null), ('技术学习资料', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-springboot/_book/', '技术学习资料及学习过程收集，新技术研发资料及相关学习资料，减少学习过程中门槛，快速正确的获取知识。', '579313994527932416', '0', '0', null, '591942300364439552', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fas fa-chalkboard-teacher', '582308759494197248', '_self', null, null, null, null), ('学生管理系统', '/public/gitbook/document?target=http://gitbook.linesno.com/echo-student-manager/_book/01_document/21_%E8%87%AA%E5%8A%A8%E5%8C%96%E6%B5%8B%E8%AF%95_%E5%AD%A6%E4%B9%A0%E8%AE%A1%E5%88%92.html', '通过一个入门的学生管理系统，学习和完善学习过程，熟悉和了解开发环境和开发流程，分布式服务，容器的使用和自动化学习测试。', '579313994527932416', '0', '0', null, '591942300368633856', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fas fa-user-graduate', '582665682907299840', '_self', null, null, null, null), ('高校毕业生论文选题系统', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-project-graduate/_book/', '通过对高校论文选题的高并发场景，多人抢课功能，通过此案例学习高并发场景和对性能要求的场景，类似于电商的秒杀项目或者抢课现状。', '579313994527932416', '0', '1', null, '591942300368633857', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-08-24 02:30:49', 'fas fa-user-astronaut', '582665973249605632', '_self', null, null, null, null), ('平台配置管理中心', 'http://boot.linesno.com/alinesno-cloud-base-boot-web', '基础权限和配置，致力于打造成企业级微服务解决方案，主要实现应用接入配置，用户管理、部门管理、角色管理、数据字典、国际化等核心功能', '579313994527932416', '0', '1', null, '591942300372828160', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-07-20 05:36:03', 'fas fa-chalkboard-teacher', '583566658245754880', '_blank', null, null, null, null), ('平台人才梯度培养方案', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-grade/_book/', '根据每个学习人员不同的特点，针对性地做出小组培训计划，安排在平台中有晋升可能的人员参加培养和成长， 充分发掘其潜力，帮助他们适应更高层次的需求', '579313994527932416', '0', '0', null, '591942300377022464', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fas fa-graduation-cap', '583566957865861120', '_self', null, null, null, null), ('学习Serenity BDD', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', '在于帮助我们编写更好、更有效的自动化验收测试', '579313994527932416', '0', '0', null, '591942300406382592', '2019-06-22 02:47:22', '0', null, null, null, '0', '0', '0', '2019-06-22 02:47:22', 'fas fa-closed-captioning', '585496080238510080', '_self', null, null, null, null), ('学习 Maven', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-developer-maven/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '564714047887376384', '0', '0', null, '597525800496922624', '2019-07-07 12:34:12', '0', null, null, null, '0', '0', '0', '2019-07-07 12:34:12', 'fab fa-magento', '580134905959153664', '_self', null, null, null, null), ('学习 SQL', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-sql/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '564714047887376384', '0', '0', null, '597525800509505536', '2019-07-07 12:34:12', '0', null, null, null, '0', '0', '0', '2019-07-07 12:34:12', 'fas fa-database', '580135038767595520', '_self', null, null, null, null), ('学习 MySQL', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-database-mysql/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '564714047887376384', '0', '0', null, '597525800513699840', '2019-07-07 12:34:12', '0', null, null, null, '0', '0', '0', '2019-07-07 12:34:12', 'fab fa-html5', '580135096909037568', '_self', null, null, null, null), ('学习 Jenkins', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-jenkins/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '564714047887376384', '0', '0', null, '597525800513699841', '2019-07-07 12:34:12', '0', null, null, null, '0', '0', '0', '2019-07-07 12:34:12', 'fab fa-jenkins', '580135457581432832', '_self', null, null, null, null), ('学习 Nginx', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-nginx/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '564714047887376384', '0', '0', null, '597525800517894144', '2019-07-07 12:34:12', '0', null, null, null, '0', '0', '0', '2019-07-07 12:34:12', 'fab fa-searchengin', '580135500984090624', '_self', null, null, null, null), ('学习 CentOS', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-operation-centos/_book/', 'Font Awesome 是一套绝佳的图标字体库和CSS框架', '564714047887376384', '0', '0', null, '597525800538865664', '2019-07-07 12:34:12', '0', null, null, null, '0', '0', '0', '2019-07-07 12:34:12', 'fab fa-redhat', '580135648724254720', '_self', null, null, null, null), ('学习 HTML', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-html/_book/', 'HTML，即超文本标记语言（Hyper Text Markup Language）', '573490862592360448', '0', '0', null, '617667222940155904', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fab fa-html5', '580133910793420800', '_self', null, null, null, null), ('学习 CSS', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', '层叠样式表（Cascading StyleSheet）', '573490862592360448', '0', '0', null, '617667222956933120', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fab fa-css3', '580134160992043008', '_self', null, null, null, null), ('学习 JavaScript', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-javascript/_book/', 'HTML5 是下一代 HTML 标准', '573490862592360448', '0', '0', null, '617667222973710336', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fab fa-js', '580134271180603392', '_self', null, null, null, null), ('学习 Bootstrap4', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-bootstrap/_book/', 'Bootstrap4 目前是 Bootstrap 最新版本', '573490862592360448', '0', '0', null, '617667222990487552', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fab fa-bootstrap', '580134381474021376', '_self', null, null, null, null), ('平台配置管理中心', 'http://boot.linesno.com/alinesno-cloud-base-boot-web', '基础权限和配置，致力于打造成企业级微服务解决方案，主要实现应用接入配置，用户管理、部门管理、角色管理、数据字典、国际化等核心功能', '573490862592360448', '0', '0', null, '617667223028236288', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fas fa-chalkboard-teacher', '583566658245754880', '_blank', null, null, null, null), ('资产管理中心', 'http://cmdb.linesno.com', '配置管理数据库，它存储与管理企业 IT 架构中设备的各种配置信息，它支撑服务流程的运转、发挥着配置信息的价值', '573490862592360448', '0', '0', null, '617667223045013504', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fab fa-redhat', '583566794799710208', '_blank', null, null, null, null), ('性能测试 PTS', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-training-font-css3/_book/', '针对APP、网站的用户账号注册场景，有效识别“恶意注册”、“小号注册”、“脚本批量注册”、“注册机”等恶意行为。', '573490862592360448', '0', '0', null, '617667223053402112', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fas fa-rocket', '602262783744016384', '_self', null, null, null, null), ('云存储接入规范', 'http://eureka.linesno.com', '应当制定强制性国家标准。强制性国家标准由国务院有关行政主管部门依据职责提出、组织起草、征求意见和技术审查，由国务院标准化行政主管部门负责立项、编号和对外通报。强制性国家标准由国务院批准发布或授权发布。', '573490862592360448', '0', '0', null, '617667223070179328', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fas fa-hdd', '616364495496806400', '_self', null, null, null, null), ('基于MinIO云存储管理', 'http://minio.linesno.com', 'Minio 是一个基于Go语言的对象存储服务。它实现了大部分亚马逊S3云存储服务接口，可以看做是是S3的开源版本，非常适合于存储大容量非结构化的数据，例如图片、视频、日志文件、备份数据和容器/虚拟机镜像等', '573490862592360448', '0', '0', null, '617667223082762240', '2019-09-01 02:29:01', '0', null, null, null, '0', '0', '0', '2019-09-01 02:29:01', 'fas fa-cloud-upload-alt', '616364830877548544', '_blank', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `user_module`
-- ----------------------------
DROP TABLE IF EXISTS `user_module`;
CREATE TABLE `user_module` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_manager` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `click_count` int(11) DEFAULT NULL,
  `mid` varchar(255) COLLATE utf8mb4_bin DEFAULT 'NULL',
  `module_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `module_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `often_module` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `module_logo` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `module_path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `open_target` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `department_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `field_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `operator_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_update_operator_id` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
--  Records of `user_module`
-- ----------------------------
BEGIN;
INSERT INTO `user_module` VALUES ('591909072983293952', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 02:21:51', '4', '580285980821422080', '服务器网络管理', '存储', null, '579313994527932416', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('591909073008459776', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-07-16 12:07:22', '4', '580286082688483328', '服务器网络管理', '前端组件', null, '579313994527932416', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('591909073008459777', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-07-16 12:07:38', '4', '580286120273641472', '服务器网络管理', '梯度培养', null, '579313994527932416', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('591909073012654080', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 01:52:09', '1', '580286150803980288', '服务器网络管理', '平台管理', null, '579313994527932416', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('591909073016848384', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 01:48:45', '1', '580287120610951168', 'APM监控', 'APM监控', null, '579313994527932416', 'fas fa-fire-extinguisher', '/pages/portal/views/apm/home', '_self', null, null, null, null), ('591909073021042688', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 00:35:20', '0', '580287275024252928', '应用监控', '应用监控', null, '579313994527932416', 'fas fa-paw', '/pages/portal/views/apm/home', '_self', null, null, null, null), ('591909073025236992', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 00:35:20', '0', '580287532432883712', '注册中心', '注册中心', null, '579313994527932416', 'fas fa-paper-plane', 'http://eureka.linesno.com/', '_blank', null, null, null, null), ('591909073029431296', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 00:35:20', '0', '580288205417349120', '日志组件', '日志组件', null, '579313994527932416', 'fas fa-archive', '/pages/portal/views/logger/home', '_self', null, null, null, null), ('591909073033625600', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-07-16 12:07:47', '1', '580288321477935104', '消息组件', '消息组件', null, '579313994527932416', 'fas fa-shield-alt', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-message/_book/', '_self', null, null, null, null), ('591909073037819904', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 01:49:07', '5', '580290397872324608', '基础入门（零基础）', '基础入门（零基础）', null, '579313994527932416', 'fab fa-earlybirds', '/pages/portal/views/training/learn', '_self', null, null, null, null), ('591909073037819905', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 01:48:48', '1', '580290532807278592', '平台人才梯度（技能）', '平台人才梯度（技能）', null, '579313994527932416', 'fas fa-user-tag', '/pages/portal/views/training/home', '_self', null, null, null, null), ('591909073042014208', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 00:35:20', '0', '586611260590653440', '分布式自动化部署项目结构和管理', '自动化部署', null, '579313994527932416', 'fab fa-dev', '/pages/portal/views/cmdb/home', '_self', null, null, null, null), ('591909073042014209', '2019-06-22 00:35:20', '0', null, null, null, '0', '0', '0', '2019-06-22 00:35:20', '0', '586822553813647360', '业务产品', '业务服务', null, '579313994527932416', 'fas fa-leaf', '/pages/portal/views/cmdb/home', '_self', null, null, null, null), ('597525741671809024', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580286046042849280', '服务器网络管理', '开发支持', null, '564714047887376384', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('597525741923467264', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580286082688483328', '服务器网络管理', '前端组件', null, '564714047887376384', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('597525741931855872', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580286182202540032', '服务器网络管理', '数据分析', null, '564714047887376384', 'fas fa-leaf', '#', '_blank', null, null, null, null), ('597525741936050176', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580286217069789184', '服务器网络管理', '人工智能', null, '564714047887376384', 'fas fa-leaf', '#', '', null, null, null, null), ('597525741952827392', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580286652505653248', '负载均衡', '公网访问', null, '564714047887376384', 'fas fa-dove', '/pages/portal/views/load/home', '_self', null, null, null, null), ('597525741965410304', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580286872752750592', '系统运维', '系统运维', null, '564714047887376384', 'fas fa-hippo', '/pages/portal/views/operation/home', '_self', null, null, null, null), ('597525741969604608', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580289073281761280', '开发规范', '开发规范', null, '564714047887376384', 'fas fa-skiing-nordic', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-service/_book/03_项目规范/01_服务工程规范.html', '_self', null, null, null, null), ('597525741994770432', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580289233462231040', '代码生成器', '代码生成器', null, '564714047887376384', 'fab fa-dev', '/public/gitbook/document?target=pages/portal/views/generator/start', '_self', null, null, null, null), ('597525741994770433', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580289359970828288', '开发接入', '开发接入', null, '564714047887376384', 'fas fa-skiing-nordic', '/public/gitbook/document?target=http://gitbook.linesno.com/linesno-cloud-guide/_book/', '_self', null, null, null, null), ('597525741998964736', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580289506129739776', '开发技术', '开发技术', null, '564714047887376384', 'fas fa-oil-can', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-service/_book', '_self', null, null, null, null), ('597525741998964737', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-24 00:55:48', '1', '580291955737493504', '人脸识别', '人脸识别', null, '564714047887376384', 'far fa-file-word', 'fas fa-user-nurse', null, null, null, null, null), ('597525742003159040', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '580292070967607296', '图像识别', '图像识别', null, '564714047887376384', 'far fa-images', 'http://cloud.linesno.com', '_blank', null, null, null, null), ('597525742057684992', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '586611260590653440', '分布式自动化部署项目结构和管理', '自动化部署', null, '564714047887376384', 'fab fa-dev', '/pages/portal/views/cmdb/home', '_self', null, null, null, null), ('597525742066073600', '2019-07-07 12:33:58', '0', null, null, null, '0', '0', '0', '2019-07-07 12:33:58', '0', '586822553813647360', '业务产品', '业务服务', null, '564714047887376384', 'fas fa-leaf', '/pages/portal/views/cmdb/home', '_self', null, null, null, null), ('617667119399567360', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 12:04:08', '1', '580289359970828288', '开发接入', '开发接入', null, '573490862592360448', 'fas fa-skiing-nordic', '/public/gitbook/document?target=http://gitbook.linesno.com/linesno-cloud-guide/_book/', '_self', null, null, null, null), ('617667119378595840', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-06 09:00:19', '1', '580289233462231040', '代码生成器', '代码生成器', null, '573490862592360448', 'fab fa-dev', '/public/gitbook/document?target=pages/portal/views/generator/start', '_self', null, null, null, null), ('617667119328264192', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580288435495895040', '通知组件', '通知组件', null, '573490862592360448', 'fas fa-cart-plus', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-notice/_book/', '_self', null, null, null, null), ('617667119319875584', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580288205417349120', '日志组件', '日志组件', null, '573490862592360448', 'fas fa-archive', '/pages/portal/views/logger/home', '_self', null, null, null, null), ('617667119303098368', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580285606282657792', '容器镜像管理和配置', '容器镜像', null, '573490862592360448', 'fas fa-crow', '/pages/portal/views/docker/home', '_self', null, null, null, null), ('617667119298904064', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580016924490989568', '', '资产管理', null, '573490862592360448', 'fas fa-cat', '/pages/portal/views/cmdb/home', '_self', null, null, null, null), ('617667119336652800', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580288545592180736', '存储组件', '存储组件', null, '573490862592360448', 'fas fa-car-side', '/pages/portal/views/files/home', '_self', null, null, null, null), ('617667119345041408', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580288659564003328', '接口组件', '接口组件', null, '573490862592360448', 'fas fa-skating', '/pages/portal/views/esb/home', '_self', null, null, null, null), ('617667119366012928', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580288861725261824', '单点登陆', '单点登陆', null, '573490862592360448', 'fas fa-horse', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-sso/_book/', '_self', null, null, null, null), ('617667119374401536', '2019-09-01 02:28:36', '0', null, null, null, '0', '0', '0', '2019-09-01 02:28:36', '0', '580289073281761280', '开发规范', '开发规范', null, '573490862592360448', 'fas fa-skiing-nordic', '/public/gitbook/document?target=http://gitbook.linesno.com/alinesno-document-service/_book/03_项目规范/01_服务工程规范.html', '_self', null, null, null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;


/*
 Navicat Premium Data Transfer

 Source Server         : 115.28.233.17_root
 Source Server Type    : MySQL
 Source Server Version : 100313
 Source Host           : 115.28.233.17
 Source Database       : alinesno_cloud_base_boot

 Target Server Type    : MySQL
 Target Server Version : 100313
 File Encoding         : utf-8

 Date: 11/02/2019 09:21:54 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `content_comments`
-- ----------------------------
DROP TABLE IF EXISTS `content_comments`;
CREATE TABLE `content_comments` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `comment_author` varchar(255) DEFAULT NULL,
  `comment_author_email` varchar(255) DEFAULT NULL,
  `comment_author_ip` varchar(255) DEFAULT NULL,
  `comment_author_url` varchar(255) DEFAULT NULL,
  `comment_content` varchar(255) DEFAULT NULL,
  `comment_date` datetime DEFAULT NULL,
  `comment_date_gmt` datetime DEFAULT NULL,
  `comment_post_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `content_links`
-- ----------------------------
DROP TABLE IF EXISTS `content_links`;
CREATE TABLE `content_links` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `link_description` varchar(255) DEFAULT NULL,
  `link_image` varchar(255) DEFAULT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `link_notes` varchar(255) DEFAULT NULL,
  `link_owner` bigint(20) DEFAULT NULL,
  `link_rating` int(11) DEFAULT NULL,
  `link_target` varchar(255) DEFAULT NULL,
  `link_updated` datetime DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_visible` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `content_notice`
-- ----------------------------
DROP TABLE IF EXISTS `content_notice`;
CREATE TABLE `content_notice` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `notice_author` varchar(255) DEFAULT NULL,
  `notice_content` varchar(255) DEFAULT NULL,
  `notice_date` datetime DEFAULT NULL,
  `notice_modifield` datetime DEFAULT NULL,
  `notice_name` varchar(255) DEFAULT NULL,
  `notice_password` varchar(255) DEFAULT NULL,
  `notice_status` int(11) DEFAULT NULL,
  `notice_title` varchar(255) DEFAULT NULL,
  `notice_type` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `content_notice`
-- ----------------------------
BEGIN;
INSERT INTO `content_notice` VALUES ('563768365793935360', '2019-04-04 08:54:13', '594506557832560640', null, null, null, '0', '1', '0', '2019-07-07 03:31:50', null, '3333333333333333333333', null, null, null, null, null, null, null, null, null, null, null), ('597389228111822848', '2019-07-07 03:31:30', '587017505361362944', null, null, null, '0', '1', '0', '2019-07-07 03:31:30', null, '123123123', null, null, null, null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `content_post_type`
-- ----------------------------
DROP TABLE IF EXISTS `content_post_type`;
CREATE TABLE `content_post_type` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `type_add_time` datetime DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `type_status` int(11) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `content_post_type`
-- ----------------------------
BEGIN;
INSERT INTO `content_post_type` VALUES ('563750776812339200', '2019-04-05 07:44:19', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 02:57:00', null, '通知公告', null, '0', null, null, null, null), ('563755805933830144', '2019-04-05 00:04:18', '564392969700900864', null, null, null, '0', '0', null, '2019-04-05 08:05:06', null, '校园风景', null, '0', null, null, null, null), ('564499705170493440', '2019-04-07 09:20:18', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 02:56:58', null, '大学生活动中心', null, '563755805933830144', null, null, null, null), ('564578610426413056', '2019-04-07 14:33:50', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 02:56:55', null, '招生就业', null, '0', null, null, null, null), ('564578978992488448', '2019-04-07 14:35:18', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 02:56:50', null, '成人高考招生', null, '564578610426413056', null, null, null, null), ('566616723554304000', '2019-04-13 05:32:34', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 02:57:04', null, '逸夫楼风采', null, '563755805933830144', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `content_postmeta`
-- ----------------------------
DROP TABLE IF EXISTS `content_postmeta`;
CREATE TABLE `content_postmeta` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `content_posts`
-- ----------------------------
DROP TABLE IF EXISTS `content_posts`;
CREATE TABLE `content_posts` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL,
  `post_author` varchar(255) DEFAULT NULL,
  `post_content` varchar(255) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `post_mime_type` varchar(255) DEFAULT NULL,
  `post_modifield` datetime DEFAULT NULL,
  `post_name` varchar(255) DEFAULT NULL,
  `post_password` varchar(255) DEFAULT NULL,
  `post_status` int(11) DEFAULT NULL,
  `post_title` varchar(255) DEFAULT NULL,
  `post_type` varchar(255) DEFAULT NULL,
  `to_ping` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `content_posts`
-- ----------------------------
BEGIN;
INSERT INTO `content_posts` VALUES ('564505214170693632', '2019-04-07 09:42:11', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 03:09:57', null, '563707888443326464', '2019年4月7日上午，接凉山州森林草原防火指挥部办公室报告，雅砻江镇立尔村森林火灾火场受大风影响，东北面火烧迹地内悬崖处前期人工增雨降温降雪覆盖的隐蔽烟点复燃，燃烧腐烂木桩滚落至崖下引燃迹地内未燃尽的树木，形成树冠火，有飞火吹到火场外东面林地燃烧。', null, '564499705170493440', null, null, null, null, '大风致隐蔽烟点复燃 官兵支援', null, null, null, null, null, null), ('564505683559448576', '2019-04-07 09:44:03', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 03:10:00', null, '563707888443326464', '而该事件牵扯到的其他艺人也都得到了惨痛的代价。龙俊亨宣布退出组合highlight，接受警方调查；崔钟训也退出了FTISLAND，并退出娱乐圈，接受警方调查；李宗泫通过公司公开致歉，因为不够诚恳，而且没提出退团，还被韩国网友吐槽脸皮厚……', null, '564499705170493440', null, null, null, null, '前成员胜利豪宅曝光 从中可窥见他的野心', null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `info_address`
-- ----------------------------
DROP TABLE IF EXISTS `info_address`;
CREATE TABLE `info_address` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '区域ID',
  `parent_id` bigint(20) unsigned NOT NULL COMMENT '父ID',
  `district_name` varchar(200) NOT NULL COMMENT '区域名称',
  `short_name` varchar(200) NOT NULL COMMENT '简称',
  `longitude` decimal(10,7) NOT NULL DEFAULT 0.0000000 COMMENT '经度',
  `latitude` decimal(10,7) NOT NULL DEFAULT 0.0000000 COMMENT '维度',
  `level` int(11) NOT NULL DEFAULT 1 COMMENT '等级',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志: 0未删除，1已删除',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '修改时间',
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_info_address_level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=597075331697868801 DEFAULT CHARSET=utf8 COMMENT='区域表';

-- ----------------------------
--  Records of `info_address`
-- ----------------------------
 

-- ----------------------------
--  Table structure for `info_code`
-- ----------------------------
DROP TABLE IF EXISTS `info_code`;
CREATE TABLE `info_code` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code_name` varchar(255) DEFAULT NULL,
  `code_type` varchar(255) DEFAULT NULL,
  `code_value` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_job`
-- ----------------------------
DROP TABLE IF EXISTS `info_job`;
CREATE TABLE `info_job` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `job_parent` varchar(255) DEFAULT NULL,
  `job_type` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_professional`
-- ----------------------------
DROP TABLE IF EXISTS `info_professional`;
CREATE TABLE `info_professional` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `professional_prop` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_school`
-- ----------------------------
DROP TABLE IF EXISTS `info_school`;
CREATE TABLE `info_school` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `school_address` varchar(255) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_owner` varchar(255) DEFAULT NULL,
  `school_owner_code` varchar(255) DEFAULT NULL,
  `school_properties` varchar(255) DEFAULT NULL,
  `school_properties_code` varchar(255) DEFAULT NULL,
  `school_province` varchar(255) DEFAULT NULL,
  `school_province_code` varchar(255) DEFAULT NULL,
  `school_type` varchar(255) DEFAULT NULL,
  `school_type_name` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_settings`
-- ----------------------------
DROP TABLE IF EXISTS `info_settings`;
CREATE TABLE `info_settings` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_zipcode`
-- ----------------------------
DROP TABLE IF EXISTS `info_zipcode`;
CREATE TABLE `info_zipcode` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_account`
-- ----------------------------
DROP TABLE IF EXISTS `manager_account`;
CREATE TABLE `manager_account` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `last_login_time` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `role_power` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `has_editor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_account`
-- ----------------------------
BEGIN;
INSERT INTO `manager_account` VALUES ('564714047887376384', '2019-04-07 23:32:01', '564392969700900864', null, null, null, '0', '0', '0', '2019-09-19 02:24:04', '1', '127.0.0.1', '2018-12-12 12:12:12', 'admin@gmail.com', '超级管理员', null, 'landonniao', null, '9', '2312', null, '567055814016106496', null, null, null, '564714047887376384', 'luoandon@gmail.com', '15545334543', '1', null), ('573490862592360448', '2019-05-02 04:47:57', '', null, null, null, '0', '0', null, '2019-05-02 04:47:57', null, null, null, 'crm@gmail.com', '客户管理系统开发人员', null, 'admin', null, null, null, null, '573490177800929280', null, null, null, null, null, null, null, null), ('567263477580693504', '2019-04-15 00:22:32', '564392969700900864', null, null, null, '0', '0', null, '2019-04-15 00:22:32', null, null, null, 'manager@gmail.com', '测试管理员', null, 'admin', null, '0', null, null, '567055814016106496', null, null, null, null, null, null, null, null), ('579313994527932416', '2019-05-18 06:26:59', '579301304011063296', null, null, null, '0', '0', null, '2019-09-19 02:16:00', null, null, null, 'cloud@gmail.com', '【开发】罗小东', null, 'landonniao', null, null, null, null, '579313770187194368', null, null, null, '579313994527932416', 'luoandon@gmail.com', '15568956433', '1', null), ('591015904200884224', '2019-06-19 13:26:12', '567250403742187520', null, null, null, '0', '0', null, '2019-06-19 13:26:12', null, null, null, 'component@gmail.com', '基础组件管理平台', null, 'admin', null, null, null, null, '579313770187194368', null, null, null, null, null, null, null, null), ('594508188833808384', '2019-06-29 04:43:17', '594506557832560640', null, null, null, '0', '0', null, '2019-06-29 04:43:17', null, null, null, 'code@gmail.com', '智能代码管理员', null, 'admin', null, null, null, null, '594507798117613568', null, null, null, null, null, null, null, null), ('624277812634714112', '2019-09-19 08:17:09', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-19 08:17:09', null, null, null, 'register@gmail.com', '【开发】罗小东', null, 'landonniao', null, null, null, null, '579313770187194368', null, null, null, null, null, null, null, null), ('627434648128978944', '2019-09-28 01:21:17', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 01:21:17', null, null, null, 'demo@gmail.com', '互联网开发人员', null, 'admin', '621448386020638720', '1', null, null, null, null, null, null, null, null, null, null, null), ('627589877088649216', '2019-09-28 11:38:06', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 11:38:06', null, null, null, 'student', 'student', null, 'student', null, null, null, null, '627515979521327104', null, null, null, null, null, null, null, null), ('632257617925767168', '2019-10-11 08:46:02', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:02', null, null, null, 'ocj@163.com', 'tester', null, 'ocj@163.com', null, null, null, null, '', null, null, '624277812634714112', null, null, null, null, null), ('632710820274372608', '2019-10-12 14:46:54', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:46:54', null, null, null, '123@qq.com', 'those-years', null, '123@qq.com', null, null, null, null, '', null, null, '624277812634714112', null, null, null, null, null), ('633029239238557696', '2019-10-13 11:52:11', '579301304011063296', null, null, null, '0', '0', '0', '2019-10-13 11:52:11', null, null, null, '121321@gmail.com', '互联网开发人员', null, 'admin', '621448386020638720', '1', null, null, null, null, null, null, null, null, null, null, null), ('633248145500798976', '2019-10-14 02:22:03', '579301304011063296', null, null, null, '0', '0', '0', '2019-10-14 02:22:03', null, null, null, '782368028@qq.com', '互联网开发人员', null, 'xinku666', '621448386020638720', '1', null, null, null, null, null, null, null, null, null, null, null), ('633601612677382144', '2019-10-15 01:46:36', '579301304011063296', null, null, null, '0', '0', '0', '2019-10-15 01:46:36', null, null, null, 'ie421@163.com', '互联网开发人员', null, 'IEZ@1234567890', '621448386020638720', '1', null, null, null, null, null, null, null, null, null, null, null), ('635921106041044992', '2019-10-21 11:23:26', '579301304011063296', null, null, null, '0', '0', '0', '2019-10-21 11:23:26', null, null, null, 'li7hai26@qq.com', '互联网开发人员', null, 'li7hai26@qq.com', '621448386020638720', '1', null, null, null, null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_account_record`
-- ----------------------------
DROP TABLE IF EXISTS `manager_account_record`;
CREATE TABLE `manager_account_record` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `agent` varchar(1024) DEFAULT 'NULL',
  `create_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `method_desc` varchar(255) DEFAULT NULL,
  `method_time` bigint(20) NOT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `params` text DEFAULT NULL,
  `role_power` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `record_type` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_account_role`
-- ----------------------------
DROP TABLE IF EXISTS `manager_account_role`;
CREATE TABLE `manager_account_role` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_account_role`
-- ----------------------------
BEGIN;
INSERT INTO `manager_account_role` VALUES ('567691034356613120', '2019-04-16 04:41:30', '0', null, null, null, '0', '0', '0', '2019-04-16 04:41:30', '567263477580693504', '566985004698042368', null, null, null, null), ('567691034360807424', '2019-04-16 04:41:30', '0', null, null, null, '0', '0', '0', '2019-04-16 04:41:30', '567263477580693504', '567266058025566208', null, null, null, null), ('573490937397772288', '2019-05-02 04:48:14', '0', null, null, null, '0', '0', '0', '2019-05-02 04:48:14', '573490862592360448', '573488809103065088', null, null, null, null), ('567262839048241153', '2019-04-15 00:20:00', '0', null, null, null, '0', '0', '0', '2019-04-15 00:20:00', '567054406130860032', '567262525821812736', null, null, null, null), ('567262839048241152', '2019-04-15 00:20:00', '0', null, null, null, '0', '0', '0', '2019-04-15 00:20:00', '567054406130860032', '566985004698042368', null, null, null, null), ('567044148872347649', '2019-04-14 09:51:00', '0', null, null, null, '0', '0', '0', '2019-04-14 09:51:00', '565888499794837504', '566985004698042368', null, null, null, null), ('567262839044046848', '2019-04-15 00:20:00', '0', null, null, null, '0', '0', '0', '2019-04-15 00:20:00', '567054406130860032', '563847273901981696', null, null, null, null), ('567044148872347648', '2019-04-14 09:51:00', '0', null, null, null, '0', '0', '0', '2019-04-14 09:51:00', '565888499794837504', '563851604873183232', null, null, null, null), ('564739196401483776', '2019-04-08 01:11:57', '0', null, null, null, '0', '0', '0', '2019-04-08 01:11:57', '564714047887376384', '564708772195336192', null, null, null, null), ('579314091504435200', '2019-05-18 06:27:22', '0', null, null, null, '0', '0', '0', '2019-05-18 06:27:22', '579313994527932416', '579313426707251200', null, null, null, null), ('587022449455923200', '2019-06-08 12:57:38', '0', null, null, null, '0', '0', '0', '2019-06-08 12:57:38', '587022086329860096', '587020566561554432', null, null, null, null), ('591015966981226496', '2019-06-19 13:26:27', '0', null, null, null, '0', '0', '0', '2019-06-19 13:26:27', '591015904200884224', '567262525821812736', null, null, null, null), ('594508256517292032', '2019-06-29 04:43:33', '0', null, null, null, '0', '0', '0', '2019-06-29 04:43:33', '594508188833808384', '594507939100753920', null, null, null, null), ('621652455062503424', '2019-09-12 02:24:55', '0', null, null, null, '0', '0', '0', '2019-09-12 02:24:55', '621649200886579200', '621448386020638720', null, null, null, null), ('621653374537826304', '2019-09-12 02:28:34', '0', null, null, null, '0', '0', '0', '2019-09-12 02:28:34', '621653295458418688', '621448386020638720', null, null, null, null), ('621653622630907904', '2019-09-12 02:29:33', '0', null, null, null, '0', '0', '0', '2019-09-12 02:29:33', '621645715646447616', '621448386020638720', null, null, null, null), ('624278708349304832', '2019-09-19 08:20:42', '0', null, null, null, '0', '0', '0', '2019-09-19 08:20:42', '624277812634714112', '621448386020638720', null, null, null, null), ('627434649274023936', '2019-09-28 01:21:17', '0', null, null, null, '0', '0', '0', '2019-09-28 01:21:17', '627434648128978944', '621448386020638720', null, null, null, null), ('627589945183174656', '2019-09-28 11:38:22', '0', null, null, null, '0', '0', '0', '2019-09-28 11:38:22', '627589877088649216', '627589594837155840', null, null, null, null), ('632259215909453824', '2019-10-11 08:52:23', '0', null, null, null, '0', '0', '0', '2019-10-11 08:52:23', '632257617925767168', '632257722263273472', null, null, null, null), ('632711071525765120', '2019-10-12 14:47:54', '0', null, null, null, '0', '0', '0', '2019-10-12 14:47:54', '632710820274372608', '632710993285218304', null, null, null, null), ('633029239456661504', '2019-10-13 11:52:11', '0', null, null, null, '0', '0', '0', '2019-10-13 11:52:11', '633029239238557696', '621448386020638720', null, null, null, null), ('633248145672765440', '2019-10-14 02:22:03', '0', null, null, null, '0', '0', '0', '2019-10-14 02:22:03', '633248145500798976', '621448386020638720', null, null, null, null), ('633601612811599872', '2019-10-15 01:46:36', '0', null, null, null, '0', '0', '0', '2019-10-15 01:46:36', '633601612677382144', '621448386020638720', null, null, null, null), ('635921106724716544', '2019-10-21 11:23:26', '0', null, null, null, '0', '0', '0', '2019-10-21 11:23:26', '635921106041044992', '621448386020638720', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_application`
-- ----------------------------
DROP TABLE IF EXISTS `manager_application`;
CREATE TABLE `manager_application` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `application_desc` varchar(255) DEFAULT NULL,
  `application_icons` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `application_link` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_application`
-- ----------------------------
BEGIN;
INSERT INTO `manager_application` VALUES ('564392969700900864', '2019-04-13 20:57:56', '564392969700900864', null, null, null, '0', '0', null, '2019-07-05 02:12:35', '基础的管理功能，实现基础平台配置，权限管理及费用监控配置等', 'fa fa-ravelry', '管理应用', null, null, null, null, null, null), ('566970396281143296', '2019-04-13 20:57:56', null, null, null, null, '0', '0', null, '2019-07-05 02:12:49', '负责公司内容管理，招聘、文章等', 'fa fa-grav', '内容管理', null, null, null, null, null, null), ('567250403742187520', '2019-04-14 15:30:35', null, null, null, null, '0', '0', null, '2019-07-05 01:56:54', '基础服务和组件管理', 'fa fa-etsy', '基础服务', null, null, null, null, null, null), ('573482921575317504', '2019-05-02 04:16:23', null, null, null, null, '0', '0', null, '2019-05-02 04:16:23', '客户关系管理系统，用于市场客户关系开发和市场营销', 'fa fa-bandcamp', '客户关系', null, null, null, null, null, null), ('579301304011063296', '2019-05-18 05:36:33', null, null, null, null, '0', '0', null, '2019-09-08 14:31:14', '企业级统一研发平台门户', 'fa fa-envelope-open', '研发平台', null, null, null, null, null, null), ('587017505361362944', '2019-06-07 04:37:59', null, null, null, null, '0', '0', null, '2019-06-08 12:47:40', '通过一个入门的学生管理系统，学习和完善学习过程', 'fa fa-grav', '学生管理', null, null, null, null, null, null), ('594506557832560640', '2019-06-29 04:36:48', null, null, null, null, '0', '0', null, '2019-06-29 04:36:48', '生成代码并上传git，同时发布到jenkins中并打成容器包', 'fa fa-quora', '智能代码', null, null, null, null, null, null), ('627515979521327104', '2019-09-28 06:44:28', '0', null, null, null, '0', '0', '0', '2019-09-28 06:46:09', '学生管理系统', 'fa fa-envelope-open', '学生管理系统', null, null, null, null, '627434648128978944', '627434648128978944'), ('632255308793643008', '2019-10-11 08:36:52', '0', null, null, null, '0', '0', '0', '2019-10-11 08:36:52', '学生管理系统', '', '学生管理系统', null, null, null, null, '624277812634714112', null), ('632709927269302272', '2019-10-12 14:43:21', '0', null, null, null, '0', '0', '0', '2019-10-12 14:43:21', '演示管理系统', '', '演示管理系统', null, null, null, null, '624277812634714112', null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_code`
-- ----------------------------
DROP TABLE IF EXISTS `manager_code`;
CREATE TABLE `manager_code` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code_name` varchar(255) DEFAULT NULL,
  `code_type_name` varchar(255) DEFAULT NULL,
  `code_type_value` varchar(255) DEFAULT NULL,
  `code_value` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_code`
-- ----------------------------
BEGIN;
INSERT INTO `manager_code` VALUES ('564584553318973440', '2019-04-07 14:57:27', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:34:35', '超级管理', null, 'role_power', '9', null, null, null, null), ('61748afba7584bb2a8181b62b2514a72', null, '564392969700900864', null, null, null, '0', '0', null, null, '超级管理员', '平台管理员类型', 'manager_type', '1', null, null, null, null), ('7e6c61229403461a978683eb56f7a3d9', null, '564392969700900864', null, null, null, '0', '0', null, null, '禁止', '公共状态', 'common_status', '0', null, null, null, null), ('d7970178cd354c349c5f40da319b194e', null, '564392969700900864', null, null, null, '0', '0', null, null, '正常', '公共状态', 'common_status', '1', null, null, null, null), ('564477465297158144', '2019-04-06 23:51:55', '564392969700900864', null, null, null, '0', '0', null, '2019-04-07 08:21:18', '平台', null, 'menus_type', '9', null, null, null, null), ('564477599837847552', '2019-04-06 15:52:27', '564392969700900864', null, null, null, '0', '0', null, '2019-04-07 08:20:48', '标题', null, 'menus_type', '1', null, null, null, null), ('564477678673985536', '2019-04-06 23:52:46', '564392969700900864', null, null, null, '0', '0', null, '2019-04-07 08:21:03', '功能', null, 'menus_type', '0', null, null, null, null), ('564485658798718976', '2019-04-07 08:24:29', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:34:26', '按钮', null, 'menus_type', '8', null, null, null, null), ('564584318853185536', '2019-04-07 14:56:31', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:34:30', '租户权限', null, 'role_power', '1', null, null, null, null), ('564584467096666112', '2019-04-07 14:57:06', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:34:32', '用户权限', null, 'role_power', '1', null, null, null, null), ('543392305650860032', '2019-02-08 11:27:01', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:36:49', '测试代码名称', null, 'manager_type', '999', null, null, null, null), ('543393259423006720', '2019-02-08 11:30:49', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:36:47', '测试代码名称', null, 'manager_type', '0909', null, null, null, null), ('567834185473982464', '2019-04-16 14:10:20', '564392969700900864', null, null, null, '0', '0', null, '2019-04-16 14:10:20', '正常', null, 'has_status', '1', null, null, null, null), ('543421471318343680', '2019-02-08 13:22:55', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:36:46', '测试代码名称', null, 'manager_type', '000000000', null, null, null, null), ('543421977990266880', '2019-02-08 05:24:56', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:50:42', '测试代码名称', null, 'banner_type', '55000', null, null, null, null), ('543424362808606720', '2019-02-08 13:34:24', '564392969700900864', null, null, null, '0', '0', null, '2019-07-06 03:37:00', '测试代码名称', null, 'manager_type', '5555555', null, null, null, null), ('567834250166927360', '2019-04-16 14:10:35', '564392969700900864', null, null, null, '0', '0', null, '2019-04-16 14:10:35', '禁止', null, 'has_status', '0', null, null, null, null), ('579605641534898176', '2019-05-19 01:45:53', '579301304011063296', null, null, null, '0', '0', null, '2019-07-06 03:36:41', '导航菜单', null, 'nav_type', '1', null, null, null, null), ('579605788578807808', '2019-05-19 01:46:28', '579301304011063296', null, null, null, '0', '0', null, '2019-07-06 03:43:02', '功能菜单', null, 'nav_type', '2', null, null, null, null), ('579608842912923648', '2019-05-19 01:58:36', '579301304011063296', null, null, null, '0', '0', null, '2019-07-06 03:42:59', '新窗口', null, 'open_target_type', '_blank', null, null, null, null), ('579609048219910144', '2019-05-19 01:59:25', '579301304011063296', null, null, null, '0', '0', null, '2019-07-06 04:20:22', '本窗口', null, 'open_target_type', '_self', null, null, null, null), ('580020717265879040', '2019-05-20 05:15:15', '579301304011063296', null, null, null, '0', '0', null, '2019-07-06 04:20:25', '海报', null, 'banner_type', '1', null, null, null, null), ('580020816452780032', '2019-05-20 05:15:38', '579301304011063296', null, null, null, '0', '0', null, '2019-07-06 04:20:26', '图标', null, 'banner_type', '4', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_code_type`
-- ----------------------------
DROP TABLE IF EXISTS `manager_code_type`;
CREATE TABLE `manager_code_type` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code_type_name` varchar(255) DEFAULT NULL,
  `code_type_value` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_code_type`
-- ----------------------------
BEGIN;
INSERT INTO `manager_code_type` VALUES ('543373042005311488', '2019-02-08 10:10:29', '564392969700900864', null, null, null, '0', '0', '0', '2019-02-08 10:10:29', '公共状态', 'common_status', null, null, null, null), ('543373042080808960', '2019-02-08 10:10:29', '564392969700900864', null, null, null, '0', '0', '0', '2019-02-08 10:10:29', '管理员类型', 'manager_type', null, null, null, null), ('564476753548935168', '2019-04-07 07:49:06', '564392969700900864', null, null, null, '0', '1', null, '2019-04-07 07:49:06', '菜单类型', 'menus_type', null, null, null, null), ('564584142335901696', '2019-04-07 14:55:49', '564392969700900864', null, null, null, '0', '1', null, '2019-04-07 14:55:49', '角色类型', 'role_power', null, null, null, null), ('567834541629112320', '2019-04-16 14:11:45', '564392969700900864', null, null, null, '0', '0', null, '2019-04-16 14:11:45', '公共状态', 'has_status', null, null, null, null), ('579604736106299392', '2019-05-19 01:42:17', '579301304011063296', null, null, null, '0', '1', null, '2019-05-19 01:42:17', '导航类型', 'nav_type', null, null, null, null), ('579608471935123456', '2019-05-19 01:57:08', '579301304011063296', null, null, null, '0', '1', null, '2019-05-19 01:57:08', '链接打开类型', 'open_target_type', null, null, null, null), ('580020531256885248', '2019-05-20 05:14:30', '579301304011063296', null, null, null, '0', '1', null, '2019-05-20 05:14:30', '海报图片类型', 'banner_type', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_department`
-- ----------------------------
DROP TABLE IF EXISTS `manager_department`;
CREATE TABLE `manager_department` (
  `pid` bigint(20) DEFAULT NULL COMMENT '父部门id',
  `pids` varchar(512) DEFAULT NULL COMMENT '父级ids',
  `simple_name` varchar(45) DEFAULT NULL COMMENT '简称',
  `full_name` varchar(255) DEFAULT NULL COMMENT '全称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `versions` int(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_department`
-- ----------------------------
BEGIN;
INSERT INTO `manager_department` VALUES ('564392969700900864', null, null, '办公室', '', null, null, '567055361656225792', '2019-04-14 10:35:34', '564392969700900864', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('564392969700900864', null, null, '采编部', '', null, null, '567055454169989120', '2019-04-14 10:35:56', '564392969700900864', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('564392969700900864', null, null, '读者服务部', '', null, null, '567055814016106496', '2019-04-14 10:37:21', '564392969700900864', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('564392969700900864', null, null, '技术部', '', null, null, '567055928084398080', '2019-04-14 10:37:49', '564392969700900864', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('567055928084398080', null, null, '综合科', '', null, null, '567257554699157504', '2019-04-14 23:59:00', '564392969700900864', null, null, null, '0', '0', null, '2019-04-14 23:59:00', null, null, null, null), ('567055928084398080', null, null, '成果科', '', null, null, '567257645484867584', '2019-04-14 23:59:22', '564392969700900864', null, null, null, '0', '0', null, '2019-04-14 23:59:22', null, null, null, null), ('567055361656225792', null, null, '项目科', '', null, null, '567257737780527104', '2019-04-14 23:59:44', '564392969700900864', null, null, null, '0', '0', null, '2019-04-14 23:59:44', null, null, null, null), ('567055361656225792', null, null, '社会科学', '', null, null, '567257838573846528', '2019-04-15 00:00:08', '564392969700900864', null, null, null, '0', '0', null, '2019-04-15 00:00:08', null, null, null, null), ('566970396281143296', null, null, '运营部', '', null, null, '568187284755578880', '2019-04-17 13:33:25', '566970396281143296', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('568187284755578880', null, null, '营销部', '', null, null, '568187449646252032', '2019-04-17 13:34:04', '566970396281143296', null, null, null, '0', '0', null, '2019-04-17 13:34:04', null, null, null, null), ('566970396281143296', null, null, '销售部', '', null, null, '568187685349359616', '2019-04-17 13:35:01', '566970396281143296', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('568187284755578880', null, null, '研发部', '', null, null, '568187869244424192', '2019-04-17 13:35:44', '566970396281143296', null, null, null, '0', '0', null, '2019-04-17 13:35:44', null, null, null, null), ('573482921575317504', null, null, '市场营销部', '市场营销和管理', null, null, '573490177800929280', '2019-05-02 04:45:13', '573482921575317504', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('579301304011063296', null, null, '研发组', '', null, null, '579313770187194368', '2019-05-18 06:26:05', '579301304011063296', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null), ('594506557832560640', null, null, '开发组', '平台开发和管理', null, null, '594507798117613568', '2019-06-29 04:41:44', '594506557832560640', null, null, null, '0', '0', null, '2019-08-29 03:54:46', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_resource`
-- ----------------------------
DROP TABLE IF EXISTS `manager_resource`;
CREATE TABLE `manager_resource` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `menu_type` int(11) DEFAULT NULL,
  `resource_icon` varchar(255) DEFAULT NULL,
  `resource_link` varchar(255) DEFAULT NULL,
  `resource_name` varchar(255) DEFAULT NULL,
  `resource_order` int(11) DEFAULT NULL,
  `resource_parent` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  `open_target` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `permission_script` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_resource`
-- ----------------------------
BEGIN;
INSERT INTO `manager_resource` VALUES ('536478251871109120', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '9', 'fa fa-envelope-open', 'http://linesno.cloud.com', '基础平台', null, '0', null, null, null, null, null, null, null), ('536478252194070528', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '1', 'fa fa-envelope-open', 'http://linesno.cloud.com', '监控管理', null, '536478251871109120', null, null, null, null, null, null, null), ('536478252248596480', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '1', 'fa fa-envelope-open', 'http://linesno.cloud.com', '平台管理', null, '536478251871109120', null, null, null, null, null, null, null), ('536478252261179392', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '组织结构', null, '536478252248596480', null, null, null, null, null, null, null), ('536478252277956608', null, '564392969700900864', null, null, null, '0', '0', null, '2019-07-05 07:57:58', '1', 'fa fa-envelope-open', 'boot/platform/department/list', '部门管理', null, '536478252261179392', null, null, null, null, null, null, null), ('536478252290539520', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/role/list', '角色管理', null, '536478252261179392', null, null, null, null, null, null, null), ('536478252324093952', null, '564392969700900864', null, null, null, '0', '0', null, '2019-07-05 07:58:09', '9', 'fa fa-envelope-open', 'boot/platform/account/list', '账号管理', null, '536478252261179392', null, null, null, null, null, null, null), ('536478252336676864', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-05 08:07:42', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '平台配置', null, '536478252248596480', null, null, null, null, null, null, null), ('536478252349259776', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-09-12 01:53:41', '0', 'fa fa-envelope-open', 'boot/platform/application/list', '应用管理', null, '536478252261179392', null, null, null, null, null, null, null), ('536478252361842688', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/menus/list', '菜单管理', null, '536478252336676864', null, null, null, null, null, null, null), ('536478252387008512', null, '564392969700900864', null, null, null, '0', '1', null, '2019-08-29 03:56:07', '9', 'fa fa-envelope-open', 'boot/platform/search/list', '搜索管理', null, '536478252336676864', null, null, null, null, null, null, null), ('536478252399591424', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/dictionary/list', '字典管理', null, '536478252336676864', null, null, null, null, null, null, null), ('536478252420562944', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '系统配置', null, '536478252248596480', null, null, null, null, null, null, null), ('536478252437340160', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '结算管理', null, '536478252248596480', null, null, null, null, null, null, null), ('536478252449923072', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '计费管理', null, '536478252437340160', null, null, null, null, null, null, null), ('536478252458311680', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '充值管理', null, '536478252437340160', null, null, null, null, null, null, null), ('536478252470894592', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '订购管理', null, '536478252437340160', null, null, null, null, null, null, null), ('536478252483477504', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '定制管理', null, '536478252437340160', null, null, null, null, null, null, null), ('536478252496060416', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'http://linesno.cloud.com', '催缴管理', null, '536478252437340160', null, null, null, null, null, null, null), ('536478252517031936', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/content/list', '内容管理', null, '536478252248596480', null, null, null, null, null, null, null), ('536478252529614848', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/notice/list', '通知公告', null, '536478252517031936', null, null, null, null, null, null, null), ('536478252546392064', '2019-01-20 01:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-07 02:36:25', '0', 'fa fa-envelope-open', 'boot/platform/contentType/list', '分类管理', null, '536478252517031936', null, null, null, null, null, null, null), ('536478252558974976', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/content/list', '文章管理', null, '536478252517031936', null, null, null, null, null, null, null), ('536478252571557888', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '1', 'fa fa-envelope-open', '', '版本声明', null, '536478251871109120', null, null, null, null, null, null, null), ('536478252588335104', '2019-01-17 09:33:03', '564392969700900864', null, null, null, '0', '1', '0', '2019-04-14 00:29:17', '0', 'fa fa-envelope-open', 'boot/platform/account/list', '使用协议', null, '536478252571557888', null, null, null, null, null, null, null), ('536478252194070529', '2019-01-20 09:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', 'fa fa-envelope-open', 'boot/platform/monitor/display', '系统预警', null, '536478252194070528', null, null, null, null, null, null, null), ('536478252194070530', '2019-01-19 17:33:03', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-14 09:42:01', '0', 'fa fa-envelope-open', 'dashboard/common/monitor/server', '应用监控', null, '536478252194070528', null, null, null, null, null, null, null), ('563273421400571904', null, '564392969700900864', null, null, null, '0', '1', null, '2019-04-08 14:31:49', '1', 'fa fa-envelope-open', 'boot/platform/record/list', '操作记录', null, '536478252420562944', null, null, null, null, null, null, null), ('566601898900062208', '2019-04-12 12:33:40', '564392969700900864', null, null, null, '0', '1', '0', '2019-07-06 08:10:00', '1', 'fa fa-envelope-open', 'boot/platform/settings/list', '系统参数', null, '536478252420562944', null, null, null, null, null, null, null), ('566970396302114816', '2019-04-12 20:57:56', '566970396281143296', null, null, null, '0', '0', null, '2019-07-06 03:05:24', '9', 'fa fa-envelope-open', '', '内容管理', null, '0', null, null, null, null, null, null, null), ('566964252926017536', '2019-04-13 20:33:32', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:33:37', '1', 'fa fa-envelope-open', '', '系统管理', null, '536478251871109190', null, null, null, null, null, null, null), ('566897548824936448', '2019-04-11 00:08:28', '564392969700900864', null, null, null, '0', '1', null, '2019-09-10 01:07:29', '1', 'fa fa-envelope-open', 'boot/platform/source/list', '代码生成', null, '620385383795916800', null, null, null, null, null, null, null), ('566964214392946688', '2019-04-13 20:33:22', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:33:35', '1', 'fa fa-envelope-open', '', '运营管理', null, '536478251871109190', null, null, null, null, null, null, null), ('566963971203006464', '2019-04-12 20:32:24', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:53:16', '0', 'fa fa-envelope-open', '', '新增订单', null, '566963554767339520', null, null, null, null, null, null, null), ('566963554767339520', '2019-04-12 12:30:45', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:48:37', '0', 'fa fa-envelope-open', '', '订单管理', null, '566964214392946688', null, null, null, null, null, null, null), ('566963614473256960', '2019-04-13 04:30:59', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:48:29', '0', 'fa fa-envelope-open', '', '商品管理', null, '566964214392946688', null, null, null, null, null, null, null), ('566963681804419072', '2019-04-13 04:31:15', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:48:32', '0', 'fa fa-envelope-open', '', '会员管理', null, '566964214392946688', null, null, null, null, null, null, null), ('566963805293117440', '2019-04-12 04:31:45', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:51:27', '0', 'fa fa-envelope-open', '', '账户管理', null, '566964252926017536', null, null, null, null, null, null, null), ('566750963755384832', '2019-04-12 22:25:59', '564392969700900864', null, null, null, '0', '1', '0', '2019-09-12 01:54:17', '0', 'fa fa-envelope-open', 'boot/platform/dictionaryType/list', '字典类型', null, '536478252336676864', null, null, null, null, null, null, null), ('566963764310573056', '2019-04-13 04:31:35', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:48:18', '0', 'fa fa-envelope-open', '', '参数管理', null, '566964252926017536', null, null, null, null, null, null, null), ('536478251871109190', '2019-01-20 09:33:03', '564474243555786752', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '9', 'fa fa-envelope-open', 'http://linesno.cloud.com', '跑腿管理', null, '0', null, null, null, null, null, null, null), ('566963452782837760', '2019-04-12 04:30:21', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:56:17', '0', 'fa fa-envelope-open', '', '仪盘表', null, '566964252926017536', null, null, null, null, null, null, null), ('566969364662714368', '2019-04-13 12:53:50', '564474243555786752', null, null, null, '0', '0', null, '2019-04-14 04:54:11', '0', 'fa fa-envelope-open', '', '订单查询', null, '566963554767339520', null, null, null, null, null, null, null), ('566972060576776192', '2019-04-13 21:04:33', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:35:01', '1', 'fa fa-envelope-open', '', '我的面板', null, '566970396302114816', null, null, null, null, null, null, null), ('566972131494068224', '2019-04-13 13:04:50', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:35:02', '1', 'fa fa-envelope-open', '', '内容管理', null, '566970396302114816', null, null, null, null, null, null, null), ('566972216520998912', '2019-04-13 13:05:10', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:35:04', '1', 'fa fa-envelope-open', '', '设置管理', null, '566970396302114816', null, null, null, null, null, null, null), ('566979874347024384', '2019-04-13 21:35:36', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:36:19', '0', 'fa fa-envelope-open', '', '个人信息', null, '566972060576776192', null, null, null, null, null, null, null), ('566979948783337472', '2019-04-13 05:35:54', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:36:28', '0', 'fa fa-envelope-open', '', '修改密码', null, '566972060576776192', null, null, null, null, null, null, null), ('566980019872595968', '2019-04-13 21:36:11', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:36:24', '0', 'fa fa-envelope-open', '', '生成首页', null, '566972060576776192', null, null, null, null, null, null, null), ('566980497616404480', '2019-04-12 13:38:05', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:40:19', '0', 'fa fa-envelope-open', '', '邮件设置', null, '566972216520998912', null, null, null, null, null, null, null), ('566980563815104512', '2019-04-13 13:38:20', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:39:15', '0', 'fa fa-envelope-open', '', '管理附件', null, '566972131494068224', null, null, null, null, null, null, null), ('566980638335303680', '2019-04-13 13:38:38', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:39:18', '0', 'fa fa-envelope-open', '', '管理采集', null, '566972131494068224', null, null, null, null, null, null, null), ('566980714625499136', '2019-04-13 13:38:56', '566970396281143296', null, null, null, '0', '0', null, '2019-04-14 05:39:20', '0', 'fa fa-envelope-open', '', '评论管理', null, '566972131494068224', null, null, null, null, null, null, null), ('567250403758964736', '2019-04-14 23:30:35', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:30:35', '9', 'fa fa-envelope-open', null, '基础服务', null, '0', null, null, null, null, null, null, null), ('567250523137245184', '2019-04-12 23:31:04', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:37:08', '1', 'fa fa-envelope-open', '', '所有组件', null, '567250403758964736', null, null, null, null, null, null, null), ('567250886120701952', '2019-04-14 15:32:30', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:32:34', '1', 'fa fa-envelope-open', '', '监控管理', null, '567250403758964736', null, null, null, null, null, null, null), ('567250962478006272', '2019-04-14 15:32:48', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:32:57', '0', 'fa fa-envelope-open', '', '组件监控', null, '567250886120701952', null, null, null, null, null, null, null), ('567251391144263680', '2019-04-14 15:34:31', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:36:44', '0', 'fa fa-envelope-open', '', '消息组件', null, '567250523137245184', null, null, null, null, null, null, null), ('567251437654900736', '2019-04-14 15:34:42', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:36:46', '0', 'fa fa-envelope-open', '', '打印组件', null, '567250523137245184', null, null, null, null, null, null, null), ('567251483649638400', '2019-04-14 15:34:53', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:36:48', '0', 'fa fa-envelope-open', '', '通知组件', null, '567250523137245184', null, null, null, null, null, null, null), ('567251529501769728', '2019-04-14 15:35:04', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:36:50', '0', 'fa fa-envelope-open', '', '微信组件', null, '567250523137245184', null, null, null, null, null, null, null), ('567251725514178560', '2019-04-14 15:35:50', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:36:52', '0', 'fa fa-envelope-open', '', '存储组件', null, '567250523137245184', null, null, null, null, null, null, null), ('567252181112061952', '2019-04-13 15:37:39', '567250403742187520', null, null, null, '0', '0', null, '2019-06-20 14:25:16', '0', 'fa fa-envelope-open', 'base/message/transactionMessage/list', '消息管理', null, '567251391144263680', null, null, null, null, null, null, null), ('567252304839835648', '2019-04-13 15:38:09', '567250403742187520', null, null, null, '0', '0', null, '2019-06-20 14:25:06', '0', 'fa fa-envelope-open', 'base/message/transactionMessageHistory/list', '历史消息', null, '567251391144263680', null, null, null, null, null, null, null), ('567252359516782592', '2019-04-14 15:38:22', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:38:24', '0', 'fa fa-envelope-open', '', '消息设置', null, '567251391144263680', null, null, null, null, null, null, null), ('567252408439144448', '2019-04-13 15:38:33', '567250403742187520', null, null, null, '0', '0', null, '2019-06-20 14:24:36', '0', 'fa fa-envelope-open', 'base/message/transactionMessage/monitor', '监控管理', null, '567251391144263680', null, null, null, null, null, null, null), ('567252467201343488', '2019-04-14 07:38:47', '567250403742187520', null, null, null, '0', '0', null, '2019-06-07 12:03:37', '0', 'fa fa-envelope-open', 'base/print/template/all', '打印模板', null, '567251437654900736', null, null, null, null, null, null, null), ('567252514672476160', '2019-04-14 15:38:59', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:39:00', '0', 'fa fa-envelope-open', 'base/print/template/list', '打印设置', null, '567251437654900736', null, null, null, null, null, null, null), ('567252558305820672', '2019-04-14 15:39:09', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:39:11', '0', 'fa fa-envelope-open', '', '签名管理', null, '567251437654900736', null, null, null, null, null, null, null), ('567252667454193664', '2019-04-13 07:39:35', '567250403742187520', null, null, null, '0', '1', null, '2019-06-20 13:51:54', '0', 'fa fa-envelope-open-o', 'base/notice/smsHistory/monitor', '监控管理', null, '567251483649638400', null, null, null, null, null, null, null), ('567252763872854016', '2019-04-13 23:39:58', '567250403742187520', null, null, null, '0', '0', null, '2019-06-07 22:52:56', '0', 'fa fa-envelope-open', 'base/notice/smsHistory/list', '发送记录', null, '567251483649638400', null, null, null, null, null, null, null), ('567252817794826240', '2019-04-14 07:40:11', '567250403742187520', null, null, null, '0', '0', null, '2019-06-07 22:55:24', '0', 'fa fa-envelope-open', 'base/notice/smsTemplate/list', '通知模板', null, '567251483649638400', null, null, null, null, null, null, null), ('567252849352769536', '2019-04-14 15:40:18', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:40:44', '0', 'fa fa-envelope-open', '', '任务管理', null, '567251483649638400', null, null, null, null, null, null, null), ('567252894554783744', '2019-04-14 07:40:29', '567250403742187520', null, null, null, '0', '0', null, '2019-06-07 22:56:35', '0', 'fa fa-envelope-open', 'base/notice/smsSettings/list', '通知设置', null, '567251483649638400', null, null, null, null, null, null, null), ('567253179394162688', '2019-04-14 07:41:37', '567250403742187520', null, null, null, '0', '0', null, '2019-06-20 14:31:28', '0', 'fa fa-envelope-open', 'base/storage/storageFile/list', '存储管理', null, '567251725514178560', null, null, null, null, null, null, null), ('567253275787657216', '2019-04-14 07:42:00', '567250403742187520', null, null, null, '0', '0', null, '2019-06-20 14:31:47', '0', 'fa fa-envelope-open', 'base/storage/storageFile/monitor', '存储监控', null, '567251725514178560', null, null, null, null, null, null, null), ('567253355928223744', '2019-04-14 15:42:19', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:42:21', '0', 'fa fa-envelope-open', '', '微信设置', null, '567251529501769728', null, null, null, null, null, null, null), ('567253396831076352', '2019-04-14 15:42:29', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:42:32', '0', 'fa fa-envelope-open', '', '消息模板', null, '567251529501769728', null, null, null, null, null, null, null), ('567253455840739328', '2019-04-14 15:42:43', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:42:45', '0', 'fa fa-envelope-open', '', '支付记录', null, '567251529501769728', null, null, null, null, null, null, null), ('567253500753346560', '2019-04-14 15:42:54', '567250403742187520', null, null, null, '0', '0', null, '2019-04-14 23:42:56', '0', 'fa fa-envelope-open', '', '访问记录', null, '567251529501769728', null, null, null, null, null, null, null), ('567691771727839232', '2019-04-15 04:44:26', '566970396281143296', null, null, null, '0', '1', null, '2019-04-16 04:56:57', '1', 'fa fa-envelope-open', 'business/cms/article/list', '文章管理', null, '566972131494068224', null, null, null, null, null, null, null), ('567974409260761088', '2019-04-16 15:27:32', '567250403742187520', null, null, null, '0', '0', null, '2019-04-16 23:30:25', '0', 'fa fa-envelope-open', '', '流程管理', null, '567974346165846016', null, null, null, null, null, null, null), ('567974346165846016', '2019-04-16 15:27:17', '567250403742187520', null, null, null, '0', '0', null, '2019-04-16 23:30:22', '0', 'fa fa-envelope-open', '', '工作流组件', null, '567250523137245184', null, null, null, null, null, null, null), ('567974457973407744', '2019-04-16 15:27:43', '567250403742187520', null, null, null, '0', '0', null, '2019-04-16 23:30:28', '0', 'fa fa-envelope-open', '', '流程设计', null, '567974346165846016', null, null, null, null, null, null, null), ('567975073445576704', '2019-04-16 15:30:10', '567250403742187520', null, null, null, '0', '0', null, '2019-04-16 23:30:31', '0', 'fa fa-envelope-open', '', '流程监控', null, '567974346165846016', null, null, null, null, null, null, null), ('569403167805014016', '2019-04-20 14:04:54', '567250403742187520', null, null, null, '0', '0', null, '2019-04-20 22:06:44', '1', 'fa fa-envelope-open', '', '负载均衡', null, '567250403758964736', null, null, null, null, null, null, null), ('569403513797345280', '2019-04-20 14:06:17', '567250403742187520', null, null, null, '0', '0', null, '2019-04-20 22:06:34', '0', 'fa fa-envelope-open', '', '负载监控', null, '569403167805014016', null, null, null, null, null, null, null), ('573482921634037760', '2019-05-02 04:16:23', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:16:23', '9', 'fa fa-envelope-open', null, '客户关系管理系统', null, '0', null, null, null, null, null, null, null), ('573483261288775680', '2019-04-30 20:17:44', '573482921575317504', null, null, null, '0', '1', null, '2019-05-02 04:58:22', '1', 'fa fa-envelope-open', '', '应用监控', null, '573482921634037760', null, null, null, null, null, null, null), ('573485974525313024', '2019-05-01 12:28:31', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:32:28', '0', 'fa fa-envelope-open', 'business/crm/customerInfo/list', '客户信息管理', null, '573485855566462976', null, null, null, null, null, null, null), ('573485855566462976', '2019-05-01 04:28:03', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:52:23', '0', 'fa fa-envelope-open', 'business/crm/customerInfo/list', '客户管理', null, '573491902947524608', null, null, null, null, null, null, null), ('573486027511955456', '2019-05-01 12:28:44', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:29:02', '0', 'fa fa-envelope-open', 'business/crm/customerRelation/list', '客户关系', null, '573485855566462976', null, null, null, null, null, null, null), ('573486069253668864', '2019-05-01 20:28:54', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:29:05', '0', 'fa fa-envelope-open', '', '客户分析', null, '573485855566462976', null, null, null, null, null, null, null), ('573486166817374208', '2019-05-01 12:29:17', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:52:27', '0', 'fa fa-envelope-open', '', '市场管理', null, '573491902947524608', null, null, null, null, null, null, null), ('573486210748514304', '2019-05-01 20:29:28', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:29:30', '0', 'fa fa-envelope-open', '', '市场分析', null, '573486166817374208', null, null, null, null, null, null, null), ('573486268147564544', '2019-05-01 12:29:41', '573482921575317504', null, null, null, '0', '0', null, '2019-07-04 23:32:06', '0', 'fa fa-envelope-open', '', '市场计划', null, '573486166817374208', null, null, null, null, null, null, null), ('597017301056749568', '2019-07-04 18:53:36', '564392969700900864', null, null, null, '0', '1', null, '2019-07-06 05:26:35', '0', 'fa fa-meetup', 'boot/platform/address/list', '区域管理', null, '536478252336676864', null, null, null, null, null, null, null), ('573486401748729856', '2019-05-01 12:30:13', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:52:31', '0', 'fa fa-envelope-open', '', '呼叫中心', null, '573491902947524608', null, null, null, null, null, null, null), ('573486461949575168', '2019-05-01 20:30:27', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:31:05', '0', 'fa fa-envelope-open', '', '电子商务平台', null, '573486401748729856', null, null, null, null, null, null, null), ('573486514181242880', '2019-05-01 12:30:40', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:52:35', '0', 'fa fa-envelope-open', '', '服务管理', null, '573491902947524608', null, null, null, null, null, null, null), ('573486564630331392', '2019-05-01 20:30:52', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:31:08', '0', 'fa fa-envelope-open', '', '客户反馈', null, '573486514181242880', null, null, null, null, null, null, null), ('573486604425887744', '2019-05-01 20:31:01', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:31:10', '0', 'fa fa-envelope-open', '', '合同档案管理', null, '573486514181242880', null, null, null, null, null, null, null), ('573491902947524608', '2019-05-01 20:52:05', '573482921575317504', null, null, null, '0', '0', null, '2019-05-02 04:56:44', '1', '', '', '客户管理', null, '573482921634037760', null, null, null, null, null, null, null), ('573493343363792896', '2019-04-30 20:57:48', '573482921575317504', null, null, null, '0', '1', null, '2019-05-02 04:58:41', '0', 'fa fa-envelope-open', '', '统计管理', null, '573483261288775680', null, null, null, null, null, null, null), ('573864340722024448', '2019-05-02 13:32:01', '567250403742187520', null, null, null, '0', '0', null, '2019-05-03 05:32:36', '0', 'fa fa-envelope-open', '', '打印监控', null, '567251437654900736', null, null, null, null, null, null, null), ('579301304204001280', '2019-05-18 05:36:33', '579301304011063296', null, null, null, '0', '0', null, '2019-05-18 05:36:33', '9', null, null, '研发平台', null, '0', null, null, null, null, null, null, null), ('579312605248618496', '2019-05-17 22:21:28', '579301304011063296', null, null, null, '0', '1', null, '2019-05-18 06:21:32', '1', '', '', '监控管理', null, '579301304204001280', null, null, null, null, null, null, null), ('579312708516577280', '2019-05-17 22:21:52', '579301304011063296', null, null, null, '0', '1', null, '2019-05-18 06:21:56', '1', 'fa fa-envelope-open', '', '门户功能', null, '579301304204001280', null, null, null, null, null, null, null), ('579312829195091968', '2019-05-16 14:22:21', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-19 06:36:23', '0', 'fa fa-envelope-open', 'public/portal/ucenter/home', '研发门户', null, '579312605248618496', null, null, null, '564714047887376384', null, null, null), ('579312906630332416', '2019-05-17 06:22:40', '579301304011063296', null, null, null, '0', '1', null, '2019-05-18 13:54:06', '0', 'fa fa-envelope-open', 'portal/desktop/web/menus/list', '导航管理', null, '579312708516577280', null, null, null, null, null, null, null), ('579313103074754560', '2019-05-17 06:23:26', '579301304011063296', null, null, null, '0', '1', null, '2019-05-18 13:54:19', '0', 'fa fa-envelope-open', 'portal/desktop/web/module/list', '模块管理', null, '579312708516577280', null, null, null, null, null, null, null), ('579313158582173696', '2019-05-17 06:23:40', '579301304011063296', null, null, null, '0', '1', null, '2019-05-18 13:54:30', '0', 'fa fa-envelope-open', 'portal/desktop/web/linkPath/list', '链接管理', null, '579312708516577280', null, null, null, null, null, null, null), ('579313195768872960', '2019-05-17 06:23:48', '579301304011063296', null, null, null, '0', '1', null, '2019-05-18 13:54:45', '0', 'fa fa-envelope-open', 'portal/desktop/web/uploadImages/list', '海报管理', null, '579312708516577280', null, null, null, null, null, null, null), ('580141241799278592', '2019-05-19 13:14:10', '579301304011063296', null, null, null, '0', '1', null, '2019-05-20 13:16:02', '0', 'fa fa-envelope-open', 'portal/desktop/web/linkPathType/list', '链接类型', null, '579312708516577280', null, null, null, null, null, null, null), ('585849790688919552', '2019-06-04 07:17:54', '564392969700900864', null, null, null, '0', '1', null, '2019-07-06 06:53:56', '0', 'fa fa-superpowers', 'boot/platform/button/list', '数据权限', null, '536478252336676864', null, null, null, null, null, null, null), ('587018734783168512', '2019-06-08 04:42:52', '587017505361362944', null, null, null, '0', '0', null, '2019-06-08 12:42:56', '1', 'fa fa-envelope-open', '', '监控管理', null, '587018476162383872', null, null, null, null, null, null, null), ('587018816999915520', '2019-06-08 04:43:12', '587017505361362944', null, null, null, '0', '0', null, '2019-06-08 12:43:16', '1', 'fa fa-envelope-open', '', '功能管理', null, '587018476162383872', null, null, null, null, null, null, null), ('587018476162383872', '2019-06-08 12:41:50', '587017505361362944', null, null, null, '0', '0', null, '2019-06-08 12:41:50', '9', null, null, '学生管理', null, '0', null, null, null, null, null, null, null), ('587018909824057344', '2019-06-08 04:43:34', '587017505361362944', null, null, null, '0', '0', null, '2019-06-08 12:43:40', '0', 'fa fa-envelope-open', '', '学生管理', null, '587018816999915520', null, null, null, null, null, null, null), ('587019005416439808', '2019-06-08 04:43:57', '587017505361362944', null, null, null, '0', '0', null, '2019-06-08 12:44:01', '0', 'fa fa-envelope-open', '', '教师管理', null, '587018816999915520', null, null, null, null, null, null, null), ('587019097565298688', '2019-06-07 20:44:19', '587017505361362944', null, null, null, '0', '1', null, '2019-07-05 07:52:44', '1', 'fa fa-envelope-open', 'boot/platform/account/list', '成绩管理', null, '587018816999915520', null, null, null, null, null, null, null), ('587019425513734144', '2019-06-07 20:45:37', '587017505361362944', null, null, null, '0', '0', null, '2019-06-08 12:48:16', '9', 'fa fa-bandcamp', '', '应用监控', null, '587018734783168512', null, null, null, null, null, null, null), ('591388001427259392', '2019-06-20 06:04:46', '567250403742187520', null, null, null, '0', '1', null, '2019-06-20 14:08:10', '0', 'fa fa-envelope-open-o', 'base/notice/smsFail/list', '手工发送', null, '567251483649638400', null, null, null, null, null, null, null), ('594506557836754944', '2019-06-29 04:36:48', '594506557832560640', null, null, null, '0', '0', null, '2019-06-29 04:36:48', '9', null, null, '智能代码', null, '0', null, null, null, null, null, null, null), ('594507009877868544', '2019-06-28 20:38:36', '594506557832560640', null, null, null, '0', '1', null, '2019-06-29 04:38:52', '1', '', '', '监控管理', null, '594506557836754944', null, null, null, null, null, null, null), ('594507054937276416', '2019-06-28 20:38:47', '594506557832560640', null, null, null, '0', '1', null, '2019-06-29 04:38:55', '1', '', '', '功能列表', null, '594506557836754944', null, null, null, null, null, null, null), ('594507157429288960', '2019-06-28 04:39:11', '594506557832560640', null, null, null, '0', '1', null, '2019-06-29 04:48:09', '0', 'fa fa-envelope-open', '', '仪盘表', null, '594507009877868544', null, null, null, null, null, null, null), ('594507198013374464', '2019-06-28 04:39:21', '594506557832560640', null, null, null, '0', '1', null, '2019-06-29 04:48:02', '0', 'fa fa-envelope-open', '', '项目管理', null, '594507054937276416', null, null, null, null, null, null, null), ('594507554302722048', '2019-06-27 20:40:46', '594506557832560640', null, null, null, '0', '1', null, '2019-06-29 04:46:10', '0', 'fa fa-envelope-open', '', '版本控制器', null, '594507054937276416', null, null, null, null, null, null, null), ('595726661790466048', '2019-07-01 21:25:04', '594506557832560640', null, null, null, '0', '1', null, '2019-07-02 13:28:48', '0', 'fa fa-quora', 'compoment/code/module/list', '模块管理', null, '594507054937276416', null, null, null, null, null, null, null), ('600702353468817408', '2019-07-15 22:56:41', '594506557832560640', null, null, null, '0', '1', '0', '2019-07-16 06:56:48', '0', 'fa fa-address-book', '/compoment/code/mapFieldColumn/list', '字段属性映射信息管理', null, '594507054937276416', null, null, null, null, null, null, null), ('620385383795916800', '2019-09-08 14:30:01', '564392969700900864', null, null, null, '0', '1', '0', '2019-09-10 01:07:56', '0', 'fa fa-envelope-open', '', '开发配置', null, '536478252248596480', null, null, null, null, null, null, null), ('621387890336727040', '2019-09-11 08:53:37', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-11 08:53:43', '1', 'fa fa-envelope-open', '', '开发配置', null, '579301304204001280', null, null, null, null, null, null, null), ('621387968342392832', '2019-09-11 08:53:56', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:01:45', '0', 'fa fa-envelope-open', 'portal/ucenter/project/list', '项目管理', null, '621387890336727040', null, null, null, '564714047887376384', null, null, null), ('621388205438009344', '2019-09-11 08:54:53', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:02:02', '0', 'fa fa-envelope-open', 'portal/ucenter/role/list', '角色管理', null, '621387890336727040', null, null, null, '564714047887376384', null, null, null), ('621388258592423936', '2019-09-11 08:55:05', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:02:22', '0', 'fa fa-envelope-open', 'portal/ucenter/account/list', '账号管理', null, '621387890336727040', null, null, null, '564714047887376384', null, null, null), ('621388308399783936', '2019-09-11 08:55:17', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:02:37', '0', 'fa fa-envelope-open', 'portal/ucenter/dictionary/list', '字典管理', null, '621387890336727040', null, null, null, '564714047887376384', null, null, null), ('621388329274834944', '2019-09-11 08:55:22', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:02:45', '0', 'fa fa-envelope-open', 'portal/ucenter/content/list', '公告管理', null, '621387890336727040', null, null, null, '564714047887376384', null, null, null), ('621388582644350976', '2019-09-11 08:56:22', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:02:53', '0', 'fa fa-envelope-open', 'portal/ucenter/settings/list', '参数配置', null, '621387890336727040', null, null, null, '564714047887376384', null, null, null), ('627474915766829056', '2019-09-28 04:01:17', '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:21:51', '0', 'fa fa-envelope-open', 'portal/ucenter/source/list', '持续集成', null, '621387890336727040', null, null, '564714047887376384', '564714047887376384', null, null, null), ('627475545189253120', '2019-09-28 04:03:47', '564392969700900864', null, null, null, '0', '1', '0', '2019-09-28 04:04:45', '0', 'fa fa-envelope-open', 'boot/platform/account/list', '租户管理', null, '536478252261179392', null, null, '564714047887376384', '564714047887376384', null, null, null), ('627515979995283456', '2019-09-28 06:44:28', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 06:44:28', '9', null, null, '学生管理系统', null, '0', null, null, '627434648128978944', null, null, null, null), ('627588901409652736', '2019-09-28 11:34:14', '627515979521327104', null, null, null, '0', '1', '0', '2019-09-28 11:35:49', '0', '', '', '学生管理', null, '627589233275568128', null, null, '564714047887376384', null, null, null, null), ('627588945139466240', '2019-09-28 11:34:24', '627515979521327104', null, null, null, '0', '1', '0', '2019-09-28 11:35:46', '0', '', '', '教师管理', null, '627589233275568128', null, null, '564714047887376384', null, null, null, null), ('627588983467016192', '2019-09-28 11:34:33', '627515979521327104', null, null, null, '0', '1', '0', '2019-09-28 11:35:42', '0', '', '', '成绩管理', null, '627589233275568128', null, null, '564714047887376384', null, null, null, null), ('627589233275568128', '2019-09-28 11:35:33', '627515979521327104', null, null, null, '0', '1', '0', '2019-09-28 11:35:38', '1', '', '', '功能管理', null, '627515979995283456', null, null, '564714047887376384', null, null, null, null), ('631522362700857344', '2019-10-09 08:04:24', '564392969700900864', null, null, null, '0', '1', '0', '2019-10-09 08:04:24', '0', 'fa fa-envelope-open', 'boot/platform/record/list', '节假日管理', null, '536478252420562944', null, null, '564714047887376384', null, null, null, null), ('632257420713787392', '2019-10-11 08:45:15', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:47:42', '0', '', 'alinesno/oc/student/teacher/list', '教师管理', null, '632255939486941184', null, null, '624277812634714112', '624277812634714112', null, null, null), ('632709927277690880', '2019-10-12 14:43:21', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:43:21', '9', null, null, '演示管理系统', null, '0', null, null, '624277812634714112', null, null, null, null), ('632255939486941184', '2019-10-11 08:39:22', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:45:08', '1', '', '', '教师服务', null, '632255308802031616', null, null, '624277812634714112', '624277812634714112', null, null, null), ('632255844578230272', '2019-10-11 08:39:00', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:47:48', '0', '', 'alinesno/oc/student/score/list', '分数管理', null, '632255524787716096', null, null, '624277812634714112', '624277812634714112', null, null, null), ('632255308802031616', '2019-10-11 08:36:52', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:36:52', '9', null, null, '学生管理系统', null, '0', null, null, '624277812634714112', null, null, null, null), ('632255524787716096', '2019-10-11 08:37:43', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:37:48', '1', '', '', '学生服务', null, '632255308802031616', null, null, '624277812634714112', null, null, null, null), ('632255708053635072', '2019-10-11 08:38:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:47:54', '0', '', 'alinesno/oc/student/student/list', '学生管理', null, '632255524787716096', null, null, '624277812634714112', '624277812634714112', null, null, null), ('632710168685051904', '2019-10-12 14:44:19', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 15:25:02', '0', '', 'alinsno/lin/demo/student/list', '学生管理', null, '632710402140012544', null, null, '624277812634714112', '624277812634714112', null, null, null), ('632710246577471488', '2019-10-12 14:44:37', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:44:50', '1', '', '', '教师管理服务', null, '632709927277690880', null, null, '624277812634714112', null, null, null, null), ('632710402140012544', '2019-10-12 14:45:15', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:45:25', '1', '', '', '学生管理服务', null, '632709927277690880', null, null, '624277812634714112', '624277812634714112', null, null, null), ('632710509761658880', '2019-10-12 14:45:40', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 15:23:44', '0', '', 'alinsno/lin/demo/teacher/list', '教师管理', null, '632710246577471488', null, null, '624277812634714112', '624277812634714112', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_resource_action`
-- ----------------------------
DROP TABLE IF EXISTS `manager_resource_action`;
CREATE TABLE `manager_resource_action` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `resource_action_icon` varchar(255) DEFAULT NULL,
  `resource_action_method` varchar(255) DEFAULT NULL,
  `resource_action_name` varchar(255) DEFAULT NULL,
  `resource_action_order` int(11) DEFAULT NULL,
  `resource_action_status` bit(1) DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_role`
-- ----------------------------
DROP TABLE IF EXISTS `manager_role`;
CREATE TABLE `manager_role` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `role_status` bit(1) DEFAULT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_role`
-- ----------------------------
BEGIN;
INSERT INTO `manager_role` VALUES ('564708772195336192', '2019-04-07 23:11:03', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 14:15:05', null, null, '部门管理', null, null, null, null), ('563847273901981696', '2019-04-05 14:07:46', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 14:15:08', null, null, '软件研发岗', null, null, null, null), ('563851604873183232', '2019-04-05 14:24:58', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 23:17:30', null, null, '班级管理岗', null, null, null, null), ('566985004698042368', null, '566970396281143296', null, null, null, '0', '0', null, '2019-07-07 23:17:47', null, null, '内容运营人员', null, null, null, null), ('567262525821812736', null, '567250403742187520', null, null, null, '0', '0', null, '2019-07-07 23:17:44', null, null, '组件管理管理员', null, null, null, null), ('567266058025566208', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', null, '2019-07-07 23:17:40', null, null, '平台管理员', null, null, null, null), ('573488809103065088', null, '573482921575317504', null, null, null, '0', '0', null, '2019-07-07 23:17:38', null, null, '客户管理系统管理员', null, null, null, null), ('579313426707251200', null, '579301304011063296', null, null, null, '0', '0', null, '2019-07-07 23:17:36', null, null, '研发平台管理员', null, null, null, null), ('587020566561554432', null, '587017505361362944', null, null, null, '0', '0', null, '2019-07-07 23:17:34', null, null, '学生管理系统管理员', null, null, null, null), ('594507939100753920', null, '594506557832560640', null, null, null, '0', '2', '0', '2019-07-30 02:19:48', null, null, '代码生成管理员', null, null, null, null), ('604325556556660736', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:11', null, null, '云平台租户', null, null, null, null), ('621448386020638720', null, '579301304011063296', null, null, null, '0', '1', '0', '2019-09-28 04:05:29', null, null, '互联网角色', null, null, null, null), ('627589594837155840', null, '627515979521327104', null, null, null, '0', '1', '0', '2019-09-28 11:39:20', null, null, '班主任', null, null, null, null), ('627887026238128128', '2019-09-29 07:18:52', '627885723361476608', null, null, null, '0', '1', '0', '2019-09-29 07:18:52', null, null, '4444', null, null, null, null), ('632710993285218304', '2019-10-12 14:47:36', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:47:40', null, null, 'those-years', null, null, '624277812634714112', null), ('632257722263273472', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:29', null, null, '测试工', null, null, '624277812634714112', null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_role_resource`
-- ----------------------------
DROP TABLE IF EXISTS `manager_role_resource`;
CREATE TABLE `manager_role_resource` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `resource_type` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `role_type` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_role_resource`
-- ----------------------------
BEGIN;
INSERT INTO `manager_role_resource` VALUES ('564708772291805185', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252303122432', null, '564708772195336192', null, null, null, null, null), ('564708772291805184', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252290539520', null, '564708772195336192', null, null, null, null, null), ('564708772287610880', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252277956608', null, '564708772195336192', null, null, null, null, null), ('564708772283416576', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252261179392', null, '564708772195336192', null, null, null, null, null), ('564708772279222272', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252248596480', null, '564708772195336192', null, null, null, null, null), ('564708772275027969', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252194070530', null, '564708772195336192', null, null, null, null, null), ('564708772275027968', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252194070529', null, '564708772195336192', null, null, null, null, null), ('564708772270833664', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252194070528', null, '564708772195336192', null, null, null, null, null), ('564708772266639360', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478251871109120', null, '564708772195336192', null, null, null, null, null), ('563851604902543360', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '563273421400571904', null, '563851604873183232', null, null, null, null, null), ('563851604898349057', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252420562944', null, '563851604873183232', null, null, null, null, null), ('563851604898349056', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252324093952', null, '563851604873183232', null, null, null, null, null), ('563851604894154752', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252315705344', null, '563851604873183232', null, null, null, null, null), ('563851604889960449', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252303122432', null, '563851604873183232', null, null, null, null, null), ('563851604889960448', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252290539520', null, '563851604873183232', null, null, null, null, null), ('563851604885766145', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252277956608', null, '563851604873183232', null, null, null, null, null), ('563851604885766144', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252261179392', null, '563851604873183232', null, null, null, null, null), ('563851604881571840', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252248596480', null, '563851604873183232', null, null, null, null, null), ('563851604877377536', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478251871109120', null, '563851604873183232', null, null, null, null, null), ('563847273948119040', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252588335104', null, '563847273901981696', null, null, null, null, null), ('563847273943924736', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252571557888', null, '563847273901981696', null, null, null, null, null), ('563847273939730433', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252399591424', null, '563847273901981696', null, null, null, null, null), ('563847273939730432', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252387008512', null, '563847273901981696', null, null, null, null, null), ('563847273935536128', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252361842688', null, '563847273901981696', null, null, null, null, null), ('563847273931341824', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252349259776', null, '563847273901981696', null, null, null, null, null), ('563847273927147520', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252336676864', null, '563847273901981696', null, null, null, null, null), ('563847273922953217', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252324093952', null, '563847273901981696', null, null, null, null, null), ('563847273922953216', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252315705344', null, '563847273901981696', null, null, null, null, null), ('563847273918758912', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252303122432', null, '563847273901981696', null, null, null, null, null), ('563847273906176000', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478251871109120', null, '563847273901981696', null, null, null, null, null), ('563847273910370304', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252248596480', null, '563847273901981696', null, null, null, null, null), ('563847273910370305', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252261179392', null, '563847273901981696', null, null, null, null, null), ('563847273914564608', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252277956608', null, '563847273901981696', null, null, null, null, null), ('563847273914564609', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252290539520', null, '563847273901981696', null, null, null, null, null), ('564708772333748224', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252437340160', null, '564708772195336192', null, null, null, null, null), ('564708772329553920', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '564580377914507264', null, '564708772195336192', null, null, null, null, null), ('564708772325359617', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '563273421400571904', null, '564708772195336192', null, null, null, null, null), ('564708772325359616', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252420562944', null, '564708772195336192', null, null, null, null, null), ('564708772321165312', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252399591424', null, '564708772195336192', null, null, null, null, null), ('564708772316971008', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252387008512', null, '564708772195336192', null, null, null, null, null), ('564708772312776704', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252361842688', null, '564708772195336192', null, null, null, null, null), ('564708772308582400', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252349259776', null, '564708772195336192', null, null, null, null, null), ('564708772304388097', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252336676864', null, '564708772195336192', null, null, null, null, null), ('564708772304388096', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252324093952', null, '564708772195336192', null, null, null, null, null), ('564708772300193792', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252315705344', null, '564708772195336192', null, null, null, null, null), ('564708772333748225', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252449923072', null, '564708772195336192', null, null, null, null, null), ('564708772337942528', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252458311680', null, '564708772195336192', null, null, null, null, null), ('564708772342136832', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252470894592', null, '564708772195336192', null, null, null, null, null), ('564708772342136833', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252483477504', null, '564708772195336192', null, null, null, null, null), ('564708772346331136', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252496060416', null, '564708772195336192', null, null, null, null, null), ('564708772350525440', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252517031936', null, '564708772195336192', null, null, null, null, null), ('564708772350525441', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252529614848', null, '564708772195336192', null, null, null, null, null), ('564708772354719744', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252546392064', null, '564708772195336192', null, null, null, null, null), ('564708772358914048', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252558974976', null, '564708772195336192', null, null, null, null, null), ('564708772363108352', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252571557888', null, '564708772195336192', null, null, null, null, null), ('564708772367302656', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252588335104', null, '564708772195336192', null, null, null, null, null), ('591388391895990275', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252467201343488', null, '567262525821812736', null, null, null, null, null), ('591388391895990274', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567251437654900736', null, '567262525821812736', null, null, null, null, null), ('591388391895990273', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252408439144448', null, '567262525821812736', null, null, null, null, null), ('591388391895990272', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252359516782592', null, '567262525821812736', null, null, null, null, null), ('591388391891795972', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252304839835648', null, '567262525821812736', null, null, null, null, null), ('591388391891795971', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252181112061952', null, '567262525821812736', null, null, null, null, null), ('591388391891795970', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567251391144263680', null, '567262525821812736', null, null, null, null, null), ('567266058075897856', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478251871109120', null, '567266058025566208', null, null, null, null, null), ('567266058080092160', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252194070528', null, '567266058025566208', null, null, null, null, null), ('567266058080092161', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252194070530', null, '567266058025566208', null, null, null, null, null), ('567266058084286464', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252248596480', null, '567266058025566208', null, null, null, null, null), ('567266058084286465', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252261179392', null, '567266058025566208', null, null, null, null, null), ('567266058088480768', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252277956608', null, '567266058025566208', null, null, null, null, null), ('567266058092675072', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252290539520', null, '567266058025566208', null, null, null, null, null), ('567266058096869376', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252303122432', null, '567266058025566208', null, null, null, null, null), ('567266058105257984', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252315705344', null, '567266058025566208', null, null, null, null, null), ('567266058109452288', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252324093952', null, '567266058025566208', null, null, null, null, null), ('567266058109452289', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252336676864', null, '567266058025566208', null, null, null, null, null), ('567266058113646592', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252349259776', null, '567266058025566208', null, null, null, null, null), ('567266058113646593', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252361842688', null, '567266058025566208', null, null, null, null, null), ('567266058117840896', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252387008512', null, '567266058025566208', null, null, null, null, null), ('567266058117840897', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252399591424', null, '567266058025566208', null, null, null, null, null), ('567266058122035200', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252420562944', null, '567266058025566208', null, null, null, null, null), ('567266058122035201', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '563273421400571904', null, '567266058025566208', null, null, null, null, null), ('567266058122035202', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '566601898900062208', null, '567266058025566208', null, null, null, null, null), ('567266058126229504', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '566897548824936448', null, '567266058025566208', null, null, null, null, null), ('567266058126229505', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '566750963755384832', null, '567266058025566208', null, null, null, null, null), ('567266058130423808', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252437340160', null, '567266058025566208', null, null, null, null, null), ('567266058130423809', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252449923072', null, '567266058025566208', null, null, null, null, null), ('567266058130423810', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252458311680', null, '567266058025566208', null, null, null, null, null), ('567266058134618112', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252470894592', null, '567266058025566208', null, null, null, null, null), ('567266058134618113', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252483477504', null, '567266058025566208', null, null, null, null, null), ('567266058134618114', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252496060416', null, '567266058025566208', null, null, null, null, null), ('567266058138812416', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252517031936', null, '567266058025566208', null, null, null, null, null), ('567266058138812417', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252529614848', null, '567266058025566208', null, null, null, null, null), ('567266058143006720', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252546392064', null, '567266058025566208', null, null, null, null, null), ('567266058143006721', '2019-04-15 00:32:48', '564392969700900864', null, null, null, '0', '0', '0', '2019-04-15 00:32:48', '536478252558974976', null, '567266058025566208', null, null, null, null, null), ('567691891680739328', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566970396302114816', null, '566985004698042368', null, null, null, null, null), ('567691891684933632', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566972060576776192', null, '566985004698042368', null, null, null, null, null), ('567691891684933633', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566980019872595968', null, '566985004698042368', null, null, null, null, null), ('567691891689127936', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566972131494068224', null, '566985004698042368', null, null, null, null, null), ('567691891689127937', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566980563815104512', null, '566985004698042368', null, null, null, null, null), ('567691891693322240', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566980638335303680', null, '566985004698042368', null, null, null, null, null), ('567691891693322241', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566980714625499136', null, '566985004698042368', null, null, null, null, null), ('567691891697516544', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '567691771727839232', null, '566985004698042368', null, null, null, null, null), ('567691891697516545', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566972216520998912', null, '566985004698042368', null, null, null, null, null), ('567691891701710848', '2019-04-16 04:44:54', '566970396281143296', null, null, null, '0', '0', '0', '2019-04-16 04:44:54', '566980497616404480', null, '566985004698042368', null, null, null, null, null), ('591388391891795969', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567250523137245184', null, '567262525821812736', null, null, null, null, null), ('591388391891795968', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567250403758964736', null, '567262525821812736', null, null, null, null, null), ('573493663510822912', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486564630331392', null, '573488809103065088', null, null, null, null, null), ('573493663506628610', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486514181242880', null, '573488809103065088', null, null, null, null, null), ('573493663506628609', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486461949575168', null, '573488809103065088', null, null, null, null, null), ('573493663506628608', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486401748729856', null, '573488809103065088', null, null, null, null, null), ('573493663502434306', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486315895521280', null, '573488809103065088', null, null, null, null, null), ('573493663502434305', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486268147564544', null, '573488809103065088', null, null, null, null, null), ('573493663502434304', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486210748514304', null, '573488809103065088', null, null, null, null, null), ('573493663498240002', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486166817374208', null, '573488809103065088', null, null, null, null, null), ('573493663498240001', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486069253668864', null, '573488809103065088', null, null, null, null, null), ('573493663498240000', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486027511955456', null, '573488809103065088', null, null, null, null, null), ('573493663494045698', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573485974525313024', null, '573488809103065088', null, null, null, null, null), ('573493663494045697', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573485855566462976', null, '573488809103065088', null, null, null, null, null), ('573493663494045696', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573491902947524608', null, '573488809103065088', null, null, null, null, null), ('573493663489851393', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573493343363792896', null, '573488809103065088', null, null, null, null, null), ('573493663489851392', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573483261288775680', null, '573488809103065088', null, null, null, null, null), ('573493663485657088', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573482921634037760', null, '573488809103065088', null, null, null, null, null), ('573493663510822913', '2019-05-02 04:59:04', '573482921575317504', null, null, null, '0', '0', '0', '2019-05-02 04:59:04', '573486604425887744', null, '573488809103065088', null, null, null, null, null), ('587020817854889984', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587018476162383872', null, '587020566561554432', null, null, null, null, null), ('580141472846708736', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579301304204001280', null, '579313426707251200', null, null, null, null, null), ('580141472850903040', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579312605248618496', null, '579313426707251200', null, null, null, null, null), ('580141472855097344', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579312829195091968', null, '579313426707251200', null, null, null, null, null), ('580141472855097345', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579312708516577280', null, '579313426707251200', null, null, null, null, null), ('580141472855097346', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579312906630332416', null, '579313426707251200', null, null, null, null, null), ('580141472855097347', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579313103074754560', null, '579313426707251200', null, null, null, null, null), ('580141472855097348', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579313158582173696', null, '579313426707251200', null, null, null, null, null), ('580141472859291648', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '579313195768872960', null, '579313426707251200', null, null, null, null, null), ('580141472859291649', '2019-05-20 13:15:05', '579301304011063296', null, null, null, '0', '0', '0', '2019-05-20 13:15:05', '580141241799278592', null, '579313426707251200', null, null, null, null, null), ('587020817859084288', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587018734783168512', null, '587020566561554432', null, null, null, null, null), ('587020817859084289', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587019425513734144', null, '587020566561554432', null, null, null, null, null), ('587020817863278592', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587018816999915520', null, '587020566561554432', null, null, null, null, null), ('587020817863278593', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587018909824057344', null, '587020566561554432', null, null, null, null, null), ('587020817863278594', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587019005416439808', null, '587020566561554432', null, null, null, null, null), ('587020817863278595', '2019-06-08 12:51:09', '587017505361362944', null, null, null, '0', '0', '0', '2019-06-08 12:51:09', '587019097565298688', null, '587020566561554432', null, null, null, null, null), ('604325556686684160', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252290539520', null, '604325556556660736', null, null, null, null, null), ('604325556682489861', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252277956608', null, '604325556556660736', null, null, null, null, null), ('604325556682489860', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252261179392', null, '604325556556660736', null, null, null, null, null), ('604325556682489859', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252248596480', null, '604325556556660736', null, null, null, null, null), ('604325556682489858', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252194070530', null, '604325556556660736', null, null, null, null, null), ('604325556682489857', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252194070529', null, '604325556556660736', null, null, null, null, null), ('604325556682489856', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252194070528', null, '604325556556660736', null, null, null, null, null), ('591388391895990276', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252514672476160', null, '567262525821812736', null, null, null, null, null), ('591388391895990277', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252558305820672', null, '567262525821812736', null, null, null, null, null), ('591388391900184576', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '573864340722024448', null, '567262525821812736', null, null, null, null, null), ('591388391900184577', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567251483649638400', null, '567262525821812736', null, null, null, null, null), ('591388391900184578', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252667454193664', null, '567262525821812736', null, null, null, null, null), ('591388391900184579', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252763872854016', null, '567262525821812736', null, null, null, null, null), ('591388391900184580', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252817794826240', null, '567262525821812736', null, null, null, null, null), ('591388391900184581', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252849352769536', null, '567262525821812736', null, null, null, null, null), ('591388391904378880', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567252894554783744', null, '567262525821812736', null, null, null, null, null), ('591388391904378881', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '591388001427259392', null, '567262525821812736', null, null, null, null, null), ('591388391904378882', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567251529501769728', null, '567262525821812736', null, null, null, null, null), ('591388391904378883', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567253355928223744', null, '567262525821812736', null, null, null, null, null), ('591388391904378884', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567253396831076352', null, '567262525821812736', null, null, null, null, null), ('591388391904378885', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567253455840739328', null, '567262525821812736', null, null, null, null, null), ('591388391904378886', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567253500753346560', null, '567262525821812736', null, null, null, null, null), ('591388391908573184', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567251725514178560', null, '567262525821812736', null, null, null, null, null), ('591388391908573185', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567253179394162688', null, '567262525821812736', null, null, null, null, null), ('591388391908573186', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567253275787657216', null, '567262525821812736', null, null, null, null, null), ('591388391908573187', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567251819089100800', null, '567262525821812736', null, null, null, null, null), ('591388391908573188', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567974346165846016', null, '567262525821812736', null, null, null, null, null), ('591388391908573189', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567974409260761088', null, '567262525821812736', null, null, null, null, null), ('591388391908573190', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567974457973407744', null, '567262525821812736', null, null, null, null, null), ('591388391912767488', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567975073445576704', null, '567262525821812736', null, null, null, null, null), ('591388391912767489', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567250886120701952', null, '567262525821812736', null, null, null, null, null), ('591388391912767490', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '567250962478006272', null, '567262525821812736', null, null, null, null, null), ('591388391912767491', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '569403167805014016', null, '567262525821812736', null, null, null, null, null), ('591388391912767492', '2019-06-20 14:06:20', '567250403742187520', null, null, null, '0', '0', '0', '2019-06-20 14:06:20', '569403513797345280', null, '567262525821812736', null, null, null, null, null), ('604325556678295552', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478251871109120', null, '604325556556660736', null, null, null, null, null), ('632710993704648706', '2019-10-12 14:47:36', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:47:36', '632710168685051904', null, '632710993285218304', null, null, null, null, null), ('627590188041764864', '2019-09-28 11:39:20', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 11:39:20', '627588983467016192', null, '627589594837155840', null, null, null, null, null), ('627590188037570560', '2019-09-28 11:39:20', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 11:39:20', '627588945139466240', null, '627589594837155840', null, null, null, null, null), ('627590188033376256', '2019-09-28 11:39:20', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 11:39:20', '627588901409652736', null, '627589594837155840', null, null, null, null, null), ('627590188029181952', '2019-09-28 11:39:20', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 11:39:20', '627589233275568128', null, '627589594837155840', null, null, null, null, null), ('627590187945295872', '2019-09-28 11:39:20', '627515979521327104', null, null, null, '0', '0', '0', '2019-09-28 11:39:20', '627515979995283456', null, '627589594837155840', null, null, null, null, null), ('632710993704648705', '2019-10-12 14:47:36', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:47:36', '632710402140012544', null, '632710993285218304', null, null, null, null, null), ('632710993704648704', '2019-10-12 14:47:36', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:47:36', '632710509761658880', null, '632710993285218304', null, null, null, null, null), ('604325556686684161', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252303122432', null, '604325556556660736', null, null, null, null, null), ('604325556690878464', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252315705344', null, '604325556556660736', null, null, null, null, null), ('604325556690878465', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252324093952', null, '604325556556660736', null, null, null, null, null), ('604325556690878466', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252336676864', null, '604325556556660736', null, null, null, null, null), ('604325556690878467', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252349259776', null, '604325556556660736', null, null, null, null, null), ('604325556690878468', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252361842688', null, '604325556556660736', null, null, null, null, null), ('604325556690878469', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252387008512', null, '604325556556660736', null, null, null, null, null), ('604325556690878470', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252399591424', null, '604325556556660736', null, null, null, null, null), ('604325556695072768', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '597017301056749568', null, '604325556556660736', null, null, null, null, null), ('604325556695072769', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '585849790688919552', null, '604325556556660736', null, null, null, null, null), ('604325556695072770', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252420562944', null, '604325556556660736', null, null, null, null, null), ('604325556695072771', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '563273421400571904', null, '604325556556660736', null, null, null, null, null), ('604325556695072772', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '566601898900062208', null, '604325556556660736', null, null, null, null, null), ('604325556695072773', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '566897548824936448', null, '604325556556660736', null, null, null, null, null), ('604325556695072774', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '566750963755384832', null, '604325556556660736', null, null, null, null, null), ('604325556695072775', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252437340160', null, '604325556556660736', null, null, null, null, null), ('604325556699267072', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252449923072', null, '604325556556660736', null, null, null, null, null), ('604325556699267073', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252458311680', null, '604325556556660736', null, null, null, null, null), ('604325556699267074', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252470894592', null, '604325556556660736', null, null, null, null, null), ('604325556699267075', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252483477504', null, '604325556556660736', null, null, null, null, null), ('604325556699267076', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252496060416', null, '604325556556660736', null, null, null, null, null), ('604325556699267077', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252517031936', null, '604325556556660736', null, null, null, null, null), ('604325556699267078', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252529614848', null, '604325556556660736', null, null, null, null, null), ('604325556703461376', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252546392064', null, '604325556556660736', null, null, null, null, null), ('604325556703461377', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252558974976', null, '604325556556660736', null, null, null, null, null), ('604325556703461378', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252571557888', null, '604325556556660736', null, null, null, null, null), ('604325556703461379', '2019-07-26 06:54:00', '564392969700900864', null, null, null, '0', '0', '0', '2019-07-26 06:54:00', '536478252588335104', null, '604325556556660736', null, null, null, null, null), ('632710993700454400', '2019-10-12 14:47:36', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:47:36', '632710246577471488', null, '632710993285218304', null, null, null, null, null), ('627887026460426240', '2019-09-29 07:18:52', '627885723361476608', null, null, null, '0', '0', '0', '2019-09-29 07:18:52', '627885723374059520', null, '627887026238128128', null, null, null, null, null), ('605706104625692674', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '595726661790466048', null, '594507939100753920', null, null, null, null, null), ('605706104625692673', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '594507554302722048', null, '594507939100753920', null, null, null, null, null), ('605706104625692672', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '594507198013374464', null, '594507939100753920', null, null, null, null, null), ('605706104621498369', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '594507054937276416', null, '594507939100753920', null, null, null, null, null), ('605706104621498368', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '594507157429288960', null, '594507939100753920', null, null, null, null, null), ('605706104617304065', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '594507009877868544', null, '594507939100753920', null, null, null, null, null), ('605706104617304064', '2019-07-30 02:19:48', '594506557832560640', null, null, null, '0', '0', '0', '2019-07-30 02:19:48', '594506557836754944', null, '594507939100753920', null, null, null, null, null), ('627475971640918016', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '579301304204001280', null, '621448386020638720', null, null, null, null, null), ('627475971649306624', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '579312605248618496', null, '621448386020638720', null, null, null, null, null), ('627475971653500928', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '579312829195091968', null, '621448386020638720', null, null, null, null, null), ('627475971653500929', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621387890336727040', null, '621448386020638720', null, null, null, null, null), ('627475971657695232', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621387968342392832', null, '621448386020638720', null, null, null, null, null), ('627475971657695233', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621388205438009344', null, '621448386020638720', null, null, null, null, null), ('627475971661889536', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621388258592423936', null, '621448386020638720', null, null, null, null, null), ('627475971661889537', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621388308399783936', null, '621448386020638720', null, null, null, null, null), ('627475971666083840', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621388329274834944', null, '621448386020638720', null, null, null, null, null), ('627475971749969920', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '621388582644350976', null, '621448386020638720', null, null, null, null, null), ('627475971754164224', '2019-09-28 04:05:29', '579301304011063296', null, null, null, '0', '0', '0', '2019-09-28 04:05:29', '627474915766829056', null, '621448386020638720', null, null, null, null, null), ('632710993633345536', '2019-10-12 14:47:36', '632709927269302272', null, null, null, '0', '0', '0', '2019-10-12 14:47:36', '632709927277690880', null, '632710993285218304', null, null, null, null, null), ('632257722422657024', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:27', '632255708053635072', null, '632257722263273472', null, null, null, null, null), ('632257722418462722', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:27', '632255844578230272', null, '632257722263273472', null, null, null, null, null), ('632257722418462721', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:27', '632255524787716096', null, '632257722263273472', null, null, null, null, null), ('632257722410074112', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:27', '632255308802031616', null, '632257722263273472', null, null, null, null, null), ('632257722418462720', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:27', '632257420713787392', null, '632257722263273472', null, null, null, null, null), ('632257722414268416', '2019-10-11 08:46:27', '632255308793643008', null, null, null, '0', '0', '0', '2019-10-11 08:46:27', '632255939486941184', null, '632257722263273472', null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_search`
-- ----------------------------
DROP TABLE IF EXISTS `manager_search`;
CREATE TABLE `manager_search` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `has_date` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `option_json` varchar(255) DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `search_type` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  `dict` varchar(255) DEFAULT NULL,
  `search_script` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_settings`
-- ----------------------------
DROP TABLE IF EXISTS `manager_settings`;
CREATE TABLE `manager_settings` (
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(100) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `config_remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
--  Records of `manager_settings`
-- ----------------------------
BEGIN;
INSERT INTO `manager_settings` VALUES ('主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow', '1d2c1f7b542d495373d7d94ed57b9db7', null, null, null, null, null, null, null, '0', '0', null, null, '2019-07-07 03:49:08', null), ('用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', '初始化密码 123456', '7ad799c0001f4a65bd38e17b17c43333', null, null, null, null, null, null, null, '0', '0', null, null, '2019-07-07 03:49:10', null), ('验证码状态', 'sys.captcha.status', 'close', 'Y', '验证码状态，open为开启，其它为关闭', '599999011876765696', '2019-07-12 08:21:51', '0', null, null, null, null, null, '0', '0', null, '0', '2019-07-30 08:17:43', null), ('首页类型', 'sys.portal.style', 'link', 'Y', 'link所有链接|portal门户类型', '604386042493009920', '2019-07-26 02:54:21', '0', null, null, null, null, null, '0', '0', null, '0', '2019-07-26 10:54:45', null), ('注册用户默认应用', 'sys.regist.default.application', '579301304011063296', 'Y', '值为应用的主键', '623612402151194624', '2019-09-17 12:13:03', '0', null, null, null, null, null, '0', '0', '564714047887376384', '0', '2019-09-17 12:19:15', null), ('注册用户默认角色', 'sys.regist.default.role', '621448386020638720', 'Y', '值为角色的主键', '623612743722729472', '2019-09-17 12:14:24', '0', null, null, null, null, null, '0', '0', '564714047887376384', '0', '2019-09-28 01:05:37', '564714047887376384'), ('默认用户注册名', 'sys.regist.default.name', '互联网开发人员', 'Y', '默认用户注册名', '623630325213298688', '2019-09-17 13:24:16', '0', null, null, null, null, null, '0', '0', '564714047887376384', '0', '2019-09-17 13:24:26', null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_source_generate`
-- ----------------------------
DROP TABLE IF EXISTS `manager_source_generate`;
CREATE TABLE `manager_source_generate` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `db_driver` varchar(255) DEFAULT NULL,
  `db_pwd` varchar(255) DEFAULT NULL,
  `db_url` varchar(255) DEFAULT NULL,
  `db_user` varchar(255) DEFAULT NULL,
  `boot_prefix` varchar(255) DEFAULT NULL,
  `feign_server_path` varchar(255) DEFAULT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `git_password` varchar(255) DEFAULT NULL,
  `git_repository_url` varchar(255) DEFAULT NULL,
  `git_user_name` varchar(255) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  `docker_project` bit(1) NOT NULL,
  `jenkins_jobname` varchar(255) DEFAULT NULL,
  `jenkins_password` varchar(255) DEFAULT NULL,
  `jenkins_url` varchar(255) DEFAULT NULL,
  `jenkins_user_name` varchar(255) DEFAULT NULL,
  `service_project` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_source_generate`
-- ----------------------------
BEGIN;
INSERT INTO `manager_source_generate` VALUES ('569785393084366848', '2019-04-21 23:23:44', null, null, null, null, '0', '0', null, '2019-04-21 23:23:44', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569785441788624896', '2019-04-21 23:23:55', null, null, null, null, '0', '0', null, '2019-04-21 23:23:55', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569785876570177536', '2019-04-21 23:25:39', null, null, null, null, '0', '0', null, '2019-04-21 23:25:39', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569787445218902016', '2019-04-21 23:31:53', null, null, null, null, '0', '0', null, '2019-04-21 23:31:53', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569792532267925504', '2019-04-21 23:52:06', null, null, null, null, '0', '0', null, '2019-04-21 23:52:06', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569792668851240960', '2019-04-21 23:52:38', null, null, null, null, '0', '0', null, '2019-04-21 23:52:38', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569792799612862464', '2019-04-21 23:53:10', null, null, null, null, '0', '0', null, '2019-04-21 23:53:10', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569793180195618816', '2019-04-21 23:54:40', null, null, null, null, '0', '0', null, '2019-04-21 23:54:40', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569793471771049984', '2019-04-21 23:55:50', null, null, null, null, '0', '0', null, '2019-04-21 23:55:50', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569794781463445504', '2019-04-22 00:01:02', null, null, null, null, '0', '0', null, '2019-04-22 00:01:02', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569796509629939712', '2019-04-22 00:07:54', null, null, null, null, '0', '0', null, '2019-04-22 00:07:54', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569796630509780992', '2019-04-22 00:08:23', null, null, null, null, '0', '0', null, '2019-04-22 00:08:23', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569798793604628480', '2019-04-22 00:16:59', null, null, null, '/Users/luodong/alinesno-generator/2019/04/alinesno-cloud-base-boot-1555892218713.zip', '0', '0', null, '2019-04-22 00:16:59', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_storage', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('569799845028560896', '2019-04-22 00:21:09', null, null, null, '/Users/luodong/alinesno-generator/2019/04/alinesno-cloud-base-boot-1555892469092.zip', '0', '0', null, '2019-04-22 00:21:09', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud.base.boot', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573475929448251392', '2019-05-02 03:48:36', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-logger-1556768915836.zip', '0', '0', null, '2019-05-02 03:48:36', 'List', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_logger', 'root', 'Alinesno', 'alinesno-cloud-base-logger', 'com.alinesno.cloud.base.logger', '日志应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573873306835353600', '2019-05-03 06:07:38', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-print-1556863658347.zip', '0', '0', null, '2019-05-03 06:07:38', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_print', 'root', 'Alinesno', 'alinesno-cloud-base-print', 'com.alinesno.cloud.base.print', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573941436089630720', '2019-05-03 10:38:22', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556879901329.zip', '0', '0', null, '2019-05-03 10:38:22', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573942385097048064', '2019-05-03 10:42:08', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556880127603.zip', '0', '0', null, '2019-05-03 10:42:08', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573942777474187264', '2019-05-03 10:43:41', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556880220792.zip', '0', '0', null, '2019-05-03 10:43:41', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573943212184436736', '2019-05-03 10:45:25', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556880324797.zip', '0', '0', null, '2019-05-03 10:45:25', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573943373522534400', '2019-05-03 10:46:04', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556880362792.zip', '0', '0', null, '2019-05-03 10:46:04', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573944178732433408', '2019-05-03 10:49:16', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556880555252.zip', '0', '0', null, '2019-05-03 10:49:16', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573945357642235904', '2019-05-03 10:53:57', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556880836216.zip', '0', '0', null, '2019-05-03 10:53:57', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573946278971441152', '2019-05-03 10:57:36', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881055665.zip', '0', '0', null, '2019-05-03 10:57:36', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573946601630859264', '2019-05-03 10:58:53', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881132332.zip', '0', '0', null, '2019-05-03 10:58:53', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573947086446264320', '2019-05-03 11:00:49', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881248127.zip', '0', '0', null, '2019-05-03 11:00:49', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573947261176774656', '2019-05-03 11:01:30', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881290104.zip', '0', '0', null, '2019-05-03 11:01:30', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573947404387090432', '2019-05-03 11:02:05', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881324293.zip', '0', '0', null, '2019-05-03 11:02:05', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573947656041136128', '2019-05-03 11:03:05', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881384282.zip', '0', '0', null, '2019-05-03 11:03:05', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573948910364524544', '2019-05-03 11:08:04', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881682981.zip', '0', '0', null, '2019-05-03 11:08:04', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573949133363085312', '2019-05-03 11:08:57', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881736529.zip', '0', '0', null, '2019-05-03 11:08:57', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573949472468369408', '2019-05-03 11:10:18', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881817376.zip', '0', '0', null, '2019-05-03 11:10:18', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573949946194034688', '2019-05-03 11:12:11', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556881929944.zip', '0', '0', null, '2019-05-03 11:12:11', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573950731707482112', '2019-05-03 11:15:18', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556882117222.zip', '0', '0', null, '2019-05-03 11:15:18', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573951372441944064', '2019-05-03 11:17:51', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556882270031.zip', '0', '0', null, '2019-05-03 11:17:51', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('573951842241740800', '2019-05-03 11:19:43', null, null, null, '/Users/luodong/alinesno-generator/2019/05/alinesno-cloud-base-boot-1556882381948.zip', '0', '0', null, '2019-05-03 11:19:43', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'admin', 'jdbc:mysql://localhost:3306/alinesno_cloud_base_boot', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('612959471068512256', '2019-08-19 02:42:06', '0', null, null, 'C:\\Users\\Administrator/alinesno-generator/2019/08/alinesno-cloud-base-boot-1566182525566.zip', '0', '0', '0', '2019-08-19 02:42:06', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'ectrip', 'jdbc:mysql://localhost:3306/activiti?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&useSSL=false&serverTimezone=GMT', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('613026139383791616', '2019-08-19 07:07:01', '0', null, null, 'C:\\Users\\Administrator/alinesno-generator/2019/08/alinesno-cloud-base-boot-1566198420489.zip', '0', '0', '0', '2019-08-19 07:07:01', 'LuoAnDong', 'com.mysql.jdbc.Driver', 'ectrip', 'jdbc:mysql://localhost:3306/activiti?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&useSSL=false&serverTimezone=GMT', 'root', 'Alinesno', 'alinesno-cloud-base-boot', 'com.alinesno.cloud', '测试应用', null, null, null, null, null, null, null, b'0', null, null, null, null, b'0'), ('627515980108529664', '2019-09-28 06:44:28', '0', null, null, null, '0', '0', '0', '2019-09-28 06:44:42', '罗小东', 'com.mysql.cj.jdbc.Driver', 'alinesno_demo', 'jdbc:mysql://db4free.net/alinesno_demo', 'alinesno_demo', null, 'alinesno-demo-student', 'com.alinesno.cloud', '学生管理系统', null, null, '627434648128978944', 'LuoAnDong@Switch', 'https://gitee.com/landonniao/alinesno-demo-student.git', 'luoandon@gmail.com', null, b'1', '', '', '', '', b'0'), ('632255308806225920', '2019-10-11 08:36:52', '0', null, null, null, '0', '0', '0', '2019-10-11 08:37:11', '张三', 'com.mysql.cj.jdbc.Driver', 'ocj@gmail.com', 'jdbc:mysql://db4free.net:3306/dev_student', 'adminoc', null, 'alinesno-oc-student', 'com.alinesno.cloud', '学生管理系统', null, null, '624277812634714112', '512308cj', 'https://github.com/Oc204/alinesno-oc-student.git', 'Oc204', null, b'1', '', '', '', '', b'0'), ('632709927281885184', '2019-10-12 14:43:21', '0', null, null, null, '0', '0', '0', '2019-10-12 14:43:38', '张三', 'com.mysql.cj.jdbc.Driver', '110120lin..', 'jdbc:mysql://db4free.net:3306/thoseyears', 'thoseyears', null, 'alinsno-lin-demo', 'com.alinesno.cloud', '演示管理系统', null, null, '624277812634714112', '110120lin..', 'https://github.com/those-years/alinsno-lin-demo.git', 'those-years', null, b'1', '', '', '', '', b'0'), ('633354108765470720', '2019-10-14 09:23:06', '0', null, null, null, '0', '0', '0', '2019-10-14 09:27:10', 'gaoyu', null, '123456', 'jdbc:mysql://localhost:3306/xtjy?characterEncoding=utf8&amp;useUnicode=true&amp;useSSL=false&amp;serverTimezone=UTC&amp;allowMultiQueries=true', 'root', null, 'helloworld-linesno', 'com.alinesno.cloud', 'helloworld', null, null, '633248145500798976', 'weng1990', 'https://gitee.com/fengyiingbanbo/linesno-cloud-service.git', '782368028@qq.com', '633248145500798976', b'1', '', '', '', '', b'1'), ('635922110916591616', '2019-10-21 11:27:26', '0', null, null, null, '0', '0', '0', '2019-10-21 11:27:26', 'lihaifeng', null, '123456', 'jdbc:mysql://localhost:3306/test', 'root', null, 'middleware', 'com.alinesno.cloud', 'test', null, null, '635921106041044992', '952726SS507.', 'https://git.oschina.net/dlhf/middleware.git', 'li7hai26@aliyun.com', null, b'1', 'middleware-auto-k8s', 'lihaifeng', 'http://jenkins.192.168.0.232.nip.io', 'lihaifeng', b'1');
COMMIT;

-- ----------------------------
--  Table structure for `manager_template`
-- ----------------------------
DROP TABLE IF EXISTS `manager_template`;
CREATE TABLE `manager_template` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `template_addtime` datetime DEFAULT NULL,
  `template_content` varchar(255) DEFAULT NULL,
  `template_data` varchar(255) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `template_owner` varchar(255) DEFAULT NULL,
  `template_status` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_tenant`
-- ----------------------------
DROP TABLE IF EXISTS `manager_tenant`;
CREATE TABLE `manager_tenant` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `province_id` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `tenant_account` varchar(255) DEFAULT NULL,
  `tenant_address` varchar(255) DEFAULT NULL,
  `tenant_cost` int(11) DEFAULT NULL,
  `tenant_name` varchar(255) DEFAULT NULL,
  `tenant_phone` varchar(255) DEFAULT NULL,
  `tenant_status` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_tenant_log`
-- ----------------------------
DROP TABLE IF EXISTS `manager_tenant_log`;
CREATE TABLE `manager_tenant_log` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `action_type` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `log_business_id` varchar(255) DEFAULT NULL,
  `log_channel` varchar(255) DEFAULT NULL,
  `log_content` varchar(255) DEFAULT NULL,
  `log_ip` varchar(255) DEFAULT NULL,
  `log_machine` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_user`
-- ----------------------------
DROP TABLE IF EXISTS `manager_user`;
CREATE TABLE `manager_user` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_account` varchar(255) DEFAULT NULL,
  `user_addtime` datetime DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_role` varchar(255) DEFAULT NULL,
  `user_salt` varchar(255) DEFAULT NULL,
  `user_status` bit(1) DEFAULT NULL,
  `user_title` varchar(255) DEFAULT NULL,
  `user_type` bit(1) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_account`
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_status` int(11) DEFAULT NULL,
  `login_account` varchar(255) DEFAULT NULL,
  `login_password` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `phone_code` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_address`
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `address_name` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `room_num` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_classes`
-- ----------------------------
DROP TABLE IF EXISTS `user_classes`;
CREATE TABLE `user_classes` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `classes` varchar(255) DEFAULT NULL,
  `collge_name` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `school_end_time` datetime DEFAULT NULL,
  `school_id` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_num` varchar(255) DEFAULT NULL,
  `school_start_time` datetime DEFAULT NULL,
  `shool_job` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_info`
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `avatar_head` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `classes` varchar(255) DEFAULT NULL,
  `collge_name` varchar(255) DEFAULT NULL,
  `company_dept` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `english_name` varchar(255) DEFAULT NULL,
  `facsimile` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `home_address` varchar(255) DEFAULT NULL,
  `home_city` varchar(255) DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `living_adress` varchar(255) DEFAULT NULL,
  `living_city` varchar(255) DEFAULT NULL,
  `main_connection` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `native_address` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `parent_father` varchar(255) DEFAULT NULL,
  `parent_mother` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `really_name` varchar(255) DEFAULT NULL,
  `regist_ip` varchar(255) DEFAULT NULL,
  `regist_source` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `school_end_time` datetime DEFAULT NULL,
  `school_id` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_num` varchar(255) DEFAULT NULL,
  `school_start_time` datetime DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `shool_job` varchar(255) DEFAULT NULL,
  `user_code` varchar(255) DEFAULT NULL,
  `user_height` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL,
  `wechat` varchar(255) DEFAULT NULL,
  `weibo` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_integer`
-- ----------------------------
DROP TABLE IF EXISTS `user_integer`;
CREATE TABLE `user_integer` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_job`
-- ----------------------------
DROP TABLE IF EXISTS `user_job`;
CREATE TABLE `user_job` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_location`
-- ----------------------------
DROP TABLE IF EXISTS `user_location`;
CREATE TABLE `user_location` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `precisions` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_pic`
-- ----------------------------
DROP TABLE IF EXISTS `user_pic`;
CREATE TABLE `user_pic` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `pic_name` varchar(255) DEFAULT NULL,
  `pic_path` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_professional`
-- ----------------------------
DROP TABLE IF EXISTS `user_professional`;
CREATE TABLE `user_professional` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_school`
-- ----------------------------
DROP TABLE IF EXISTS `user_school`;
CREATE TABLE `user_school` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `department_id` varchar(36) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `operator_id` varchar(36) DEFAULT NULL,
  `last_update_operator_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;

