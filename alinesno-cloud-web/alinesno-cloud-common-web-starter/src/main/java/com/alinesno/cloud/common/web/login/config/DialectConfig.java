package com.alinesno.cloud.common.web.login.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alinesno.cloud.common.web.login.dialect.PagesDialect;

/**
 * 方言配置
 * @author LuoAnDong
 * @since 2019年9月29日 下午18:21:35
 */
@Configuration
public class DialectConfig{
	
    @Bean
    public PagesDialect dataMaskingDialect() {
        return new PagesDialect();
    }
    
//    @Bean
//    public TemplateEngine templateEngine(){
//        TemplateEngine templateEngine = new TemplateEngine();
//        templateEngine.addDialect(new PagesDialect());
//        return templateEngine;
//    }
   
//    @Bean
//    public SpringTemplateEngine templateEngine(){
//        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//        
//        templateEngine.setEnableSpringELCompiler(true);
//        templateEngine.setTemplateResolver(templateResolver());
//        templateEngine.addDialect(new PagesDialect());
//        
//        return new PagesDialect();
//    }
    
}