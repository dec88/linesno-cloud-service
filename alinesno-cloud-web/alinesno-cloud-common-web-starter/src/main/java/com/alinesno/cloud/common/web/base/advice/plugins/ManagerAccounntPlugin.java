package com.alinesno.cloud.common.web.base.advice.plugins;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;

/**
 * 应用代码转换插件
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:23:50
 */
@Component
public class ManagerAccounntPlugin implements TranslatePlugin {

//	@Autowired
	@Reference
	private IManagerAccountService managerAccountService ; 
	
	@Override
	public void translate(JSONObject node, TranslateCode convertCode) {

		String idField = convertCode.operatorField() ; 
		Object jsonNode = node.get(idField) ; 
		String name = "未知用户" ; 
		
		if(jsonNode != null) {
			String id = jsonNode.toString() ; //.asText() ; 
			if(StringUtils.isNotBlank(id)&& id != null) {
				ManagerAccountEntity dto = managerAccountService.findEntityById(id) ; 
				if(dto != null) {
					name = dto.getName() ;
				}
			}
		}
		
		node.put(idField + LABEL_SUFFER, name) ; 
	}

}
