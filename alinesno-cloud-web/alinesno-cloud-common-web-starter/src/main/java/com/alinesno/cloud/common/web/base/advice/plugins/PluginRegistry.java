package com.alinesno.cloud.common.web.base.advice.plugins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;

/**
 * 插件注册器
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:46:18
 */
public class PluginRegistry {
	
	private static final Map<String , Class<? extends TranslatePlugin>> plugins = new HashMap<>() ; 
	
	/**
	 * 初始化插件 
	 */
	public static void init() {
		regist(DirectoryPlugin.class);   // 代码转换注册
		regist(ApplicationPlugin.class);   // 应用代码转换注册
		regist(ManagerAccounntPlugin.class);   // 操作员转换注册
	}

	static {
		init() ; 
	}
	
	/**
	 * 插件注册
	 * @param c
	 * @return
	 */
	public static void regist(Class<? extends TranslatePlugin> c){
		plugins.put(c.getName() , c) ; 
	}
	
	public static List<Class<? extends TranslatePlugin>> query(){
		List<Class<? extends TranslatePlugin>> list = new ArrayList<>() ; 
		
		for(String k : plugins.keySet()) {
			list.add(plugins.get(k)) ; 
		}
		
		return list ; 
	}
	
}
