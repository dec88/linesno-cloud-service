package com.alinesno.cloud.common.web.login.filter;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 请求url权限拦截策略
 * @author LuoAnDong
 * @since 2019年10月2日 下午7:14:34
 */
public class URLPathMatchingFilter extends PathMatchingFilter {
	
	private static final Logger log = LoggerFactory.getLogger(URLPathMatchingFilter.class) ; 
	
	@Override
	protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        String requestUrl = getPathWithinApplication(request);
        Subject subject = SecurityUtils.getSubject();
        
		log.debug("用户状态:[{}],shiro 权限 url [{}] 过滤." , subject.isAuthenticated() , requestUrl);
		
		return true ;
	}
	
//    @Autowired
//    LoginService loginService;
//
//    @Override
//    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
//
//          if (loginService==null){
//              loginService= SpringContextUtil.getContext().getBean(LoginService.class);
//          }
//        //请求的url
//        String requestURL = getPathWithinApplication(request);
//        System.out.println("请求的url :"+requestURL);
//        Subject subject = SecurityUtils.getSubject();
//        if (!subject.isAuthenticated()){
//            // 如果没有登录, 直接返回true 进入登录流程
//            return  true;
//        }
//        String email = TokenManager.getEmail();
//        List<Upermission> permissions = loginService. upermissions(email);
//
//        boolean hasPermission = false;
//        for (Upermission url : permissions) {
//
//              if (url.getUrl().equals(requestURL)){
//                  hasPermission = true;
//                  break;
//              }
//        }
//        if (hasPermission){
//            return true;
//        }else {
//            UnauthorizedException ex = new UnauthorizedException("当前用户没有访问路径" + requestURL + "的权限");
//            subject.getSession().setAttribute("ex",ex);
//            WebUtils.issueRedirect(request, response, "/unauthorized");
//            return false;
//
//        }
//
//    }
	
}