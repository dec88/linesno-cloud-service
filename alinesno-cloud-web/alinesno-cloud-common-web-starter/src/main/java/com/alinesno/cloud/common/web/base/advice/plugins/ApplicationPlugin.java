package com.alinesno.cloud.common.web.base.advice.plugins;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;

/**
 * 应用代码转换插件
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:23:50
 */
@Component
public class ApplicationPlugin implements TranslatePlugin {

//	@Autowired
	@Reference
	private IManagerApplicationService managerApplicationFeigin ; 
	
	@Override
	public void translate(JSONObject node, TranslateCode convertCode) {

		Object jsonNode = node.get(APPLICATION_ID) ; 
		String applicationName = "公共属性" ; 
		
		log.trace("====> 开始应用转换_start:{}" , node); 
		
		if(jsonNode != null) {
			String id = jsonNode.toString() ; //.asText() ; 
			if(StringUtils.isNotBlank(id)&& id != null) {
				ManagerApplicationEntity dto = managerApplicationFeigin.findEntityById(id) ; 
				if(dto != null) {
					applicationName = dto.getApplicationName() ;
				}
			}
		}
		
		log.trace("====> 结束应用转换_start:{}" , node); 
		
		node.put(APPLICATION_ID + LABEL_SUFFER, applicationName) ; 
	}

}
