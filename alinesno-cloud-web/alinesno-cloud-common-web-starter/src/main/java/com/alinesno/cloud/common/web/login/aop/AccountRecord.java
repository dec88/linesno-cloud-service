package com.alinesno.cloud.common.web.login.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用户操作记录
 * 
 * @author LuoAnDong
 * @since 2019年4月8日 下午9:57:06
 */
@Retention(RetentionPolicy.RUNTIME) // 注解会在class中存在，运行时可通过反射获取
@Target(ElementType.METHOD) // 目标是方法
public @interface AccountRecord {

	// 记录类型
	enum RecordType {
		
		LOGIN_GET("login_access","登陆页面") , // 登陆 
		LOGIN_POST("login_validate","登陆验证") , // 登陆 
		LOGIN_REGISTER("login_register", "注册页面") , // 注册页面
		LOGOUT("logout","退出系统") ,  // 登出
		OPERATE_ACTION("operate_action","操作内容") , // 操作
		OPERATE_DELETE("operate_delete","操作删除") , // 操作
		OPERATE_UPDATE("operate_update","操作更新") , // 操作
		OPERATE_SAVE("operate_save" , "操作保存") , // 操作
		OPERATE_QUERY("operate_query" , "操作查询") , // 操作
		ACCESS_PAGE("access_page", "访问页面") , // 访问 
		ACCESS_ADD("access_add", "访问添加页面") , // 访问 
		ACCESS_MODIFY("access_modify", "访问更新页面") ; // 访问 

		RecordType(String type , String label){
			this.type = type ; 
			this.label = label ; 
		}
		
		private String type ; 
		private String label ; 
		
		RecordType(String type){
			this.type = type ; 
		}
	
		public String value() {
			return type ;
		}
		
		public String label() {
			return label ;
		}
	
		/**
		 * 通过类型查询对象
		 * @param type
		 * @return
		 */
		public static RecordType queryLabel(String type) {
			RecordType ce = null ; 
			for (RecordType e : RecordType.values()) {
	            if(e.value().equals(type)) {
	            	ce = e ; 
	            	break ; 
	            }
	        }
			return ce ; 
		}
	}
	
	/**
	 * 其它说明
	 * @return
	 */
	RecordType type() default RecordType.ACCESS_PAGE ;

	/**
	 * 说明
	 * @return
	 */
	String value() default "";

}