package com.alinesno.cloud.common.web.basic.auth.controller;

import java.util.List;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;

public interface HttpBasicControllerService {
	
	String MENU_TYPE_TITLE = "1" ; 
	String MENU_TYPE_FUNCTION = "0" ; 

	/**
	 * 初始化应用名称
	 * @return
	 */
	List<ManagerApplicationEntity> initApplication();

	/**
	 * 通过应用查询菜单 
	 * @param applicationId
	 * @return
	 */
	List<ManagerResourceEntity> findResourceByApplicationId(String applicationId);

}
