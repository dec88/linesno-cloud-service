package com.alinesno.cloud.common.web.basic.auth.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
public class HttpBasicDashboardController {

	private static final Logger log = LoggerFactory.getLogger(HttpBasicDashboardController.class) ; 

	@ResponseBody
	@GetMapping("/dashboard/side")
    public ResponseBean side(String resourceParent , String applicationId){
		log.debug("resourceParant:{} , applicationId:{}" , resourceParent , applicationId);
		
		List<ManagerResourceEntity> resources = 
				ApplicationContextProvider.getBean(HttpBasicControllerService.class)
				.findResourceByApplicationId(applicationId) ; 
				
		return ResponseGenerator.ok(resources); 
    }
	
	@RequestMapping("/dashboard")
    public String dashboard(Model model){
		log.debug("dashboard:{}" , model);
		
		// 查询用户权限
		List<ManagerApplicationEntity> list = 
				ApplicationContextProvider.getBean(HttpBasicControllerService.class)
				.initApplication() ; 
		
		model.addAttribute("applications", list) ; 
		
		return "dashboard/dashboard" ; 
    }
	
	@RequestMapping("/dashboard/home")
    public String home(){
		log.debug("home");
		return "dashboard/home" ;
    }
	
	@RequestMapping("/")
    public String index(){
		log.debug("index");
		return "redirect:/dashboard" ;
    }
	
	
}
