package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="manager_tenant")
public class ManagerTenantEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 租户名称
     */
	@Column(name="tenant_name")
	private String tenantName;
    /**
     * 开始时间
     */
	@Column(name="start_time")
	private Date startTime;
    /**
     * 结束时间
     */
	@Column(name="end_time")
	private Date endTime;
    /**
     * 租户状态(1正常/0禁止)
     */
	@Column(name="tenant_status")
	private String tenantStatus;
    /**
     * 租户账户
     */
	@Column(name="tenant_account")
	private String tenantAccount;
    /**
     * 租房费用
     */
	@Column(name="tenant_cost")
	private Integer tenantCost;
    /**
     * 所属城市
     */
	@Column(name="city_id")
	private String cityId;
    /**
     * 所属省份
     */
	@Column(name="province_id")
	private String provinceId;
    /**
     * 租户地址
     */
	@Column(name="tenant_address")
	private String tenantAddress;
    /**
     * 租户联系电话
     */
	@Column(name="tenant_phone")
	private String tenantPhone;


	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTenantStatus() {
		return tenantStatus;
	}

	public void setTenantStatus(String tenantStatus) {
		this.tenantStatus = tenantStatus;
	}

	public String getTenantAccount() {
		return tenantAccount;
	}

	public void setTenantAccount(String tenantAccount) {
		this.tenantAccount = tenantAccount;
	}

	public Integer getTenantCost() {
		return tenantCost;
	}

	public void setTenantCost(Integer tenantCost) {
		this.tenantCost = tenantCost;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getTenantAddress() {
		return tenantAddress;
	}

	public void setTenantAddress(String tenantAddress) {
		this.tenantAddress = tenantAddress;
	}

	public String getTenantPhone() {
		return tenantPhone;
	}

	public void setTenantPhone(String tenantPhone) {
		this.tenantPhone = tenantPhone;
	}


	@Override
	public String toString() {
		return "ManagerTenantEntity{" +
			"tenantName=" + tenantName +
			", startTime=" + startTime +
			", endTime=" + endTime +
			", tenantStatus=" + tenantStatus +
			", tenantAccount=" + tenantAccount +
			", tenantCost=" + tenantCost +
			", cityId=" + cityId +
			", provinceId=" + provinceId +
			", tenantAddress=" + tenantAddress +
			", tenantPhone=" + tenantPhone +
			"}";
	}
}
