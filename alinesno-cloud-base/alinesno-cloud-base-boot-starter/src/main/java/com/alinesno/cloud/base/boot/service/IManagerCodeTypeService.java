package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
@NoRepositoryBean
public interface IManagerCodeTypeService extends IBaseService< ManagerCodeTypeEntity, String> {

	ManagerCodeTypeEntity findByCodeTypeValue(String codeTypeValue);

}
