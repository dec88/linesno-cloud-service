package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="manager_code")
public class ManagerCodeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 代码名称
     */
	@Column(name="code_name")
	private String codeName;
    /**
     * 代码值
     */
	@Column(name="code_value")
	private String codeValue;
    /**
     * 代码类型值
     */
	@Column(name="code_type_value")
	private String codeTypeValue;
    /**
     * 代码类型名称
     */
	@Column(name="code_type_name")
	private String codeTypeName;


	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getCodeTypeValue() {
		return codeTypeValue;
	}

	public void setCodeTypeValue(String codeTypeValue) {
		this.codeTypeValue = codeTypeValue;
	}

	public String getCodeTypeName() {
		return codeTypeName;
	}

	public void setCodeTypeName(String codeTypeName) {
		this.codeTypeName = codeTypeName;
	}


	@Override
	public String toString() {
		return "ManagerCodeEntity{" +
			"codeName=" + codeName +
			", codeValue=" + codeValue +
			", codeTypeValue=" + codeTypeValue +
			", codeTypeName=" + codeTypeName +
			"}";
	}
}
