package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerSearchEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@NoRepositoryBean
public interface IManagerSearchService extends IBaseService< ManagerSearchEntity, String> {

}
