package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="manager_role")
public class ManagerRoleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
	@Column(name="role_name")
	private String roleName ;
    /**
     * 角色状态(1正常/0非法)
     */
	@Column(name="role_status")
	private Boolean roleStatus;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Boolean getRoleStatus() {
		return roleStatus;
	}

	public Boolean isRoleStatus() {
		return roleStatus;
	}

	public void setRoleStatus(Boolean roleStatus) {
		this.roleStatus = roleStatus;
	}


	@Override
	public String toString() {
		return "ManagerRoleEntity{" +
			"roleName =" + roleName +
			", roleStatus=" + roleStatus +
			"}";
	}
}
