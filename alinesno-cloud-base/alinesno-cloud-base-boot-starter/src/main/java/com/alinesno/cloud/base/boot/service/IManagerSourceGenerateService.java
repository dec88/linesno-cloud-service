package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerSourceGenerateEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
@NoRepositoryBean
public interface IManagerSourceGenerateService extends IBaseService< ManagerSourceGenerateEntity, String> {

	/**
	 * 生成代码并返回生成的代码数据包
	 * @param id
	 * @return
	 * @throws IOException 
	 */
	byte[] generatorCode(String id) throws IOException;

	/**
	 * 生成git主键地址 <br/>
	 * 思路:<br/>
	 * 1. 判断仓库是否存在,不存在则远程创建<br/>
	 * 2. 克隆远程仓库到本地,然后代码生成器生成代码并提交<br/>
	 * 3. 提交完成之后，返回远程仓库地址，同时更新数据库远程仓库地址<br/>
	 * @param id
	 * @return
	 * @throws IOException 
	 */
	String git(String id) throws IOException ; 

	/**
	 * 处理思路:<br/>
	 * 1. 配置Jenkins工程和基础命令<br/>
	 * 2. 配置完成之后，触发构建，如果配置了docker,同时返回docker镜像地址<br/>
	 * @param id
	 * @return
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	String jenkins(String id) throws IOException, URISyntaxException;

	/**
	 * 自动创建工程过程 
	 * @param app
	 * @param source
	 * @return  返回参数列表 <br/>
	 * gitPath git地址  <br/>
	 * jenkinsPath jenkins地址  <br/>
	 * dockerPath docker镜像路径 
	 */
	Map<String, String> projectAutoBuild(ManagerApplicationEntity app, ManagerSourceGenerateEntity source); 
	
}
