package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ContentNoticeEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-04 14:20:07
 */
@NoRepositoryBean
public interface IContentNoticeService extends IBaseService< ContentNoticeEntity, String> {

}
