package com.alinesno.cloud.base.boot.enable;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 实现基础服务
 * 
 * @author LuoAnDong
 * @sine 2019年4月5日 上午11:36:41
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({BaseBootConfigurationSelector.class})
@DubboComponentScan(basePackages = {"com.alinesno.cloud.**.*.service"})
public @interface EnableAlinesnoBaseBoot {

	// //扫描feign包下的，变成接口可调用包

	/**
	 * If true, the ServiceRegistry will automatically register the local server.
	 */
	boolean autoRegister() default true;
	
}
