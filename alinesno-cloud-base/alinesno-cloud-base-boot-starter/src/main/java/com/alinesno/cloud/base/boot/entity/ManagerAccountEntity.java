package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@SuppressWarnings("serial")
@Proxy(lazy = false)
@Entity
@Table(name="manager_account")
public class ManagerAccountEntity extends BaseEntity {

    /**
     * 所属者
     */
	private String owners;
    /**
     * 账户状态
     */
	@Column(name="account_status")
	private String accountStatus;
    /**
     * 最后登陆ip
     */
	@Column(name="last_login_ip")
	private String lastLoginIp;
    /**
     * 最后登陆时间
     */
	@Column(name="last_login_time")
	private String lastLoginTime;
    /**
     * 登陆名称
     */
	@Column(name="login_name" , unique=true)
	private String loginName;
    /**
     * 登陆密码
     */
	private String password;
    /**
     * 加密字符
     */
	private String salt;
    /**
     * 用户信息id
     */
	@Column(name="user_id")
	private String userId;
    /**
     * 所属角色
     */
	@Column(name="role_id")
	private String roleId;
	
    /**
     * 用户名称.
     */
	private String name;
    /**
     * 用户权限(9超级管理员/1租户权限/0用户权限)
     */
	@Column(name="role_power")
	private String rolePower;

	/**
	 * 用户手机号
	 */
	@Column(name="phone")
	private String phone ; 

	/**
	 * 用户邮箱
	 */
	@Column(name="email")
	private String email ; 
	
	/**
	 * 是否包含编辑权限
	 */
	@Column(name="has_editor")
	private String hasEditor ; 

	/**
	 * 性别
	 */
	@Column(name="sex")
	private String sex ; 

	/**
	 * 所属部门
	 */
	@Column(name="department")
	private String department ;  

	@Transient
	private Set<String> role ;
	
	@Transient
	private Set<String> permission ;
	
	public String getHasEditor() {
		return hasEditor;
	}

	public void setHasEditor(String hasEditor) {
		this.hasEditor = hasEditor;
	}

	public Set<String> getRole() {
		return role;
	}

	public void setRole(Set<String> role) {
		this.role = role;
	}

	public Set<String> getPermission() {
		return permission;
	}

	public void setPermission(Set<String> permission) {
		this.permission = permission;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRolePower() {
		return rolePower;
	}

	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ManagerAccountEntity [owners=" + owners + ", accountStatus=" + accountStatus + ", lastLoginIp="
				+ lastLoginIp + ", lastLoginTime=" + lastLoginTime + ", loginName=" + loginName + ", password="
				+ password + ", salt=" + salt + ", userId=" + userId + ", roleId=" + roleId + ", name=" + name
				+ ", rolePower=" + rolePower + ", phone=" + phone + ", email=" + email + ", sex=" + sex
				+ ", department=" + department + "]";
	}
	
}
