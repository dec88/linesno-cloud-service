package com.alinesno.cloud.base.boot.constants;

/**
 * 角色常量
 * @author LuoAnDong
 * @since 2019年9月17日 下午8:22:37
 */
public enum RolePowerEnums {

	// 	9超级管理员/1租户权限/0用户权限
	
	SUPER_ADMIN("9","超级管理员") , 
	TENANT("1","租户权限") , 
	MEMBER("0","用户权限") ; 

	private String code ; 
	private String label ; 
	
	private RolePowerEnums(String code , String label) {
		this.code = code ; 
		this.label = label ; 
	}

	public String getCode() {
		return code;
	}
	
	public String getLabel() {
		return label;
	}
	
}
