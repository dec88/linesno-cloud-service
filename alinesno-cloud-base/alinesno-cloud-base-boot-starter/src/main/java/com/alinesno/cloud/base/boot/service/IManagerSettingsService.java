package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p> 参数配置表 服务类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@NoRepositoryBean
public interface IManagerSettingsService extends IBaseService< ManagerSettingsEntity, String> {

	ManagerSettingsEntity findOne(RestWrapper restWrapper);
	
	ManagerSettingsEntity queryKey(String key , String applicationId) ; 

}
