package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-03-24 13:24:58
 */
@NoRepositoryBean
public interface IManagerDepartmentService extends IBaseService< ManagerDepartmentEntity, String> {

	List<ManagerDepartmentEntity> findAllWithApplication(RestWrapper restWrapper);

}
