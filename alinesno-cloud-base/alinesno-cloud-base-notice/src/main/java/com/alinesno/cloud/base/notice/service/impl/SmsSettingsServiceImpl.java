package com.alinesno.cloud.base.notice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.notice.entity.SmsSettingsEntity;
import com.alinesno.cloud.base.notice.service.ISmsSettingsService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Service
public class SmsSettingsServiceImpl extends IBaseServiceImpl< SmsSettingsEntity, String> implements ISmsSettingsService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SmsSettingsServiceImpl.class);

}
