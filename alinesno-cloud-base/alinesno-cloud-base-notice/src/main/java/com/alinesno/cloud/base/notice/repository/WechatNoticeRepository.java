package com.alinesno.cloud.base.notice.repository;

import com.alinesno.cloud.base.notice.entity.WechatNoticeEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface WechatNoticeRepository extends IBaseJpaRepository<WechatNoticeEntity, String> {
	
}