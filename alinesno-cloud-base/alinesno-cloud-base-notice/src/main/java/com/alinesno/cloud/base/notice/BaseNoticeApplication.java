package com.alinesno.cloud.base.notice;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alinesno.cloud.common.core.auto.EnableCore;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018-12-16 18:12:212
 */
@EnableHystrix
@EnableAsync // 开启异步任务
@EnableTransactionManagement // 开启事务
@DubboComponentScan
@EnableCore
@SpringBootApplication  // 启动入口(必须)
public class BaseNoticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseNoticeApplication.class, args);
	}
}
