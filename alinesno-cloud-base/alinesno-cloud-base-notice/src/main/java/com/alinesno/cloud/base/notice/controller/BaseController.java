package com.alinesno.cloud.base.notice.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;

/**
 * 工程基类
 * 
 * @author LuoAnDong
 * @since 2018年11月20日 上午8:10:06
 */
public class BaseController {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(BaseController.class);

	public final static String DASHBAORD_PATH = "/dashboard"; // 主面板入口

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected HttpServletResponse response;

	@Autowired
	protected HttpSession session;

	/**
	 * Spring mvc 重定向
	 * 
	 * @param url
	 * @return
	 */
	public String redirect(String url) {
		return "redirect:" + url;
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	protected ResponseBean ok() {
		return ok(null);
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	protected ResponseBean ok(String message) {
		return ResponseGenerator.ok(message);
	}

	/**
	 * 返回失败
	 * 
	 * @return
	 */
	protected ResponseBean fail() {
		return fail(null);
	}

	/**
	 * 返回失败
	 * 
	 * @return
	 */
	protected ResponseBean fail(String message) {
		return ResponseGenerator.fail(message);
	}

	@RequestMapping("/list")
    public void list(){
    }
	
	@RequestMapping("/add")
    public void add(){
    }
	
	@RequestMapping("/modify")
    public void modify(){
    }
	
	@RequestMapping("/detail")
    public void detail(){
    }
	
}
