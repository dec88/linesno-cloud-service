package com.alinesno.cloud.base.notice.service.impl;

import static org.junit.Assert.fail;

import org.databene.contiperf.PerfTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.entity.SmsSendEntity;
import com.alinesno.cloud.base.notice.euums.NoticeSendTypeEnum;
import com.alinesno.cloud.base.notice.euums.SimpleTemplate;
import com.alinesno.cloud.base.notice.service.INoticeSendService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.extra.template.Template;
import cn.hutool.extra.template.TemplateConfig;
import cn.hutool.extra.template.TemplateEngine;
import cn.hutool.extra.template.TemplateUtil;

public class NoticeSendServiceImplTest extends JUnitBase {

	@Autowired
	private INoticeSendService noticeSendServiceImpl ; 
	
	@Test
	public void testSendRealtimeSms() {
		fail("Not yet implemented");
	}

	/**
	 * 模版类型:
		验证码
		模版名称:
		用户身份验证验证码
		模版CODE:
		SMS_126620228
		模版内容:
		您的验证码：${code}，您正进行身份验证，打死不告诉别人！
		申请说明:
		用户身份验证通知。
	 * @throws Exception
	 */
	@Test
	public void testSendSms() throws Exception {
		
		SmsSendEntity sms = new SmsSendEntity() ; 
		String code = JSON.toJSONString(MapUtil.builder("code", RandomUtil.randomInt(10000)).build()) ; 
		log.debug("code:{}" , code);

		sms.setPhone("15578942583");
		sms.setTemplateCode("SMS_126620228");
		sms.setTemplateParam(JSON.toJSONString(MapUtil.builder("code", code))) ; 
			
		sms = noticeSendServiceImpl.sendSms(sms, NoticeSendTypeEnum.NOTICE_SMS_ALIYUN) ; 
		Assert.assertNotNull(sms);
	}

	@Test
	public void testSendTemplateSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendBatchSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendWechatMessage() {
		fail("Not yet implemented");
	}

	@PerfTest(invocations = 2 , threads=1)
	@Test
	public void testSendTextEmail() {
		//TemplateConfig为模板引擎的选项，可选内容有字符编码、模板路径、模板加载方式等，默认通过模板字符串渲染
		TemplateEngine engine = TemplateUtil.createEngine(new TemplateConfig());
		Template template = engine.getTemplate(SimpleTemplate.EMAIL_REGISTER_VALIDATE_TEMPLATE);
		
		EmailSendEntity email = new EmailSendEntity() ; 
		
		email.setEmailSubject("测试邮件.");
		email.setEmailReceiver("landonniao@163.com"); 

		String result = template.render(MapUtil.builder("code", RandomUtil.randomInt(10000)).build()) ; 
		email.setEmailContent(result) ; 
		
		email = noticeSendServiceImpl.sendTextEmail(email, NoticeSendTypeEnum.NOTICE_EMAIL_163) ; 
		Assert.assertNotNull(email);
	}

	@PerfTest(invocations = 2 , threads=1)
	@Test
	public void testSendHtmlEmail() throws Exception {
		
		int code = RandomUtil.randomInt(10000) ; 
		log.debug("code:{}" , code);
		
		String html = "<head>\n" + 
				"    <base target=\"_blank\" />\n" + 
				"    <style type=\"text/css\">::-webkit-scrollbar{ display: none; }</style>\n" + 
				"    <style id=\"cloudAttachStyle\" type=\"text/css\">#divNeteaseBigAttach, #divNeteaseBigAttach_bak{display:none;}</style>\n" + 
				"    <style id=\"blockquoteStyle\" type=\"text/css\">blockquote{display:none;}</style>\n" + 
				"    <style type=\"text/css\">\n" + 
				"        body{font-size:14px;font-family:arial,verdana,sans-serif;line-height:1.666;padding:0;margin:0;overflow:auto;white-space:normal;word-wrap:break-word;min-height:100px}\n" + 
				"        td, input, button, select, body{font-family:Helvetica, 'Microsoft Yahei', verdana}\n" + 
				"        pre {white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word;width:95%}\n" + 
				"        th,td{font-family:arial,verdana,sans-serif;line-height:1.666}\n" + 
				"        img{ border:0}\n" + 
				"        header,footer,section,aside,article,nav,hgroup,figure,figcaption{display:block}\n" + 
				"        blockquote{margin-right:0px}\n" + 
				"    </style>\n" + 
				"</head>\n" + 
				"<body tabindex=\"0\" role=\"listitem\">\n" + 
				"<table width=\"700\" border=\"0\" align=\"center\" cellspacing=\"0\" style=\"width:700px;\">\n" + 
				"    <tbody>\n" + 
				"    <tr>\n" + 
				"        <td>\n" + 
				"            <div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;\">\n" + 
				"                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" height=\"39\" style=\"font:12px Tahoma, Arial, 宋体;\">\n" + 
				"                    <tbody><tr><td width=\"210\"></td></tr></tbody>\n" + 
				"                </table>\n" + 
				"            </div>\n" + 
				"            <div style=\"width:680px;padding:0 10px;margin:0 auto;\">\n" + 
				"                <div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">\n" + 
				"                    <strong style=\"display:block;margin-bottom:15px;\">尊敬的用户：<span style=\"color:#f60;font-size: 16px;\"></span>您好！</strong>\n" + 
				"                    <strong style=\"display:block;margin-bottom:15px;\">\n" + 
				"                        您正在进行<span style=\"color: red\">注销账号</span>操作，请在验证码输入框中输入：<span style=\"color:#f60;font-size: 24px\">"+ code +"</span>，以完成操作。\n" + 
				"                    </strong>\n" + 
				"                </div>\n" + 
				"                <div style=\"margin-bottom:30px;\">\n" + 
				"                    <small style=\"display:block;margin-bottom:20px;font-size:12px;\">\n" + 
				"                        <p style=\"color:#747474;\">\n" + 
				"                            注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全\n" + 
				"                            <br>（工作人员不会向你索取此验证码，请勿泄漏！)\n" + 
				"                        </p>\n" + 
				"                    </small>\n" + 
				"                </div>\n" + 
				"            </div>\n" + 
				"            <div style=\"width:700px;margin:0 auto;\">\n" + 
				"                <div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">\n" + 
				"                    <p>此为系统邮件，请勿回复<br>\n" + 
				"                        请保管好您的邮箱，避免账号被他人盗用\n" + 
				"                    </p>\n" + 
				"                    <p>Alinesno 企业统一平台研发团队</p>\n" + 
				"                </div>\n" + 
				"            </div>\n" + 
				"        </td>\n" + 
				"    </tr>\n" + 
				"    </tbody>\n" + 
				"</table>\n" + 
				"</body>" ; 
		
		EmailSendEntity email = new EmailSendEntity() ; 
		
		email.setEmailSubject("测试发送[HTML]邮件");
		email.setEmailReceiver("landonniao@163.com"); 
		email.setEmailContent(html) ; 
		
		email = noticeSendServiceImpl.sendHtmlEmail(email, NoticeSendTypeEnum.NOTICE_EMAIL_163) ; 
		Assert.assertNotNull(email);
	}

	@Test
	public void testSendTemplateEmail() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendBatchTextEmail() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendBatchHtmlEmail() {
		fail("Not yet implemented");
	}

}
