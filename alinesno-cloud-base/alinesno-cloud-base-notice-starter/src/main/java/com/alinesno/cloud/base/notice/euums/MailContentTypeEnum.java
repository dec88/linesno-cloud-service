package com.alinesno.cloud.base.notice.euums;

/**
 * 邮件内容的类型
 * @author LuoAnDong
 * @since 2019年3月24日 下午8:27:07
 */
public enum MailContentTypeEnum {
	
    //text格式
    TEXT("text"),
    //html格式
    HTML("html"),
    //模板
    TEMPLATE("template")
    ;

    private String value;

    MailContentTypeEnum(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}