package com.alinesno.cloud.base.notice.euums;

/**
 * 邮件内容的类型
 * @author LuoAnDong
 * @since 2019年3月24日 下午8:27:07
 */
public enum NoticeSendTypeEnum {
	
    NOTICE_EMAIL_163("email163"), // 163邮箱通知
    NOTICE_EMAIL_ALIYUN("emailAliyun"),  // 阿里云通知 
    NOTICE_WECHAT("webchat") ,  // 微信通知
    NOTICE_SMS_ALIYUN("smsAliyun") ,  // 阿里云短信通知
    ;

    private String value;

    NoticeSendTypeEnum(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}