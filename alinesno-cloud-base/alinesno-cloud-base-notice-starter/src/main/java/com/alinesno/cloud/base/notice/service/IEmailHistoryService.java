package com.alinesno.cloud.base.notice.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.notice.entity.EmailHistoryEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@NoRepositoryBean
public interface IEmailHistoryService extends IBaseService<EmailHistoryEntity, String> {

}
