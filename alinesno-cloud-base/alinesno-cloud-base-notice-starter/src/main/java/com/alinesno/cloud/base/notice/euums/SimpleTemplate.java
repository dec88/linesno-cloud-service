package com.alinesno.cloud.base.notice.euums;

/**
 * 简单通知模板
 * @author LuoAnDong
 * @since 2019年10月2日 下午12:16:17
 */
public interface SimpleTemplate {

	String EMAIL_REGISTER_VALIDATE_TEMPLATE = "您的验证码：${code}，您正进行身份验证，打死不告诉别人！" ; 
	
}
