package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@Entity
@Table(name="sms_fail")
public class SmsFailEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	@Column(name="biz_id")
	private String bizId;
	@Column(name="business_id")
	private String businessId;
	private String content;
	@Column(name="out_id")
	private String outId;
	private String phone;
	@Column(name="sign_name")
	private String signName;
	private String template;
	@Column(name="template_code")
	private String templateCode;
	@Column(name="validate_code")
	private String validateCode;


	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getOutId() {
		return outId;
	}

	public void setOutId(String outId) {
		this.outId = outId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}


	@Override
	public String toString() {
		return "SmsFailEntity{" +
			"bizId=" + bizId +
			", businessId=" + businessId +
			", content=" + content +
			", outId=" + outId +
			", phone=" + phone +
			", signName=" + signName +
			", template=" + template +
			", templateCode=" + templateCode +
			", validateCode=" + validateCode +
			"}";
	}
}
