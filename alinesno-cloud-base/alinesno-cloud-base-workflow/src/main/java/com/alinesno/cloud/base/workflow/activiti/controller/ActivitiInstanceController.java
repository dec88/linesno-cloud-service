package com.alinesno.cloud.base.workflow.activiti.controller;

import com.alinesno.cloud.base.workflow.activiti.WorkFlowService;
import com.alinesno.cloud.base.workflow.service.IWorkflowBpsService;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import org.activiti.engine.repository.Model;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Activiti工作流流程实例模型操作
 * 
 * @author LuoAnDong
 * @since 2019年9月1日 下午4:24:50
 */
@RestController
@RequestMapping(value = "/activiti/instance")
public class ActivitiInstanceController {

	private static final Logger log = LoggerFactory.getLogger(ActivitiInstanceController.class);

	@Autowired
	private IWorkflowBpsService workflowBpsService ; 
	
	@Autowired
	private WorkFlowService workFlowService ; 
	
	/**
	 * 查询户端分页
	 * 
	 * @return
	 */
	@ResponseBody
	@PostMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page:{}", ToStringBuilder.reflectionToString(page));
		
		Page<Map<String , Object>> pageMap = workflowBpsService.findActivitiInstance(PageRequest.of((page.getPageNum()-1)*page.getPageSize(), page.getPageSize())) ; 
		
		page.setRows(pageMap.getContent());
		page.setTotal((int) pageMap.getTotalElements());
		page.setCode(HttpStatus.OK.value());
		
		return page ; 
	}
	
	/**
	 * 修改状态
	 * @return
	 */
	@ResponseBody
	@GetMapping("/updateBpsStatus")
    public ResponseBean updateBpsStatus(String state ,String deploymentId){
		
		Assert.hasLength(deploymentId, "工作流主键不能为空.");
		Assert.hasLength(state , "状态不能为空.");
		
		String b = workFlowService.updateState(state , deploymentId) ; 
		return ResponseGenerator.ok(b) ; 
    }

}
