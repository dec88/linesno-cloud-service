package com.alinesno.cloud.base.workflow;

import com.alinesno.cloud.common.core.auto.EnableAlinesnoCommonCore;
import com.alinesno.cloud.common.core.auto.EnableAlinesnoDubboScan;
import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动入口
 * @EnableSwagger2 //开启swagger2 
 * @author LuoAnDong 
 * @since 2018-12-16 17:12:901
 */
@EnableAsync // 开启异步任务
@EnableAlinesnoCommonCore
@EnableAlinesnoDubboScan
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class BaseWorkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseWorkflowApplication.class, args);
		
		// 启动 Provider 容器，注意这里的 Main 是 com.alibaba.dubbo.container 包下的
        // Main.main(args);
	}
	
}
