package com.alinesno.cloud.base.workflow.controller;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
public class DashboardController {

	private static final Logger log = LoggerFactory.getLogger(DashboardController.class) ; 
	
	@ResponseBody
	@GetMapping("/dashboard/side")
    public ResponseBean side(String resourceParent , String applicationId , HttpServletRequest request){
		log.debug("resourceParant:{} , applicationId:{}" , resourceParent , applicationId);
//		return ResponseGenerator.ok(resources.getSubResource()) ; 
		return ResponseGenerator.fail(null) ; 
    }
	
	@RequestMapping("/dashboard")
    public String dashboard(Model model , HttpServletRequest request){
		log.debug("dashboard");
	
		model.addAttribute("applicationTitle", "工作流管理平台") ; 
		
		return "dashboard/dashboard" ; 
    }
	
	@RequestMapping("/dashboard/home")
    public String home(){
		log.debug("home");
		return "dashboard/home" ;
    }
	
	
	
}
