package com.alinesno.cloud.base.workflow.repository;

import com.alinesno.cloud.base.workflow.entity.WorkflowBpsEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Map;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author WeiXiaoJin
 * @since 2019-09-02 08:11:19
 */
public interface WorkflowBpsRepository extends IBaseJpaRepository<WorkflowBpsEntity, String> {

	/**
	 * 使用原生sql查询 
	 * @param pageable
	 * @return
	 */
	@Query(value="select a.maxversion as MAXVERSION , a.deploytime as DEPLOYTIME , def.*   from ( " + 
			"	select def.KEY_ , max(def.VERSION_) as maxversion  , max(dep.DEPLOY_TIME_) as deploytime " + 
			"	from act_re_procdef def , act_re_deployment dep " + 
			"	where def.DEPLOYMENT_ID_=dep.ID_ " + 
			"	GROUP BY def.KEY_ " + 
			") a ,act_re_procdef def " + 
			"where def.VERSION_=a.maxversion and def.KEY_=a.KEY_ " + 
			"ORDER BY a.KEY_ ASC" , nativeQuery = true)
	Page<Map<String , Object>> findActivitiInstance(Pageable pageable) ; 
	
}
