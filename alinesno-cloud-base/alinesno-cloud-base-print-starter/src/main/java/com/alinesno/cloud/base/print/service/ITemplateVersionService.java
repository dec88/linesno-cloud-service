package com.alinesno.cloud.base.print.service;

import com.alinesno.cloud.base.print.entity.TemplateVersionEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@NoRepositoryBean
public interface ITemplateVersionService extends IBaseService<TemplateVersionEntity, String> {

}
