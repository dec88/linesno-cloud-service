package com.alinesno.cloud.base.storage.service.strategy;

import com.alinesno.cloud.base.storage.service.StorageService;
import com.alinesno.cloud.common.core.junit.JUnitBase;
import net.sf.jmimemagic.*;
import org.junit.Test;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Files;

import static org.junit.Assert.fail;

public class MinIOStrageServiceImplTest extends JUnitBase {

	@Resource(name = "minioStorageService")
	private StorageService minIOStrageServiceImpl ; 
	
	private String bucket = "test-001" ; 

	@SuppressWarnings("static-access")
	@Test
    public void test() throws MagicParseException, MagicMatchNotFoundException, MagicException, IOException {
		String pathname = "/Users/luodong/Desktop/logo_aws.png" ; 
			   pathname = "/Users/luodong/Desktop/2019_PDF.pdf" ; 

		Magic parser = new Magic() ;
		MagicMatch match = parser.getMagicMatch(new File(pathname),false);
		System.out.println("第一种Magic: " + match.getMimeType()) ;

        String type = new MimetypesFileTypeMap().getContentType(new File(pathname));
        System.out.println("第二种javax.activation: "+type);

		String s = Files.probeContentType(new File(pathname).toPath());
		System.out.println("第三种java.nio: "+s);

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentType = fileNameMap.getContentTypeFor(pathname);
        System.out.println("第四种java.net: "+contentType);
    }
	
	@Test
	public void testUploadDataStringString() throws Exception {
		log.debug("minioStorageService:{}" , minIOStrageServiceImpl);
		
		String fileLoalAbcPath = "/Users/luodong/Desktop/logo_aws.png" ; 
			   fileLoalAbcPath = "/Users/luodong/Desktop/2019_PDF.pdf" ; 
		
		minIOStrageServiceImpl.uploadData(fileLoalAbcPath, bucket) ; 
		
	}
	
	@Test
	public void testPresignedUrl() {
		String filePath = "2019/08/28/e1098512-78e8-426a-9172-ceac5e1523cb.pdf" ;  
		String url = minIOStrageServiceImpl.presignedUrl(filePath, bucket) ; 
		
		log.debug("url:{}" , url);
	}

	@Test
	public void testDownloadData() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteData() {
		fail("Not yet implemented");
	}

	@Test
	public void testUploadDataStringStringString() {
		fail("Not yet implemented");
	}

}
