package com.alinesno.cloud.base.logger.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.logger.entity.LogLoginEntity;
import com.alinesno.cloud.base.logger.service.ILogLoginService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Service
public class LogLoginServiceImpl extends IBaseServiceImpl<LogLoginEntity, String> implements ILogLoginService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LogLoginServiceImpl.class);

}
