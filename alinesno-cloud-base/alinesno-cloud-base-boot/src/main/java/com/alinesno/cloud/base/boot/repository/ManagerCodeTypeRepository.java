package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
public interface ManagerCodeTypeRepository extends IBaseJpaRepository<ManagerCodeTypeEntity, String> {

	ManagerCodeTypeEntity findByCodeTypeValue(String codeTypeValue);

}
