package com.alinesno.cloud.base.boot.service.impl;

import java.util.List;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.base.boot.repository.ManagerAccountRoleRepository;
import com.alinesno.cloud.base.boot.service.IManagerAccountRoleService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
@Service
public class ManagerAccountRoleServiceImpl extends IBaseServiceImpl< ManagerAccountRoleEntity, String> implements IManagerAccountRoleService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ManagerAccountRoleServiceImpl.class);
	
	@Autowired
	private ManagerAccountRoleRepository managerAccountRoleRepository; 
	
	@Override
	public List<ManagerAccountRoleEntity> findAllByAccountId(String accountId) {
		return managerAccountRoleRepository.findAllByAccountId(accountId);
	}

	@Override
	public void deleteByAccountId(String id) {
		managerAccountRoleRepository.deleteByAccountId(id); // .deleteById(id);
	}

}
