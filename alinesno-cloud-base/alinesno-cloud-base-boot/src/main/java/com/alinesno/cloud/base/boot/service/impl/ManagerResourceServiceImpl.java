package com.alinesno.cloud.base.boot.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.enums.MenuEnums;
import com.alinesno.cloud.base.boot.enums.ResourceTypeEnmus;
import com.alinesno.cloud.base.boot.repository.ManagerResourceRepository;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class ManagerResourceServiceImpl extends IBaseServiceImpl< ManagerResourceEntity, String> implements IManagerResourceService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerResourceServiceImpl.class);

	@Autowired
	private IManagerAccountService managerAccountService ; 
	
	@Autowired
	private ManagerResourceRepository managerResourceRepository ; 
	
	/**
	 * 通过一级目录构建菜单 
	 */
	@Override
	public ManagerResourceEntity findMenus(String resourceParent , String applicationId) {
		return this.findMenus(resourceParent, applicationId, null); 
	}

	@Override
	public ManagerResourceEntity findMenus(String resourceParent, String applicationId, String accountId) {
		
		Optional<ManagerResourceEntity> optional = null ; 
		if(StringUtils.isNotBlank(applicationId)) {
			Assert.hasLength(applicationId, "所属应用不能为空.");
			optional = managerResourceRepository.findByResourceParentAndApplicationId(ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value , applicationId)  ; 
		}else {
			Assert.hasLength(resourceParent , "菜单父类不能为空.");
			optional = jpa.findById(resourceParent)  ; 
		}
		
		if(optional.isPresent()) {
			ManagerResourceEntity r =  optional.get() ; 
			
			if(StringUtils.isNotBlank(accountId)) {
				Optional<ManagerAccountEntity> ac =  managerAccountService.findById(accountId) ; 
				if(ac.isPresent()) {
					ManagerAccountEntity a = ac.get() ; 
					if(MenuEnums.MENU_PLATFORM.value.equals(a.getRolePower())) {
						findSubMenus(r) ; 
					}else {
						List<ManagerResourceEntity> subResource = managerResourceRepository.findAllByApplicationAndAccount(applicationId , accountId) ; 
						for(ManagerResourceEntity e : subResource) {
							log.debug("e:{}" , ToStringBuilder.reflectionToString(e));
						}
						findSubMenus(r , subResource) ; 
					}
				}
			}else {
				findSubMenus(r) ; 
			}
			return r ; 
		}
		
		return null;
	}

	/**
	 * 查询菜单
	 * @param r
	 * @param subResource
	 */
	private void findSubMenus(ManagerResourceEntity r, List<ManagerResourceEntity> subResource) {
		List<ManagerResourceEntity> subItem = findAllByResourceParent(subResource ,r.getId()) ; 
		if(subResource != null && subResource.size() > 0) {
			r.setSubResource(subItem); 
			for(ManagerResourceEntity e : subItem) {
				findSubMenus(e , subResource) ; 
			}
		}
	}
	private List<ManagerResourceEntity> findAllByResourceParent(List<ManagerResourceEntity> subResource, String parentId) {
		List<ManagerResourceEntity> list = new ArrayList<ManagerResourceEntity>() ; 
		for(ManagerResourceEntity l : subResource) {
			if(parentId.equals(l.getResourceParent())) {
				list.add(l) ; 
			}
		}
		return list ;
	}

	/**
	 * 递归查询所有菜单子类
	 * @return
	 */
	private void findSubMenus(ManagerResourceEntity resource) {
		List<ManagerResourceEntity> subResource = managerResourceRepository.findAllByResourceParent(resource.getId()) ; 
		if(subResource != null && subResource.size() > 0) {
			resource.setSubResource(subResource); 
			for(ManagerResourceEntity r : subResource) {
				findSubMenus(r) ; 
			}
		}
	}

	@Override
	public void deleteByApplicationId(String id) {
		managerResourceRepository.deleteByApplicationId(id) ; 
	}

	@Override
	public ManagerResourceEntity findMenusByApplicationAndAccount(String resourceParent, String applicationId, String accountId) {
		ManagerResourceEntity e = this.findMenus(resourceParent, applicationId, accountId) ;
		
		log.debug("e:{}" , e);
		
		return e ; 
	}

	@Override
	public List<ManagerResourceEntity> findResoucePermission(ManagerAccountEntity account, String pageId) {
		// TODO 待加权限判断
		List<ManagerResourceEntity> list = managerResourceRepository.findAllByResourceParent(pageId) ; 
		
		return list ;
	}


}
