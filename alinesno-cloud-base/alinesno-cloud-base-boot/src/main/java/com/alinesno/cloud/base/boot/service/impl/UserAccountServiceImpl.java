package com.alinesno.cloud.base.boot.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.alinesno.cloud.base.boot.entity.UserAccountEntity;
import com.alinesno.cloud.base.boot.service.IUserAccountService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;

/**
 * <p> 基础账户表 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class UserAccountServiceImpl extends IBaseServiceImpl< UserAccountEntity, String> implements IUserAccountService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(UserAccountServiceImpl.class);

	@Override
	public boolean registUser(String phone, String password, String phoneCode) {
		log.debug("phone:{} , password:{} , phoneCode:{}" , phone , password , phoneCode);
		
		Assert.hasLength(phone , "手机号不能为空.");
		Assert.hasLength(password, "密码不能为空.");
		Assert.hasLength(phoneCode , "验证码不能为空.");

		String salt = RandomUtil.randomNumbers(6)+"" ; 
		
		UserAccountEntity e = new UserAccountEntity() ;
		e.setLoginAccount(phone);
		e.setPhoneCode(phoneCode); 
		e.setSalt(salt); 
		e.setLoginPassword(SecureUtil.md5(SecureUtil.md5(password)+salt));
		
		e = this.save(e) ; 
		
		return e.getId()==null?false:true;
	}

}
