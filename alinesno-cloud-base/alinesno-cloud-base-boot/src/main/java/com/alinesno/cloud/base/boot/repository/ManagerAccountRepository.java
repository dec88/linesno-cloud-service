package com.alinesno.cloud.base.boot.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
public interface ManagerAccountRepository extends IBaseJpaRepository<ManagerAccountEntity, String> {

	ManagerAccountEntity findByLoginName(String loginName);

	/**
	 * 更新用户密码
	 * @param newPassword
	 * @param userId
	 */
	@Modifying
	@Transactional
	@Query("UPDATE ManagerAccountEntity e SET e.password = ?1 WHERE e.id = ?2")
	void updateManagerAccountPassword(String newPassword, String userId);

}
