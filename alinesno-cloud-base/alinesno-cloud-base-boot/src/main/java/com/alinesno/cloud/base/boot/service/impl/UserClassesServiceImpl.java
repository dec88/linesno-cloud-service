package com.alinesno.cloud.base.boot.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.boot.entity.UserClassesEntity;
import com.alinesno.cloud.base.boot.service.IUserClassesService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class UserClassesServiceImpl extends IBaseServiceImpl< UserClassesEntity, String> implements IUserClassesService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(UserClassesServiceImpl.class);

}
