/*
 Navicat Premium Data Transfer

 Source Server         : localhost_root
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost
 Source Database       : alinesno_cloud_base_boot

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : utf-8

 Date: 04/08/2019 09:12:53 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `content_comments`
-- ----------------------------
DROP TABLE IF EXISTS `content_comments`;
CREATE TABLE `content_comments` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `comment_author` varchar(255) DEFAULT NULL,
  `comment_author_email` varchar(255) DEFAULT NULL,
  `comment_author_ip` varchar(255) DEFAULT NULL,
  `comment_author_url` varchar(255) DEFAULT NULL,
  `comment_content` varchar(255) DEFAULT NULL,
  `comment_date` datetime DEFAULT NULL,
  `comment_date_gmt` datetime DEFAULT NULL,
  `comment_post_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `content_links`
-- ----------------------------
DROP TABLE IF EXISTS `content_links`;
CREATE TABLE `content_links` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `link_description` varchar(255) DEFAULT NULL,
  `link_image` varchar(255) DEFAULT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `link_notes` varchar(255) DEFAULT NULL,
  `link_owner` bigint(20) DEFAULT NULL,
  `link_rating` int(11) DEFAULT NULL,
  `link_target` varchar(255) DEFAULT NULL,
  `link_updated` datetime DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_visible` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `content_notice`
-- ----------------------------
DROP TABLE IF EXISTS `content_notice`;
CREATE TABLE `content_notice` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `notice_author` varchar(255) DEFAULT NULL,
  `notice_content` varchar(255) DEFAULT NULL,
  `notice_date` datetime DEFAULT NULL,
  `notice_modifield` datetime DEFAULT NULL,
  `notice_name` varchar(255) DEFAULT NULL,
  `notice_password` varchar(255) DEFAULT NULL,
  `notice_status` int(11) DEFAULT NULL,
  `notice_title` varchar(255) DEFAULT NULL,
  `notice_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `content_notice`
-- ----------------------------
BEGIN;
INSERT INTO `content_notice` VALUES ('563767373023150080', '2019-04-05 08:50:16', '1', null, null, null, '0', '1', null, '2019-04-05 08:50:16', null, '234234234234', null, null, null, null, null, null, null), ('563767547791409152', '2019-04-05 08:50:58', '21', null, null, null, '0', '1', null, '2019-04-05 08:50:58', null, '23413asdfasdfasdfasdf', null, null, null, null, null, null, null), ('563768365793935360', '2019-04-04 16:54:13', '123', null, null, null, '0', '1', null, '2019-04-05 08:56:27', null, '123123123123', null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `content_post_type`
-- ----------------------------
DROP TABLE IF EXISTS `content_post_type`;
CREATE TABLE `content_post_type` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `type_add_time` datetime DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `type_status` int(11) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `content_post_type`
-- ----------------------------
BEGIN;
INSERT INTO `content_post_type` VALUES ('563750776812339200', '2019-04-05 07:44:19', '0', null, null, null, '0', '1', null, '2019-04-05 07:44:19', null, '通知公告', null, null), ('563755805933830144', '2019-04-05 00:04:18', '0', null, null, null, '0', '0', null, '2019-04-05 08:05:06', null, '校园风景', null, null), ('564499705170493440', '2019-04-07 09:20:18', '123123', null, null, null, '0', '1', null, '2019-04-07 09:20:18', null, '大学生活动中心', null, '563755805933830144'), ('564578610426413056', '2019-04-07 14:33:50', '', null, null, null, '0', '1', null, '2019-04-07 14:33:50', null, '招生就业', null, ''), ('564578978992488448', '2019-04-07 14:35:18', '23123', null, null, null, '0', '1', null, '2019-04-07 14:35:18', null, '成人高考招生', null, '564578610426413056');
COMMIT;

-- ----------------------------
--  Table structure for `content_postmeta`
-- ----------------------------
DROP TABLE IF EXISTS `content_postmeta`;
CREATE TABLE `content_postmeta` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `content_posts`
-- ----------------------------
DROP TABLE IF EXISTS `content_posts`;
CREATE TABLE `content_posts` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL,
  `post_author` varchar(255) DEFAULT NULL,
  `post_content` varchar(255) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `post_mime_type` varchar(255) DEFAULT NULL,
  `post_modifield` datetime DEFAULT NULL,
  `post_name` varchar(255) DEFAULT NULL,
  `post_password` varchar(255) DEFAULT NULL,
  `post_status` int(11) DEFAULT NULL,
  `post_title` varchar(255) DEFAULT NULL,
  `post_type` varchar(255) DEFAULT NULL,
  `to_ping` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `content_posts`
-- ----------------------------
BEGIN;
INSERT INTO `content_posts` VALUES ('564505214170693632', '2019-04-07 09:42:11', '0', null, null, null, '0', '1', null, '2019-04-07 09:42:11', null, '563707888443326464', '2019年4月7日上午，接凉山州森林草原防火指挥部办公室报告，雅砻江镇立尔村森林火灾火场受大风影响，东北面火烧迹地内悬崖处前期人工增雨降温降雪覆盖的隐蔽烟点复燃，燃烧腐烂木桩滚落至崖下引燃迹地内未燃尽的树木，形成树冠火，有飞火吹到火场外东面林地燃烧。', null, '564499705170493440', null, null, null, null, '大风致隐蔽烟点复燃 官兵支援', null, null), ('564505683559448576', '2019-04-07 09:44:03', '0', null, null, null, '0', '1', null, '2019-04-07 09:44:03', null, '563707888443326464', '而该事件牵扯到的其他艺人也都得到了惨痛的代价。龙俊亨宣布退出组合highlight，接受警方调查；崔钟训也退出了FTISLAND，并退出娱乐圈，接受警方调查；李宗泫通过公司公开致歉，因为不够诚恳，而且没提出退团，还被韩国网友吐槽脸皮厚……', null, '564499705170493440', null, null, null, null, '前成员胜利豪宅曝光 从中可窥见他的野心', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `info_address`
-- ----------------------------
DROP TABLE IF EXISTS `info_address`;
CREATE TABLE `info_address` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_code`
-- ----------------------------
DROP TABLE IF EXISTS `info_code`;
CREATE TABLE `info_code` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code_name` varchar(255) DEFAULT NULL,
  `code_type` varchar(255) DEFAULT NULL,
  `code_value` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_job`
-- ----------------------------
DROP TABLE IF EXISTS `info_job`;
CREATE TABLE `info_job` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `job_parent` varchar(255) DEFAULT NULL,
  `job_type` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_professional`
-- ----------------------------
DROP TABLE IF EXISTS `info_professional`;
CREATE TABLE `info_professional` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `professional_prop` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_school`
-- ----------------------------
DROP TABLE IF EXISTS `info_school`;
CREATE TABLE `info_school` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `school_address` varchar(255) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_owner` varchar(255) DEFAULT NULL,
  `school_owner_code` varchar(255) DEFAULT NULL,
  `school_properties` varchar(255) DEFAULT NULL,
  `school_properties_code` varchar(255) DEFAULT NULL,
  `school_province` varchar(255) DEFAULT NULL,
  `school_province_code` varchar(255) DEFAULT NULL,
  `school_type` varchar(255) DEFAULT NULL,
  `school_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_settings`
-- ----------------------------
DROP TABLE IF EXISTS `info_settings`;
CREATE TABLE `info_settings` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `info_zipcode`
-- ----------------------------
DROP TABLE IF EXISTS `info_zipcode`;
CREATE TABLE `info_zipcode` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_account`
-- ----------------------------
DROP TABLE IF EXISTS `manager_account`;
CREATE TABLE `manager_account` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `last_login_time` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `role_power` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_account`
-- ----------------------------
BEGIN;
INSERT INTO `manager_account` VALUES ('564714047887376384', '2019-04-07 23:32:01', '0', null, null, null, '0', '0', '0', '2019-04-07 23:32:01', '1', '127.0.0.1', '2018-12-12 12:12:12', 'admin@gmail.com', '超级管理员', null, 'admin', null, '9', '2312', null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_account_role`
-- ----------------------------
DROP TABLE IF EXISTS `manager_account_role`;
CREATE TABLE `manager_account_role` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_account_role`
-- ----------------------------
BEGIN;
INSERT INTO `manager_account_role` VALUES ('564739196401483776', '2019-04-08 01:11:57', '0', null, null, null, '0', '0', '0', '2019-04-08 01:11:57', '564714047887376384', '564708772195336192');
COMMIT;

-- ----------------------------
--  Table structure for `manager_application`
-- ----------------------------
DROP TABLE IF EXISTS `manager_application`;
CREATE TABLE `manager_application` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `application_desc` varchar(255) DEFAULT NULL,
  `application_icons` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_application`
-- ----------------------------
BEGIN;
INSERT INTO `manager_application` VALUES ('564474243555786752', '2019-04-06 23:39:07', null, null, null, null, '0', '0', null, '2019-04-07 07:42:37', '跑腿管理系统后台管理及运维', 'fa fa-envelope-open', '跑腿管理系统', null), ('564392969700900864', null, null, null, null, null, '0', '0', null, '2019-04-07 15:00:00', '基础的管理功能，实现基础平台配置，权限管理及费用监控配置等', 'fa fa-ravelry', '管理应用', null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_code`
-- ----------------------------
DROP TABLE IF EXISTS `manager_code`;
CREATE TABLE `manager_code` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code_name` varchar(255) DEFAULT NULL,
  `code_type_name` varchar(255) DEFAULT NULL,
  `code_type_value` varchar(255) DEFAULT NULL,
  `code_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_code`
-- ----------------------------
BEGIN;
INSERT INTO `manager_code` VALUES ('11d587652d444d488a46ff8396241bf2', null, '000001', null, null, null, '0', '0', null, null, '输入', '搜索项类型', 'common_select_type', 'input'), ('2b142335256148ad92e4290d8b3d128b', null, '000001', null, null, null, '0', '0', null, null, '账户权限', '账户权限', 'account_role_power', '0'), ('2cc0cecd50e24d1e91e19c64be068842', null, '000001', null, null, null, '0', '0', null, null, '租户权限', '租房权限', 'account_role_power', '1'), ('564584553318973440', '2019-04-07 14:57:27', '', null, null, null, '0', '1', null, '2019-04-07 14:57:27', '超级管理', null, 'role_power', '9'), ('3c6e687a39f941b3ba784dafd2c25652', null, '000001', null, null, null, '0', '0', null, null, '下拉', '搜索项类型', 'common_select_type', 'select'), ('61748afba7584bb2a8181b62b2514a72', null, '000001', null, null, null, '0', '0', null, null, '超级管理员', '平台管理员类型', 'manager_type', '1'), ('7e6c61229403461a978683eb56f7a3d9', null, '000001', null, null, null, '0', '0', null, null, '禁止', '公共状态', 'common_status', '0'), ('d7970178cd354c349c5f40da319b194e', null, '000001', null, null, null, '0', '0', null, null, '正常', '公共状态', 'common_status', '1'), ('564477465297158144', '2019-04-06 23:51:55', '1', null, null, null, '0', '0', null, '2019-04-07 08:21:18', '平台', null, 'menus_type', '9'), ('564477599837847552', '2019-04-06 15:52:27', '9', null, null, null, '0', '0', null, '2019-04-07 08:20:48', '标题', null, 'menus_type', '1'), ('564477678673985536', '2019-04-06 23:52:46', '0', null, null, null, '0', '0', null, '2019-04-07 08:21:03', '功能', null, 'menus_type', '0'), ('564485658798718976', '2019-04-07 08:24:29', '9', null, null, null, '0', '1', null, '2019-04-07 08:24:29', '按钮', null, 'menus_type', '8'), ('564584318853185536', '2019-04-07 14:56:31', '0', null, null, null, '0', '1', null, '2019-04-07 14:56:31', '租户权限', null, 'role_power', '1'), ('564584467096666112', '2019-04-07 14:57:06', '', null, null, null, '0', '1', null, '2019-04-07 14:57:06', '用户权限', null, 'role_power', '1'), ('543392305650860032', '2019-02-08 11:27:01', null, null, null, null, '0', '1', null, '2019-02-08 11:27:01', '测试代码名称', null, 'manager_type', '999'), ('543393259423006720', '2019-02-08 11:30:49', null, null, null, null, '0', '1', null, '2019-02-08 11:30:49', '测试代码名称', null, 'manager_type', '0909'), ('d7970178cd354c349c5f40da319b194f', null, '000001', null, null, null, '0', '0', null, null, '正常', '公共状态', 'has_status', '1'), ('543421471318343680', '2019-02-08 13:22:55', null, null, null, null, '0', '1', null, '2019-02-08 13:22:55', '测试代码名称', null, 'manager_type', '000000000'), ('543421977990266880', '2019-02-08 13:24:56', null, null, null, null, '0', '1', null, '2019-02-08 13:24:56', '测试代码名称', null, 'manager_type', '000000000'), ('543422106679902208', '2019-02-08 13:25:27', null, null, null, null, '0', '1', null, '2019-02-08 13:25:27', '测试代码名称', null, 'manager_type', '李四'), ('543422194210832384', '2019-02-08 13:25:47', null, null, null, null, '0', '1', null, '2019-02-08 13:25:47', '测试代码名称', null, 'manager_type', '李四'), ('543424362808606720', '2019-02-08 13:34:24', null, null, null, null, '0', '1', null, '2019-02-08 13:34:24', '测试代码名称', null, 'manager_type', '5555555'), ('7e6c61229403461a978683eb56f7a3d8', null, '000001', null, null, null, '0', '0', null, null, '禁止', '公共状态', 'has_status', '0');
COMMIT;

-- ----------------------------
--  Table structure for `manager_code_type`
-- ----------------------------
DROP TABLE IF EXISTS `manager_code_type`;
CREATE TABLE `manager_code_type` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code_type_name` varchar(255) DEFAULT NULL,
  `code_type_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_code_type`
-- ----------------------------
BEGIN;
INSERT INTO `manager_code_type` VALUES ('543373042005311488', '2019-02-08 10:10:29', '0', null, null, null, '0', '0', '0', '2019-02-08 10:10:29', '公共状态', 'common_status'), ('543373042080808960', '2019-02-08 10:10:29', '0', null, null, null, '0', '0', '0', '2019-02-08 10:10:29', '管理员类型', 'manager_type'), ('563488858147127296', '2019-04-04 14:23:33', '123', null, null, null, '0', '1', null, '2019-04-04 14:23:33', '3422342', '34234'), ('564476753548935168', '2019-04-07 07:49:06', '0', null, null, null, '0', '1', null, '2019-04-07 07:49:06', '菜单类型', 'menus_type'), ('564584142335901696', '2019-04-07 14:55:49', '0', null, null, null, '0', '1', null, '2019-04-07 14:55:49', '角色类型', 'role_power');
COMMIT;

-- ----------------------------
--  Table structure for `manager_department`
-- ----------------------------
DROP TABLE IF EXISTS `manager_department`;
CREATE TABLE `manager_department` (
  `pid` bigint(20) DEFAULT NULL COMMENT '父部门id',
  `pids` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父级ids',
  `simple_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '简称',
  `full_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '全称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `versions` int(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_department`
-- ----------------------------
BEGIN;
INSERT INTO `manager_department` VALUES ('0', null, null, '123123', '1231', null, null, '563494237866295296', '2019-04-04 14:44:56', '123123', null, null, null, '0', '1', null, '2019-04-04 14:44:56'), ('0', null, null, '4234234', '234234', null, null, '563494306740961280', '2019-04-04 14:45:12', '4323423', null, null, null, '0', '1', null, '2019-04-04 14:45:12'), ('563494306740961280', null, null, '234234', '234234', null, null, '563495202069676032', '2019-04-04 14:48:45', '', null, null, null, '0', '1', null, '2019-04-04 14:48:45'), ('563494306740961280', null, null, '23123', '123123', null, null, '563495299566272512', '2019-04-04 14:49:09', '123123', null, null, null, '0', '1', null, '2019-04-04 14:49:09'), ('563495202069676032', null, null, '234234', '234234', null, null, '563495642006028288', '2019-04-04 14:50:30', '4234234', null, null, null, '0', '1', null, '2019-04-04 14:50:30'), ('563494306740961280', null, null, '234234', '234234', null, null, '563496447731826688', '2019-04-04 14:53:42', '234234', null, null, null, '0', '1', null, '2019-04-04 14:53:42'), ('563494306740961280', null, null, '234234', '234234', null, null, '563497077137473536', '2019-04-04 14:56:12', '234234', null, null, null, '0', '1', null, '2019-04-04 14:56:12'), ('563495642006028288', null, null, '234234234', '234234', null, null, '563500863767707648', '2019-04-04 15:11:15', '34234', null, null, null, '0', '0', null, '2019-04-04 15:11:15'), ('563494237866295296', null, null, '2312312', '3123', null, null, '563501220019306496', '2019-04-04 15:12:40', '1231', null, null, null, '0', '1', null, '2019-04-04 15:12:40'), ('563495642006028288', null, null, 'oiyioyiu', 'ipouo', null, null, '563623391332925440', '2019-04-04 23:18:08', '6', null, null, null, '0', '1', null, '2019-04-04 23:18:08');
COMMIT;

-- ----------------------------
--  Table structure for `manager_resource`
-- ----------------------------
DROP TABLE IF EXISTS `manager_resource`;
CREATE TABLE `manager_resource` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `menu_type` int(11) DEFAULT NULL,
  `resource_icon` varchar(255) DEFAULT NULL,
  `resource_link` varchar(255) DEFAULT NULL,
  `resource_name` varchar(255) DEFAULT NULL,
  `resource_order` int(11) DEFAULT NULL,
  `resource_parent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_resource`
-- ----------------------------
BEGIN;
INSERT INTO `manager_resource` VALUES ('536478251871109120', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '9', null, 'http://linesno.cloud.com', '基础平台', null, '0'), ('536478252194070528', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '1', null, 'http://linesno.cloud.com', '监控管理', null, '536478251871109120'), ('563072461336215552', null, '12123', null, null, null, '0', '0', null, '2019-04-03 19:19:12', '9', '123123', 'http://www.baidu.com', '测试菜单999', null, '12312'), ('536478252248596480', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '1', null, 'http://linesno.cloud.com', '平台管理', null, '536478251871109120'), ('536478252261179392', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '组织结构', null, '536478252248596480'), ('536478252277956608', null, '0', null, null, null, '0', '1', null, '2019-04-04 14:25:50', '9', '', 'boot/platform/department/list', '部门管理', null, '536478252261179392'), ('536478252290539520', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/role/list', '角色管理', null, '536478252261179392'), ('536478252303122432', null, '0', null, null, null, '0', '1', null, '2019-04-05 14:26:26', '9', '123', 'boot/platform/tenant/list', '租户管理', null, '536478252261179392'), ('536478252315705344', null, '0', null, null, null, '0', '1', null, '2019-04-05 14:27:59', '9', '', 'boot/platform/member/list', '用户管理', null, '536478252261179392'), ('536478252324093952', null, '0', null, null, null, '0', '1', null, '2019-04-04 15:17:41', '9', '123', 'boot/platform/account/list', '账号管理', null, '536478252261179392'), ('536478252336676864', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '平台配置', null, '536478252248596480'), ('536478252349259776', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/application/list', '应用管理', null, '536478252336676864'), ('536478252361842688', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/menus/list', '菜单管理', null, '536478252336676864'), ('536478252387008512', null, '0', null, null, null, '0', '1', null, '2019-04-05 14:27:25', '9', '', 'boot/platform/search/list', '搜索管理', null, '536478252336676864'), ('536478252399591424', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/dictionary/list', '字典管理', null, '536478252336676864'), ('536478252420562944', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '系统配置', null, '536478252248596480'), ('536478252437340160', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '结算管理', null, '536478252248596480'), ('536478252449923072', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '计费管理', null, '536478252437340160'), ('536478252458311680', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '充值管理', null, '536478252437340160'), ('536478252470894592', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '订购管理', null, '536478252437340160'), ('536478252483477504', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '定制管理', null, '536478252437340160'), ('536478252496060416', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'http://linesno.cloud.com', '催缴管理', null, '536478252437340160'), ('536478252517031936', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/content/list', '内容管理', null, '536478252248596480'), ('536478252529614848', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/notice/list', '通知公告', null, '536478252517031936'), ('536478252546392064', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/content/typeList', '分类管理', null, '536478252517031936'), ('536478252558974976', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/content/list', '文章管理', null, '536478252517031936'), ('536478252571557888', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '1', null, '', '版本声明', null, '536478251871109120'), ('536478252588335104', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/copyright/about', '平台协议', null, '536478252571557888'), ('536478252194070529', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/monitor/display', '系统预警', null, '536478252194070528'), ('536478252194070530', '2019-01-20 09:33:03', '0', null, null, null, '0', '0', '0', '2019-01-20 09:33:03', '0', null, 'boot/platform/monitor/display', '平台监控', null, '536478252194070528'), ('563273421400571904', null, '11', null, null, null, '0', '1', null, '2019-04-03 19:19:39', '1', '12', '12', '测试菜单', null, '536478252420562944'), ('564580377914507264', '2019-04-07 14:40:51', '测试菜单2', null, null, null, '0', '1', null, '2019-04-07 14:40:51', '9', 'fa fa-bandcamp', '123123', '部门管理', null, '536478252420562944');
COMMIT;

-- ----------------------------
--  Table structure for `manager_resource_action`
-- ----------------------------
DROP TABLE IF EXISTS `manager_resource_action`;
CREATE TABLE `manager_resource_action` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `resource_action_icon` varchar(255) DEFAULT NULL,
  `resource_action_method` varchar(255) DEFAULT NULL,
  `resource_action_name` varchar(255) DEFAULT NULL,
  `resource_action_order` int(11) DEFAULT NULL,
  `resource_action_status` bit(1) DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_role`
-- ----------------------------
DROP TABLE IF EXISTS `manager_role`;
CREATE TABLE `manager_role` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `role_status` bit(1) DEFAULT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_role`
-- ----------------------------
BEGIN;
INSERT INTO `manager_role` VALUES ('564708772195336192', '2019-04-07 23:11:03', '123', null, null, null, '0', '1', null, '2019-04-07 23:11:03', null, null, '部门管理'), ('563847273901981696', '2019-04-05 14:07:46', '0', null, null, null, '0', '1', null, '2019-04-05 14:07:46', null, null, '软件研发岗'), ('563851604873183232', '2019-04-05 14:24:58', '1', null, null, null, '0', '1', null, '2019-04-05 14:24:58', null, null, '班级管理岗');
COMMIT;

-- ----------------------------
--  Table structure for `manager_role_resource`
-- ----------------------------
DROP TABLE IF EXISTS `manager_role_resource`;
CREATE TABLE `manager_role_resource` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `resource_type` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `role_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `manager_role_resource`
-- ----------------------------
BEGIN;
INSERT INTO `manager_role_resource` VALUES ('564708772291805185', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252303122432', null, '564708772195336192', null), ('564708772291805184', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252290539520', null, '564708772195336192', null), ('564708772287610880', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252277956608', null, '564708772195336192', null), ('564708772283416576', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252261179392', null, '564708772195336192', null), ('564708772279222272', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252248596480', null, '564708772195336192', null), ('564708772275027969', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252194070530', null, '564708772195336192', null), ('564708772275027968', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252194070529', null, '564708772195336192', null), ('564708772270833664', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252194070528', null, '564708772195336192', null), ('564708772266639360', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478251871109120', null, '564708772195336192', null), ('563851604902543360', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '563273421400571904', null, '563851604873183232', null), ('563851604898349057', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252420562944', null, '563851604873183232', null), ('563851604898349056', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252324093952', null, '563851604873183232', null), ('563851604894154752', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252315705344', null, '563851604873183232', null), ('563851604889960449', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252303122432', null, '563851604873183232', null), ('563851604889960448', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252290539520', null, '563851604873183232', null), ('563851604885766145', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252277956608', null, '563851604873183232', null), ('563851604885766144', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252261179392', null, '563851604873183232', null), ('563851604881571840', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478252248596480', null, '563851604873183232', null), ('563851604877377536', '2019-04-05 14:24:58', '0', null, null, null, '0', '0', '0', '2019-04-05 14:24:58', '536478251871109120', null, '563851604873183232', null), ('563847273948119040', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252588335104', null, '563847273901981696', null), ('563847273943924736', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252571557888', null, '563847273901981696', null), ('563847273939730433', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252399591424', null, '563847273901981696', null), ('563847273939730432', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252387008512', null, '563847273901981696', null), ('563847273935536128', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252361842688', null, '563847273901981696', null), ('563847273931341824', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252349259776', null, '563847273901981696', null), ('563847273927147520', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252336676864', null, '563847273901981696', null), ('563847273922953217', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252324093952', null, '563847273901981696', null), ('563847273922953216', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252315705344', null, '563847273901981696', null), ('563847273918758912', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252303122432', null, '563847273901981696', null), ('563847273906176000', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478251871109120', null, '563847273901981696', null), ('563847273910370304', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252248596480', null, '563847273901981696', null), ('563847273910370305', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252261179392', null, '563847273901981696', null), ('563847273914564608', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252277956608', null, '563847273901981696', null), ('563847273914564609', '2019-04-05 14:07:46', '0', null, null, null, '0', '0', '0', '2019-04-05 14:07:46', '536478252290539520', null, '563847273901981696', null), ('564708772333748224', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252437340160', null, '564708772195336192', null), ('564708772329553920', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '564580377914507264', null, '564708772195336192', null), ('564708772325359617', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '563273421400571904', null, '564708772195336192', null), ('564708772325359616', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252420562944', null, '564708772195336192', null), ('564708772321165312', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252399591424', null, '564708772195336192', null), ('564708772316971008', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252387008512', null, '564708772195336192', null), ('564708772312776704', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252361842688', null, '564708772195336192', null), ('564708772308582400', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252349259776', null, '564708772195336192', null), ('564708772304388097', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252336676864', null, '564708772195336192', null), ('564708772304388096', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252324093952', null, '564708772195336192', null), ('564708772300193792', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252315705344', null, '564708772195336192', null), ('564708772333748225', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252449923072', null, '564708772195336192', null), ('564708772337942528', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252458311680', null, '564708772195336192', null), ('564708772342136832', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252470894592', null, '564708772195336192', null), ('564708772342136833', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252483477504', null, '564708772195336192', null), ('564708772346331136', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252496060416', null, '564708772195336192', null), ('564708772350525440', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252517031936', null, '564708772195336192', null), ('564708772350525441', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252529614848', null, '564708772195336192', null), ('564708772354719744', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252546392064', null, '564708772195336192', null), ('564708772358914048', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252558974976', null, '564708772195336192', null), ('564708772363108352', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252571557888', null, '564708772195336192', null), ('564708772367302656', '2019-04-07 23:11:03', '0', null, null, null, '0', '0', '0', '2019-04-07 23:11:03', '536478252588335104', null, '564708772195336192', null);
COMMIT;

-- ----------------------------
--  Table structure for `manager_search`
-- ----------------------------
DROP TABLE IF EXISTS `manager_search`;
CREATE TABLE `manager_search` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `has_date` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `option_json` varchar(255) DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `search_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_template`
-- ----------------------------
DROP TABLE IF EXISTS `manager_template`;
CREATE TABLE `manager_template` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `template_addtime` datetime DEFAULT NULL,
  `template_content` varchar(255) DEFAULT NULL,
  `template_data` varchar(255) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `template_owner` varchar(255) DEFAULT NULL,
  `template_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_tenant`
-- ----------------------------
DROP TABLE IF EXISTS `manager_tenant`;
CREATE TABLE `manager_tenant` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `province_id` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `tenant_account` varchar(255) DEFAULT NULL,
  `tenant_address` varchar(255) DEFAULT NULL,
  `tenant_cost` int(11) DEFAULT NULL,
  `tenant_name` varchar(255) DEFAULT NULL,
  `tenant_phone` varchar(255) DEFAULT NULL,
  `tenant_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_tenant_log`
-- ----------------------------
DROP TABLE IF EXISTS `manager_tenant_log`;
CREATE TABLE `manager_tenant_log` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `action_type` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `log_business_id` varchar(255) DEFAULT NULL,
  `log_channel` varchar(255) DEFAULT NULL,
  `log_content` varchar(255) DEFAULT NULL,
  `log_ip` varchar(255) DEFAULT NULL,
  `log_machine` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `manager_user`
-- ----------------------------
DROP TABLE IF EXISTS `manager_user`;
CREATE TABLE `manager_user` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_account` varchar(255) DEFAULT NULL,
  `user_addtime` datetime DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_role` varchar(255) DEFAULT NULL,
  `user_salt` varchar(255) DEFAULT NULL,
  `user_status` bit(1) DEFAULT NULL,
  `user_title` varchar(255) DEFAULT NULL,
  `user_type` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_account`
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_status` int(11) DEFAULT NULL,
  `login_account` varchar(255) DEFAULT NULL,
  `login_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_address`
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `address_name` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `room_num` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_classes`
-- ----------------------------
DROP TABLE IF EXISTS `user_classes`;
CREATE TABLE `user_classes` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `classes` varchar(255) DEFAULT NULL,
  `collge_name` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `school_end_time` datetime DEFAULT NULL,
  `school_id` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_num` varchar(255) DEFAULT NULL,
  `school_start_time` datetime DEFAULT NULL,
  `shool_job` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_info`
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `avatar_head` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `classes` varchar(255) DEFAULT NULL,
  `collge_name` varchar(255) DEFAULT NULL,
  `company_dept` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `english_name` varchar(255) DEFAULT NULL,
  `facsimile` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `home_address` varchar(255) DEFAULT NULL,
  `home_city` varchar(255) DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `living_adress` varchar(255) DEFAULT NULL,
  `living_city` varchar(255) DEFAULT NULL,
  `main_connection` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `native_address` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `parent_father` varchar(255) DEFAULT NULL,
  `parent_mother` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `really_name` varchar(255) DEFAULT NULL,
  `regist_ip` varchar(255) DEFAULT NULL,
  `regist_source` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `school_end_time` datetime DEFAULT NULL,
  `school_id` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_num` varchar(255) DEFAULT NULL,
  `school_start_time` datetime DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `shool_job` varchar(255) DEFAULT NULL,
  `user_code` varchar(255) DEFAULT NULL,
  `user_height` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL,
  `wechat` varchar(255) DEFAULT NULL,
  `weibo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_integer`
-- ----------------------------
DROP TABLE IF EXISTS `user_integer`;
CREATE TABLE `user_integer` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_job`
-- ----------------------------
DROP TABLE IF EXISTS `user_job`;
CREATE TABLE `user_job` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_location`
-- ----------------------------
DROP TABLE IF EXISTS `user_location`;
CREATE TABLE `user_location` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `precisions` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_pic`
-- ----------------------------
DROP TABLE IF EXISTS `user_pic`;
CREATE TABLE `user_pic` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `pic_name` varchar(255) DEFAULT NULL,
  `pic_path` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_professional`
-- ----------------------------
DROP TABLE IF EXISTS `user_professional`;
CREATE TABLE `user_professional` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  `professional_name` varchar(255) DEFAULT NULL,
  `use_end_time` varchar(255) DEFAULT NULL,
  `use_start_time` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_school`
-- ----------------------------
DROP TABLE IF EXISTS `user_school`;
CREATE TABLE `user_school` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `owners` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
