package com.alinesno.cloud.base.boot.service;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.enums.MenuEnums;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 资源单元测试
 * @author LuoAnDong
 * @since 2019年1月12日 下午12:24:05
 */
public class IManagerResourceServiceTest extends JUnitBase {

	@Autowired
	private IManagerResourceService managerResourceService ; 
	
	@Test
	public void testFindAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByIdIterableOfID() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAllIterableOfS() {
		List<ManagerResourceEntity> es = new ArrayList<ManagerResourceEntity>() ; 
		
		ManagerResourceEntity e = new ManagerResourceEntity(); 
		e.setResourceName("基础平台");
		e.setMenuType(MenuEnums.MENU_PLATFORM.value);
		
		e.setApplicationId("0");
		e.setTenantId("0");
		e.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e) ; 
			
		es.add(e) ; 

		// ************************************************************
		ManagerResourceEntity e1 = new ManagerResourceEntity(); 
		e1.setResourceName("监控管理");
		e1.setMenuType(MenuEnums.MENU_PACKAGE.value);
		e1.setResourceParent(e.getId());
		e1.setApplicationId("0");
		e1.setTenantId("0");
		e1.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e1) ; 
		es.add(e1) ; 
		
		ManagerResourceEntity e2 = new ManagerResourceEntity(); 
		e2.setResourceName("平台监控");
		e2.setMenuType(MenuEnums.MENU_ITEM.value);
		e2.setResourceParent(e1.getId());
		e2.setApplicationId("0");
		e2.setTenantId("0");
		e2.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e2) ; 
		es.add(e2) ; 
		
		ManagerResourceEntity e21 = new ManagerResourceEntity(); 
		e21.setResourceName("监控管理");
		e21.setMenuType(MenuEnums.MENU_ITEM.value);
		e21.setResourceParent(e1.getId());
		e21.setApplicationId("0");
		e21.setTenantId("0");
		e21.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e21) ; 
		es.add(e21) ; 
		
		ManagerResourceEntity e22 = new ManagerResourceEntity(); 
		e22.setResourceName("系统预警");
		e22.setMenuType(MenuEnums.MENU_ITEM.value);
		e22.setResourceParent(e1.getId());
		e22.setApplicationId("0");
		e22.setTenantId("0");
		e22.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e22) ; 
		es.add(e22) ; 
		// ************************************************************
		
		// ************************************************************
		ManagerResourceEntity e3 = new ManagerResourceEntity(); 
		e3.setResourceName("平台管理");
		e3.setMenuType(MenuEnums.MENU_PACKAGE.value);
		e3.setResourceParent(e.getId());
		e3.setApplicationId("0");
		e3.setTenantId("0");
		e3.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e3) ; 
		es.add(e3) ; 
		
		ManagerResourceEntity e31 = new ManagerResourceEntity(); 
		e31.setResourceName("组织结构");
		e31.setMenuType(MenuEnums.MENU_ITEM.value);
		e31.setResourceParent(e3.getId());
		e31.setApplicationId("0");
		e31.setTenantId("0");
		e31.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e31) ; 
		es.add(e31) ; 
		
		ManagerResourceEntity e311 = new ManagerResourceEntity(); 
		e311.setResourceName("部门管理");
		e311.setMenuType(MenuEnums.MENU_ITEM.value);
		e311.setResourceParent(e31.getId());
		e311.setApplicationId("0");
		e311.setTenantId("0");
		e311.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e311) ; 
		es.add(e311) ; 
		
		ManagerResourceEntity e312 = new ManagerResourceEntity(); 
		e312.setResourceName("角色管理");
		e312.setMenuType(MenuEnums.MENU_ITEM.value);
		e312.setResourceParent(e31.getId());
		e312.setApplicationId("0");
		e312.setTenantId("0");
		e312.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e312) ; 
		es.add(e312) ; 
		
		ManagerResourceEntity e313 = new ManagerResourceEntity(); 
		e313.setResourceName("租户管理");
		e313.setMenuType(MenuEnums.MENU_ITEM.value);
		e313.setResourceParent(e31.getId());
		e313.setApplicationId("0");
		e313.setTenantId("0");
		e313.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e313) ; 
		es.add(e313) ; 
		
		ManagerResourceEntity e314 = new ManagerResourceEntity(); 
		e314.setResourceName("用户管理");
		e314.setMenuType(MenuEnums.MENU_ITEM.value);
		e314.setResourceParent(e31.getId());
		e314.setApplicationId("0");
		e314.setTenantId("0");
		e314.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e314) ; 
		es.add(e314) ; 
		
		ManagerResourceEntity e315 = new ManagerResourceEntity(); 
		e315.setResourceName("账号管理");
		e315.setMenuType(MenuEnums.MENU_ITEM.value);
		e315.setResourceParent(e31.getId());
		e315.setApplicationId("0");
		e315.setTenantId("0");
		e315.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e315) ; 
		es.add(e315) ; 
		
		// -----------------------------------------------------------------------------
		
		ManagerResourceEntity e32 = new ManagerResourceEntity(); 
		e32.setResourceName("平台配置");
		e32.setMenuType(MenuEnums.MENU_PACKAGE.value);
		e32.setResourceParent(e3.getId());
		e32.setApplicationId("0");
		e32.setTenantId("0");
		e32.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e32) ; 
		es.add(e32) ; 
		
		ManagerResourceEntity e321 = new ManagerResourceEntity(); 
		e321.setResourceName("应用管理");
		e321.setMenuType(MenuEnums.MENU_ITEM.value);
		e321.setResourceParent(e32.getId());
		e321.setApplicationId("0");
		e321.setTenantId("0");
		e321.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e321) ; 
		es.add(e321) ; 
		
		ManagerResourceEntity e322 = new ManagerResourceEntity(); 
		e322.setResourceName("菜单管理");
		e322.setMenuType(MenuEnums.MENU_ITEM.value);
		e322.setResourceParent(e32.getId());
		e322.setApplicationId("0");
		e322.setTenantId("0");
		e322.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e322) ; 
		es.add(e322) ; 
		
		ManagerResourceEntity e323 = new ManagerResourceEntity(); 
		e323.setResourceName("搜索管理");
		e323.setMenuType(MenuEnums.MENU_ITEM.value);
		e323.setResourceParent(e32.getId());
		e323.setApplicationId("0");
		e323.setTenantId("0");
		e323.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e323) ; 
		es.add(e323) ; 
		
		ManagerResourceEntity e324 = new ManagerResourceEntity(); 
		e324.setResourceName("字典管理");
		e324.setMenuType(MenuEnums.MENU_ITEM.value);
		e324.setResourceParent(e32.getId());
		e324.setApplicationId("0");
		e324.setTenantId("0");
		e324.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e324) ; 
		es.add(e324) ; 
		
		// -----------------------------------------------------------------------------
		
		ManagerResourceEntity e33 = new ManagerResourceEntity(); 
		e33.setResourceName("系统配置");
		e33.setMenuType(MenuEnums.MENU_ITEM.value);
		e33.setResourceParent(e3.getId());
		e33.setApplicationId("0");
		e33.setTenantId("0");
		e33.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e33) ; 
		
		// -----------------------------------------------------------------------------
		
		ManagerResourceEntity e34 = new ManagerResourceEntity(); 
		e34.setResourceName("结算管理");
		e34.setMenuType(MenuEnums.MENU_ITEM.value);
		e34.setResourceParent(e3.getId());
		e34.setApplicationId("0");
		e34.setTenantId("0");
		e34.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e34) ; 
		es.add(e34) ; 
		
		ManagerResourceEntity e341 = new ManagerResourceEntity(); 
		e341.setResourceName("计费管理");
		e341.setMenuType(MenuEnums.MENU_ITEM.value);
		e341.setResourceParent(e34.getId());
		e341.setApplicationId("0");
		e341.setTenantId("0");
		e341.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e341) ; 
		es.add(e341) ; 
		
		ManagerResourceEntity e342 = new ManagerResourceEntity(); 
		e342.setResourceName("充值管理");
		e342.setMenuType(MenuEnums.MENU_ITEM.value);
		e342.setResourceParent(e34.getId());
		e342.setApplicationId("0");
		e342.setTenantId("0");
		e342.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e342) ; 
		es.add(e342) ; 
		
		ManagerResourceEntity e343 = new ManagerResourceEntity(); 
		e343.setResourceName("订购管理");
		e343.setMenuType(MenuEnums.MENU_ITEM.value);
		e343.setResourceParent(e34.getId());
		e343.setApplicationId("0");
		e343.setTenantId("0");
		e343.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e343) ; 
		es.add(e343) ; 
		
		ManagerResourceEntity e344 = new ManagerResourceEntity(); 
		e344.setResourceName("定制管理");
		e344.setMenuType(MenuEnums.MENU_ITEM.value);
		e344.setResourceParent(e34.getId());
		e344.setApplicationId("0");
		e344.setTenantId("0");
		e344.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e344) ; 
		es.add(e344) ; 
		
		ManagerResourceEntity e345 = new ManagerResourceEntity(); 
		e345.setResourceName("催缴管理");
		e345.setMenuType(MenuEnums.MENU_ITEM.value);
		e345.setResourceParent(e34.getId());
		e345.setApplicationId("0");
		e345.setTenantId("0");
		e345.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e345) ; 
		es.add(e345) ; 
		
		// -----------------------------------------------------------------------------
		
		ManagerResourceEntity e35 = new ManagerResourceEntity(); 
		e35.setResourceName("内容管理");
		e35.setMenuType(MenuEnums.MENU_PACKAGE.value);
		e35.setResourceParent(e3.getId());
		e35.setApplicationId("0");
		e35.setTenantId("0");
		e35.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e35) ; 
		es.add(e35) ; 
		
		ManagerResourceEntity e351 = new ManagerResourceEntity(); 
		e351.setResourceName("通知公告");
		e351.setMenuType(MenuEnums.MENU_ITEM.value);
		e351.setResourceParent(e35.getId());
		e351.setApplicationId("0");
		e351.setTenantId("0");
		e351.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e351) ; 
		es.add(e351) ; 
		
		ManagerResourceEntity e352 = new ManagerResourceEntity(); 
		e352.setResourceName("分类管理");
		e352.setMenuType(MenuEnums.MENU_ITEM.value);
		e352.setResourceParent(e35.getId());
		e352.setApplicationId("0");
		e352.setTenantId("0");
		e352.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e352) ; 
		es.add(e352) ; 
		
		ManagerResourceEntity e353 = new ManagerResourceEntity(); 
		e353.setResourceName("内容管理");
		e353.setMenuType(MenuEnums.MENU_ITEM.value);
		e353.setResourceParent(e35.getId());
		e353.setApplicationId("0");
		e353.setTenantId("0");
		e353.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e353) ; 
		es.add(e353) ; 
		// ************************************************************
		
		// ************************************************************
		ManagerResourceEntity e5 = new ManagerResourceEntity(); 
		e5.setResourceName("版本声明");
		e5.setMenuType(MenuEnums.MENU_PACKAGE.value);
		e5.setResourceParent(e.getId());
		e5.setApplicationId("0");
		e5.setTenantId("0");
		e5.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e5) ; 
		es.add(e5) ; 
		
		ManagerResourceEntity e6 = new ManagerResourceEntity(); 
		e6.setResourceName("平台协议");
		e6.setMenuType(MenuEnums.MENU_ITEM.value);
		e6.setResourceParent(e5.getId());
		e6.setApplicationId("0");
		e6.setTenantId("0");
		e6.setResourceLink("http://linesno.cloud.com");
		managerResourceService.save(e6) ; 
		es.add(e6) ; 
		// ************************************************************
		
//		for(ManagerResourceEntity b : es) {
//			b.setApplicationId("0");
//			b.setTenantId("0");
//			b.setResourceLink("http://linesno.cloud.com");
//			
//			managerResourceService.save(b) ; 
//		}
		
//		managerResourceService.saveAll(es) ; 
	}

	@Test
	public void testFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSort1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testSave() {
		ManagerResourceEntity e = new ManagerResourceEntity(); 
	
		managerResourceService.save(e) ; 
	}

	@Test
	public void testSaveAllIterableOfS1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAll1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByIdIterableOfID1() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsT() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testExists() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneSpecificationOfT() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfT() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfTPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfTSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountSpecificationOfT() {
		fail("Not yet implemented");
	}

}
