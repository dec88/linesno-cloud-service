package com.alinesno.cloud.base.boot.repository;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class ManagerAccountRepositoryTest extends JUnitBase {

	@Autowired
	private ManagerAccountRepository managerAccountRepository ; 
	
	@Test
	public void testFindByLoginName() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByApplicationId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByTenantId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByTenantIdAndApplicationId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAll() {
		List<ManagerAccountEntity> list = managerAccountRepository.findAll() ; 
		
		Assert.assertNotNull(list);
		
		for(ManagerAccountEntity e : list) {
			log.debug("e:{}" , JSONObject.toJSON(e));
		}
	}

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByIdIterableOfID() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAllIterableOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSort1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testSave() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAllIterableOfS1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAll1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByIdIterableOfID1() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsT() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort1() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testExists() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneSpecificationOfT() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfT() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfTPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfTSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountSpecificationOfT() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testUpdateManagerAccountPassword() {
		
		String userId = "623977931256889344" ; 
		String newPassword = "landonniao" ; 
		
		managerAccountRepository.updateManagerAccountPassword(newPassword, userId);
	}

}
