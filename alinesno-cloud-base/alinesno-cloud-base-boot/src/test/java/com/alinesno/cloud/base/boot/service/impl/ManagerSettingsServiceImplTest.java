package com.alinesno.cloud.base.boot.service.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.core.junit.JUnitBase;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 参数配置
 * @author LuoAnDong
 * @since 2019年10月6日 下午12:19:02
 */
public class ManagerSettingsServiceImplTest extends JUnitBase {

	@Autowired
	IManagerSettingsService managerSettingsService ; 
	
	@Test
	public void testTenantFindList() {
	}

	@Test
	public void testTenantFindOne() {
		
		RestWrapper wrapper = RestWrapper.create() ;  
		wrapper.eq("configKey", "234") ; 
		
		ManagerSettingsEntity m = managerSettingsService.tenantFindOne(wrapper) ; 
		
		log.debug("m :{}" , m);
	}

}
