package com.alinesno.cloud.base.logger.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Entity
@Table(name="log_login")
public class LogLoginEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 登陆ip
     */
	@Column(name="login_ip")
	private String loginIp;
    /**
     * 登陆时间
     */
	@Column(name="login_time")
	private Date loginTime;
    /**
     * 退出时间
     */
	@Column(name="logout_time")
	private Date logoutTime;
    /**
     * 登陆用户
     */
	@Column(name="login_user")
	private String loginUser;
    /**
     * 其它登陆信息
     */
	@Column(name="login_msg")
	private String loginMsg;


	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public String getLoginMsg() {
		return loginMsg;
	}

	public void setLoginMsg(String loginMsg) {
		this.loginMsg = loginMsg;
	}


	@Override
	public String toString() {
		return "LogLoginEntity{" +
			"loginIp=" + loginIp +
			", loginTime=" + loginTime +
			", logoutTime=" + logoutTime +
			", loginUser=" + loginUser +
			", loginMsg=" + loginMsg +
			"}";
	}
}
