package com.alinesno.cloud.base.logger.enable;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ArrayList;
import java.util.List;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class BaseLoggerConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 
		
		return importBean.toArray(new String[] {}) ;
	}

}
