package com.alinesno.cloud.base.logger.service;

import com.alinesno.cloud.base.logger.entity.LogLoginEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@NoRepositoryBean
public interface ILogLoginService extends IBaseService<LogLoginEntity, String> {

}
