package com.alinesno.cloud.base.logger.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Entity
@Table(name="log_record")
public class LogRecordEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 记录时间
     */
	@Column(name="record_msg")
	private String recordMsg;
    /**
     * 记录渠道
     */
	@Column(name="record_channel")
	private String recordChannel;
    /**
     * 日志参数
     */
	@Column(name="record_params")
	private String recordParams;
    /**
     * 用户名称
     */
	@Column(name="record_user")
	private String recordUser;
    /**
     * 日志用户名
     */
	@Column(name="record_user_name")
	private String recordUserName;
    /**
     * 日志记录类型
     */
	@Column(name="record_type")
	private String recordType;


	public String getRecordMsg() {
		return recordMsg;
	}

	public void setRecordMsg(String recordMsg) {
		this.recordMsg = recordMsg;
	}

	public String getRecordChannel() {
		return recordChannel;
	}

	public void setRecordChannel(String recordChannel) {
		this.recordChannel = recordChannel;
	}

	public String getRecordParams() {
		return recordParams;
	}

	public void setRecordParams(String recordParams) {
		this.recordParams = recordParams;
	}

	public String getRecordUser() {
		return recordUser;
	}

	public void setRecordUser(String recordUser) {
		this.recordUser = recordUser;
	}

	public String getRecordUserName() {
		return recordUserName;
	}

	public void setRecordUserName(String recordUserName) {
		this.recordUserName = recordUserName;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}


	@Override
	public String toString() {
		return "LogRecordEntity{" +
			"recordMsg=" + recordMsg +
			", recordChannel=" + recordChannel +
			", recordParams=" + recordParams +
			", recordUser=" + recordUser +
			", recordUserName=" + recordUserName +
			", recordType=" + recordType +
			"}";
	}
}
