package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_selector")
public class GateSelectorEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 插件id
     */
	@Column(name="plugin_id")
	private String pluginId;
    /**
     * 选择器名称
     */
	private String name;
    /**
     * 匹配方式（0 and  1 or)
     */
	@Column(name="match_mode")
	private Integer matchMode;
    /**
     * 类型（0，全流量，1自定义流量）
     */
	private Integer type;
    /**
     * 排序
     */
	private Integer sort;
    /**
     * 处理逻辑（此处针对不同的插件，会有不同的字段来标识不同的处理，所有存储json格式数据）
     */
	private String handle;
    /**
     * 是否开启
     */
	private Integer enabled;
    /**
     * 是否打印日志
     */
	private Integer loged;
    /**
     * 是否继续执行
     */
	private Integer continued;
    /**
     * 创建时间
     */
	@Column(name="date_created")
	private Date dateCreated;
    /**
     * 更新时间
     */
	@Column(name="date_updated")
	private Date dateUpdated;


	public String getPluginId() {
		return pluginId;
	}

	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMatchMode() {
		return matchMode;
	}

	public void setMatchMode(Integer matchMode) {
		this.matchMode = matchMode;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getLoged() {
		return loged;
	}

	public void setLoged(Integer loged) {
		this.loged = loged;
	}

	public Integer getContinued() {
		return continued;
	}

	public void setContinued(Integer continued) {
		this.continued = continued;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}


	@Override
	public String toString() {
		return "GateSelectorEntity{" +
			"pluginId=" + pluginId +
			", name=" + name +
			", matchMode=" + matchMode +
			", type=" + type +
			", sort=" + sort +
			", handle=" + handle +
			", enabled=" + enabled +
			", loged=" + loged +
			", continued=" + continued +
			", dateCreated=" + dateCreated +
			", dateUpdated=" + dateUpdated +
			"}";
	}
}
