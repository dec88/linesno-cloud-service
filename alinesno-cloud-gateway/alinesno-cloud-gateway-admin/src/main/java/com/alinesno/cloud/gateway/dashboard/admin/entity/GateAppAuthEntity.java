package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_app_auth")
public class GateAppAuthEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 应用标识key
     */
	@Column(name="app_key")
	private String appKey;
    /**
     * 加密算法secret
     */
	@Column(name="app_secret")
	private String appSecret;
    /**
     * 是否删除
     */
	private Integer enabled;
    /**
     * 创建时间
     */
	@Column(name="date_created")
	private Date dateCreated;
    /**
     * 更新时间
     */
	@Column(name="date_updated")
	private Date dateUpdated;


	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}


	@Override
	public String toString() {
		return "GateAppAuthEntity{" +
			"appKey=" + appKey +
			", appSecret=" + appSecret +
			", enabled=" + enabled +
			", dateCreated=" + dateCreated +
			", dateUpdated=" + dateUpdated +
			"}";
	}
}
