package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 最好编辑完后删除主键，这样就是只读状态，不能随意更改。需要更改就重新加上主键。

每次启动服务器时加载整个表到内存。
这个表不可省略，model内注解的权限只是客户端能用的，其它可以保证即便服务端代码错误时也不会误删数据。
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_request")
public class GateRequestEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * GET,HEAD可用任意结构访问任意开放内容，不需要这个字段。
其它的操作因为写入了结构和内容，所以都需要，按照不同的version选择对应的structure。

自动化版本管理：
Request JSON最外层可以传  “version”:Integer 。
1.未传或 <= 0，用最新版。 “@order”:”version-“
2.已传且 > 0，用version以上的可用版本的最低版本。 “@order”:”version+”, “version{}”:”>={version}”
     */
	private Integer version;
    /**
     * 只限于GET,HEAD外的操作方法。
     */
	private String method;
    /**
     * 标签
     */
	private String tag;
    /**
     * 结构。
TODO 里面的 PUT 改为 UPDATE，避免和请求 PUT 搞混。
     */
	private String structure;
    /**
     * 详细说明
     */
	private String detail;
    /**
     * 创建日期
     */
	private Date date;


	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getStructure() {
		return structure;
	}

	public void setStructure(String structure) {
		this.structure = structure;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "GateRequestEntity{" +
			"version=" + version +
			", method=" + method +
			", tag=" + tag +
			", structure=" + structure +
			", detail=" + detail +
			", date=" + date +
			"}";
	}
}
