package com.alinesno.cloud.gateway.dashboard.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateBusinessServerEntity;
import com.alinesno.cloud.gateway.dashboard.admin.service.IGateBusinessServerService;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("gateway/dashboard/admin/gateBusinessServer")
public class GateBusinessServerController extends LocalMethodBaseController<GateBusinessServerEntity, IGateBusinessServerService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(GateBusinessServerController.class);

	
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }


}


























