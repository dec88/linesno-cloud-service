package com.alinesno.cloud.gateway.dashboard.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_collect")
public class GateCollectEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */
	@Column(name="template_name")
	private String templateName;
    /**
     * 模板内容
     */
	@Column(name="template_content")
	private String templateContent;


	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}


	@Override
	public String toString() {
		return "GateCollectEntity{" +
			"templateName=" + templateName +
			", templateContent=" + templateContent +
			"}";
	}
}
