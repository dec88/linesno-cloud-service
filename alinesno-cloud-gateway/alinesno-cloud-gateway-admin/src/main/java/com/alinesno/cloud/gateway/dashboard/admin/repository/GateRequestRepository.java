package com.alinesno.cloud.gateway.dashboard.admin.repository;

import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateRequestEntity;

/**
 * <p>
  * 最好编辑完后删除主键，这样就是只读状态，不能随意更改。需要更改就重新加上主键。

每次启动服务器时加载整个表到内存。
这个表不可省略，model内注解的权限只是客户端能用的，其它可以保证即便服务端代码错误时也不会误删数据。 持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
public interface GateRequestRepository extends IBaseJpaRepository<GateRequestEntity, String> {

}
