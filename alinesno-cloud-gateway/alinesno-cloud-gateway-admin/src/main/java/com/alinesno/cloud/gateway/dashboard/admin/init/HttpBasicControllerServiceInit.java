package com.alinesno.cloud.gateway.dashboard.admin.init;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.common.web.basic.auth.controller.HttpBasicControllerService;

/**
 * 实现界面菜单 
 * @author LuoAnDong
 * @since 2019年9月23日 上午11:01:02
 */
@Service("httpBasicControllerService")
public class HttpBasicControllerServiceInit implements HttpBasicControllerService {

	private static final Logger log = LoggerFactory.getLogger(HttpBasicControllerServiceInit.class) ; 
	
	private static final String APPLICATION_ID = "1" ; 
	
	@Override
	public List<ManagerApplicationEntity> initApplication() {
		log.debug("初始化菜单") ; 
		List<ManagerApplicationEntity> list = new ArrayList<ManagerApplicationEntity>() ; 
	
		ManagerApplicationEntity e = new ManagerApplicationEntity(); 
		e.setApplicationIcons("fa fa-leaf");
		e.setApplicationName("网关管理平台");
		e.setId(APPLICATION_ID);
		
		list.add(e) ; 
		return list ;
	}

	@Override
	public List<ManagerResourceEntity> findResourceByApplicationId(String applicationId) {
		log.debug("applicationId:{}" , applicationId);
		
		ManagerResourceEntity t1 = new ManagerResourceEntity("监控管理" , MENU_TYPE_TITLE , applicationId) ; 
		List<ManagerResourceEntity> subResource = new ArrayList<ManagerResourceEntity>() ;
		
		ManagerResourceEntity t1f1 = new ManagerResourceEntity("仪盘表", "" , MENU_TYPE_FUNCTION,t1) ; 
		subResource.add(t1f1) ; 
		t1.setSubResource(subResource); 
		
		ManagerResourceEntity t2 = new ManagerResourceEntity("功能管理" , MENU_TYPE_TITLE , applicationId) ; 
		List<ManagerResourceEntity> subResource2 = new ArrayList<ManagerResourceEntity>() ;
		
		ManagerResourceEntity t2f1 = new ManagerResourceEntity("路由管理" , "gateway/dashboard/admin/gateBusinessServer/list" ,  MENU_TYPE_FUNCTION , t2) ; 
		ManagerResourceEntity t2f2 = new ManagerResourceEntity("服务监控" , "gateway/dashboard/admin/gateBusinessServer/list" ,  MENU_TYPE_FUNCTION , t2) ; 
		ManagerResourceEntity t2f3 = new ManagerResourceEntity("过滤监控" , "gateway/dashboard/admin/gateBusinessServer/list" ,  MENU_TYPE_FUNCTION , t2) ; 
		ManagerResourceEntity t2f4 = new ManagerResourceEntity("限制设置" , "gateway/dashboard/admin/gateBusinessServer/list" , MENU_TYPE_FUNCTION , t2) ; 
		ManagerResourceEntity t2f5 = new ManagerResourceEntity("网关设置" , "gateway/dashboard/admin/gateBusinessServer/list" , MENU_TYPE_FUNCTION , t2) ; 
		ManagerResourceEntity t2f6 = new ManagerResourceEntity("日志审计" , "gateway/dashboard/admin/gateBusinessServer/list" , MENU_TYPE_FUNCTION , t2) ; 
		ManagerResourceEntity t2f7 = new ManagerResourceEntity("预警设置" , "gateway/dashboard/admin/gateBusinessServer/list" , MENU_TYPE_FUNCTION , t2) ; 
		
		subResource2.add(t2f1) ; 
		subResource2.add(t2f2) ; 
		subResource2.add(t2f3) ; 
		subResource2.add(t2f4) ; 
		subResource2.add(t2f5) ; 
		subResource2.add(t2f6) ; 
		subResource2.add(t2f7) ; 
		t2.setSubResource(subResource2); 
	
		List<ManagerResourceEntity> list = new ArrayList<ManagerResourceEntity>() ; 
		list.add(t1) ; 
		list.add(t2) ; 
        
		return list ;
	}

}
