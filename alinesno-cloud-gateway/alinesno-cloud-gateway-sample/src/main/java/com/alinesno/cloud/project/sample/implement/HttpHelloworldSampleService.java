package com.alinesno.cloud.project.sample.implement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.alinesno.cloud.gateway.core.annotation.HttpService;
import com.alinesno.cloud.gateway.core.dispather.factory.HttpServiceFactory;

/**
 * HttpRestful接口请求示例<br/>
 * http://localhost:27008/api/v1/sample/helloworld
 * @author LuoAnDong
 * @since 2019年9月21日 下午6:08:49
 */
@HttpService("api_v1_sample_helloworld")
public class HttpHelloworldSampleService implements HttpServiceFactory {

	private static final Logger log = LoggerFactory.getLogger(HttpHelloworldSampleService.class);

//	private static WebClient webClient = WebClient.create();
	
	// 后端服务url
	@Value("${alinesno.gateway.http.proxy.url.prefix:-1}")
	private String proxyServiceUrlPrefix;

	// 后端服务超时
	@Value("${alinesno.gateway.http.proxy.timeout:30000}")
	private long proxyServiceTimeout;

	@Override
	public Object handler(String version, String model, String method, HttpServletRequest frontEndReq, HttpServletResponse frontEndResp) {

		log.debug("进入接口转发{}代理 , 超时时间:{}", proxyServiceUrlPrefix, proxyServiceTimeout);
//		Assert.isTrue(!"-1".equals(proxyServiceUrlPrefix), "后端请求地址未完善.");
		
		String path = frontEndReq.getRequestURI() ; 
		String httpMethod = frontEndReq.getMethod();
		
		return "转发请求: path:" + path + " , httpMethod:" + httpMethod ; 
		
	}

	
}
