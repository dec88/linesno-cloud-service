package com.alinesno.cloud.project.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.alinesno.cloud.gateway.dashboard.enable.EnableAlinesnoGateway;
import com.alinesno.cloud.gateway.dashboard.enable.GatewayBaseScan;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@EnableAlinesnoGateway
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) 
@ComponentScan(basePackages = { GatewayBaseScan.baseScan , "com.alinesno.cloud.project" })
public class GateProxySampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GateProxySampleApplication.class, args);
	}
	
}