package com.alinesno.cloud.gateway.core.context;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.gateway.core.annotation.SocketService;

@Component
public class ApplicationContextProvider implements ApplicationContextAware {
 
    private static ApplicationContext applicationContext = null;
 
    @Override
    public void setApplicationContext(ApplicationContext app) throws BeansException {
        if(applicationContext == null){
            applicationContext  = app ;
        }
    }
 
    //获取applicationContext
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
 
    //通过name获取 Bean.
    public static Object getBean(String name){
        return getApplicationContext().getBean(name);
 
    }
 
    //通过class获取Bean.
    public static <T> T getBean(Class<T> clazz){
        return getApplicationContext().getBean(clazz);
    }
 
    //通过name,以及Clazz返回指定的Bean
    public static <T> T getBean(String name,Class<T> clazz){
        return getApplicationContext().getBean(name, clazz);
    }
    
    public static Map<String, Object> queryBeanWithAnno(Class<?> classes){
    	return  ApplicationContextProvider.getApplicationContext().getBeansWithAnnotation(SocketService.class) ;  
    }
}