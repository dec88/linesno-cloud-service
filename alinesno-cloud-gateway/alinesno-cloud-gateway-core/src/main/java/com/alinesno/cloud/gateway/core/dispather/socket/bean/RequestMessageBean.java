package com.alinesno.cloud.gateway.core.dispather.socket.bean;

/**
 * 消息传递实体
 * 
 * @author LuoAnDong
 * @since 2019年9月21日 下午7:38:43
 */
@SuppressWarnings("serial")
public class RequestMessageBean extends MessageBaseBean {
	
	private String magic = "0x80" ; // 固定头部
	private int length ; // 报文长度
	private String data ; // 内容
	
	public String getMagic() {
		return magic;
	}
	public void setMagic(String magic) {
		this.magic = magic;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
