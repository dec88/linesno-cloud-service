package com.alinesno.cloud.gateway.core;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) 
public class GateProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(GateProxyApplication.class, args);
	}
	
}