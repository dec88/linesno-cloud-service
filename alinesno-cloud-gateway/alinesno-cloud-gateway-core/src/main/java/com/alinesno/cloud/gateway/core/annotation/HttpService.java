package com.alinesno.cloud.gateway.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.gateway.core.dispather.annotation.HttpApiRequestMethod;
import com.alinesno.cloud.gateway.core.dispather.annotation.HttpReturnType;

/**
 * Http服务注解  
 * @author LuoAnDong
 * @since 2019年9月22日 上午12:42:25
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface HttpService {

	/**
	 * The value may indicate a suggestion for a logical component name,
	 * to be turned into a Spring bean in case of an autodetected component.
	 * @return the suggested component name, if any (or empty String otherwise)
	 */
	@AliasFor(annotation = Component.class)
	String value() default "";

	/**
	 * 	数据请求的路径
	 * @return
	 */
    public String path() default "";
   
    /**
     * 版本号
     * @return
     */
    public String version() default "" ; 
    
    /**
     * 请求数据类型
     * @return
     */
    public HttpApiRequestMethod method() default HttpApiRequestMethod.HTTP ;
    
    /**
     * 接口描述
     * @return
     */
    public String desc() default ""; 
   
    /**
     * 返回类型
     * @return
     */
    public HttpReturnType back() default HttpReturnType.JSON ; 
    
    /**
     * 接口是否需要权限验证
     * @return
     */
    public boolean validate() default false ; 
}
