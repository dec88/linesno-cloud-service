package com.alinesno.cloud.gateway.core.dispather.socket.bean;

/**
 * 服务实体
 * @author LuoAnDong
 * @since 2019年9月21日 下午11:43:30
 */
public class BusinessServerBean {

	private String serviceIp = "0.0.0.0" ; 
	private int serverPort = 18080 ; 
	private String businessCode ; // 服务代码
	private String businessDesc ; // 服务描述
	
	private String encoder ; //自定义编码器
	private String decoder ; //自定义解码器

	public String getEncoder() {
		return encoder;
	}
	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}
	public String getDecoder() {
		return decoder;
	}
	public void setDecoder(String decoder) {
		this.decoder = decoder;
	}
	public String getBusinessDesc() {
		return businessDesc;
	}
	public void setBusinessDesc(String businessDesc) {
		this.businessDesc = businessDesc;
	}
	public String getServiceIp() {
		return serviceIp;
	}
	public void setServiceIp(String serviceIp) {
		this.serviceIp = serviceIp;
	}
	public int getServerPort() {
		return serverPort;
	}
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}
	public String getBusinessCode() {
		return businessCode;
	}
	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}
}
