package com.alinesno.cloud.gateway.core.filter;

import java.net.InetSocketAddress;
import java.util.Map;

import com.alinesno.cloud.gateway.core.exception.GatewayException;

/**
 * 过滤器
 * @author LuoAnDong
 * @since 2019年9月26日 下午6:58:58
 */
public interface Filter {
	
	void doFilter(InetSocketAddress insocket , Map<String , Object> params) throws GatewayException ; 
	
}
