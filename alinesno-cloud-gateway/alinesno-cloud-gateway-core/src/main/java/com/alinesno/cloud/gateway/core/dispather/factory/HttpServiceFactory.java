package com.alinesno.cloud.gateway.core.dispather.factory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alinesno.cloud.gateway.core.dispather.BaseHandle;

/**
 * HTTP请求分发器
 * 
 * @author LuoAnDong
 * @sine 2018年8月5日 下午12:48:27
 */
public interface HttpServiceFactory extends BaseHandle {

	/**
	 * 接口请求基础分发器
	 * 
	 * @param version      版本
	 * @param model        模块
	 * @param method       方法
	 * @param frontEndReq
	 * @param frontEndResp
	 * @return
	 */
	Object handler(String version, 
			String model, 
			String method, 
			HttpServletRequest frontEndReq,
			HttpServletResponse frontEndResp);

}