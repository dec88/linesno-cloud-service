package com.alinesno.cloud.gateway.core.dispather.socket.bean;

import java.io.Serializable;

/**
 * 消息传递实体
 * @author LuoAnDong
 * @since 2019年9月21日 下午7:38:43
 */
@SuppressWarnings("serial")
public class MessageBaseBean implements Serializable {

	// [业务](业务代码) --> [接入方](银行代码) --> [接口标识](接口代码) --> [业务标识](交易码) <br/>

	private String clientIp ;  // 服务ip
	private String clientPort ; // 服务端口
	
	private String business ; // 所属业务代码
	private String bank ; // 银行代码 
	private String api ; // 所属接口
	private String transaction ; // 交易代码

	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public MessageBaseBean() {
		super();
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getClientPort() {
		return clientPort;
	}
	public void setClientPort(String clientPort) {
		this.clientPort = clientPort;
	} 

	
}
