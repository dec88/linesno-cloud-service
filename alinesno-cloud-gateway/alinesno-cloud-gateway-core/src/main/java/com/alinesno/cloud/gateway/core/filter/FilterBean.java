package com.alinesno.cloud.gateway.core.filter;

/**
 * 过滤实体
 * 
 * @author LuoAnDong
 * @since 2019年9月22日 上午12:17:30
 */
public class FilterBean {
	
	private boolean tag;

	public FilterBean(boolean tag) {
		this.tag = tag ; 
	}

	public FilterBean() {
	}

	public boolean isTag() {
		return tag;
	}

	public void setTag(boolean tag) {
		this.tag = tag;
	}

}