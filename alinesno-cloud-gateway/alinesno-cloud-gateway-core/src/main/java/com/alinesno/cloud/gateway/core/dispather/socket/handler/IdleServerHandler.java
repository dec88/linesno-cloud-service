package com.alinesno.cloud.gateway.core.dispather.socket.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * 连接空闲时间
 * 
 * @author LuoAnDong
 * @since 2019年9月21日 下午7:04:28
 */
@Component
public class IdleServerHandler extends ChannelInboundHandlerAdapter {

	public static final Logger log = LoggerFactory.getLogger(IdleServerHandler.class);

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		
		if (evt instanceof IdleStateEvent) {
			
			IdleStateEvent event = (IdleStateEvent) evt;
			String type = "";

			if (event.state() == IdleState.READER_IDLE) {

				type = "read idle";

			} else if (event.state() == IdleState.WRITER_IDLE) {

				type = "write idle";

			} else if (event.state() == IdleState.ALL_IDLE) {

				type = "all idle";
			}

			log.debug("{}超时类型:{}", ctx.channel().remoteAddress(), type);

		} else {
			super.userEventTriggered(ctx, evt);
		}
	}
}