package com.alinesno.cloud.gateway.core.dispather;

/**
 * 基础分发器
 * @author LuoAnDong
 * @since 2019年9月21日 下午19:19:01
 */
public interface BaseDispather {

	String REQUEST_METHOD_ERROR = "请求方式不正确.";
	String REQUEST_VERSION_ERROR = "请求版本号不正确.";
	String REQUEST_FAIL = "请求失败.";
	String BUSINESS_NOT_USE = "业务处理不存在.";
	String USER_NOT_LOGIN = "用户未登陆.";
	String BUSINESS_SYSTEM_ERROR = "系统内部错误,请及时联系管理员";
	
}
