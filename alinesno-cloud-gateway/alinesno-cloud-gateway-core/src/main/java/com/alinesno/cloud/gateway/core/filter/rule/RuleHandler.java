package com.alinesno.cloud.gateway.core.filter.rule;

import com.alinesno.cloud.gateway.core.filter.Filter;

/**
 * 规则引擎基础
 * @author LuoAnDong
 * @since 2019年9月21日 下午11:32:08
 */
public interface RuleHandler  extends Filter {

	/**
	 * 顺序
	 * @return
	 */
    int getOrder();

    /**
     * 名称
     * @return
     */
    String named();

    /**
     * 是否执行
     * @param exchange
     * @return
     */
    default Boolean skip() {
        return false;
    }
}
