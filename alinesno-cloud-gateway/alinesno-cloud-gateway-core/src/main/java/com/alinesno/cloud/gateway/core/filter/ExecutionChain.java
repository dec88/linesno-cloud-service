package com.alinesno.cloud.gateway.core.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

/**
 * 执行链
 * @author LuoAnDong
 * @since 2019年9月22日 上午2:31:20
 */
public abstract class ExecutionChain {
	
	public static final Logger log = LoggerFactory.getLogger(ExecutionChain.class); 
	
	String HTTP_RESPONSE_KEY = "HTTP_RESPONSE_KEY" ; 
	
	/**
	 * 请求实体对象 
	 */
	@Nullable
	protected static List<Filter> interceptorList = new ArrayList<Filter>() ;
	
	public static void addFilter(Filter interceptor) {
		interceptorList.add(interceptor);
	}

	public static void addFilterList(Map<String, Object> filters) {
		for (Object bean : filters.values()) {
			FilterExecutionChain.addFilter((Filter)bean);
			log.debug("过滤器:{}" , ToStringBuilder.reflectionToString(bean));
		}
	}
}
