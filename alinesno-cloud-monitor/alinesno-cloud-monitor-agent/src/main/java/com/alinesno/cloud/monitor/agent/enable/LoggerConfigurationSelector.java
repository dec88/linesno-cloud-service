package com.alinesno.cloud.monitor.agent.enable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.monitor.agent.aop.CurrentAccountMonitorAspect;
import com.alinesno.cloud.monitor.agent.aop.ServiceMethonRunningAspect;
import com.alinesno.cloud.monitor.agent.config.SecurityPermitAllConfig;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class LoggerConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 
	
		// common web 
		importBean.add(CurrentAccountMonitorAspect.class.getName()) ; 
		importBean.add(ServiceMethonRunningAspect.class.getName()) ; 
		
		// monitor
		importBean.add(SecurityPermitAllConfig.class.getName()) ; 
		
		return importBean.toArray(new String[] {}) ;
	}
	
}
